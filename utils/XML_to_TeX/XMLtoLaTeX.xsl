<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei"
    version="2.0">
    <xsl:output method="text" encoding="UTF-8"/>

    <xsl:template match="/">
        <xsl:for-each select="//TEI">

            \documentclass{article}
            \usepackage[utf8]{inputenc}

            \title{<xsl:value-of select="normalize-space(.//orth)"/>}
            \author{<xsl:value-of select=".//author"/>}
            \date{}

            \begin{document}

            \maketitle


            \section*{}
            <xsl:value-of select="normalize-space(.//def)"/>
            \section*{Références scientifiques}
            \subsection*{Sources archivistiques et imprimées:}
             \begin{itemize}
            <xsl:choose>
                <xsl:when test=".//bibl/bibl/idno">
                    \subsection*{Sources archivistiques:}
                    <ul>
                        <xsl:for-each select=".//bibl/bibl/idno">
                             \item
                            <xsl:element name="li">
                               <xsl:value-of select="."/>
                            </xsl:element>
                        </xsl:for-each>
                        \subsection*{Sources imprimées:}
                        <xsl:for-each select=".//bibl/bibl[@type = 'sources']">
                            \item
                            <xsl:element name="li">
                                <xsl:value-of select="."/>
                            </xsl:element>
                        </xsl:for-each>
                    </ul>
                    <br/>
                </xsl:when>
                <xsl:otherwise/>
            </xsl:choose>
            <br/>
            \end{itemize}
            \subsection*{Bibliographie scientifique:}
            \begin{itemize}
            <xsl:choose>
                <xsl:when test=".//bibl/bibl[not(.//idno)][not(@type)]">
                    <xsl:for-each select=".">
                    <ul>
                        <xsl:for-each select=".//bibl/bibl[not(.//idno)][not(@type)]">
                            \item
                            <xsl:element name="li">
                                <xsl:value-of select="."/>
                            </xsl:element>
                        </xsl:for-each>
                    </ul></xsl:for-each>
                </xsl:when>
                <xsl:otherwise/>
            </xsl:choose>
            \end{itemize}
            \section*{Citer cette notice:}
            <xsl:value-of select=".//author"/>, \textit{<xsl:value-of select="translate(.//entry/@xml:id, '_', ' ')"/>} in Marie-Laure Legay, Thomas Boullu (dir.), \textit{Dictionnaire numérique de la Ferme générale}, [date de la dernière maj], [en ligne], [url du site].
        \end{document}

        </xsl:for-each>
    </xsl:template>

</xsl:stylesheet>