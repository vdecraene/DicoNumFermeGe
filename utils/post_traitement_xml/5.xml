<?xml version='1.0' encoding='UTF-8'?>
<TEI n="5" xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                  <fileDesc>
                        <titleStmt>
                              <title type="notice"> Dieuze </title>
                              <author>Marie-Laure Legay</author>
                        </titleStmt>
                        <publicationStmt>
                              <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer
                                    le privilège la Ferme générale dans l'espace français et
                                    européen (1664-1794)Axe 1 : Dictionnaire de la Ferme générale,
                                    objet d'histoire totale </publisher>
                              <pubPlace>
                                    <address>
                                          <addrLine> Maison Européenne des Sciences de l'Homme et de
                                                la Société </addrLine>
                                          <addrLine> 2 rue des cannoniers </addrLine>
                                          <addrLine> 59002 Lille Cedex </addrLine>
                                    </address>
                                    2021-2025
                              </pubPlace>
                              <availability>
                                    <licence>Creative Commons Attribution 3.0 non
                                          transposé (CC BY 3.0)</licence>
                              </availability>
                        </publicationStmt>
                        <seriesStmt>
                              <title> Dictionnaire numérique de la Ferme générale </title>
                              <respStmt>
                                    <resp> Coordinateurs de l'axe : Dictionnaire numérique de la
                                          Ferme générale, objet d'histoire totale. </resp>
                                    <persName> Marie-Laure Legay </persName>
                                    <persName> Thomas Boullu </persName>
                              </respStmt>
                        </seriesStmt>
                        <sourceDesc><bibl><idno type="ArchivalIdentifier">AN, G<hi rend="sup">1</hi> 95,
                                                  dossier 1  état des recettes et dépenses des
                                                  Gabelles de Lorraine, 2e année du bail Mager,
                                                  1788  dossier 11 : source salée de Salzbronn</idno></bibl>
                                          <bibl>Pierre Boyé, <hi rend="italic">Les salines et le sel en Lorraine au
                                              XVIIIe siècle</hi>, Nancy, 1904</bibl>
                                          <bibl>Charles Hiegel, « L’industrie du sel en Lorraine du
                                                IXe au XVIIe siècle, Thèse de l'Ecole des Chartes »,
                                                1961</bibl>
                                          <bibl>Pierre Vicq, « Une prise de pouvoir de la Ferme
                                                générale de Lorraine : bois des salines et
                                                faux-saunage de 1698 à la Révolution », thèse de
                                                doctorat en droit, Nancy, Université de Nancy 2,
                                                1998</bibl>
                                          <bibl>François Lormant, « La question fiscale du sel au
                                                temps de Louis XIV et de Vauban : la gabelle et le
                                                faux-saunage en Lorraine », HAL, id : hal-02170889</bibl>
                                          <bibl>François Lormant, « Du bois au charbon dans les
                                                salines lorraines, de la fin du XVIIIe siècle au
                                                début du XIXe siècle », dans <hi rend="italic">Le travail avant la
                                                révolution industrielle</hi>, Actes du 127e Congrès
                                                  national des sociétés historiques et scientifiques,
                                                « Le travail et les hommes », Paris, Editions du
                                                CTHS, 2006, p. 359-367</bibl>
                                    </sourceDesc></fileDesc>
                  <encodingDesc>
                        <projectDesc source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
                              <p> Le projet FermGé vise à étudier l’impact d’une organisation
                                    fiscale (1664-1794), discriminante mais rationnelle, sur les
                                    territoires et les sociétés de la France moderne/p </p>
                        </projectDesc>
                  </encodingDesc>
                  <profileDesc>
                        <textClass>
                              <keywords scheme="#fr_RAMEAU">
                                    <list>
                                          <item> Histoire -- Histoire moderne -- Histoire fiscale </item>
                                          <item> Histoire -- Histoire moderne -- Histoire des
                                                relations internationales </item>
                                    </list>
                              </keywords>
                        </textClass>
                  </profileDesc>
                  <revisionDesc>
                        <change type="AutomaticallyEncoded"> 2022-12-14T10:56:08.187763+2:00
                        </change>
                  </revisionDesc>
            </teiHeader>
            <text>
                  <body>
                        <entry n="5" type="D" xml:id="Dieuze">
                              <form type="lemma">
                                    <orth> Dieuze </orth>
                              </form>
                              <sense>
                                    <def> Principale saline de <orgName subtype="province" type="pays_et_provinces"> Lorraine</orgName> avec
                                          <placeName>Moyenvic</placeName> et <placeName>Château-Salins</placeName>, elle disposait de 37 poêles et
                                          autant de poêlons. En 1791,
                                          ces trois salines produisaient 520 000 quintaux par an (la
                                          saline ducale de <ref target="#Rosières"> Rosières</ref>, sur la Meurthe, fut fermée en 1760, faute de rentabilité). Seuls 20 % étaient
                                          destinés à la consommation locale. 80 % étaient vendus aux
                                          communautés alsaciennes (Haguenau, Strasbourg) et à
                                        l’étranger, au <orgName>district de Gobert</orgName>, en <ref target="#Allemagne"> Allemagne</ref> (<placeName>Trèves</placeName> sur la
                                        Moselle, dans le <orgName subtype="pays_fiscaux" type="administrations_et_juridictions"> comté de Nassau</orgName>, le <orgName>duché des Deux-Ponts</orgName>, de la <orgName>Souabe</orgName>, à <placeName>Mayence</placeName> et dans
                                          le <orgName>Palatinat</orgName>). A Dieuze en particulier, le sel produit en
                                                1788 (217 377 quintaux de
                                          menus sels et 94 603 quintaux de gros sels) était destiné
                                          aux Gabelles de <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Lorraine"> Lorraine</ref>
                                         </orgName> et aux <orgName subtype="administrations_juridictions_fermes" type="administrations_et_juridictions"> entrepôts de
                                            Saint-Avold</orgName>, <placeName>Delme</placeName> et <placeName>Bertrichamps</placeName>. Là,
                                          comme dans toute la <orgName subtype="province" type="pays_et_provinces"> Lorraine</orgName>, la
                                          production des salines générait une exploitation excessive
                                          des forêts qui finit, à terme, par contrarier la
                                          production. Pas moins de 37 000 cordes de bois (une corde
                                          de saline correspondait à environ 3, 37 stères) étaient
                                          nécessaires pour chauffer les poêles de Dieuze, contre
                                        12 500 cordes à Château-Salins, et 15 500 à <placeName>Moyenvic</placeName>.
                                          Faute de bois, la production de Dieuze diminua de 500 000
                                          à moins de 400 000 quintaux par an, ce qui engagea la
                                          Ferme générale à s’intéresser à l’exploitation de la
                                          saline de <placeName>Salzbronn</placeName> (à une demie lieue de la <placeName type="lieux_habitations"> ville de Sarralbe</placeName>). Décision fut prise en 1778 de constater la qualité de cette
                                          source. On estima à 19 800 cordes le bois nécessaire à son
                                          activité. Les Fermiers généraux espéraient en tirer au
                                          moins 40 à 50 000 quintaux de sel, menus et gros, et
                                          procédèrent donc à son scellement en 1783. Salzbronn présentait l’avantage
                                          d’être sur la Sarre, ce qui favorisait la vente à
                                          l’étranger, notamment par rapport à Dieuze, éloignée de
                                          plus de huit lieues. Salzbronn ne pouvait
                                                par ailleurs craindre la concurrence de la saline
                                                voisine de <placeName>Herbesheim</placeName>, car le comte de <ref target="#Nassau"> Nassau</ref> avait renoncé par
                                                convention en 1741 à son
                                                exploitation moyennant une rente de 10 000 livres
                                                assignée sur les recettes de la saline de
                                                Dieuze.
                                    </def>
                                    <listBibl><bibl><idno type="ArchivalIdentifier">AN, G<hi rend="sup">1</hi> 95,
                                                  dossier 1  état des recettes et dépenses des
                                                  Gabelles de Lorraine, 2e année du bail Mager,
                                                  1788  dossier 11 : source salée de Salzbronn</idno></bibl>
                                          <bibl>Pierre Boyé, <hi rend="italic">Les salines et le sel en Lorraine au
                                              XVIIIe siècle</hi>, Nancy, 1904</bibl>
                                          <bibl>Charles Hiegel, « L’industrie du sel en Lorraine du
                                                IXe au XVIIe siècle, Thèse de l'Ecole des Chartes »,
                                                1961</bibl>
                                          <bibl>Pierre Vicq, « Une prise de pouvoir de la Ferme
                                                générale de Lorraine : bois des salines et
                                                faux-saunage de 1698 à la Révolution », thèse de
                                                doctorat en droit, Nancy, Université de Nancy 2,
                                                1998</bibl>
                                          <bibl>François Lormant, « La question fiscale du sel au
                                                temps de Louis XIV et de Vauban : la gabelle et le
                                                faux-saunage en Lorraine », HAL, id : hal-02170889</bibl>
                                          <bibl>François Lormant, « Du bois au charbon dans les
                                                salines lorraines, de la fin du XVIIIe siècle au
                                                début du XIXe siècle », dans <hi rend="italic">Le travail avant la
                                                révolution industrielle</hi>, Actes du 127e Congrès
                                                  national des sociétés historiques et scientifiques,
                                                « Le travail et les hommes », Paris, Editions du
                                                CTHS, 2006, p. 359-367</bibl>
                                    </listBibl></sense>
                        </entry>
                  </body>
            </text>
      </TEI>
      