<?xml version='1.0' encoding='UTF-8'?>
<TEI n="127" xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                  <fileDesc>
                        <titleStmt>
                              <title type="notice"> Pont-de-Beauvoisin </title>
                              <author>Marie-Laure Legay</author>
                        </titleStmt>
                        <publicationStmt>
                              <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer
                                    le privilège la Ferme générale dans l'espace français et
                                    européen (1664-1794)Axe 1 : Dictionnaire de la Ferme générale,
                                    objet d'histoire totale </publisher>
                              <pubPlace>
                                    <address>
                                          <addrLine> Maison Européenne des Sciences de l'Homme et de
                                                la Société </addrLine>
                                          <addrLine> 2 rue des cannoniers </addrLine>
                                          <addrLine> 59002 Lille Cedex </addrLine>
                                    </address>
                                    2021-2025
                              </pubPlace>
                              <availability>
                                    <licence>Creative Commons Attribution 3.0 non
                                          transposé (CC BY 3.0)</licence>
                              </availability>
                        </publicationStmt>
                        <seriesStmt>
                              <title> Dictionnaire numérique de la Ferme générale </title>
                              <respStmt>
                                    <resp> Coordinateurs de l'axe : Dictionnaire numérique de la
                                          Ferme générale, objet d'histoire totale. </resp>
                                    <persName> Marie-Laure Legay </persName>
                                    <persName> Thomas Boullu </persName>
                              </respStmt>
                        </seriesStmt>
                        <sourceDesc><bibl><idno type="ArchivalIdentifier">Archivio di Stato di Turino, Materie economiche, Dacito di Susa, Mazzo 2, fascicule 22, f° 720 et suivants</idno></bibl>
                                        <bibl><idno type="ArchivalIdentifier">Archivio di Stato di Turino, Materie economiche, Dacito di Susa, Mazzo 4, fascicule 18, f° 893 et suivants</idno></bibl>
                                        <bibl><idno type="ArchivalIdentifier">AD Isère, 2C
                                                  635 : Etat des marchandises venues des Etats du
                                                  Roy de Sardaigne pendant la sixième année du bail
                                                  de Bourgois (1725-1726)</idno></bibl>
                                    </sourceDesc></fileDesc>
                  <encodingDesc>
                        <projectDesc source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
                              <p> Le projet FermGé vise à étudier l’impact d’une organisation
                                    fiscale (1664-1794), discriminante mais rationnelle, sur les
                                    territoires et les sociétés de la France moderne/p </p>
                        </projectDesc>
                  </encodingDesc>
                  <profileDesc>
                        <textClass>
                              <keywords scheme="#fr_RAMEAU">
                                    <list>
                                          <item> Histoire -- Histoire moderne -- Histoire
                                                commerciale </item>
                                          <item> Histoire -- Histoire moderne -- Histoire fiscale
                                          </item>
                                    </list>
                              </keywords>
                        </textClass>
                  </profileDesc>
                  <revisionDesc>
                        <change type="AutomaticallyEncoded"> 2022-12-20T10:14:03.026377+2:00
                        </change>
                  </revisionDesc>
            </teiHeader>
            <text>
                  <body>
                        <entry n="127" type="P" xml:id="Pont_de_Beauvoisin">
                              <form type="lemma">
                                    <orth> Pont-de-Beauvoisin </orth>
                              </form>
                              <sense>
                                    <def>
Cette localité était située entre la Savoie et le Dauphiné, territoires séparés par la rivière du Guiers. Depuis François Ier, elle constituait la principale porte d’entrée des marchandises venues d’Italie, de Piémont et de Savoie. Dès le XVIe siècle (règlements du 18 juillet 1540, 14 octobre 1564, 26 juillet 1566, 8 novembre 1583, 2 mars 1585, puis 31 décembre 1605 et 17 août 1647), il fut ordonné que les draps d’or et d’argent, soieries, passementeries, rubans, ceintures et ornements venant d’Italie devaient passer par le Pont-de-Beauvoisin et delà, entrer à <ref target="#Lyon">Lyon</ref> par la porte du <ref target="#Rhône">Rhône</ref>, sans être entreposés sur la route. L’article dix du traité d’Utrecht du 11 avril 1713 réitéra ces dispositions. A <ref target="#Lyon">Lyon</ref>, les marchandises étaient taxées par la <ref target="#Douane">Douane</ref> de la ville. Dans l’autre sens, les marchandises destinées à la Savoie, au Piémont et à l’Italie sortaient par cette localité, passaient par le dace de Suze où elles étaient taxées au profit du duc de <ref target="#Savoie">Savoie</ref>.<ref target="#contrebande">contrebande</ref> (patentes du 12 février). Inversement, des commis du dacier étaient autorisés à résider à <ref target="#Lyon">Lyon</ref>. Les employés des deux « douanes » avaient même autorité pour les saisies et confiscations. En outre, les fermiers généraux obtinrent à cette époque d’entretenir gardes et commis en Bresse, Bugey, pays de <ref target="#Gex">Gex</ref> à condition de recevoir du dacier 3 livres tournois par balle ou caisse de draps ou soieries qui passait par la route Suze-Pont-de-Beauvoisin. Le duc de <ref target="#Savoie">Savoie</ref> était attaché à la recette de sa douane qui rapportait plus de 5 000 écus d’or d’Italie par an au milieu du XVIIe siècle (le bail pour 1674-1678 fut établi pour le prix de 28 000 écus). D’autres passages demeuraient possibles, notamment par Collonges et Genève, ce que le duc craignait. En 1698, un arrêt (5 novembre) ouvrit le passage par le village de Dortan.</def>
                                    <listBibl><bibl><idno type="ArchivalIdentifier">Archivio di Stato di Turino, Materie economiche, Dacito di Susa, Mazzo 2, fascicule 22, f° 720 et suivants</idno></bibl>
                                        <bibl><idno type="ArchivalIdentifier">Archivio di Stato di Turino, Materie economiche, Dacito di Susa, Mazzo 4, fascicule 18, f° 893 et suivants</idno></bibl>
                                        <bibl><idno type="ArchivalIdentifier">AD Isère, 2C
                                                  635 : Etat des marchandises venues des Etats du
                                                  Roy de Sardaigne pendant la sixième année du bail
                                                  de Bourgois (1725-1726)</idno></bibl>
                                    </listBibl></sense>
                        </entry>
                  </body>
            </text>
      </TEI>
      