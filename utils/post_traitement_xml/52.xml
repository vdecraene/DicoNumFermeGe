<?xml version='1.0' encoding='UTF-8'?>
<TEI n="52" xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                  <fileDesc>
                        <titleStmt>
                              <title type="notice"> Patente du Languedoc </title>
                              <author>Marie-Laure Legay</author>
                        </titleStmt>
                        <publicationStmt>
                              <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer
                                    le privilège la Ferme générale dans l'espace français et
                                    européen (1664-1794)Axe 1 : Dictionnaire de la Ferme générale,
                                    objet d'histoire totale </publisher>
                              <pubPlace>
                                    <address>
                                          <addrLine> Maison Européenne des Sciences de l'Homme et de
                                                la Société </addrLine>
                                          <addrLine> 2 rue des cannoniers </addrLine>
                                          <addrLine> 59002 Lille Cedex </addrLine>
                                    </address>
                                    2021-2025
                              </pubPlace>
                              <availability>
                                    <licence>Creative Commons Attribution 3.0 non
                                          transposé (CC BY 3.0)</licence>
                              </availability>
                        </publicationStmt>
                        <seriesStmt>
                              <title> Dictionnaire numérique de la Ferme générale </title>
                              <respStmt>
                                    <resp> Coordinateurs de l'axe : Dictionnaire numérique de la
                                          Ferme générale, objet d'histoire totale. </resp>
                                    <persName> Marie-Laure Legay </persName>
                                    <persName> Thomas Boullu </persName>
                              </respStmt>
                        </seriesStmt>
                        <sourceDesc><bibl><idno type="ArchivalIdentifier">AN, G<hi rend="sup">1</hi> 83,
                                                  dossier 11, inspections des contrôleurs de la
                                                  direction d’Auch, Affaire Daugerot</idno>
                                          </bibl>
                                          <bibl type="sources">Arrêt du Conseil d’Etat qui règle à
                                                huit sols les droits de sortie sur chaque porc,
                                                truie et porcelet, qui sortiront des provinces
                                                sujettes aux droits de la patente de Languedoc et de
                                                la traite d'Arzac, pour passer dans les provinces où
                                                les Aides n'ont point cours, 22 décembre 1750</bibl>
                                          <bibl type="sources">Encyclopédie méthodique, Finances,
                                                chez Panckouke, vol. 3, 1787, p. 306</bibl>
                                    </sourceDesc></fileDesc>
                  <encodingDesc>
                        <projectDesc source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
                              <p> Le projet FermGé vise à étudier l’impact d’une organisation
                                    fiscale (1664-1794), discriminante mais rationnelle, sur les
                                    territoires et les sociétés de la France moderne/p </p>
                        </projectDesc>
                  </encodingDesc>
                  <profileDesc>
                        <textClass>
                              <keywords scheme="#fr_RAMEAU">
                                    <list>
                                          <item> Histoire -- Histoire moderne -- Histoire fiscale
                                          </item>
                                    </list>
                              </keywords>
                        </textClass>
                  </profileDesc>
                  <revisionDesc>
                        <change type="AutomaticallyEncoded"> 2022-12-20T09:44:21.397483+2:00
                        </change>
                  </revisionDesc>
            </teiHeader>
            <text>
                  <body>
                        <entry n="52" type="P" xml:id="Patente_du_Languedoc">
                              <form type="lemma">
                                    <orth> Patente du Languedoc </orth>
                              </form>
                              <sense>
                                    <def> Il s’agit des « droits d’imposition, foraine, rêve,
                     haut-passage et réappréciation », tous droits unifiés en
                     un seul dans le ressort des <ref target="#fermes">Cinq grosses fermes</ref> et également
                                          rassemblés en <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Languedoc"> Languedoc</ref>
                                         </orgName> en une traite domaniale levée sur les
                                          marchandises des territoires de <placeName>Toulouse</placeName>, <placeName>Narbonne</placeName> et
                                          <placeName>Villeneuve-lès-Avignon</placeName> destinées à l’étranger ou aux
                                          provinces où les <ref target="#aides"> aides</ref> n’ont
                                          pas cours. La déclaration du 21
                                                décembre 1605 évoque la
                                                patente du <orgName subtype="province" type="pays_et_provinces"> Languedoc</orgName>
                                                sous le terme de « traite domaniale », mais le nom
                                                primitif demeura. Le bail de Forceville (1738-1744) la mentionne encore comme telle.
                                          Cette traite était attachée à la <ref target="#Foraine">
                                                Foraine</ref>, mais ne s’appliquait qu’au <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Languedoc"> Languedoc</ref></orgName>, et non à la <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Provence"> Provence</ref>
                                         </orgName> et au <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Dauphiné"> Dauphiné</ref>
                                         </orgName> comme cette dernière. Les tarifs étaient
                                          usuels, c’est-à-dire établis selon des usages incertains
                                          par les commis. Ils s’avéraient le plus souvent
                                          défectueux, comme pour les tarifs de la <ref target="#douane_de_Lyon">Douane de Lyon</ref> ou de la <ref target="#Foraine"> Foraine</ref>. Tous les grands
                                          administrateurs en convenaient : les tarifs manuscrits
                                          maintes fois raturés par les receveurs rendaient les
                                          usages confus. Le tarif général fut imprimé en 1741.
                                                Le passage des
                                                  bureaux était l’occasion de contestations
                                                  incessantes, par exemple sur le tarif des porcs
                                                  qui passaient en pays où les <ref target="#aides">
                                                  aides</ref> n’avaient pas lieu, ou encore sur les
                                                  marchandises que les habitants des contrées du
                                        Sud-Ouest (<orgName>Armagnac</orgName>, <orgName>Bigorre</orgName>, <orgName>Comminges</orgName>, <orgName>Couserans</orgName>
                                                  et du <orgName subtype="pays_fiscaux" type="administrations_et_juridictions"> comté de Foix</orgName>) faisaient sortir par le <orgName subtype="province" type="pays_et_provinces">
                                                  <ref target="#Languedoc"> Languedoc</ref>
                                                 </orgName>. Propriétaire d’une manufacture de bonneterie et
                                          teinture de coton à <placeName>Nay</placeName> en <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Béarn"> Béarn</ref></orgName>, Daugerot dut également régler les droits de
                                          sortie de la patente du <orgName subtype="province" type="pays_et_provinces"> Languedoc</orgName> levés
                                          sur 13 942 racines de garance qu’il avait fait venir car
                                          il n’avait pas présenté les acquits du bureau d’entrée de
                                          Villeneuve-lès-Avignon. </def>
                                    <listBibl><bibl><idno type="ArchivalIdentifier">AN, G<hi rend="sup">1</hi> 83,
                                                  dossier 11, inspections des contrôleurs de la
                                                  direction d’Auch, Affaire Daugerot</idno>
                                          </bibl>
                                          <bibl type="sources">Arrêt du Conseil d’Etat qui règle à
                                                huit sols les droits de sortie sur chaque porc,
                                                truie et porcelet, qui sortiront des provinces
                                                sujettes aux droits de la patente de Languedoc et de
                                                la traite d'Arzac, pour passer dans les provinces où
                                                les Aides n'ont point cours, 22 décembre 1750</bibl>
                                          <bibl type="sources">Encyclopédie méthodique, Finances,
                                                chez Panckouke, vol. 3, 1787, p. 306</bibl>
                                    </listBibl></sense>
                        </entry>
                  </body>
            </text>
      </TEI>
      