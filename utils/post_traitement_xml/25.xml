<?xml version='1.0' encoding='UTF-8'?>
<TEI n="25" xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                  <fileDesc>
                        <titleStmt>
                              <title type="notice"> Darigrand Edme-François (1735-après 1796) </title>
                              <author>Marie-Laure Legay</author>
                        </titleStmt>
                        <publicationStmt>
                              <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer
                                    le privilège la Ferme générale dans l'espace français et
                                    européen (1664-1794)Axe 1 : Dictionnaire de la Ferme générale,
                                    objet d'histoire totale </publisher>
                              <pubPlace>
                                    <address>
                                          <addrLine> Maison Européenne des Sciences de l'Homme et de
                                                la Société </addrLine>
                                          <addrLine> 2 rue des cannoniers </addrLine>
                                          <addrLine> 59002 Lille Cedex </addrLine>
                                    </address>
                                    2021-2025
                              </pubPlace>
                              <availability>
                                    <licence>Creative Commons Attribution 3.0 non
                                          transposé (CC BY 3.0)</licence>
                              </availability>
                        </publicationStmt>
                        <seriesStmt>
                              <title> Dictionnaire numérique de la Ferme générale </title>
                              <respStmt>
                                    <resp> Coordinateurs de l'axe : Dictionnaire numérique de la
                                          Ferme générale, objet d'histoire totale. </resp>
                                    <persName> Marie-Laure Legay </persName>
                                    <persName> Thomas Boullu </persName>
                              </respStmt>
                        </seriesStmt>
                        <sourceDesc><bibl type="sources">Edme-François Darigrand,
                                                <hi rend="italic">L'anti-financier, ou relevé de quelques-unes des
                                                malversations dont se rendent journellement
                                                coupables les Fermiers-Généraux, des vexations
                                                qu'ils commettent dans les Provinces, servant de
                                                réfutation d'un écrit intitulé : Lettre servant de
                                                Réponse aux remonstrances du Parlement de Bordeaux.
                                                Amsterdam</hi>, in-12 de 100 p., 1763</bibl>
                                          <bibl type="sources">Edme-François Darigrand, <hi rend="italic">Mémoire
                                                pour les Maîtres et gardes de l’épicerie sur les
                                                fraudes qui se commettent aux entrées de Paris, les
                                              causes de ces fraudes, le moyen de les détruire</hi>,
                                                Paris, 1785</bibl>
                                          <bibl type="sources">Anonyme, <hi rend="italic">Réponse à l’auteur de
                                                l’Anti-financier</hi>, La Haye, 1764, 22 p</bibl>
                                          <bibl>John Shovlin, <hi rend="italic">The political Economy of Virtue,
                                                Luxury, Patriotism, and the Origins of the French
                                              Revolution</hi>, Ithaca et Londres, Cornell University
                                                Press, 2006</bibl>
                                          <bibl>Antonella Alimento, <hi rend="italic">Réformes fiscales et crises
                                                politiques dans la France de Louis XV. De la taille
                                              tarifée au cadastre général</hi>, Bruxelles, P.I.E. Peter
                                                Lang, 2008, p. 271-272</bibl>
                                          <bibl>Antonella Alimento, « La « querelle » intorno alla
                                                Richesse de l’état: imposta unica e lotta politica
                                                in Francia attorno alla metà del Settecento »,
                                              <hi rend="italic">Annali della Fondazione L. Einaudi</hi>, XVIII, Turin,
                                                1984, p. 273-323</bibl>
                                    </sourceDesc></fileDesc>
                  <encodingDesc>
                        <projectDesc source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
                              <p> Le projet FermGé vise à étudier l’impact d’une organisation
                                    fiscale (1664-1794), discriminante mais rationnelle, sur les
                                    territoires et les sociétés de la France moderne/p </p>
                        </projectDesc>
                  </encodingDesc>
                  <profileDesc>
                        <textClass>
                              <keywords scheme="#fr_RAMEAU">
                                    <list>
                                          <item> Histoire -- Histoire moderne -- Histoire politique
                                          </item>
                                    </list>
                              </keywords>
                        </textClass>
                  </profileDesc>
                  <revisionDesc>
                        <change type="AutomaticallyEncoded"> 2022-12-14T10:35:49.364051+2:00
                        </change>
                  </revisionDesc>
            </teiHeader>
            <text>
                  <body>
                        <entry n="25" type="D" xml:id="Darigrand_Edme_François_1735_après_1796">
                              <form type="lemma">
                                    <orth> Darigrand Edme-François (1735-après 1796) </orth>
                              </form>
                              <sense>
                                  <def> Cet avocat au parlement de <placeName>Paris</placeName> et pamphlétaire était un
                     ancien employé de la Ferme générale. Il exerça dans le
                     département des droits d’<ref target="#aides"> aides
                                         </ref> et non de la <ref target="#gabelle"> gabelle</ref>
                                          comme on peut le lire parfois. Comme nombre d’avocats
                                          patriotes de sa génération, ― « Je suis François »,
                                          écrit-il dans son <hi rend="italic">Epître au parlement de France</hi> ―
                                                il fit campagne en faveur du  « but unique de
                                          toutes les classes du Parlement» : « l’abolition des
                                          Fermes ». Il affirma que personne, en réalité, n’était
                                          capable de saisir l’ampleur de la fiscalité indirecte,
                                          d’en connaître tous les droits tant elle était d’une
                                          infinie complexité. « Que de vexations, que d’indues
                                          exactions doit couvrir une pareille obscurité ! ».
                                          D’inspiration janséniste, cette épître de 37 pages
                                          constituait le préambule d’un pamphlet plus détaillé
                                          intitulé : <hi rend="italic">L'anti-financier, ou relevé de quelques-unes
                                          des malversations dont se rendent journellement coupables
                                          les Fermiers-Généraux, des vexations qu'ils commettent
                                            dans les Provinces</hi>, servant de réfutation d'un écrit
                                          intitulé : <hi rend="italic">Lettre servant de Réponse aux remonstrances du
                                            Parlement de Bordeaux</hi>. Ce pamphlet faisait en lui-même 55
                                          pages ; il répondait à la Lettre écrite par Henry Bertin
                                          contre les remontrances du parlement bordelais et passait
                                          en revue les aberrations administratives et <ref target="#vexations"> vexations</ref> à propos des
                                          droits sur les <ref target="#vins"> vins</ref> et sur les
                                                <ref target="#eaux-de-vie"> eaux-de-vie</ref> qu’il
                                          connaissait très bien, des droits des inspecteurs aux <ref target="#boucheries"> boucheries</ref>, ou des
                                          droits du Contrôle des <ref target="#actes"> actes</ref>
                                          dont il jugea la <ref target="#régie"> régie</ref> comme
                                          la plus obscure qui fût. Les deux textes, <hi rend="italic">Epître</hi> et
                                          L’<hi rend="italic">Anti-financier</hi>, furent publiés ensemble à Amsterdam en
                                                1763. L’objectif de
                                          l’auteur était noble. Il le précisa lui-même : « mon
                                          dessein n’a pas été de prostituer ma plume au vif plaisir
                                          d’une critique amère de la finance ». Il voulut
                                          sincèrement défendre le peuple contre des impôts indirects
                                          aberrants et réclamer un impôt unique, à l’instar du
                                          parlement de Bordeaux et sur le modèle des propositions de
                                          Roussel de la Tour, conseiller de la troisième chambre des
                                          enquêtes du parlement de Paris. Pierre-Philippe Roussel de
                                          la Tour publiait la même année <hi rend="italic">La Richesse de l’Etat</hi> qui
                                          fit grand bruit. Au lendemain de la guerre de Sept-Ans (1746-1763), le débat sur la réforme fiscale
                                          faisait rage. Il était mené par des hommes éclairés de
                                          l’administration comme Henry Bertin, partisan du cadastre
                                          qui prit position contre l’impôt unique, par des
                                          physiocrates favorables à la refonte de l’impôt sur la
                                          base de l’appréciation de la valeur ajoutée, mais aussi
                                          par les avocats et Magistrats soucieux de défendre le rôle
                                          des Parlements dans la mise en œuvre de la réforme.
                                          Darigrand dénonçait à ce propos les « sectateurs du
                                          despotisme » qui prenaient des arrêts en finances sans
                                          enregistrement des cours souveraines.<hi rend="italic">Anti-financier</hi>
                                          dirigé contre la Ferme générale eut un franc succès et fut
                                          de nouveau mis sous presse en 1764
 pour être diffusé sous le manteau. On lit dans
                                          les Mémoires secrets pour servir à la République des
                                          Lettres, tome premier, page 308, à la date du 8 décembre
                                                1763 : « On y épuise
                                          contre la gent financière tous les traits de la critique
                                          la plus amère et l’on y rapporte assez de faits, quoique
                                          très succincts, pour justifier ce qu'on en dit. Cet
                                          ouvrage, où l’auteur exalte peut-être un peu trop sa bile,
                                          n’est point dénué de mérite. La forme d’y traiter la
                                          matière est assez dure, mais le fonds en est de la plus
                                          grande vérité. Il y a des endroits sublimes ». Antonella
                                          Alimento rapporte les hésitations de l’avocat général
                                          Jean-Omer de Fleury à condamner le livre ; il jugea
                                          l’<hi rend="italic">Epître au parlement de France</hi> trop hardie, mais
                                          l’<hi rend="italic">Anti-financier</hi> lui-même peu condamnable dès lors qu’il
                                          rapportait des faits établis. Le pamphlétaire n’était
                                          pourtant pas toujours exact dans ces assertions. Il
                                          dénonçait la Ferme comme voulant surprendre des arrêts au
                                          Conseil et cacher ses démarches aux tribunaux ordinaires,
                                          par exemple pour taxer les <ref target="#bouchers">
                                                bouchers</ref> qui s’établissaient à la campagne.
                                          Mais en l’occurrence, cette mesure avait fait l’objet
                                          d’une déclaration royale dûment enregistrée et non d’un
                                          arrêt en finances.<hi rend="italic">Epître</hi>, mais
                                          aussi dans l’<hi rend="italic">Anti-financier</hi>. Sa charge contre la Finance
                                          n’était guère originale ; de même, ses arguments contre
                                          l’impôt indirect :  « il est exactement faux que les impôts
                                          sur les denrées la consommation établissent l’équilibre la
                                          proportion entre le riche et le pauvre ». Pour lui, le
                                          pauvre consommant plus de denrées de première nécessité,
                                          se trouvait davantage taxé à proportion. Darigrand se
                                          révélait plus séditieux en insistant sur l’utilité de
                                          supprimer les <ref target="#privilèges"> privilèges</ref>, tous les privilèges, comme conséquence nécessaire de la
                                          mise en œuvre d’un impôt unique. Reprenant les arguments
                                          de Henry Bertin, auteur de la Lettre servant de Réponse
                                          aux remonstrances du Parlement de <placeName>Bordeaux</placeName>, il argumenta
                                          judicieusement pour montrer que les <ref target="#privilégiés"> privilégiés</ref> comme
                                          nobles, ecclésiastiques, corps, provinces… n’étaient en
                                          réalité que partiellement exemptés, ce qui n’en faisait
                                          plus, à proprement parler, des « privilégiés ».
                                          L’<hi rend="italic">Anti-financier</hi> reçut une courte réponse anonyme en 1764. L’auteur tenta de
                                          réduire « les rêveries » de Darigrand à néant en
                                          argumentant plus spécifiquement sur les frais de régie, le
                                          trop-bu ou encore le <ref target="#bon_de_masse">bon de masse</ref>, sans néanmoins emporter l’adhésion.<ref target="#eaux-de-vie"> eaux-de-vie</ref> (1764), un <hi rend="italic">Mémoire sur la
                                          caisse de Poissy</hi> (1768) ou
                                          encore un <hi rend="italic">Mémoire pour les habitants de la banlieue de
                                          Paris</hi> (1789).
                                                Célèbre pour avoir attaqué en règle la Ferme
                                                générale, on lui attribue d’autres pamphlets
                                                accusateurs contre la compagnie comme <hi rend="italic">Antropophagie
                                          ou les antropophages</hi>, publié à <placeName>Amsterdam</placeName> en 1764, sans certitude
                                                néanmoins : le style du pamphlet est somme toute
                                                bien différent et surtout, la portée économique est
                                                de bien moindre envergure : l’auteur n’y évoque pas
                                                l’impôt unique. En tant qu’avocat, Darigrand
                                          prit plusieurs fois la défense de particuliers contre la
                                          Ferme générale : en 1765, il
                                          plaida pour la demoiselle Chanlaire, propriétaire de vigne
                                          en <orgName subtype="province" type="pays_et_provinces">
                                                Champagne</orgName> contre la Compagnie; l’affaire
                                          concernait le paiement des <ref target="#gros">droits de
                                                gros</ref> sur les vins ; en
1767, il défendit les Thiébault, également
                                          champenois, contre une accusation de <ref target="#contrebande"> contrebande</ref> ; en 1768, il défendit
                                          Pierre-Nicolas Sommié, marchand-orfèvre, également contre
                                          la Ferme. Encore en 1785, il
                                          combattit le projet d’augmentation des droits sur les <ref target="#eaux-de-vie"> eaux-de-vie</ref> destinées
                                          aux banlieues de Paris : « il
                                          est aisé d’apercevoir (sic) que le projet des fermiers
                                          généraux ne tend qu’à augmenter leur profit personnel ». </def>
                                    <listBibl><bibl type="sources">Edme-François Darigrand,
                                                <hi rend="italic">L'anti-financier, ou relevé de quelques-unes des
                                                malversations dont se rendent journellement
                                                coupables les Fermiers-Généraux, des vexations
                                                qu'ils commettent dans les Provinces, servant de
                                                réfutation d'un écrit intitulé : Lettre servant de
                                                Réponse aux remonstrances du Parlement de Bordeaux.
                                                Amsterdam</hi>, in-12 de 100 p., 1763</bibl>
                                          <bibl type="sources">Edme-François Darigrand, <hi rend="italic">Mémoire
                                                pour les Maîtres et gardes de l’épicerie sur les
                                                fraudes qui se commettent aux entrées de Paris, les
                                              causes de ces fraudes, le moyen de les détruire</hi>,
                                                Paris, 1785</bibl>
                                          <bibl type="sources">Anonyme, <hi rend="italic">Réponse à l’auteur de
                                                l’Anti-financier</hi>, La Haye, 1764, 22 p</bibl>
                                          <bibl>John Shovlin, <hi rend="italic">The political Economy of Virtue,
                                                Luxury, Patriotism, and the Origins of the French
                                              Revolution</hi>, Ithaca et Londres, Cornell University
                                                Press, 2006</bibl>
                                          <bibl>Antonella Alimento, <hi rend="italic">Réformes fiscales et crises
                                                politiques dans la France de Louis XV. De la taille
                                              tarifée au cadastre général</hi>, Bruxelles, P.I.E. Peter
                                                Lang, 2008, p. 271-272</bibl>
                                          <bibl>Antonella Alimento, « La « querelle » intorno alla
                                                Richesse de l’état: imposta unica e lotta politica
                                                in Francia attorno alla metà del Settecento »,
                                              <hi rend="italic">Annali della Fondazione L. Einaudi</hi>, XVIII, Turin,
                                                1984, p. 273-323</bibl>
                                    </listBibl></sense>
                        </entry>
                  </body>
            </text>
      </TEI>
      