<?xml version='1.0' encoding='UTF-8'?>
<TEI n="76" xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                  <fileDesc>
                        <titleStmt>
                              <title type="notice"> Commis aux caves </title>
                              <author>Marie-Laure Legay</author>
                        </titleStmt>
                        <publicationStmt>
                              <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer
                                    le privilège la Ferme générale dans l'espace français et
                                    européen (1664-1794)Axe 1 : Dictionnaire de la Ferme générale,
                                    objet d'histoire totale </publisher>
                              <pubPlace>
                                    <address>
                                          <addrLine> Maison Européenne des Sciences de l'Homme et de
                                                la Société </addrLine>
                                          <addrLine> 2 rue des cannoniers </addrLine>
                                          <addrLine> 59002 Lille Cedex </addrLine>
                                    </address>
                                    2021-2025
                              </pubPlace>
                              <availability>
                                    <licence>Creative Commons Attribution 3.0 non
                                          transposé (CC BY 3.0)</licence>
                              </availability>
                        </publicationStmt>
                        <seriesStmt>
                              <title> Dictionnaire numérique de la Ferme générale </title>
                              <respStmt>
                                    <resp> Coordinateurs de l'axe : Dictionnaire numérique de la
                                          Ferme générale, objet d'histoire totale. </resp>
                                    <persName> Marie-Laure Legay </persName>
                                    <persName> Thomas Boullu </persName>
                              </respStmt>
                        </seriesStmt>
                        <sourceDesc><bibl type="sources"> <hi rend="italic">Instruction pour bien exercer la
                                                charge de commis aux caves dans la province de
                                                Normandie, sans contrevenir à la nouvelle ordonnance
                                              du Roy, portant règlement sur les fermes</hi>, 16
                                                septembre 1680 </bibl>
                                    </sourceDesc></fileDesc>
                  <encodingDesc>
                        <projectDesc source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
                              <p> Le projet FermGé vise à étudier l’impact d’une organisation
                                    fiscale (1664-1794), discriminante mais rationnelle, sur les
                                    territoires et les sociétés de la France moderne/p </p>
                        </projectDesc>
                  </encodingDesc>
                  <profileDesc>
                        <textClass>
                              <keywords scheme="#fr_RAMEAU">
                                    <list>
                                          <item> Histoire -- Histoire moderne -- Histoire
                                                administrative </item>
                                          <item> Histoire -- Histoire moderne -- Histoire judiciaire
                                          </item>
                                    </list>
                              </keywords>
                        </textClass>
                  </profileDesc>
                  <revisionDesc>
                        <change type="AutomaticallyEncoded"> 2022-12-13T13:17:55.470699+2:00
                        </change>
                  </revisionDesc>
            </teiHeader>
            <text>
                  <body>
                        <entry n="76" type="C" xml:id="Commis_aux_caves">
                              <form type="lemma">
                                    <orth> Commis aux caves </orth>
                              </form>
                              <sense>
                                    <def> Ces commis étaient affectés spécialement à la visite des
                        <ref target="#cabaretiers"> cabaretiers</ref> et
                                          hôteliers. Ils allaient par deux, faisaient ouvrir les
                                          caves et celliers et inventoriaient les boissons, puis les
                                          marquaient et les rouannaient. L’enregistrement se
                                                faisait dans les <ref target="#registres"> registres
                                               </ref> portatifs signés d’eux et du vendant (ou, en
                                                son absence, de son domestique), registres arrêtés
                                                chaque mois et à partir desquels les états mensuels
                                                de recette étaient dressés par le receveur des aides. A partir de 1680, les
                                          commis aux caves devaient en outre obtenir du <ref target="#cabaretier"> cabaretier</ref> ou hôtelier
                                          le prix auquel il vendait ses boissons, au besoin
                                          interroger les clients. Ils devaient porter une attention
                                          particulière aux barils percés : les débitants ne devaient
                                          pas en avoir plus de deux ouverts en même temps pour
                                          chaque type de boisson, pour éviter qu’ils ne les
                                          remplissent frauduleusement entre deux visites des commis.
                                          La Ferme enjoignit régulièrement les commis aux caves de
                                          vérifier les cachettes, les communications entre les
                                          maisons, de multiplier les visites, contre-visites et
                                          perquisitions nocturnes, de vérifier les rouannes et
                                          fausses marques. Les constats de <ref target="#fraude">
                                                fraude</ref> faisaient l’objet de <ref target="#procès-verbaux"> procès-verbaux</ref>
                                          dressés selon les usages ordinaires.<ref target="#portatifs"> registres portatifs</ref>
                                          permettaient d’établir les feuilles mensuelles de collecte
                                          des droits d’<ref target="#aides"> aides</ref> (ou
                                          feuilles de <ref target="#quêtes"> quêtes</ref>). Les
                                          commis aux caves étaient responsables des défauts de
                                          paiement des <ref target="#cabaretiers"> cabaretiers
                                         </ref>. Au terme de deux mois, ces derniers étaient
                                          déclarés insolvables et les sommes non enregistrées
                                          déduites des appointements des commis. Les commis devaient
                                          en outre lever le <ref target="#annuel">droit annuel
                                         </ref>. Enfin, la compagnie les exhortait à « ne faire
                                          injustice à personne garder toujours l’équité entre le
                                          fermier et le cabaretier, ne rien faire de lâche avec eux,
                                          c’est-à-dire ne se point familiariser, jouer ny boire, en
                                          sorte que l’on ne leur ait point obligation qui puisse
                                          engager à leur faire faveur, afin qu’ils n’ayent rien à
                                          leur reprocher ; ne recevoir aucuns présents des
                                          cabaretiers, payer la dépense que l’on fait chez eux, à
                                          peine de révocation de retranchement des appointemens qui
                                          leur seroient deus ; et si quelque commis manque à la
                                          fidélité ou à son devoir, son camarade en ayant
                                          connaissance en sera tenu, s’il n’en a donné avis au
                                          receveur». </def>
                                    <listBibl><bibl type="sources"> <hi rend="italic">Instruction pour bien exercer la
                                                charge de commis aux caves dans la province de
                                                Normandie, sans contrevenir à la nouvelle ordonnance
                                              du Roy, portant règlement sur les fermes</hi>, 16
                                                septembre 1680 </bibl>
                                    </listBibl></sense>
                        </entry>
                  </body>
            </text>
      </TEI>
      