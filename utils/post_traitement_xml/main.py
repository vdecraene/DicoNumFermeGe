import re
from lxml import etree
import copy

# Charger le fichier XML/TEI dans un objet ElementTree
tree = etree.parse("test_clean.xml")
root = tree.getroot()

# Boucle sur les éléments <tei> enfants du <teiCorpus>
for tei in root.findall(".//TEI"):
    # Récupérer la valeur de l'attribut 'n'
    n = tei.get("n")

    # Ajouter le namespace TEI à la balise <tei>
    tei.set("xmlns", "http://www.tei-c.org/ns/1.0")

    # Supprimer les balises <span>
    for span in tei.findall(".//span"):
        span.getparent().remove(span)

    # Supprimer les balises <br>
    for br in tei.findall(".//br"):
        br.getparent().remove(br)

    # Supprimer les balises <lb/>
    for lb in tei.findall(".//lb"):
        lb.getparent().remove(lb)

    # Rechercher les balises <bibl type="references">
    for bibl in tei.findall(".//bibl[@type='references']"):
        # Créer un nouvel élément <listBibl>
        listbibl = etree.Element("listBibl")

        # Déplacer le contenu de <bibl> vers <listBibl>
        for child in bibl.getchildren():
            listbibl.append(child)

        # Remplacer <bibl> par <listBibl>
        parent = bibl.getparent()
        parent.replace(bibl, listbibl)

        # Copier le contenu de listBibl dans sourceDesc
        copied_list = copy.deepcopy(listbibl)
        sourceDesc = tei.find(".//sourceDesc")
        sourceDesc.clear()  # Supprime le contenu existant de sourceDesc
        for child in copied_list:
            sourceDesc.append(child)

    tei_tree = etree.ElementTree(tei)

    # Créer un nouveau fichier
    with open(f"{n}.xml", "wb") as f:
        f.write(etree.tostring(tei_tree, xml_declaration=True, encoding="UTF-8"))
