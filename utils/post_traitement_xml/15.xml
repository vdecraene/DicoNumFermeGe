<?xml version='1.0' encoding='UTF-8'?>
<TEI n="15" xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                  <fileDesc>
                        <titleStmt>
                              <title type="notice"> Régie des accises de Prusse </title>
                              <author>Florian Schui</author>
                            <author>    </author>
                            <author>(traduction: Dominique Macabies)</author>

                        </titleStmt>
                        <publicationStmt>
                              <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer
                                    le privilège la Ferme générale dans l'espace français et
                                    européen (1664-1794)Axe 1 : Dictionnaire de la Ferme générale,
                                    objet d'histoire totale </publisher>
                              <pubPlace>
                                    <address>
                                          <addrLine> Maison Européenne des Sciences de l'Homme et de
                                                la Société </addrLine>
                                          <addrLine> 2 rue des cannoniers </addrLine>
                                          <addrLine> 59002 Lille Cedex </addrLine>
                                    </address>
                                    2021-2025
                              </pubPlace>
                              <availability>
                                    <licence>Creative Commons Attribution 3.0 non
                                          transposé (CC BY 3.0)</licence>
                              </availability>
                        </publicationStmt>
                        <seriesStmt>
                              <title> Dictionnaire numérique de la Ferme générale </title>
                              <respStmt>
                                    <resp> Coordinateurs de l'axe : Dictionnaire numérique de la
                                          Ferme générale, objet d'histoire totale. </resp>
                                    <persName> Marie-Laure Legay </persName>
                                    <persName> Thomas Boullu </persName>
                              </respStmt>
                        </seriesStmt>
                        <sourceDesc><bibl type="sources">Marc Antoine De la Haye De
                                                Launay, <hi rend="italic">Justification du système d’économie
                                                politique et financière de Frédéric II., roi de
                                                Prusse: pour servir de réfutation à tout ce que M.
                                                le Comte de Mirabeau a hazardé à ce sujet dans son
                                                  ouvrage de la monarchie prussienne</hi>, 1789</bibl>
                                          <bibl type="sources">Frederick William II, “Verordnung
                                                für sämmtliche Provinzen diesseits der Weser, wegen
                                                einer neuen Einrichtung des Accise- und Zoll-Wesens,
                                                De Dato Berlin, den 25sten Jan. 1787″’, <hi rend="italic">Novum Corpus
                                                Constitutionum Prussico-Brandenburgensium Praecipue
                                                  Marchicarum</hi>, 12 vols., Berlin, 1787, vol. 8, p.
                                                256–8</bibl>
                                          <bibl>Johann Georg Hamann, “Au Salomon de Prusse”, in
                                                Josef Nadler (ed.), <hi rend="italic">Sämtliche Werke/Johann Georg
                                                  Hamann</hi>, 6 vols., Vienna, 1951, vol. 3, p. 55–60 et
                                                p. 423–4</bibl>
                                          <bibl type="sources">Honoré Gabriel Riqueti de
                                                Mirabeau, <hi rend="italic">Lettre remise à Fréderic Guillaume II, roi
                                                régnant de Prusse, le jour de son avènement au
                                                  trône</hi>, 1787</bibl>
                                          <bibl type="sources">Honoré Gabriel de Riqueti de
                                                Mirabeau, <hi rend="italic">De la monarchie prussienne, sous Frédéric
                                                le Grand, avec un appendice contenant des recherches
                                                sur la situation actuelle des principales contrées
                                                  de l’Allemagne</hi>, 1788</bibl>
                                          <bibl>Ingrid Mittenzwei, <hi rend="italic">Preußen nach dem Siebenjährigen
                                                Krieg: Auseinandersetzungen zwischen Bürgertum und
                                              Staat um die Wirtschaftspolitik</hi>, Akademie, 1979</bibl>
                                          <bibl>Johanna Schopenhauer, <hi rend="italic">Im Wechsel der Zeiten, im
                                              Gedränge der Welt</hi>, Winkler, 1986</bibl>
                                          <bibl> Florian Schui, <hi rend="italic">Rebellious Prussians: Urban
                                              political culture under Frederick the Great</hi>, Oxford
                                                University Press, 2013 </bibl>
                                    </sourceDesc></fileDesc>
                  <encodingDesc>
                        <projectDesc source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
                              <p> Le projet FermGé vise à étudier l’impact d’une organisation
                                    fiscale (1664-1794), discriminante mais rationnelle, sur les
                                    territoires et les sociétés de la France moderne/p </p>
                        </projectDesc>
                  </encodingDesc>
                  <profileDesc>
                        <textClass>
                              <keywords scheme="#fr_RAMEAU">
                                    <list>
                                          <item> Histoire -- Histoire moderne -- Histoire fiscale </item>
                                          <item> Histoire -- Histoire moderne -- Histoire financière </item>
                                          <item> Histoire -- Histoire moderne -- Histoire politique </item>
                                          <item> Histoire -- Histoire moderne -- Histoire des
                                                relations internationales </item>
                                    </list>
                              </keywords>
                        </textClass>
                  </profileDesc>
                  <revisionDesc>
                        <change type="AutomaticallyEncoded"> 2022-12-20T13:49:17.597640+2:00
                        </change>
                  </revisionDesc>
            </teiHeader>
            <text>
                  <body>
                        <entry n="15" type="R" xml:id="Régie_des_accises_de_Prusse">
                              <form type="lemma">
                                    <orth> Régie des accises de Prusse </orth>
                              </form>
                              <sense>
                                  <def> En <placeName>Prusse</placeName> entre 1766 et 1787, La Régie – comme était
                                          communément appelée l’Administration générale des accises
                                          et de péages – était une administration en charge de la
                                          taxe d’accise, dirigée par des fonctionnaires français.
                                          Bien qu’éphémère, la Régie fut associée à d’importants
                                          développements pendant cette période de l’histoire
                                          prussienne, notamment en rapport avec les transferts de
                                          connaissances européennes, la construction de l’État en
                                          Prusse et l’émergence dans ce même pays d’un public
                                          critique éclairé. La discussion suivante se concentre sur
                                          ces questions. Elle est structurée chronologiquement.<emph>La création de la Régie:</emph> À l’issue de la guerre de Sept Ans
                                          (1756-1763), les puissances participantes se
                                          retrouvèrent avec des ressources financières exsangues et
                                          un besoin urgent de générer des revenus supplémentaires.
                                          En France, cela inaugura la période d’instabilité
                                          financière qui mena à la Révolution. Quant aux
                                          Britanniques, ils tentèrent d’augmenter leurs recettes
                                          fiscales dans les <ref target="#colonies"> colonies</ref>
                                          d’Amérique du Nord, ce qui contribua au déclenchement de
                                          la Révolution américaine. En Prusse, Frédéric le Grand
                                          chercha à obtenir des recettes supplémentaires en
                                          augmentant les droits d’accises et en collectant l’impôt
                                          de manière plus efficace. Ses plans se heurtèrent à la
                                          résistance de l’administration : elle refusa d’exécuter
                                          les ordres du roi, jugeant ses exigences trop lourdes pour
                                          la population – qui avait déjà souffert de la guerre. Afin
                                          de briser la résistance de l’administration établie, il
                                          recruta des fonctionnaires fiscaux français, qui allaient
                                          créer une toute nouvelle administration des accises :
                                          l’Administration générale des accises et des <ref target="#péages"> péages</ref>. Les fonctionnaires
                                          français arrivèrent en 1766,
                                          sous la direction de Marc Antoine de la Haye de Launay. De
                                          Launay avait été Sous-Fermier en <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Languedoc"> Languedoc</ref>
                                         </orgName> et avait auparavant tenté de faire fortune dans
                                          d’autres États allemands comme entrepreneur en temps de
                                          guerre. On ne sait pas grand-chose des réseaux par
                                          lesquels lui et ses collègues furent recrutés, mais
                                          Mirabeau affirme qu’Helvétius, lui-même ancien fermier
                                          fiscal et visiteur à la cour de Frédéric, joua un rôle central. Le
                                          nombre exact de ces fonctionnaires est également confus,
                                          mais une estimation à 350 semble raisonnable. Ces
                                          fonctionnaires français occupaient exclusivement les
                                          niveaux hiérarchiques les plus élevés de
                                          l’administration.<emph>Principales
                                          caractéristiques de la Régie:</emph> La Régie était chargée des
                                          accises, des douanes et de certains monopoles. Les accises
                                          étaient les plus importantes de ces taxes. Elles n’étaient
                                          perçues que dans les villes ; dans les campagnes, le
                                          principal impôt était l’impôt foncier. Dans la plupart des
                                          cas, l’accise était prélevée sur les marchandises vendues
                                          dans les villes ou qui y transitaient. Elle s’apparentait
                                          à un impôt sur les ventes ou à une taxe douanière, perçue
                                          non pas à une frontière internationale mais à la frontière
                                          entre ville et campagne. Magasins et portes des villes
                                          étaient donc les principaux points de collecte de l’impôt.
                                          De nombreuses villes étaient entourées de murs d’accises
                                          et de dispositifs similaires destinés à entraver la <ref target="#contrebande"> contrebande</ref>.<ref target="#bière"> bière
                                         </ref> locale traditionnelle était préférable, au
                                          petit-déjeuner, à des boissons exotiques importées, telles
                                        que <ref target="#café"> café</ref> ou <ref target="#thé">thé</ref>, soupçonnées
                                          également d’être consommées dans des tasses en porcelaine
                                          fabriquées à l’étranger.<emph>Résistance contre la Régie:</emph> Avec des contrôles
                                          plus stricts, la perception des impôts par la Régie devint
                                          plus efficace et, très rapidement, les contribuables
                                          urbains mirent en œuvre diverses formes de résistance. Les
                                          contrôles fiscaux se déroulaient non seulement aux portes
                                          des agglomérations – où étaient fouillés cargaisons
                                          commerciales et particuliers – mais aussi dans les
                                          ateliers et magasins des villes. Parfois, même les
                                          domiciles privés étaient fouillés lorsqu’était soupçonnée
                                          une violation des règles du monopole. Le <ref target="#café"> café</ref>, par exemple, devait
                                          être acheté déjà torréfié auprès d’une société
                                          monopolistique. Lorsque les fonctionnaires de la Régie
                                          soupçonnaient que le <ref target="#café"> café</ref>
                                          avait été torréfié à domicile, ils avaient le droit de
                                          fouiller la maison, ce qui leur valut le surnom de
                                          « renifleurs de café ». Ces intrusions dans l’espace privé
                                          des citadins et dans les sphères de circulation
                                          commerciale suscitèrent indignation et résistance. La
                                          résistance commença chez les marchands du petit territoire
                                      occidental de <placeName>Clèves</placeName>. Juste après la création de la
                                          Régie, ils proposèrent de mettre en place leur propre
                                          collecte de l’impôt agricole: ils verseraient à la
                                          couronne une somme forfaitaire en échange du droit de
                                          collecter l’impôt eux-mêmes. En l’occurrence, Frédéric
                                          céda rapidement à la pression locale : Clèves était une
                                          petite province périphérique et les marchands étaient plus
                                          puissants dans ce territoire commercialement plus avancé
                                          qu’ailleurs en Prusse. La résistance dans
                                                d’autres parties de la Prusse prit d’abord des
                                                formes similaires : les habitants et administrateurs
                                                locaux adressaient des pétitions au roi et plus
                                                Frédéric s’entêtait, plus la résistance adoptait des
                                                formes différentes. L’excise et ses agents
                                          étaient défiés frontalement par la <ref target="#contrebande"> contrebande</ref> et par des
                                          attaques violentes à l’encontre des fonctionnaires de la
                                          Régie, qui affrontaient les contrebandiers ou leurs
                                          clients. Du point de vue de l’État, la contrebande n’était
                                          rien moins qu’un délit, mais le grand public ne prenait
                                          pas les contrebandiers pour des criminels gouvernement
                                          s’opposant à la cause naturelle du libre-échange. Les
                                          consommateurs rejetaient toute notion selon laquelle leurs
                                          décisions sur le marché devraient être régies par autre
                                          chose que leurs propres choix et préférences. Les
                                          monopoles gouvernementaux et les structures fiscales
                                          visant à favoriser l’industrie nationale ou à protéger les
                                          citoyens des effets moralement corrosifs de la
                                          consommation de luxe étaient rejetés comme condescendants
                                          et mus par des intérêts particuliers. Vus sous cet angle,
                                          les contrebandiers étaient bien plus que des délinquants
                                          et jouissaient d’une popularité considérable.
                                                La sympathie de l’opinion publique à l’endroit des
                                                  <ref target="#contrebandiers"> contrebandiers
                                               </ref> prussiens fait écho au mythe populaire
                                                entourant le contrebandier français <ref target="#Mandrin">Louis Mandrin</ref> et à la valeur
                                                politique associée par les habitants des colonies
                                                britanniques d’Amérique du Nord à la résistance aux
                                        monopoles, comme par exemple la <hi rend="italic">Boston Tea Party</hi>,
                                                entre autres événements similaires. Le
                                          conflit autour de la Régie fut également l’un des points
                                          centraux des débats contradictoires qui se développèrent
                                          en Prusse pendant la seconde moitié du XVIIIe siècle.
                                          Certaines des institutions et des figures les plus
                                          importantes du public bourgeois prussien à cette époque
                                          prenaient part à ces discussions. Ces échanges se
                                          déroulaient dans les salons et les maisons d’édition
                                        savantes de <placeName>Berlin</placeName>, telles que la <hi rend="italic">Mittwochsgesellschaft</hi> et
                                        la <hi rend="italic">Berlinische Monatsschrift</hi>. Ils étaient commentés par
                                          des sommités de l’époque des Lumières en Prusse, telles
                                          que Johann George Hamann, Johann Gottfried Herder,
                                          Friedrich Hartknoch, Friedrich Nicolai, Friedrich Gedike,
                                          Johann Zimmer, entre autres. Des fonctionnaires, dont de
                                          Launay et le premier ministre Ewald Friedrich von
                                          Hertzberg, alimentèrent également le débat public, qui
                                          dépassait d’ailleurs le contexte prussien immédiat. L’un
                                          des contempteurs les plus virulents de la Régie était
                                          Mirabeau, qui séjourna en Prusse en
1786-1787 et
                                          s’attaqua abondamment à la Régie dans son ouvrage en
                                        quatre volumes intitulé <hi rend="italic">De la monarchie Prussienne</hi>, ainsi
                                          que dans d’autres écrits. En Prusse, les controverses
                                          étaient également liées à d’autres plus larges concernant
                                          le commerce et la fiscalité. Les travaux de l’abbé de
                                        Raynal, de l’abbé Galiani et le <hi rend="italic">Compte Rendu de Necker</hi>,
                                          entre autres, ont tous été lus et discutés par les
                                          participants à ce débat. <emph>La fin de
                                          la Régie:</emph> Pour apaiser les protestations populaires,
                                          Frédéric-Guillaume II abolit la Régie en 1787. Il accéda au trône après la mort de
                                          Frédéric le Grand en 1786 et
                                          appela immédiatement à la création d’une commission pour
                                          la réforme de l’accise. La Régie fut dissoute et
                                          l’administration des accises fut à nouveau intégrée à
                                          l’administration générale. L’édit de
1787 ordonnant la fin de la Régie reprit
                                          intégralement les arguments et le langage du public
                                          protestataire. Frédéric-Guillaume condamna les
                                          « incessantes perquisitions », les « pernicieuses
                                          formalités » et les  « <ref target="#vexations"> vexations
                                         </ref> » perpétrées par la Régie. Il admit que la <ref target="#contrebande"> contrebande</ref> était le
                                          résultat inévitable des méthodes abusives et intrusives de
                                          la Régie. Le nouveau monarque fit siennes les exigences de
                                          la bourgeoisie urbaine et promit de mettre fin à ces
                                          vexations tout en supprimant ce qui « limite le commerce
                                          et la circulation des biens ». Au contraire, le nouveau
                                          régime fiscal s’attacherait désormais à « ranimer le
                                          commerce bourgeois » en lui rendant sa « légitime
                                          liberté ». L’abolition des monopoles sur le <ref target="#café"> café</ref> et le <ref target="#tabac"> tabac</ref> s’inscrivait dans cet
                                          esprit libéral. Ces changements s’accompagnèrent également
                                          d’une nouvelle structure tarifaire plus régressive qui
                                          augmentait de nouveau la charge fiscale sur les produits
                                          de consommation courante en taxant plus lourdement la
                                          mouture des céréales. Les riches citadins retirèrent un
                                          certain nombre d’avantages de cette réforme.
                                                Outre qu’elle favorisa la libéralisation économique
                                                et la réduction de l’impôt sur les produits de luxe,
                                                le renvoi des fonctionnaires fiscaux français offrit
                                                également aux classes aisées des opportunités
                                                d’accéder à des postes élevés au sein de
                                                l’administration, et l’administration traditionnelle
                                                y gagna l’accroissement de son pouvoir. <orgName subtype="province" type="pays_et_provinces"> Bretagne</orgName> avait contribué de manière
                                          cruciale au déclenchement de la Révolution américaine.<ref target="#domaines"> domaines</ref>
                                          avaient déjà été vendus à ce stade. Du point de vue
                                          politique, cette différence était cruciale car les
                                          conflits entre la jeune bourgeoisie et l’État étaient
                                          étroitement liés aux institutions de l’État fiscal
                                          émergent. Contrairement à la fiscalité habituelle, les
                                          revenus des domaines n’obligeaient pas le gouvernement à
                                          instituer une administration intrusive destinée à
                                          contrôler la vie professionnelle et privée des citoyens en
                                          vue de confisquer une partie de leurs revenus. L’État
                                          prussien était non seulement trop faible pour imposer sa
                                          férule à son peuple bourgeois, mais sa situation fiscale
                                          signifiait également qu’il pouvait se permettre de
                                          capituler politiquement plutôt que de risquer une nouvelle
                                          escalade…
                                    <figure>
                                        <graphic url="/static/images/regie_Prusse.jpg"/>
                                        <head>‘Die Einwanderung der Franzosen zur Errichtung der Regie’(The arrival of the French for the creation of the Régie), Daniel Nikolaus Chodowiecki, etching, 1771. Reproduced with permission from Klassik Stiftung Weimar, Museen, Inv. DK 293/84. </head>
                                    </figure></def>
                                    <listBibl><bibl type="sources">Marc Antoine De la Haye De
                                                Launay, <hi rend="italic">Justification du système d’économie
                                                politique et financière de Frédéric II., roi de
                                                Prusse: pour servir de réfutation à tout ce que M.
                                                le Comte de Mirabeau a hazardé à ce sujet dans son
                                                  ouvrage de la monarchie prussienne</hi>, 1789</bibl>
                                          <bibl type="sources">Frederick William II, “Verordnung
                                                für sämmtliche Provinzen diesseits der Weser, wegen
                                                einer neuen Einrichtung des Accise- und Zoll-Wesens,
                                                De Dato Berlin, den 25sten Jan. 1787″’, <hi rend="italic">Novum Corpus
                                                Constitutionum Prussico-Brandenburgensium Praecipue
                                                  Marchicarum</hi>, 12 vols., Berlin, 1787, vol. 8, p.
                                                256–8</bibl>
                                          <bibl>Johann Georg Hamann, “Au Salomon de Prusse”, in
                                                Josef Nadler (ed.), <hi rend="italic">Sämtliche Werke/Johann Georg
                                                  Hamann</hi>, 6 vols., Vienna, 1951, vol. 3, p. 55–60 et
                                                p. 423–4</bibl>
                                          <bibl type="sources">Honoré Gabriel Riqueti de
                                                Mirabeau, <hi rend="italic">Lettre remise à Fréderic Guillaume II, roi
                                                régnant de Prusse, le jour de son avènement au
                                                  trône</hi>, 1787</bibl>
                                          <bibl type="sources">Honoré Gabriel de Riqueti de
                                                Mirabeau, <hi rend="italic">De la monarchie prussienne, sous Frédéric
                                                le Grand, avec un appendice contenant des recherches
                                                sur la situation actuelle des principales contrées
                                                  de l’Allemagne</hi>, 1788</bibl>
                                          <bibl>Ingrid Mittenzwei, <hi rend="italic">Preußen nach dem Siebenjährigen
                                                Krieg: Auseinandersetzungen zwischen Bürgertum und
                                              Staat um die Wirtschaftspolitik</hi>, Akademie, 1979</bibl>
                                          <bibl>Johanna Schopenhauer, <hi rend="italic">Im Wechsel der Zeiten, im
                                              Gedränge der Welt</hi>, Winkler, 1986</bibl>
                                          <bibl> Florian Schui, <hi rend="italic">Rebellious Prussians: Urban
                                              political culture under Frederick the Great</hi>, Oxford
                                                University Press, 2013 </bibl>
                                    </listBibl></sense>
                        </entry>
                  </body>
            </text>
      </TEI>
      