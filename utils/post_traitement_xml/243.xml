<?xml version='1.0' encoding='UTF-8'?>
<TEI n="243" xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                  <fileDesc>
                        <titleStmt>
                              <title type="notice"> Compagnie des Indes orientales </title>
                              <author>Marie-Laure Legay</author>
                        </titleStmt>
                        <publicationStmt>
                              <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer
                                    le privilège la Ferme générale dans l'espace français et
                                    européen (1664-1794)Axe 1 : Dictionnaire de la Ferme générale,
                                    objet d'histoire totale </publisher>
                              <pubPlace>
                                    <address>
                                          <addrLine> Maison Européenne des Sciences de l'Homme et de
                                                la Société </addrLine>
                                          <addrLine> 2 rue des cannoniers </addrLine>
                                          <addrLine> 59002 Lille Cedex </addrLine>
                                    </address>
                                    2021-2025
                              </pubPlace>
                              <availability>
                                    <licence>Creative Commons Attribution 3.0 non
                                          transposé (CC BY 3.0)</licence>
                              </availability>
                        </publicationStmt>
                        <seriesStmt>
                              <title> Dictionnaire numérique de la Ferme générale </title>
                              <respStmt>
                                    <resp> Coordinateurs de l'axe : Dictionnaire numérique de la
                                          Ferme générale, objet d'histoire totale. </resp>
                                    <persName> Marie-Laure Legay </persName>
                                    <persName> Thomas Boullu </persName>
                              </respStmt>
                        </seriesStmt>
                        <sourceDesc><bibl><idno type="ArchivalIdentifier">AN, G<hi rend="sup">1</hi> 15,
                                                  <hi rend="italic">Délibération de la compagnie des Fermiers généraux
                                                      de 1760/contrôleurs de magasin à Lorient</hi></idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN, G<hi rend="sup">1</hi> 80, dossier
                                                  21 : <hi rend="italic">Inventaire des marchandises restant en
                                                        magasin d’après les recensement d’août 1786</hi></idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN, G<hi rend="sup">1</hi> 83, dossier
                                                  9 : « Ordre de régie pour Lorient », 1781</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN, K 885, n° 1A,
                                                  Mémoire personnel aux Sieurs Pâris sur les
                                                  affaires générales où ils furent employés, 1740 
                                                  K 885 n° 2, Mémoire sur les finances, après 1740
                                                  cité par Marc Cheynet de Beaupré, « Joseph
                                                  Pâris-Duverney, financier d’Etat (1684-1770) »,
                                                  thèse soutenue en 2010 à l’université de Sorbonne
                                                  Paris 1, tome 1, p. 700</idno></bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN, KK 1005b :
                                                  Claude Pâris de la Montagne, Traité des
                                                  administrations des recettes et dépenses du
                                                  royaume, Paris, 1733</idno>
                                          </bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat passant à la
                                                compagnie des Indes le bail des fermes générales
                                                consenti à Aymard Lambert moyennant 48.500.000
                                              livres…</hi>, premier septembre 1719</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat pour la
                                                prise de possession du bail des fermes générales
                                                unies, par la compagnie des Indes sous le nom
                                              d'Armand Pillavoine, pour neuf ans</hi>, 12 octobre
                                                1719</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat fixant le
                                                prêt à 1.500.000.000 livres moyennant 45.000.000
                                              livres de rente 3 %</hi>, 17 octobre 1719</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui
                                                ordonne, attendu la délibération de la Compagnie des
                                                Indes de régir toutes les fermes de Sa Majesté, que
                                                l'arrêt du conseil du 31 août dernier, en ce qui
                                                regarde les publications et adjudications des
                                              sous-fermes, sera nul et non avenu</hi>, 23 septembre
                                                1719</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêts du conseil d’Etat, qui
                                                ordonnent l’exécution dans les port et ville de
                                                Dunkerque, des édits, déclarations, arrêts et
                                                règlements concernant le commerce de la compagnie
                                                des Indes et notamment le privilège exclusif de
                                                l'introduction et de la vente du café dans le
                                              royaume</hi>, 29 novembre 1729 et 17 janvier 1730</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d'Etat qui ordonne
                                                la saisie et confiscation des mousselines et toiles
                                                de coton blanches, marquées soit des plombs et
                                                bulletins contrefaits de la Compagnie des Indes,
                                                soit de plombs contrefaits et bulletins vrais soit
                                              de plombs et bulletins vrais réapposés</hi>, 4 novembre
                                                1766</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d'Etat qui fixe les
                                                droits qui seront perçus, tant à l'entrée qu'à la
                                                sortie du royaume, sur les cafés provenant des iles
                                                et colonies françaises de l'Amérique et sur ceux
                                                apportés du Levant ou provenant du commerce de la
                                              Compagnie des Indes</hi>, 25 janvier 1767</bibl>
                                          <bibl type="sources">Joseph Du Fresne de
                                                Francheville, <hi rend="italic">Histoire de la Compagnie des Indes
                                                avec les titres de ses concessions et privilèges,
                                                  dressée sur les pièces autentiques</hi>, Paris, Chez De
                                                Bure l'aîné, 2 t., 1746</bibl>
                                          <bibl type="sources">Challaye, <hi rend="italic">Mémoire pour le sieur
                                                Dupleix contre la Compagnie des Indes, avec les
                                              pièces justificatives</hi>, Paris, Leprieur, 1759</bibl>
                                          <bibl type="sources">Louis-Léon-Félicité Brancas, comte
                                                de Lauraguais, <hi rend="italic">Mémoire sur la Compagnie des Indes,
                                                  précédé d'un Discours sur le commerce en général</hi>,
                                                Paris, Lecomte, 1769</bibl>
                                          <bibl type="sources">André Morellet, <hi rend="italic">Mémoire sur la
                                              situation actuelle de la Compagnie des Indes</hi>, Paris,
                                                Desaint, 1769</bibl>
                                          <bibl type="sources"><hi rend="italic">Balance des services de la compagnie
                                                des Indes envers l'Etat, et de ceux de l'Etat envers
                                                la compagnie depuis 1719 jusqu'en 1725, Londres et
                                              Paris, Des Ventes de la Doué</hi>, 1769</bibl>
                                          <bibl>Philippe Haudrère, <hi rend="italic">La Compagnie des Indes au
                                              xviiie siècle (1719-1795)</hi>, Paris, Librairie de
                                                l’Inde, 1989</bibl>
                                        <bibl>Gérard Le Bouëdec, « Les compagnies françaises des Indes et l'économie du pivilège », dans G. Garner (dir.), <hi rend="italic">Die Ökonomie des Privilegs, Westeuropa 16.-19. Jahrhundert</hi>, Francfort-sur-le-Main, Klostermann, 2016, p. 465-494</bibl>
                                          <bibl>Gérard Le Bouëdec et Hiroyasu Kimizuka, « Lorient,
                                                grand port de dimension mondiale de la façade
                                                atlantique française (1783-1787) ? », <hi rend="italic">Annales de
                                                  Bretagne et des Pays de l’Ouest</hi>, 126-1, 2019, p.
                                                103-125</bibl>
                                    </sourceDesc></fileDesc>
                  <encodingDesc>
                        <projectDesc source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
                              <p> Le projet FermGé vise à étudier l’impact d’une organisation
                                    fiscale (1664-1794), discriminante mais rationnelle, sur les
                                    territoires et les sociétés de la France moderne/p </p>
                        </projectDesc>
                  </encodingDesc>
                  <profileDesc>
                        <textClass>
                              <keywords scheme="#fr_RAMEAU">
                                    <list>
                                          <item> Histoire -- Histoire moderne -- Histoire financière </item>
                                          <item> Histoire -- Histoire moderne -- Histoire fiscale
                                          </item>
                                    </list>
                              </keywords>
                        </textClass>
                  </profileDesc>
                  <revisionDesc>
                        <change type="AutomaticallyEncoded"> 2022-12-13T13:19:56.273054+2:00
                        </change>
                  </revisionDesc>
            </teiHeader>
            <text>
                  <body>
                        <entry n="243" type="C" xml:id="Compagnie_des_Indes_orientales">
                              <form type="lemma">
                                    <orth> Compagnie des Indes orientales </orth>
                              </form>
                              <sense>
                                    <def> Compagnie de commerce à monopole dont les statuts et
                     privilèges ont évolué durant les XVIIe et XVIIIe siècle.
                                                Ils ont été notamment régis par l’édit d’août 1664 (création), l’édit de
                                                mai 1719 (refonte) et les
                                                lois de 1725. La compagnie des Indes organisée par John Law
                                          prit la Ferme générale en charge lors de l’<ref target="#adjudication"> adjudication</ref> du 27
                                          août 1719. Le bail, connu sous
                                          le nom d’Armand Pillavoine, fut négocié à 56 millions par
                                          an dont 15 millions pour les <ref target="#Gabelles">
                                                Gabelles</ref> de France, 16,4 millions pour les
                                                <ref target="#aides"> aides</ref> et <ref target="#entrées"> entrées</ref>, 5,1 millions
                                          pour les <ref target="#petites_gabelles">petites gabelles</ref>, trois millions pour le <ref target="#Domaine"> Domaine
                                         </ref>, 430 000 livres pour le <ref target="#Occident">Domaine d’Occident</ref>, tandis que les
                                          amortissements, droits de francs-fiefs, <ref target="#actes">contrôle des actes</ref> et centième denier
                                          entraient dans le bail pour 4, 2 millions. Cette
                                                absorption de la compagnie financière par la
                                                compagnie de commerce se comprend dans le cadre du
                                                règlement de la crise monétaire et de la crise de la
                                                dette par le consortium conçu autour de la banque
                                                royale. Elle s’inscrit dans une série de
                                          réunions similaires de fermes (fermes des <ref target="#tabacs"> tabacs</ref>, monopole du <ref target="#café"> café</ref>, fabrication des
                                          monnaies, salines de <placeName>Moyenvic</placeName>… et recettes générales)
                                          grâce auxquelles la Compagnie des Indes s’engageait à
                                          prêter au roi 120 millions à trois pour cent. Law parvint
                                          à réunir les deux établissements en cassant le bail Aymard
                                          Lambert dans lequel les frères <ref target="#Pâris">Pâris
                                         </ref> s’étaient engagés un an auparavant. Il fallut donc
                                          rembourser les avances consenties en 1718. Toutefois, le bail Pillavoine fut
                                          lui-même résilié après la chute de John Law en janvier
                                                1721. A ce moment-là,
                                          Joseph <ref target="#Pâris"> Pâris</ref>-Duverney jugea
                                          que la Ferme générale se trouvait au plus bas : « La
                                          facilité des payemens en papier avoit fait enlever des
                                          greniers une grande quantité de sels et entrer dans les
                                          villes beaucoup de vins et de marchandises sujettes aux
                                          droits d’entrées, de sorte que les sujets, fournis de
                                          provisions et dénués d’argent, ne paroissoient pas devoir
                                          contribuer à rendre fort abondant le produit des fermes ».
                                          La Compagnie des Indes avait pris le parti de supprimer
                                          les sous-fermes et de régir directement les droits. En
                                                1721, les principaux
                                          droits de la Ferme générale furent mis en régie par le
                                          bail Charles Cordier, tandis que les <ref target="#tabacs"> tabacs</ref> furent affermés. Néanmoins, cette
                                          dernière ferme ne donna pas satisfaction et la Compagnie
                                          des Indes récupéra sa gestion en septembre 1723, jusqu’en 1730, date à laquelle la Ferme
                                          générale prit de nouveau le monopole du tabac en charge.<ref target="#vins"> vins</ref> à <placeName><ref target="#Rochelle">La Rochelle</ref></placeName>, ville
                                                qui disposait pourtant du privilège de ne faire
                                                entrer que les vins de sa banlieue ; la
                                          compagnie fut par ailleurs exemptée des droits sur le sel
                                          imposés par le <ref target="#grenier"> grenier du Havre
                                         </ref> (1665). <placeName>Paris</placeName>. Le comte de
                                          Lauraguais, dans son plan de charge pour douze cargaisons,
                                          établit les droits pour la Ferme générale à 600 000 livres
                                          sur un total de charges de 29 millions (1769). Parmi les marchandises ramenées des
                                          Indes à <placeName><ref target="#Lorient">Lorient</ref></placeName>, siège de la compagnie, les mousselines et
                                          toiles de <ref target="#coton"> coton</ref> blanches,
                                          prohibées pour les autres négociants, formaient un trafic
                                          lucratif. Les fausses-marques de
                                                la compagnie des Indes aposées sur les toiles
                                                passées en contrebande depuis la <ref target="#Suisse"> Suisse</ref> obligea le Conseil
                                                à durcir les conditions de <ref target="#saisies">
                                                  saisies</ref> de ces marchandises pour combattre
                                                la fraude aux droits d’entrée (1766). A la même époque, les
                                          droits sur le café des îles Bourbon et <placeName>île de France</placeName>
                                          furent protégés par une surtaxe sur les cafés étrangers, à
                                          l’instar des cafés des îles antillaises. A <placeName type="lieux_habitations"><ref target="#Lorient">Lorient</ref></placeName> même,
                                          les gardes-magasins de la compagnie des Indes
                                          travaillaient de concert avec les commis de la Ferme
                                          générale. L’arrêt du 12 juillet 1672
 précisa ces rapports (bail François Legendre) :
                                          les marchandises étaient débarquées et entreposées dans
                                          les magasins de la compagnie « en présence des commis
                                          gardes dudit Legendre ». Des états doubles de la quantité
                                          et qualité des marchandises étaient dressés et les droits
                                          d’entrée réglaient au fur et à mesure des ventes.
                                                Lorsque le monopole prit fin (1769) et que le <placeName type="lieux_habitations"> port de <ref target="#Lorient">Lorient</ref>
                                          </placeName> s’ouvrit à d’autres entreprises de
                                                négoce, un nouvel ordre de régie fut établi pour
                                                régler les modalités des déclarations, tant en gros
                                                qu’en détail, des marchandises par les capitaines de
                                                vaisseaux (17 juillet 1781). L’objectif de cette nouvelle régie
                                          consistait à rappeler au receveur du <placeName type="lieux_controle"> bureau de <ref target="#Lorient">Lorient</ref>
                                    </placeName> qu’il devait expédier les marchandises
                                          arrivées au port et revendues dans les autres ports par
                                          <ref target="#caution">acquits à caution</ref>, et non
                                          par simples <ref target="#passavants"> passavants</ref>,
                                          y compris pour celles qui acquittent par anticipation les
                                          droits des <ref target="#fermes">Cinq grosses fermes
                                         </ref>. La Ferme générale demanda en outre que les gardes
                                          magasins tinssent un nouveau registre dit de « cédés »,
                                          propres aux marchandises du commerce de l’Inde,
                                          c’est-à-dire des <ref target="#registres"> registres
                                         </ref> qui admettent les cessionnaires au même titre que
                                          les adjudicataires de marchandises et qui constatent la
                                          vente des denrées. </def>
                                    <listBibl><bibl><idno type="ArchivalIdentifier">AN, G<hi rend="sup">1</hi> 15,
                                                  <hi rend="italic">Délibération de la compagnie des Fermiers généraux
                                                      de 1760/contrôleurs de magasin à Lorient</hi></idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN, G<hi rend="sup">1</hi> 80, dossier
                                                  21 : <hi rend="italic">Inventaire des marchandises restant en
                                                        magasin d’après les recensement d’août 1786</hi></idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN, G<hi rend="sup">1</hi> 83, dossier
                                                  9 : « Ordre de régie pour Lorient », 1781</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN, K 885, n° 1A,
                                                  Mémoire personnel aux Sieurs Pâris sur les
                                                  affaires générales où ils furent employés, 1740 
                                                  K 885 n° 2, Mémoire sur les finances, après 1740
                                                  cité par Marc Cheynet de Beaupré, « Joseph
                                                  Pâris-Duverney, financier d’Etat (1684-1770) »,
                                                  thèse soutenue en 2010 à l’université de Sorbonne
                                                  Paris 1, tome 1, p. 700</idno></bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN, KK 1005b :
                                                  Claude Pâris de la Montagne, Traité des
                                                  administrations des recettes et dépenses du
                                                  royaume, Paris, 1733</idno>
                                          </bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat passant à la
                                                compagnie des Indes le bail des fermes générales
                                                consenti à Aymard Lambert moyennant 48.500.000
                                              livres…</hi>, premier septembre 1719</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat pour la
                                                prise de possession du bail des fermes générales
                                                unies, par la compagnie des Indes sous le nom
                                              d'Armand Pillavoine, pour neuf ans</hi>, 12 octobre
                                                1719</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat fixant le
                                                prêt à 1.500.000.000 livres moyennant 45.000.000
                                              livres de rente 3 %</hi>, 17 octobre 1719</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui
                                                ordonne, attendu la délibération de la Compagnie des
                                                Indes de régir toutes les fermes de Sa Majesté, que
                                                l'arrêt du conseil du 31 août dernier, en ce qui
                                                regarde les publications et adjudications des
                                              sous-fermes, sera nul et non avenu</hi>, 23 septembre
                                                1719</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêts du conseil d’Etat, qui
                                                ordonnent l’exécution dans les port et ville de
                                                Dunkerque, des édits, déclarations, arrêts et
                                                règlements concernant le commerce de la compagnie
                                                des Indes et notamment le privilège exclusif de
                                                l'introduction et de la vente du café dans le
                                              royaume</hi>, 29 novembre 1729 et 17 janvier 1730</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d'Etat qui ordonne
                                                la saisie et confiscation des mousselines et toiles
                                                de coton blanches, marquées soit des plombs et
                                                bulletins contrefaits de la Compagnie des Indes,
                                                soit de plombs contrefaits et bulletins vrais soit
                                              de plombs et bulletins vrais réapposés</hi>, 4 novembre
                                                1766</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d'Etat qui fixe les
                                                droits qui seront perçus, tant à l'entrée qu'à la
                                                sortie du royaume, sur les cafés provenant des iles
                                                et colonies françaises de l'Amérique et sur ceux
                                                apportés du Levant ou provenant du commerce de la
                                              Compagnie des Indes</hi>, 25 janvier 1767</bibl>
                                          <bibl type="sources">Joseph Du Fresne de
                                                Francheville, <hi rend="italic">Histoire de la Compagnie des Indes
                                                avec les titres de ses concessions et privilèges,
                                                  dressée sur les pièces autentiques</hi>, Paris, Chez De
                                                Bure l'aîné, 2 t., 1746</bibl>
                                          <bibl type="sources">Challaye, <hi rend="italic">Mémoire pour le sieur
                                                Dupleix contre la Compagnie des Indes, avec les
                                              pièces justificatives</hi>, Paris, Leprieur, 1759</bibl>
                                          <bibl type="sources">Louis-Léon-Félicité Brancas, comte
                                                de Lauraguais, <hi rend="italic">Mémoire sur la Compagnie des Indes,
                                                  précédé d'un Discours sur le commerce en général</hi>,
                                                Paris, Lecomte, 1769</bibl>
                                          <bibl type="sources">André Morellet, <hi rend="italic">Mémoire sur la
                                              situation actuelle de la Compagnie des Indes</hi>, Paris,
                                                Desaint, 1769</bibl>
                                          <bibl type="sources"><hi rend="italic">Balance des services de la compagnie
                                                des Indes envers l'Etat, et de ceux de l'Etat envers
                                                la compagnie depuis 1719 jusqu'en 1725, Londres et
                                              Paris, Des Ventes de la Doué</hi>, 1769</bibl>
                                          <bibl>Philippe Haudrère, <hi rend="italic">La Compagnie des Indes au
                                              xviiie siècle (1719-1795)</hi>, Paris, Librairie de
                                                l’Inde, 1989</bibl>
                                        <bibl>Gérard Le Bouëdec, « Les compagnies françaises des Indes et l'économie du pivilège », dans G. Garner (dir.), <hi rend="italic">Die Ökonomie des Privilegs, Westeuropa 16.-19. Jahrhundert</hi>, Francfort-sur-le-Main, Klostermann, 2016, p. 465-494</bibl>
                                          <bibl>Gérard Le Bouëdec et Hiroyasu Kimizuka, « Lorient,
                                                grand port de dimension mondiale de la façade
                                                atlantique française (1783-1787) ? », <hi rend="italic">Annales de
                                                  Bretagne et des Pays de l’Ouest</hi>, 126-1, 2019, p.
                                                103-125</bibl>
                                    </listBibl></sense>
                        </entry>
                  </body>
            </text>
      </TEI>
      