<?xml version='1.0' encoding='UTF-8'?>
<TEI n="4" xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                  <fileDesc>
                        <titleStmt>
                              <title type="notice">Bateau-maire</title>
                              <author>Marie-Laure Legay</author>
                        </titleStmt>
                        <publicationStmt>
                              <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer
                                    le privilège la Ferme générale dans l'espace français et
                                    européen (1664-1794)Axe 1 : Dictionnaire de la Ferme générale,
                                    objet d'histoire totale </publisher>
                              <pubPlace>
                                    <address>
                                          <addrLine> Maison Européenne des Sciences de l'Homme et de
                                                la Société </addrLine>
                                          <addrLine> 2 rue des cannoniers </addrLine>
                                          <addrLine> 59002 Lille Cedex </addrLine>
                                    </address>
                                    2021-2025
                              </pubPlace>
                              <availability>
                                    <licence>Creative Commons Attribution 3.0 non
                                          transposé (CC BY 3.0)</licence>
                              </availability>
                        </publicationStmt>
                        <seriesStmt>
                              <title> Dictionnaire numérique de la Ferme générale </title>
                              <respStmt>
                                    <resp> Coordinateurs de l'axe : Dictionnaire numérique de la
                                          Ferme générale, objet d'histoire totale. </resp>
                                    <persName> Marie-Laure Legay </persName>
                                    <persName> Thomas Boullu </persName>
                              </respStmt>
                        </seriesStmt>
                        <sourceDesc><bibl><idno type="ArchivalIdentifier">AN, G<hi rend="sup">1</hi> 98,
                                                  Voitures des sels, dossier « Péage de La
                                                  Rocheguyon »</idno>
                                          </bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui casse
                                                deux sentences du grenier à sel de Gien et un
                                                jugement de la table de marbre du palais à Paris, et
                                                ordonne que le droit de salage qui se paie sur la
                                                Loire ne sera levé que sur le bateau mère de chaque
                                              équipe</hi>, 6 juillet 1723</bibl>
                                          <bibl type="sources"><hi rend="italic">Ordonnances sur le fait des gabelles
                                              des aides…</hi>, mai et juin 1680, chapitre XII, article
                                                4 et 5</bibl>
                                    </sourceDesc></fileDesc>
                  <encodingDesc>
                        <projectDesc source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
                              <p> Le projet FermGé vise à étudier l’impact d’une organisation
                                    fiscale (1664-1794), discriminante mais rationnelle, sur les
                                    territoires et les sociétés de la France moderne/p </p>
                        </projectDesc>
                  </encodingDesc>
                  <profileDesc>
                        <textClass>
                              <keywords scheme="#fr_RAMEAU">
                                    <list>
                                          <item> Histoire -- Histoire moderne -- Histoire fiscale
                                          </item>
                                    </list>
                              </keywords>
                        </textClass>
                  </profileDesc>
                  <revisionDesc>
                        <change type="AutomaticallyEncoded"> 2022-12-12T17:16:47.782676+2:00
                        </change>
                  </revisionDesc>
            </teiHeader>
            <text>
                  <body>
                        <entry n="4" type="B" xml:id="Bateau_maire">
                              <form type="lemma">
                                    <orth> Bateau-maire </orth>
                              </form>
                              <sense>
                                    <def>Concernant le <ref target="#voiturage"> voiturage</ref>
                                          des sels, il s’agit du bateau qui est chargé aux
                                          embouchures des fleuves et est mentionné aux brevets des
                                          officiers qui y sont établis. Si la rivière est plus
                                          petite, le bateau-maire est considéré comme celui étant à
                                          la tête de l’équipage. L’<ref target="#ordonnance">
                                                ordonnance</ref> des Gabelles du mois de mai 1680 porte que le <ref target="#péage"> péage</ref> qui se lève sur chaque
                                          bateau sera pris sur le bateau-maire seulement et non sur
                                          les <ref target="#allèges"> allèges</ref>. <placeName type="lieux_controle">La
                                                Rocheguyon</placeName> également sur les allèges. La
                                          querelle commença dès 1680 et
                                          fut portée par Claude Boutet. Par arrêt du 9 août 1681, le duc de La Rocheguyon
                                          fut maintenu dans son droit de lever son péage également
                                          sur les allèges. Les Fermiers généraux revinrent à la
                                          charge. En 1785, ils furent
                                          dans l’obligation de décharger une partie des <ref target="#sacs"> sacs</ref> du bateau-maire sur de
                                          plus petits bateaux à cause de la baisse des eaux de la
                                        <ref target="#Seine">Seine</ref>. Passant par La Rocheguyon pour remonter vers <placeName>Paris</placeName>,
                                          le conducteur refusa d’obtempérer aux injonctions de la
                                          receveuse du péage de payer pour les sacs des petites
                                          embarcations. La duchesse d’Enville donna raison à son
                                          employée. Les Fermiers furent déboutés dans leurs
                                          prétentions. En revanche, le roi arbitra en 1723 au profit de la Ferme
                                          générale contre le duc de Sully, propriétaire des péages
                                          de <placeName type="lieux_controle">Sully</placeName> et
                                          de <placeName type="lieux_controle">Gien</placeName> sur
                                          la <ref target="#Loire"> Loire</ref>, et contre
                                        Jacques-François de Briançon, grand Prieur de <placeName>Saint-Gondom</placeName>
                                          et propriétaire du péage du même lieu. Chargés à <ref target="#Nantes"> Nantes</ref>, seuls les
                                          bateaux-maires pouvaient être soumis aux droits de péage.
                                          Les sentences du <ref target="#grenier"> grenier</ref> à
                                          sel de Gien furent donc cassées. </def>
                                    <listBibl><bibl><idno type="ArchivalIdentifier">AN, G<hi rend="sup">1</hi> 98,
                                                  Voitures des sels, dossier « Péage de La
                                                  Rocheguyon »</idno>
                                          </bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui casse
                                                deux sentences du grenier à sel de Gien et un
                                                jugement de la table de marbre du palais à Paris, et
                                                ordonne que le droit de salage qui se paie sur la
                                                Loire ne sera levé que sur le bateau mère de chaque
                                              équipe</hi>, 6 juillet 1723</bibl>
                                          <bibl type="sources"><hi rend="italic">Ordonnances sur le fait des gabelles
                                              des aides…</hi>, mai et juin 1680, chapitre XII, article
                                                4 et 5</bibl>
                                    </listBibl></sense>
                        </entry>
                  </body>
            </text>
      </TEI>
      