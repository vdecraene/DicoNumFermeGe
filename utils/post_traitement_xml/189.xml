<?xml version='1.0' encoding='UTF-8'?>
<TEI n="189" xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                  <fileDesc>
                        <titleStmt>
                              <title type="notice"> Ingrandes </title>
                              <author>Marie-Laure Legay</author>
                        </titleStmt>
                        <publicationStmt>
                              <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer
                                    le privilège la Ferme générale dans l'espace français et
                                    européen (1664-1794)Axe 1 : Dictionnaire de la Ferme générale,
                                    objet d'histoire totale </publisher>
                              <pubPlace>
                                    <address>
                                          <addrLine> Maison Européenne des Sciences de l'Homme et de
                                                la Société </addrLine>
                                          <addrLine> 2 rue des cannoniers </addrLine>
                                          <addrLine> 59002 Lille Cedex </addrLine>
                                    </address>
                                    2021-2025
                              </pubPlace>
                              <availability>
                                    <licence>Creative Commons Attribution 3.0 non
                                          transposé (CC BY 3.0)</licence>
                              </availability>
                        </publicationStmt>
                        <seriesStmt>
                              <title> Dictionnaire numérique de la Ferme générale </title>
                              <respStmt>
                                    <resp> Coordinateurs de l'axe : Dictionnaire numérique de la
                                          Ferme générale, objet d'histoire totale. </resp>
                                    <persName> Marie-Laure Legay </persName>
                                    <persName> Thomas Boullu </persName>
                              </respStmt>
                        </seriesStmt>
                        <sourceDesc><bibl type="sources"><hi rend="italic">Carte des confins et limites de la province de
                                                Bretagne, où se voit la ligne ponctuée de séparation
                                                d'entre cette dite province et celles de Poictou,
                                                d'Anjou, le Mayne, petit Maine et Normandie / faicte
                                                par permission du Roy aux frais des Srs. intéressez
                                                pour poster les corps de garde des gabelles et
                                                traittes par Jacques Le Loyer, de la Flèche,
                                                académiste et géographe de Sa Majesté en l'an 1684.
                                                Et depuis reveüe, corrigée et augmentée par
                                                l'autheur à ses frais ès années 1688. Puis mise au
                                                net et dessignée par Luy en l'année 1703 en l'age de
                                              79 ans, Jacques Le Loyer</hi>, 1703</bibl>
                                          <bibl>Jean-Louis Beau, <hi rend="italic">Ingrandes, petit village des bords
                                              de Loire entre Anjou et Bretagne</hi>, Petit Pavé, 2014</bibl>
                                    </sourceDesc></fileDesc>
                  <encodingDesc>
                        <projectDesc source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
                              <p> Le projet FermGé vise à étudier l’impact d’une organisation
                                    fiscale (1664-1794), discriminante mais rationnelle, sur les
                                    territoires et les sociétés de la France moderne/p </p>
                        </projectDesc>
                  </encodingDesc>
                  <profileDesc>
                        <textClass>
                              <keywords scheme="#fr_RAMEAU">
                                    <list>
                                          <item> Histoire -- Histoire moderne -- Histoire
                                                administrative </item>
                                          <item> Histoire -- Histoire moderne -- Histoire fiscale
                                          </item>
                                    </list>
                              </keywords>
                        </textClass>
                  </profileDesc>
                  <revisionDesc>
                        <change type="AutomaticallyEncoded"> 2022-12-19T14:35:28.613687+2:00
                        </change>
                  </revisionDesc>
            </teiHeader>
            <text>
                  <body>
                        <entry n="189" type="I" xml:id="Ingrandes">
                              <form type="lemma">
                                    <orth> Ingrandes </orth>
                              </form>
                              <sense>
                                    <def> Ce bourg situé sur la Loire se trouvait à la jonction de
                     pays aux régimes fiscaux contrastés, la <orgName subtype="province" type="pays_et_provinces">
                                                Bretagne</orgName> et l’<orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Anjou">Anjou</ref></orgName>. Lieu de passage obligé pour nombre de
                                          marchandises entrant dans les <ref target="#fermes">Cinq grosses fermes</ref>, ce site douanier
                                          était la principale porte d’entrée dans le royaume par la
                                          Loire. D’après l'enquête effectuée dans la <orgName subtype="administrations_juridictions_royales" type="administrations_juridictions"> Généralité de
                                                Tours</orgName> en 1766, le bureau des <ref target="#traites"> traites</ref>
                                          d’<placeName>Ingrandes</placeName> rapportait 350 000 livres par an, ce qui en
                                          faisait le plus gros bureau de recettes de l’<orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Anjou">Anjou</ref></orgName>. S’y enregistraient les droits sur les
                                          marchandises, dont les produits coloniaux, qui entraient
                                          par les ports du royaume, puis par <placeName><ref target="#Nantes">
                                                Nantes</ref></placeName>. Durant tout le XVIIIe siècle, ce
                                          port fluvial prospéra en accueillant négociants,
                                          voituriers, aubergistes. Cette prospérité décida les
                                          Walsch, famille d’armateurs nantais originaire d’Irlande,
                                          à acquérir la baronnie d’Ingrandes attachée au château de
                                          Serrant en 1749. Les nouveaux
                                          seigneurs firent construire des halles en dur en 1752. Ingrandes fonctionnait
                                          également en lien avec les salorges de <ref target="#Nantes"> Nantes</ref> qui recevaient le
                                          sel des régions productrices de l’Ouest atlantique destiné
                                          aux pays de Grandes <ref target="#gabelles"> gabelles
                                         </ref>. Le bourg était doté d’un important <ref target="#grenier"> grenier à sel</ref> et d’un
                                          tribunal de <ref target="#gabelle"> gabelle</ref> pour
                                          juger les très nombreux cas de <ref target="#fraude">
                                                fraude</ref> : vers 1770, une subdéléguation de la <ref target="#commission">
                                                commission</ref> extraordinaire de Saumur y fut
                                          établie. Elle jugea 1 497 cas de faux-saunage entre 1772 et 1789
 généraux qui employaient sur place près de 80
                                          employés : un <ref target="#contrôleur"> contrôleur</ref>
                                          général des fermes, un capitaine général, et les employés
                                          de cinq <ref target="#brigades"> brigades</ref> : la <ref target="#brigade"> brigade</ref> du grand corps de
                                          garde composée de deux lieutenants, un sous-lieutenant,
                                          neuf employés; celle du petit corps de garde composée d’un
                                          lieutenant, d’un sous-lieutenant et de dix employés; celle
                                          de <placeName>Saint-Eloy</placeName> comprenant un sous-lieutenant et cinq
                                          employés; celle de <placeName>la Riottière</placeName> avec un lieutenant et
                                          quatre employés; celle de Villeneuve avec un lieutenant et
                                          cinq employés. S’y ajoutaient les employés administratifs
                                          ou judiciaires, soit en tout, avec leurs familles, un
                                          cinquième de la population de ce bourg de 1 500 habitants
                                          environ. Comme à <ref target="#Paris"> Paris</ref>, les
                                          barrières d’Ingrandes tombèrent à l’été
1789 ; <ref target="#pataches"> pataches
                                         </ref>, bacs et bureaux de la ferme générale furent
                                          détruits. </def>
                                    <listBibl><bibl type="sources"><hi rend="italic">Carte des confins et limites de la province de
                                                Bretagne, où se voit la ligne ponctuée de séparation
                                                d'entre cette dite province et celles de Poictou,
                                                d'Anjou, le Mayne, petit Maine et Normandie / faicte
                                                par permission du Roy aux frais des Srs. intéressez
                                                pour poster les corps de garde des gabelles et
                                                traittes par Jacques Le Loyer, de la Flèche,
                                                académiste et géographe de Sa Majesté en l'an 1684.
                                                Et depuis reveüe, corrigée et augmentée par
                                                l'autheur à ses frais ès années 1688. Puis mise au
                                                net et dessignée par Luy en l'année 1703 en l'age de
                                              79 ans, Jacques Le Loyer</hi>, 1703</bibl>
                                          <bibl>Jean-Louis Beau, <hi rend="italic">Ingrandes, petit village des bords
                                              de Loire entre Anjou et Bretagne</hi>, Petit Pavé, 2014</bibl>
                                    </listBibl></sense>
                        </entry>
                  </body>
            </text>
      </TEI>
      