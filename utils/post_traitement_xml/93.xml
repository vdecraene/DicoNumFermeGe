<?xml version='1.0' encoding='UTF-8'?>
<TEI n="93" xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                  <fileDesc>
                        <titleStmt>
                              <title type="notice"> Ferme générale </title>
                              <author>Marie-Laure Legay</author>
                        </titleStmt>
                        <publicationStmt>
                              <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer
                                    le privilège la Ferme générale dans l'espace français et
                                    européen (1664-1794)Axe 1 : Dictionnaire de la Ferme générale,
                                    objet d'histoire totale </publisher>
                              <pubPlace>
                                    <address>
                                          <addrLine> Maison Européenne des Sciences de l'Homme et de
                                                la Société </addrLine>
                                          <addrLine> 2 rue des cannoniers </addrLine>
                                          <addrLine> 59002 Lille Cedex </addrLine>
                                    </address>
                                    2021-2025
                              </pubPlace>
                              <availability>
                                    <licence>Creative Commons Attribution 3.0 non
                                          transposé (CC BY 3.0)</licence>
                              </availability>
                        </publicationStmt>
                        <seriesStmt>
                              <title> Dictionnaire numérique de la Ferme générale </title>
                              <respStmt>
                                    <resp> Coordinateurs de l'axe : Dictionnaire numérique de la
                                          Ferme générale, objet d'histoire totale. </resp>
                                    <persName> Marie-Laure Legay </persName>
                                    <persName> Thomas Boullu </persName>
                              </respStmt>
                        </seriesStmt>
                        <sourceDesc><bibl type="sources"><hi rend="italic">A Monsieur Belin, conseiller du roi
                                                en son Châtelet de Paris, rapporteur du procès.
                                                (Requête de François Remond, sieur de Bréviande,
                                                l’un des intéressés au bail général des fermes unies
                                                sous le nom de Jean Fauconnet, poursuivi comme
                                                complice des détournements commis par Jean Gruslé au
                                              préjudice de la ferme générale)</hi>, février 1686, 16
                                                p</bibl>
                                          <bibl>Jean Clinquart, « Ce que nous ignorons des fermes
                                                générales », <hi rend="italic">Histoire institutionnelle, économique
                                                et financière : questions de méthode (XVIIe-XVIIIe
                                                  siècles)</hi>, Vincennes, CHEFF, IGPDE, 2004, p. 91-10</bibl>
                                    </sourceDesc></fileDesc>
                  <encodingDesc>
                        <projectDesc source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
                              <p> Le projet FermGé vise à étudier l’impact d’une organisation
                                    fiscale (1664-1794), discriminante mais rationnelle, sur les
                                    territoires et les sociétés de la France moderne/p </p>
                        </projectDesc>
                  </encodingDesc>
                  <profileDesc>
                        <textClass>
                              <keywords scheme="#fr_RAMEAU">
                                    <list>
                                          <item> Histoire -- Histoire moderne -- Histoire financière </item>
                                    </list>
                              </keywords>
                        </textClass>
                  </profileDesc>
                  <revisionDesc>
                        <change type="AutomaticallyEncoded"> 2022-12-15T10:22:10.617670+2:00
                        </change>
                  </revisionDesc>
            </teiHeader>
            <text>
                  <body>
                        <entry n="93" type="F" xml:id="Ferme_générale">
                              <form type="lemma">
                                    <orth> Ferme générale </orth>
                              </form>
                              <sense>
                                    <def> « Ferme générale » au singulier est un terme utilisé dans
                     les traités que le roi signait avec un consortium de
                     financiers à qui il confiait la « ferme générale » de tel
                     ou tel impôt. Ainsi, le terme « Ferme générale des
                     gabelles de France » ou  « Ferme générale des aides » est
                     mentionné dès le règne de Louis XIII. Les réunions
                     successives des fermes du roi ont permis de désigner sous
                     l’acception singulière un ensemble de fermes concernant
                     plusieurs types d’impôts. Ainsi, dès
1680, par le bail signé le 27 juin pour six
                                          ans, on emploie le terme de « La ferme générale des
                                          gabelles de France et des évêchés de Metz, Toul et Verdun,
                                          Domaines et salines de <orgName subtype="province" type="pays_et_provinces"> Lorraine</orgName> et du
                                          comté de <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Bourgogne"> Bourgogne</ref></orgName>, aydes de France, entrées de Paris et de
                                          Rouen, droits sur le papier et parchemin timbrez, cinq
                                          grosses fermes, tabac, estain, Douanes de Lyon et Valance
                                          (sic), Convoy et comptablie de Bordeaux, Patente de
                                                <orgName subtype="province" type="pays_et_provinces"> Languedoc</orgName>, et autres fermes et droits
                                          y joins ». Contrairement à ce qu’énonce Jean Clinquart, le
                                          singulier s’emploie donc dans le langage avant la
                                          formation du bail Carlier de 1726
 pour désigner la compagnie financière dont nous
                                          traçons l’histoire. En 1686 par
                                          exemple, François Rémond, intéressé dans le bail
                                          Fauconnet, se défend d’être « complice de divertissement
                                          de deniers de la Ferme générale dont on dit que Jean
                                          Gruslé est coupable ». A chaque bail général correspondait
                                          une « ferme générale ». Le bail général se déclinait en
                                          multiples baux particuliers de fermes et <ref target="#sous-fermes"> sous-fermes</ref>,
                                          « réunies » selon diverses modalités. </def>
                                    <listBibl><bibl type="sources"><hi rend="italic">A Monsieur Belin, conseiller du roi
                                                en son Châtelet de Paris, rapporteur du procès.
                                                (Requête de François Remond, sieur de Bréviande,
                                                l’un des intéressés au bail général des fermes unies
                                                sous le nom de Jean Fauconnet, poursuivi comme
                                                complice des détournements commis par Jean Gruslé au
                                              préjudice de la ferme générale)</hi>, février 1686, 16
                                                p</bibl>
                                          <bibl>Jean Clinquart, « Ce que nous ignorons des fermes
                                                générales », <hi rend="italic">Histoire institutionnelle, économique
                                                et financière : questions de méthode (XVIIe-XVIIIe
                                                  siècles)</hi>, Vincennes, CHEFF, IGPDE, 2004, p. 91-10</bibl>
                                    </listBibl></sense>
                        </entry>
                  </body>
            </text>
      </TEI>
      