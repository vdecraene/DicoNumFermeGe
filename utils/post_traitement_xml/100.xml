<?xml version='1.0' encoding='UTF-8'?>
<TEI n="100" xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                  <fileDesc>
                        <titleStmt>
                              <title type="notice"> Ordonnances </title>
                              <author>Marie-Laure Legay</author>
                        </titleStmt>
                        <publicationStmt>
                              <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer
                                    le privilège la Ferme générale dans l'espace français et
                                    européen (1664-1794)Axe 1 : Dictionnaire de la Ferme générale,
                                    objet d'histoire totale </publisher>
                              <pubPlace>
                                    <address>
                                          <addrLine> Maison Européenne des Sciences de l'Homme et de
                                                la Société </addrLine>
                                          <addrLine> 2 rue des cannoniers </addrLine>
                                          <addrLine> 59002 Lille Cedex </addrLine>
                                    </address>
                                    2021-2025
                              </pubPlace>
                              <availability>
                                    <licence>Creative Commons Attribution 3.0 non
                                          transposé (CC BY 3.0)</licence>
                              </availability>
                        </publicationStmt>
                        <seriesStmt>
                              <title> Dictionnaire numérique de la Ferme générale </title>
                              <respStmt>
                                    <resp> Coordinateurs de l'axe : Dictionnaire numérique de la
                                          Ferme générale, objet d'histoire totale. </resp>
                                    <persName> Marie-Laure Legay </persName>
                                    <persName> Thomas Boullu </persName>
                              </respStmt>
                        </seriesStmt>
                        <sourceDesc><bibl><idno type="ArchivalIdentifier">AD Isère, 2C
                                                  640 : « Observations sur la perception des droits
                                                  d’inspecteurs aux boucheries et sur les plaintes
                                                  de quelques bouchers de l’Election ou plutôt de la
                                                  subdélégation de Vienne », sans date, après 1747</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN, G<hi rend="sup">1</hi> 91, Mémoire
                                                  de 1782</idno>
                                          </bibl>
                                    </sourceDesc></fileDesc>
                  <encodingDesc>
                        <projectDesc source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
                              <p> Le projet FermGé vise à étudier l’impact d’une organisation
                                    fiscale (1664-1794), discriminante mais rationnelle, sur les
                                    territoires et les sociétés de la France moderne/p </p>
                        </projectDesc>
                  </encodingDesc>
                  <profileDesc>
                        <textClass>
                              <keywords scheme="#fr_RAMEAU">
                                    <list>
                                          <item> Histoire -- Histoire moderne -- Histoire
                                                administrative </item>
                                          <item> Histoire -- Histoire moderne -- Histoire politique
                                          </item>
                                    </list>
                              </keywords>
                        </textClass>
                  </profileDesc>
                  <revisionDesc>
                        <change type="AutomaticallyEncoded"> 2022-12-20T09:25:07.893357+2:00
                        </change>
                  </revisionDesc>
            </teiHeader>
            <text>
                  <body>
                        <entry n="100" type="O" xml:id="Ordonnances">
                              <form type="lemma">
                                    <orth> Ordonnances </orth>
                              </form>
                              <sense>
                                    <def> Les lois royales ayant trait aux activités des Fermes
                     générales se sont succédées tout au long du siècle pour
                     s’adapter aux évolutions socio-économiques. Les grandes
                     ordonnances des années 1680 et
                                                1681 en particulier,
                                          auxquelles l’on doit ajouter l’<hi rend="italic">ordonnance sur le fait des
                                            Cinq grosses fermes</hi> de février 1687, ont fait l’objet de multiples révisions par
                                          édits, déclarations, arrêts ou règlements, de sorte qu’il
                                          n’était pas possible, au milieu du XVIIIe siècle d’avoir
                                          une vision d’ensemble de la législation, même pour les
                                          esprits les plus éclairés de l’administration. Par
                                          exemple, la contrebande des femmes en attroupement, non prévue dans les dispositions
                                          de la loi de 1680, fut
                                          discutée lors du projet de déclaration de 1782. Les <hi rend="italic">Ordonnances de Louis
                                          XIV, Roy de France et de <orgName subtype="province" type="pays_et_provinces"> Navarre</orgName> sur le
                                            fait des Gabelles et des Aydes</hi> ont été ordonnées en mai et
                                          juin 1680. L’<hi rend="italic">Ordonnance sur le
                                            fait des Gabelles</hi> comprend vingt titres divisés en 292
                                          articles, et plus de cent pages qui s’attardent sur une
                                          infinité de détails concernant la police du sel, tant
                                          cette administration concernait de nombreuses activités
                                        humaines. L’<hi rend="italic">Ordonnance de Louis XIV sur le fait des
                                          entrées, aydes et autres droits</hi>  date de juin 1680 comprend 27 titres et plus
                                          de 200 pages ; à cette dernière, l’on doit joindre
                                          l’Ordonnance réglant les aides dans le ressort de la <ref target="#Cour"> Cour des aides</ref> de <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Normandie"> Normandie</ref></orgName>, également du mois de juin
1680. L’ordonnance de juillet 1681 visait à compléter celles
                                          de 1680 en publiant « des
                                          règlemens pour la perception des droits de plusieurs
                                          fermes non comprises dans nos Ordonnances des Gabelles,
                                          Aides et Domaines afin que les redevables de nos droits
                                          soient certains de ce qu’ils doivent légitimement payer ».
                                          Cette ordonnance comprenait le règlement pour le commerce
                                          du <ref target="#tabac"> tabac</ref> dont
                                          l’interprétation fut précisée par la déclaration du 18
                                          septembre 1703. L’ordonnance
                                          de Louis XIV sur le fait des Cinq grosses fermes donnée à
                                          Versailles en 1687 reprenait la
                                          législation des droits de <ref target="#traites"> traites
                                         </ref> en 163 articles divisés en quatorze titres. Les
                                          ordonnances ont été enregistrées en la <ref target="#Cour"> Cour des aides</ref>
                                          de Paris ou en la <ref target="#Cour"> Cour des aides</ref> de <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Normandie"> Normandie</ref></orgName>. En revanche, l’absence d’enregistrement dans
                                          les provinces privilégiées limitait la force de ces lois.
                                          En <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Provence"> Provence</ref></orgName>, on s’enorgueillit de l’absence
                                          d’enregistrement de l’ordonnance de
1687 et donc de l’impossibilité d’établir
                                          des <ref target="#limitrophes">lieues limitrophes</ref>.<ref target="#procès-verbal"> procès-verbal</ref>
                                          n’était pas traitée de la même façon dans l’ordonnance sur
                                          les aides de 1680 et dans celle
                                          de 1687 sur le fait des Cinq
                                          grosses <ref target="#fermes"> fermes</ref>. Les juges
                                          du <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Dauphiné"> Dauphiné</ref>
                                         </orgName> firent valoir par exemple que tout <ref target="#procès-verbal"> procès-verbal</ref> qui
                                          n’avait été affirmé dans les trois jours devait être
                                          déclaré nul, se référant non à l’ordonnance sur les <ref target="#aides"> aides</ref> (qui permet un délai
                                          de huit à quinze jours), mais à l’ordonnance des <ref target="#traites"> traites</ref> : « Comme en
                                                <orgName subtype="province" type="pays_et_provinces"> Dauphiné</orgName>, on ne connoît pas les aydes
                                          et qu’on ne veut point les connaitre, on regarde les
                                          inspecteurs aux boucheries à l’instar des Cinq grosses
                                          fermes et on soutient qu’ils doivent être régis par les
                                          mêmes règlements, c’est ce qui fait qu’on ne veut point
                                          leur accorder les délais de quinzaine et de huitaine pour
                                          l’affirmation des procès-verbaux et qu’on les somme au
                                          contraire au delay de 24 heures qu’on étend par grâce
                                          jusqu’à trois jours ». </def>
                                    <listBibl><bibl><idno type="ArchivalIdentifier">AD Isère, 2C
                                                  640 : « Observations sur la perception des droits
                                                  d’inspecteurs aux boucheries et sur les plaintes
                                                  de quelques bouchers de l’Election ou plutôt de la
                                                  subdélégation de Vienne », sans date, après 1747</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN, G<hi rend="sup">1</hi> 91, Mémoire
                                                  de 1782</idno>
                                          </bibl>
                                    </listBibl></sense>
                        </entry>
                  </body>
            </text>
      </TEI>
      