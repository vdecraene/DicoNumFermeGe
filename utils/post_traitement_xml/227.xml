<?xml version='1.0' encoding='UTF-8'?>
<TEI n="227" xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                  <fileDesc>
                        <titleStmt>
                              <title type="notice"> Sous-ferme </title>
                              <author>Marie-Laure Legay</author>
                        </titleStmt>
                        <publicationStmt>
                              <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer
                                    le privilège la Ferme générale dans l'espace français et
                                    européen (1664-1794)Axe 1 : Dictionnaire de la Ferme générale,
                                    objet d'histoire totale </publisher>
                              <pubPlace>
                                    <address>
                                          <addrLine> Maison Européenne des Sciences de l'Homme et de
                                                la Société </addrLine>
                                          <addrLine> 2 rue des cannoniers </addrLine>
                                          <addrLine> 59002 Lille Cedex </addrLine>
                                    </address>
                                    2021-2025
                              </pubPlace>
                              <availability>
                                    <licence>Creative Commons Attribution 3.0 non
                                          transposé (CC BY 3.0)</licence>
                              </availability>
                        </publicationStmt>
                        <seriesStmt>
                              <title> Dictionnaire numérique de la Ferme générale </title>
                              <respStmt>
                                    <resp> Coordinateurs de l'axe : Dictionnaire numérique de la
                                          Ferme générale, objet d'histoire totale. </resp>
                                    <persName> Marie-Laure Legay </persName>
                                    <persName> Thomas Boullu </persName>
                              </respStmt>
                        </seriesStmt>
                        <sourceDesc><bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui
                                                ordonne, attendu la délibération de la Compagnie des
                                                Indes de régir toutes les fermes de Sa Majesté, que
                                                l'arrêt du conseil du 31 août dernier, en ce qui
                                                regarde les publications et adjudications des
                                              sous-fermes, sera nul et non avenu</hi>, 23 septembre
                                                1719</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui ordonne
                                                que les droits de courtiers-jaugeurs des vins et
                                                boissons venant de Bourgogne et autres pays rédimés,
                                                passant par la route de Châtillon pour entrer dans
                                                les pays de gros appartiendront à A. Baillet,
                                                fermier des aides de la généralité de Paris fait défenses à A. de La Fosse, fermier de la
                                                généralité de Châlons, de les faire percevoir sur le
                                              grand chemin</hi>, 13 avril 1728</bibl>
                                          <bibl type="sources">Jean-Louis Lefebvre de Bellande,
                                                <hi rend="italic">Traité général des droits d’aides</hi>, 2 vol., Paris,
                                                chez Pierre Prault, 1760, p. 163-164</bibl>
                                          <bibl>George T. Matthews, <hi rend="italic">The royal general farms in
                                              eighteenth-century France</hi>, New York, Columbia
                                                University Press, 1958</bibl>
                                        <bibl>Thierry Claeys, <hi rend="italic">Les institutions financières de la France au XVIIIe siècle</hi>, t. II, annexes, Paris, éditions SPM, 2011</bibl>
                                    </sourceDesc></fileDesc>
                  <encodingDesc>
                        <projectDesc source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
                              <p> Le projet FermGé vise à étudier l’impact d’une organisation
                                    fiscale (1664-1794), discriminante mais rationnelle, sur les
                                    territoires et les sociétés de la France moderne/p </p>
                        </projectDesc>
                  </encodingDesc>
                  <profileDesc>
                        <textClass>
                              <keywords scheme="#fr_RAMEAU">
                                    <list>
                                          <item> Histoire -- Histoire moderne -- Histoire fiscale </item>
                                          <item> Histoire -- Histoire moderne -- Histoire financière
                                          </item>
                                    </list>
                              </keywords>
                        </textClass>
                  </profileDesc>
                  <revisionDesc>
                        <change type="AutomaticallyEncoded"> 2022-12-20T15:36:09.332695+2:00
                        </change>
                  </revisionDesc>
            </teiHeader>
            <text>
                  <body>
                        <entry n="227" type="S" xml:id="Sous_ferme">
                              <form type="lemma">
                                    <orth> Sous-ferme </orth>
                              </form>
                              <sense>
                                    <def> Les droits attachés à la Ferme générale étaient
                     si nombreux qu’il était nécessaire de procéder par
                     sous-fermes, notamment pour les <ref target="#aides">
                                                aides</ref> et les droits domaniaux. L’<ref target="#adjudicataire"> adjudication</ref> des
                                          sous-fermes étaient suivie par le Conseil qui désignait le
                                          commissaire en charge de l’attribution du bail. Elle se
                                          faisait, comme pour la Ferme générale, au plus offrant et
                                          dernier enchérisseur après trois publications.
                                          L’adjudicataire de la sous-ferme était un prête-nom qui
                                          présentait un état de ses associés et cautions. Le prix
                                          était proportionné à la nature des droits et à la taille
                                          des provinces ou généralités sur lesquelles ils étaient
                                          perçus. Le sous-fermier avait encore la liberté de faire
                                          des arrière-baux par <ref target="#Election"> Election
                                         </ref>, département, ville, seigneurie…etc. L’existence
                                          de tant de sous-fermes posait localement des problèmes
                                          lorsque le ressort de la perception demeurait incertain.
                                          Quand Adrien Delafosse, sous-fermier des <ref target="#aides"> aides</ref> de la <orgName subtype="administrations_juridictions_royales" type="administrations_juridictions"> généralité de
                                                  <orgName subtype="province" type="pays_et_provinces"> Champagne</orgName></orgName>, implanta une baraque à Obtrée pour lever les
                                          droits sur les voitures chargées de <ref target="#vins">
                                                vins</ref> en provenance de <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Bourgogne"> Bourgogne</ref></orgName>, il fut contré par le sous-fermier des <ref target="#aides"> aides</ref> de la <orgName subtype="administrations_juridictions_royales" type="administrations_juridictions"> généralité de
                                                Paris</orgName>, Alexis Baillet, qui fit valoir à
                                          juste titre la limite entre les <ref target="#élections">
                                                élections</ref> de Tonnerre et de Chalons à son
                                          profit. Les sous-fermes furent supprimées lors de la régie
                                          des Fermes générales par la <ref target="#Indes">Compagnie des Indes</ref> (bail Armand
                                          Pillavoine), ainsi qu’à partir du bail Henriet (1756). Le principe de gestion
                                          par régie directe l’emportait alors sur celui de
                                          l’affermage. </def>
                                    <listBibl><bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui
                                                ordonne, attendu la délibération de la Compagnie des
                                                Indes de régir toutes les fermes de Sa Majesté, que
                                                l'arrêt du conseil du 31 août dernier, en ce qui
                                                regarde les publications et adjudications des
                                              sous-fermes, sera nul et non avenu</hi>, 23 septembre
                                                1719</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui ordonne
                                                que les droits de courtiers-jaugeurs des vins et
                                                boissons venant de Bourgogne et autres pays rédimés,
                                                passant par la route de Châtillon pour entrer dans
                                                les pays de gros appartiendront à A. Baillet,
                                                fermier des aides de la généralité de Paris fait défenses à A. de La Fosse, fermier de la
                                                généralité de Châlons, de les faire percevoir sur le
                                              grand chemin</hi>, 13 avril 1728</bibl>
                                          <bibl type="sources">Jean-Louis Lefebvre de Bellande,
                                                <hi rend="italic">Traité général des droits d’aides</hi>, 2 vol., Paris,
                                                chez Pierre Prault, 1760, p. 163-164</bibl>
                                          <bibl>George T. Matthews, <hi rend="italic">The royal general farms in
                                              eighteenth-century France</hi>, New York, Columbia
                                                University Press, 1958</bibl>
                                        <bibl>Thierry Claeys, <hi rend="italic">Les institutions financières de la France au XVIIIe siècle</hi>, t. II, annexes, Paris, éditions SPM, 2011</bibl>
                                    </listBibl></sense>
                        </entry>
                  </body>
            </text>
      </TEI>
      