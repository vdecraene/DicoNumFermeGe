<?xml version='1.0' encoding='UTF-8'?>
<TEI n="30" xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                  <fileDesc>
                        <titleStmt>
                              <title type="notice"> Le Havre </title>
                              <author>Marie-Laure Legay</author>
                        </titleStmt>
                        <publicationStmt>
                              <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer
                                    le privilège la Ferme générale dans l'espace français et
                                    européen (1664-1794)Axe 1 : Dictionnaire de la Ferme générale,
                                    objet d'histoire totale </publisher>
                              <pubPlace>
                                    <address>
                                          <addrLine> Maison Européenne des Sciences de l'Homme et de
                                                la Société </addrLine>
                                          <addrLine> 2 rue des cannoniers </addrLine>
                                          <addrLine> 59002 Lille Cedex </addrLine>
                                    </address>
                                    2021-2025
                              </pubPlace>
                              <availability>
                                    <licence>Creative Commons Attribution 3.0 non
                                          transposé (CC BY 3.0)</licence>
                              </availability>
                        </publicationStmt>
                        <seriesStmt>
                              <title> Dictionnaire numérique de la Ferme générale </title>
                              <respStmt>
                                    <resp> Coordinateurs de l'axe : Dictionnaire numérique de la
                                          Ferme générale, objet d'histoire totale. </resp>
                                    <persName> Marie-Laure Legay </persName>
                                    <persName> Thomas Boullu </persName>
                              </respStmt>
                        </seriesStmt>
                        <sourceDesc><bibl><idno type="ArchivalIdentifier">AN, G<hi rend="sup">1</hi> 73, pièce
                                                  17 ter : Direction des traites de Rouen : tableau
                                                  des receveurs et employés existants dans les
                                                  bureaux des fermes du département de Rouen au 1er
                                                  octobre 1785</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN, H<hi rend="sup">1</hi> 1686, 1775 :
                                                  « Etat des denrées portées en 1775 des colonies
                                                  françaises de l’Amérique dans les ports de la
                                                  Métropole, leur valeur déterminée sur le prix
                                                  commun, produit des droits qu’elles ont payés à
                                                  leur sortie des isles et à leur entrée en France,
                                                  quantités de celles qui ont passé à l’étranger et
                                                  de celles qui ont été consommées dans le Royaume,
                                                  avec les droits de consommation qui ont été
                                                  perçus, argent venu des isles, valeur arbitrée des
                                                  production peu importantes qui ne sont pas
                                                  détaillées dans ce tableau ». <hi rend="italic">Arrêt du Conseil
                                                  d'Etat qui permet l'entrée des drogueries et
                                                        épiceries par le port du Hâvre-de-Grâce</hi>, 6 mars
                                                  1736</idno>
                                          </bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt de la Cour des aides qui juge
                                                que le droit de 35 sols de Brouage est dû sur les
                                                sels enlevés des marais salants de Brouage, pour la
                                                consommation des habitans de la ville du
                                                Havre-de-Grâce nonobstant les privilèges exemptions
                                              dont jouissent lesdits habitans</hi>, 26 janvier 1748</bibl>
                                          <bibl>Pierre Dardel, <hi rend="italic">Navires et marchandises dans les
                                              ports de Rouen et du Havre au XVIIIe siècle</hi>, Paris,
                                                Sevpen, 1963</bibl>
                                          <bibl>André Corvisier (dir.), <hi rend="italic">Histoire du Havre et de
                                              l’estuaire de la Seine</hi>, Toulouse, Privat, 1987</bibl>
                                        <bibl>Eric Saunier, <hi rend="italic">Histoire du Havre</hi>, Paris, Privat, 2017</bibl>
                                    </sourceDesc></fileDesc>
                  <encodingDesc>
                        <projectDesc source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
                              <p> Le projet FermGé vise à étudier l’impact d’une organisation
                                    fiscale (1664-1794), discriminante mais rationnelle, sur les
                                    territoires et les sociétés de la France moderne/p </p>
                        </projectDesc>
                  </encodingDesc>
                  <profileDesc>
                        <textClass>
                              <keywords scheme="#fr_RAMEAU">
                                    <list>
                                          <item> Histoire -- Histoire moderne -- Histoire
                                                commerciale </item>
                                          <item> Histoire -- Histoire moderne -- Histoire fiscale
                                          </item>
                                    </list>
                              </keywords>
                        </textClass>
                  </profileDesc>
                  <revisionDesc>
                        <change type="AutomaticallyEncoded"> 2022-12-19T15:43:01.165079+2:00
                        </change>
                  </revisionDesc>
            </teiHeader>
            <text>
                  <body>
                        <entry n="30" type="L" xml:id="Le_Havre">
                              <form type="lemma">
                                    <orth> Le Havre </orth>
                              </form>
                              <sense>
                                  <def> Le <placeName>port du Havre</placeName> bénéficiait d’une situation remarquable
                     qui en fit un port morutier, mais aussi un port d’entrée
                     des marchandises coloniales et d’autres denrées desservant
                     un vaste arrière-pays normand, picard et au-delà vers
                     l’Ile-de-France et Paris. A la fin de l’Ancien régime, il
                     représentait 16 % du commerce antillais français. La Ferme
                     générale y fit en 1775 une
                                          recette de 1 267 705 livres, argent de France grâce aux
                                          seules denrées d’Amérique. En 1785, le bureau des traites comprenait vingt
                                          personnes : un <ref target="#receveur"> receveur</ref>,
                                          un <ref target="#contrôleur"> contrôleur</ref>, un
                                          inspecteur à la Romaine, un inspecteur du <ref target="#transit"> transit</ref>, six visiteurs,
                                          deux contrôleurs aux entrepôts, deux <ref target="#receveurs"> receveurs</ref> aux
                                          déclarations, un commis aux <ref target="#caution">acquits à caution</ref>, un commis aux
                                          expéditions, un concierge et emballeur et trois autres
                                          emballeurs. Comme port
                                                d’enregistrement des droits, il se trouvait en
                                                concurrence avec <placeName><ref target="#Rouen">Rouen</ref></placeName>. Les deux villes étaient interdépendantes.
                                                Pour la déclaration
                                                  des épiceries et <ref target="#drogueries">
                                                  drogueries</ref> par exemple, le roi tolérait un
                                                  enregistrement au Havre malgré une législation
                                                  claire qui donnait ce privilège à Rouen. Louis XV confirma cette tolérance en 1736, notamment pour les <ref target="#sucres"> sucres</ref> étrangers et ceux
                                          des îles et <ref target="#colonies"> colonies</ref>
                                      françaises.<ref target="#privilèges"> privilèges</ref> du <placeName>Havre-de-Grâce</placeName> furent maintes
                                          fois confirmés depuis le règne de François Ier.
                                                Ils consistaient dans le droit de <ref target="#franc-salé"> franc-salé</ref>, dans la
                                                faculté de prendre à <ref target="#Brouage"> Brouage
                                               </ref> des provisions de sel pour deux ans, tant
                                                pour l’usage domestique que pour les salaisons, sans
                                                payer aucun droit de <ref target="#gabelle"> gabelle
                                               </ref>, et dans le droit donné aux échevins de
                                                procéder à la vente et distribution des sels de
                                                franchise et de salaisons achetés. Cette
                                          consommation représentait pour 1742-1743 562 muids et 7
                                          boisseaux. </def>
                                    <listBibl><bibl><idno type="ArchivalIdentifier">AN, G<hi rend="sup">1</hi> 73, pièce
                                                  17 ter : Direction des traites de Rouen : tableau
                                                  des receveurs et employés existants dans les
                                                  bureaux des fermes du département de Rouen au 1er
                                                  octobre 1785</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN, H<hi rend="sup">1</hi> 1686, 1775 :
                                                  « Etat des denrées portées en 1775 des colonies
                                                  françaises de l’Amérique dans les ports de la
                                                  Métropole, leur valeur déterminée sur le prix
                                                  commun, produit des droits qu’elles ont payés à
                                                  leur sortie des isles et à leur entrée en France,
                                                  quantités de celles qui ont passé à l’étranger et
                                                  de celles qui ont été consommées dans le Royaume,
                                                  avec les droits de consommation qui ont été
                                                  perçus, argent venu des isles, valeur arbitrée des
                                                  production peu importantes qui ne sont pas
                                                  détaillées dans ce tableau ». <hi rend="italic">Arrêt du Conseil
                                                  d'Etat qui permet l'entrée des drogueries et
                                                        épiceries par le port du Hâvre-de-Grâce</hi>, 6 mars
                                                  1736</idno>
                                          </bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt de la Cour des aides qui juge
                                                que le droit de 35 sols de Brouage est dû sur les
                                                sels enlevés des marais salants de Brouage, pour la
                                                consommation des habitans de la ville du
                                                Havre-de-Grâce nonobstant les privilèges exemptions
                                              dont jouissent lesdits habitans</hi>, 26 janvier 1748</bibl>
                                          <bibl>Pierre Dardel, <hi rend="italic">Navires et marchandises dans les
                                              ports de Rouen et du Havre au XVIIIe siècle</hi>, Paris,
                                                Sevpen, 1963</bibl>
                                          <bibl>André Corvisier (dir.), <hi rend="italic">Histoire du Havre et de
                                              l’estuaire de la Seine</hi>, Toulouse, Privat, 1987</bibl>
                                        <bibl>Eric Saunier, <hi rend="italic">Histoire du Havre</hi>, Paris, Privat, 2017</bibl>
                                    </listBibl></sense>
                        </entry>
                  </body>
            </text>
      </TEI>
      