<?xml version='1.0' encoding='UTF-8'?>
<TEI n="117" xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                  <fileDesc>
                        <titleStmt>
                              <title type="notice"> Quatrième (droits de) </title>
                              <author>Marie-Laure Legay</author>
                        </titleStmt>
                        <publicationStmt>
                              <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer
                                    le privilège la Ferme générale dans l'espace français et
                                    européen (1664-1794)Axe 1 : Dictionnaire de la Ferme générale,
                                    objet d'histoire totale </publisher>
                              <pubPlace>
                                    <address>
                                          <addrLine> Maison Européenne des Sciences de l'Homme et de
                                                la Société </addrLine>
                                          <addrLine> 2 rue des cannoniers </addrLine>
                                          <addrLine> 59002 Lille Cedex </addrLine>
                                    </address>
                                    2021-2025
                              </pubPlace>
                              <availability>
                                    <licence>Creative Commons Attribution 3.0 non
                                          transposé (CC BY 3.0)</licence>
                              </availability>
                        </publicationStmt>
                        <seriesStmt>
                              <title> Dictionnaire numérique de la Ferme générale </title>
                              <respStmt>
                                    <resp> Coordinateurs de l'axe : Dictionnaire numérique de la
                                          Ferme générale, objet d'histoire totale. </resp>
                                    <persName> Marie-Laure Legay </persName>
                                    <persName> Thomas Boullu </persName>
                              </respStmt>
                        </seriesStmt>
                        <sourceDesc><bibl><idno type="ArchivalIdentifier">AD Sommes, 1C
                                                  2449</idno>
                                          </bibl>
                                          <bibl type="sources">Jean-Louis Moreau de Beaumont,
                                                <hi rend="italic">Mémoires concernant les droits impositions en
                                                    Europe</hi>, tome 3, Paris, Imprimerie royale, 1769, p.
                                                331</bibl>
                                    </sourceDesc></fileDesc>
                  <encodingDesc>
                        <projectDesc source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
                              <p> Le projet FermGé vise à étudier l’impact d’une organisation
                                    fiscale (1664-1794), discriminante mais rationnelle, sur les
                                    territoires et les sociétés de la France moderne/p </p>
                        </projectDesc>
                  </encodingDesc>
                  <profileDesc>
                        <textClass>
                              <keywords scheme="#fr_RAMEAU">
                                    <list>
                                          <item> Histoire -- Histoire moderne -- Histoire
                                                commerciale </item>
                                          <item> Histoire -- Histoire moderne -- Histoire fiscale
                                          </item>
                                    </list>
                              </keywords>
                        </textClass>
                  </profileDesc>
                  <revisionDesc>
                        <change type="AutomaticallyEncoded"> 2022-12-20T13:38:35.935560+2:00
                        </change>
                  </revisionDesc>
            </teiHeader>
            <text>
                  <body>
                        <entry n="117" type="Q" xml:id="Quatrième_droits_de">
                              <form type="lemma">
                                    <orth> Quatrième (droits de) </orth>
                              </form>
                              <sense>
                                    <def> Il s’agit des pays où se lève un droit d’<ref target="#aides"> aides</ref> dit « quatrième », se
                                          comprenant comme la perception d’un quart du prix de vente
                                          des <ref target="#boissons"> boissons</ref> au détail. En
                                          réalité, l’<ref target="#ordonnance"> ordonnance</ref>
                                          des <ref target="#aides"> aides</ref> accorde la
                                          déduction du cinquième sur le montant des droits, de sorte
                                          que le quatrième se réduit au cinquième sur le <ref target="#vin"> vin</ref>, le cidre ou le poiré.
                                        Ces pays sont situés dans les généralités de <placeName><ref target="#Rouen">Rouen</ref></placeName>, <placeName>Caen</placeName>
                                          et <placeName>Alençon</placeName> qui forment le ressort de la <ref target="#Cour"> Cour des aides</ref> de <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Normandie"> Normandie</ref></orgName>, mais l’on doit y ajouter l’<orgName subtype="administrations_juridictions_royales" type="administrations_juridictions">élection de
                                                Bar-sur-Seine</orgName> et une partie de la
                                                <orgName subtype="administrations_juridictions_royales" type="administrations_juridictions"> généralité d’Amiens</orgName>.<ref target="#Huitième"> Huitième
                                         </ref> faisaient l’objet d’un trafic frauduleux important.
                                          De nombreux <ref target="#contrebandiers"> contrebandiers
                                         </ref> d’<ref target="#eaux-de-vie"> eaux-de-vie</ref>
                                        se firent prendre entre le <orgName>Beauvaisis</orgName> et la <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Picardie"> Picardie</ref></orgName>, comme ces deux inconnus à cheval qui
                                          s’étaient approvisionnés à <placeName>Feuquières</placeName> en Beauvaisis et se
                                          rendaient à <placeName>Hornoy</placeName>. Leurs six barils, d’une valeur estimée
                                          à 160 livres, furent saisis et consignés par les commis
                                          aux exercices du <orgName subtype="administrations_juridictions_fermes" type="administrations_et_juridictions"> département
                                                de Grandvilliers</orgName> le 29 décembre 1763. Ces derniers voulurent
                                          mener les deux fraudeurs en prison mais l’un d’eux prit la
                                          fuite. De tels épisodes se succédaient avec une grande
                                          régularité, au rythme d’une vingtaine par an en moyenne
                                          dans les années 1770 et d’une
                                          trentaine dans les années 1780
                                          dans la <ref target="#direction"> direction</ref> des
                                                <ref target="#aides"> aides</ref> d’Amiens. Il ne
                                          fait pas de doute que la <ref target="#contrebande">
                                                contrebande</ref> d’<ref target="#eaux-de-vie">
                                                eaux-de-vie</ref> constituait un métier dont les
                                          pauvres gens n’entendaient pas se défaire, jugeant qu’il
                                          relevait d’une justice sociale qui leur était due. Les
                                          fraudeurs qui firent la route entre <placeName>Hamel en Beauvaisis</placeName> et
                                          <placeName>Bocquel-Essertaux</placeName> en <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Picardie"> Picardie</ref>
                                         </orgName> en décembre 1769
                                          proclamèrent aux commis qui les appréhendaient que
                                          « chacun faisait son métier et se procurait la subsistance
                                          comme il pouvait » ; ils sortirent leurs bâtons et
                                          frappèrent les commis. </def>
                                    <listBibl><bibl><idno type="ArchivalIdentifier">AD Sommes, 1C
                                                  2449</idno>
                                          </bibl>
                                          <bibl type="sources">Jean-Louis Moreau de Beaumont,
                                                <hi rend="italic">Mémoires concernant les droits impositions en
                                                    Europe</hi>, tome 3, Paris, Imprimerie royale, 1769, p.
                                                331</bibl>
                                    </listBibl></sense>
                        </entry>
                  </body>
            </text>
      </TEI>
      