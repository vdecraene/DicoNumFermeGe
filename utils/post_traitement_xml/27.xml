<?xml version='1.0' encoding='UTF-8'?>
<TEI n="27" xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                  <fileDesc>
                        <titleStmt>
                              <title type="notice">Allemagne</title>
                              <author>Marie-Laure Legay</author>
                        </titleStmt>
                        <publicationStmt>
                              <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer
                                    le privilège la Ferme générale dans l'espace français et
                                    européen (1664-1794)Axe 1 : Dictionnaire de la Ferme générale,
                                    objet d'histoire totale </publisher>
                              <pubPlace>
                                    <address>
                                          <addrLine> Maison Européenne des Sciences de l'Homme et de
                                                la Société </addrLine>
                                          <addrLine> 2 rue des cannoniers </addrLine>
                                          <addrLine> 59002 Lille Cedex </addrLine>
                                    </address>
                                    2021-2025
                              </pubPlace>
                              <availability>
                                    <licence>Creative Commons Attribution 3.0 non
                                          transposé (CC BY 3.0)</licence>
                              </availability>
                        </publicationStmt>
                        <seriesStmt>
                              <title> Dictionnaire numérique de la Ferme générale </title>
                              <respStmt>
                                    <resp> Coordinateurs de l'axe : Dictionnaire numérique de la
                                          Ferme générale, objet d'histoire totale. </resp>
                                    <persName> Marie-Laure Legay </persName>
                                    <persName> Thomas Boullu </persName>
                              </respStmt>
                        </seriesStmt>
                        <sourceDesc><bibl><idno type="ArchivalIdentifier">AD Meurthe et
                                                  Moselle, C 311, Mémoire et lettres relatives au
                                                  projet d’établir un tarif sur la frontière qui
                                                  sépare la Lorraine de l’Allemagne en supprimant la
                                                  foraine</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN, G<hi rend="sup">1</hi> 88, dossier
                                                  19, recette de la deuxième année du bail Mager
                                                  (janvier -décembre 1788)</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN, G<hi rend="sup">1</hi> 131, Mémoire
                                                  sur la Lorraine</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN, G<hi rend="sup">1</hi> 132, dossier
                                                  1 : Projet de régie de la vente étrangère des sels
                                                  de Lorraine</idno>
                                          </bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d'Etat concernant
                                              le commerce des plombs d'Allemagne et du Nord</hi>, 3
                                                mars 1722</bibl>
                                          <bibl type="sources">Georg Friedrich Martens, <hi rend="italic">Recueil des
                                                principaux traités d’alliance, de paix, de trêves,
                                                de neutralité, de commerce, de limites, d’échange,
                                                etc., conclu par les puissances de l’Europe tant
                                                entre elles qu’avec les puissances et Etats dans
                                                d’autres parties du monde depuis 1761 jusqu’à
                                              présent</hi>, Göttingen, 1791 (tomes 1 et 2) et 1795
                                                (tomes 3 et 4)</bibl>
                                          <bibl>Pierre Boyé, <hi rend="italic">Les salines et le sel en Lorraine au
                                              XVIIIe siècle</hi>, Nancy, 1904</bibl>
                                          <bibl>Jean-François Noël,  « Les problèmes de frontières
                                              entre la France et l’Empire », <hi rend="italic">Revue historique</hi>,
                                                t. CCXXXV, 1966, p. 333-346</bibl>
                                          <bibl>Guillaume Garner, « La question douanière dans le
                                                discours économique en Allemagne (seconde moitié du
                                                XVIIIe siècle), <hi rend="italic">Revue Histoire, Economique et
                                                  Société</hi>, n° 23, Les espaces du Saint-Empire à
                                                l’époque moderne, 2004, p. 39-53</bibl>
                                          <bibl>Thierry Claeys, <hi rend="italic">Les institutions financières en
                                              France au XVIIIe siècle</hi>, tome 2, Paris, éditions
                                                SPM, 2011, p. 513-517</bibl>
                                    </sourceDesc></fileDesc>
                  <encodingDesc>
                        <projectDesc source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
                              <p> Le projet FermGé vise à étudier l’impact d’une organisation
                                    fiscale (1664-1794), discriminante mais rationnelle, sur les
                                    territoires et les sociétés de la France moderne/p </p>
                        </projectDesc>
                  </encodingDesc>
                  <profileDesc>
                        <textClass>
                              <keywords scheme="#fr_RAMEAU">
                                    <list>
                                          <item>Histoire -- Histoire moderne -- Histoire
                                                commerciale</item>
                                          <item>Histoire -- Histoire moderne -- Histoire
                                                financière</item>
                                          <item>Histoire -- Histoire moderne -- Histoire
                                                fiscale</item>
                                          <item>Histoire -- Histoire moderne -- Histoire des
                                                relations internationales</item>
                                    </list>
                              </keywords>
                        </textClass>
                  </profileDesc>
                  <revisionDesc>
                        <change type="AutomaticallyEncoded"> 2022-12-12T13:23:25.915768+2:00
                        </change>
                  </revisionDesc>
            </teiHeader>
            <text>
                  <body>
                        <entry n="27" type="A" xml:id="Allemagne">
                              <form type="lemma">
                                    <orth> Allemagne </orth>
                              </form>
                              <sense>
                                    <def> Les Fermiers généraux et leurs cautions se sont intéressés
                     aux relations commerciales entre la France et les états
                     allemands, notamment au commerce du sel et du tabac, mais
                     ont surtout participé aux montages financiers des sociétés
                     fiscales avec lesquelles les princes germaniques
                     contractaient pour alimenter leur Trésor. Régnant sur des
                     étendues de faible importance et souvent morcelées, les
                     souverains de ces états utilisaient le réseau financier de
                     la Ferme générale pour des solutions « clés en
                     main ».<orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Lorraine"> Lorraine</ref>
                                         </orgName> était le plus actif. Entre
1774 et 1777,
                                          d’après Pierre Boyé, 810 000 quintaux de sel passèrent de
                                                <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Lorraine"> Lorraine</ref>
                                         </orgName> en Allemagne. Le produit de la vente
                                                du sel de cette province au seul Palatinat
                                                rapportait 1, 4 million de livres, mais le sel
                                                lorrain approvisionnait aussi le duché des
                                                Deux-Ponts, l’évêché de Spire, le Luxembourg ou
                                                l’électorat de Trèves. La Ferme générale
                                          entretenait des <ref target="#agent"> agents</ref>
                                          diplomatiques pour négocier avec ces Etats. Contrairement
                                          aux cantons <ref target="#suisse"> suisses</ref>, la
                                          vente n’était pas encadrée par des conventions et
                                          demeurait libre. Certes, certains princes établirent un
                                          monopole de la vente sur leur territoire, comme le
                                          margrave de Baden-Baden ou le prince des Deux-Ponts ; ils
                                          traitaient alors directement avec des compagnies
                                          financières pour aller chercher le sel à <placeName><ref target="#Dieuze">Dieuze</ref></placeName> et levaient sur ce
                                          sel des droits d’accise. Mais là où la vente était libre
                                          en revanche, la Ferme générale, gestionnaire des salines
                                          de <ref target="#Dieuze"> Dieuze</ref> et de
                                          Château-Salins, agissait pour rendre ses prix attractifs.
                                          Depuis 1738, le prix du sel du
                                          fermier au marchand était fixé comme pour les gabelles d’<orgName subtype="province" type="pays_et_provinces"><ref target="#Alsace">Alsace</ref>
                                         </orgName> à 10 livres, 16 sous et 8 deniers de France le
                                          quintal, soit 2 sous et 8 deniers la livre. Les marchands,
                                          souvent modestes, lui achetaient à crédit. Elle préférait
                                          donc sous-traiter la vente à forfait à des distributeurs,
                                          comme ce Laurent Wolff qui servit d’intermédiaire dans les
                                          années 1770-1780.<placeName type="lieux_habitations"> ville de Bâle
                                                </placeName> jusqu’à l’<orgName>évêché de Spire</orgName>, soit sur le
                                          Bas-Rhin : le <orgName>marquisat de Durlach (Karlsruhe)</orgName>, le
                                          <orgName>marquisat de Baden-Baden</orgName>, le <orgName>baillage d’Oberkirch</orgName>
                                          dépendant de l’<orgName>évêché de Strasbourg</orgName>, les deux baillages de
                                          <placeName>Willstätt</placeName> et <placeName>Biesheim</placeName>, la maison d’Armstatt ; et sur le
                                          Haut-Rhin : le Brisgau, le <orgName>bas marquisat de Durlach</orgName>, les
                                          villes impériales d’<placeName>Offenbourg</placeName>, <placeName>Harmersbach</placeName>, <placeName>Biberach</placeName>, la
                                                <orgName subtype="pays_fiscaux" type="administrations_et_juridictions">
                                                seigneurie de Lohr</orgName>, <placeName>Pforzheim</placeName>, la vallée
                                        de <placeName>Kenzingen</placeName> et <placeName>Gerolseck</placeName>. Pour ces secteurs, la Ferme
                                          générale sous-traitait la distribution du sel. Cinq
                                          magasins étaient installés sur le territoire « allemand »:
                                          <placeName>Strasbourg</placeName>, <placeName>Kehl</placeName>, <placeName>Emmendingen</placeName>, <placeName>Müllheim</placeName> et <placeName>Bâle</placeName>.
                                          Relevaient également de l’« étranger » les sels vendus dans
                                          les lieux libres enclavés en <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Alsace">Alsace</ref>
                                        </orgName> en deçà du Rhin : les <placeName type="lieux_habitations"> villes de Strasbourg</placeName>, <placeName>Saverne</placeName> et ses dépendances en deçà du Rhin,
                                          la <orgName subtype="pays_fiscaux" type="administrations_et_juridictions">
                                                principauté de Murbach</orgName>, la vallée de
                                        <placeName>Saint-Amarin</placeName>, la <orgName>république de Mulhouse</orgName>, l’abbaye de
                                          <placeName>Neubourg</placeName>, le <placeName>Neuf-Brisach</placeName> et la <placeName type="lieux_habitations"> ville de Landau</placeName>. Pour ces lieux, le sous-traitant de la
                                          Ferme, Laurent Wolff, établit quatre magasins à
                                          Strasbourg, Saverne, Colmar et Buschwiller dans les années
                                                1770. Au-delà vers le
                                          Nord, la compagnie Laurent Wolff vendait dans l’électorat
                                          de Trèves et le <orgName subtype="pays_fiscaux" type="administrations_et_juridictions"> duché de Luxembourg
                                         </orgName> par la voie mosellane, mais elle n’entretenait
                                          pas de magasin particulier : elle vendait à crédit
                                                directement aux marchands de détail qui venaient
                                                s’approvisionner dans l’entrepôt du modeste <palceName>port du
                                            Crosne sur la Meurthe</palceName>, à côté de Nancy, pour les
                                                marchands luxembourgeois et dans celui de
                                        <placeName>Saint-Avold</placeName> pour les marchands de <placeName>Trèves</placeName>.
                                          Les profits tirés de ces deux derniers marchés étaient
                                          faibles car les entrepôts « français » se trouvaient
                                          décentrés par rapport aux axes commerciaux allemands et
                                          les sels de <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Lorraine"> Lorraine</ref>
                                         </orgName> subissaient la concurrence du prix de ceux de
                                          Cologne. Toutefois, comme le sel de <orgName subtype="province" type="pays_et_provinces">
                                                Lorraine</orgName> restait cher, ce sel
                                          d’exportation refluait, faisant l’objet d’une forte <ref target="#contrebande"> contrebande</ref>. Entre
                                          Landau et la <placeName type="lieux_habitations"> ville
                                                d’Haguenau</placeName>, la vente du sel était
                                          régie par d’autres sous-traitants comme Philippe Gobert et
                                          Jacques Durant.<ref target="#Dieuze"> Dieuze</ref>, et d’une
                                          gestion par marché particulier pour une certaine quantité
                                          de sel ; dans ce dernier cas,
                                                elle devait envisager des entrepôts bien placés pour
                                                une meilleure distribution au-delà du Rhin,
                                                notamment dans le duché du Luxembourg et l’électorat
                                                de Trèves. A la fin de l’Ancien régime,
                                          lors de la deuxième année du bail Mager (1788), la vente du sel à
                                          l’étranger rapporta dans la <ref target="#direction">Direction</ref> de Strasbourg :
                                        <figure>
                                            <graphic url="/static/images/allemagne.jpg"/>
                                        </figure><ref target="#Pays-Bas">
                                                Pays-Bas</ref>, en passant en <ref target="#transit"> transit</ref> par le <placeName type="lieux_controle"> bureau de Ruremonde</placeName>. En retour, les négociants importaient du
                                          fer ou des plombs assujettis à un droit de 40 sols le
                                          quintal. Ce droit avantageait les forgerons allemands par
                                          rapport à la fabrication anglaise : les plombs anglais
                                          étaient taxés à 3 livres le quintal.
                                                Toutefois, une <ref target="#fraude"> fraude</ref>
                                                conséquence s’organisait par le biais de la refonte
                                                des plombs anglais dans les forges allemandes, ce
                                                qui fit réagir le Conseil en 1722 : il demanda le contrôle plus strict des
                                                certificats d’origine. Au demeurant,
                                          l’absence de régime douanier entre l’<orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Alsace">Alsace</ref>
                                         </orgName> et la <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Lorraine"> Lorraine</ref></orgName>, provinces à l’instar de <ref target="#effectif">l’étranger effectif</ref>, et les Etats
                                          allemands reportait à l’entrée des <ref target="#fermes">Cinq grosses fermes</ref> les opérations de
                                          contrôle. L’imbrication étroite des territoires dans le
                                          Nord-Est du royaume rendait vaine toute tentative
                                          douanière. Les traités de limites furent établis
                                          tardivement. En suivant les frontières de l'Empire du
                                          Nord-Ouest au Sud-Est, les accords intéressèrent
                                          successivement : les <ref target="#Pays-Bas"> Pays-Bas
                                         </ref> (16 mai 1769 et 18
                                          novembre 1779), l’Evêché de
                                          Liège (24 mai 1772 et 9
                                          décembre 1773), l’électorat de
                                          Trèves (1er juillet 1778), les
                                                <orgName subtype="pays_fiscaux" type="administrations_et_juridictions">
                                                principautés de Nassau-Sarrebruck</orgName> (15
                                          février 1766 et 26 octobre
                                                1770) et de
                                        <orgName>Nassau-Weilbourg</orgName> (24 janvier 1776), le <orgName>comté de la Leyen</orgName> (22 septembre 1781), le <orgName subtype="pays_fiscaux" type="administrations_et_juridictions"> duché de
                                                Deux-Ponts</orgName> (10 mai 1766, 3 avril 1783 et 15
                                          novembre 1786), la <orgName subtype="pays_fiscaux" type="administrations_et_juridictions"> principauté
                                                de Salm</orgName> (21 décembre 1751), le <orgName subtype="pays_fiscaux" type="administrations_et_juridictions"> comté de Montbéliard-Würtemberg</orgName> (21 mai 1786) et enfin l’Evêché de Bâle (20 juin 1780). A ces divers traités,
                                          il convient d’ajouter celui du 7 décembre 1779 intéressant le ressort
                                          ecclésiastique des évêchés de Bâle et de Besançon, ces
                                          sièges échangeant leurs enclaves respectives situées en
                                                <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Alsace">Alsace</ref>
                                         </orgName> et dans le Bâlois.<ref target="#tabac"> tabac
                                               </ref> fabriqué à <placeName>Mannheim</placeName> (Palatinat) fut confié en
                                          1752 à une société fortement
                                          liée à la Ferme générale. On y trouvait des cadres comme
                                          Pierre Joseph de la Rivière de Montreuil, <ref target="#receveur"> receveur</ref> général des <ref target="#tabacs"> tabacs</ref> à Paris, ou François
                                          de la Fontaine, <ref target="#directeur"> directeur des aides</ref>. Dans la formation
                                          de la <ref target="#Prusse">régie des accises de Prusse
                                         </ref>, on retrouve parmi les cautions des hommes comme
                                          Claude Genty, <ref target="#directeur"> directeur</ref>
                                          de la Ferme générale, Nicolas Geuffrin, <ref target="#directeur"> directeur des aides</ref> au <orgName subtype="administrations_juridictions_fermes" type="administrations_et_juridictions"> département
                                                d’Argenteuil</orgName>, Etienne Jacques Rouillé de
                                          Marigny, receveur des gabelles à Sancerre, ou
                                          Jean Etienne Grison de la Ville-aux-Clercs, receveur des fermes
                                          également. De même, les financiers intéressés dans les
                                          salines de <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Lorraine"> Lorraine</ref>
                                         </orgName> prirent part, sous le nom de Nicolas Leclerc
                                          résidant à <ref target="#Dieuze"> Dieuze</ref>, dans la
                                          ferme générale à laquelle le prince de <ref target="#Nassau"> Nassau-Saarbrück</ref> bailla
                                          ses <ref target="#gabelles"> gabelles</ref>, banvin,
                                                <ref target="#aides"> aides</ref>, droits
                                          domaniaux et seigneuriaux, forges… tant pour sa
                                          principauté que pour les <orgName subtype="pays_fiscaux" type="administrations_et_juridictions"> comtés d’Ottweiler
                                         </orgName> et de <placeName>Saverne</placeName> (1776). Charles II, duc des Deux-Ponts, confia à bail ses
                                          fermes des <ref target="#gabelles"> gabelles</ref>, <ref target="#tabac"> tabac</ref>, droits de douane, aides dans l’étendue du
                                                <orgName subtype="pays_fiscaux" type="administrations_et_juridictions">
                                                duché de Garetenberg</orgName> au même réseau (1777). </def>
                                    <listBibl><bibl><idno type="ArchivalIdentifier">AD Meurthe et
                                                  Moselle, C 311, Mémoire et lettres relatives au
                                                  projet d’établir un tarif sur la frontière qui
                                                  sépare la Lorraine de l’Allemagne en supprimant la
                                                  foraine</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN, G<hi rend="sup">1</hi> 88, dossier
                                                  19, recette de la deuxième année du bail Mager
                                                  (janvier -décembre 1788)</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN, G<hi rend="sup">1</hi> 131, Mémoire
                                                  sur la Lorraine</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN, G<hi rend="sup">1</hi> 132, dossier
                                                  1 : Projet de régie de la vente étrangère des sels
                                                  de Lorraine</idno>
                                          </bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d'Etat concernant
                                              le commerce des plombs d'Allemagne et du Nord</hi>, 3
                                                mars 1722</bibl>
                                          <bibl type="sources">Georg Friedrich Martens, <hi rend="italic">Recueil des
                                                principaux traités d’alliance, de paix, de trêves,
                                                de neutralité, de commerce, de limites, d’échange,
                                                etc., conclu par les puissances de l’Europe tant
                                                entre elles qu’avec les puissances et Etats dans
                                                d’autres parties du monde depuis 1761 jusqu’à
                                              présent</hi>, Göttingen, 1791 (tomes 1 et 2) et 1795
                                                (tomes 3 et 4)</bibl>
                                          <bibl>Pierre Boyé, <hi rend="italic">Les salines et le sel en Lorraine au
                                              XVIIIe siècle</hi>, Nancy, 1904</bibl>
                                          <bibl>Jean-François Noël,  « Les problèmes de frontières
                                              entre la France et l’Empire », <hi rend="italic">Revue historique</hi>,
                                                t. CCXXXV, 1966, p. 333-346</bibl>
                                          <bibl>Guillaume Garner, « La question douanière dans le
                                                discours économique en Allemagne (seconde moitié du
                                                XVIIIe siècle), <hi rend="italic">Revue Histoire, Economique et
                                                  Société</hi>, n° 23, Les espaces du Saint-Empire à
                                                l’époque moderne, 2004, p. 39-53</bibl>
                                          <bibl>Thierry Claeys, <hi rend="italic">Les institutions financières en
                                              France au XVIIIe siècle</hi>, tome 2, Paris, éditions
                                                SPM, 2011, p. 513-517</bibl>
                                    </listBibl></sense>
                        </entry>
                  </body>
            </text>
      </TEI>
      