<?xml version='1.0' encoding='UTF-8'?>
<TEI n="98" xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                  <fileDesc>
                        <titleStmt>
                              <title type="notice"> Dépôt (magasin général de sel) </title>
                              <author>Marie-Laure Legay</author>
                        </titleStmt>
                        <publicationStmt>
                              <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer
                                    le privilège la Ferme générale dans l'espace français et
                                    européen (1664-1794)Axe 1 : Dictionnaire de la Ferme générale,
                                    objet d'histoire totale </publisher>
                              <pubPlace>
                                    <address>
                                          <addrLine> Maison Européenne des Sciences de l'Homme et de
                                                la Société </addrLine>
                                          <addrLine> 2 rue des cannoniers </addrLine>
                                          <addrLine> 59002 Lille Cedex </addrLine>
                                    </address>
                                    2021-2025
                              </pubPlace>
                              <availability>
                                    <licence>Creative Commons Attribution 3.0 non
                                          transposé (CC BY 3.0)</licence>
                              </availability>
                        </publicationStmt>
                        <seriesStmt>
                              <title> Dictionnaire numérique de la Ferme générale </title>
                              <respStmt>
                                    <resp> Coordinateurs de l'axe : Dictionnaire numérique de la
                                          Ferme générale, objet d'histoire totale. </resp>
                                    <persName> Marie-Laure Legay </persName>
                                    <persName> Thomas Boullu </persName>
                              </respStmt>
                        </seriesStmt>
                        <sourceDesc><bibl><idno type="ArchivalIdentifier">AN, G<hi rend="sup">1</hi> 97, dossier 5 : Traité de l’entreprise de la
                                                  voiture des sels pendant le bail Mager
                                                  (1787-1793), Fournissement pour la 2e année
                                                  (1788)</idno>
                                          </bibl>
                                        <bibl type="sources"><hi rend="italic">Encyclopédie méthodique, Finances</hi>,
                                                1784, article « Dépôt », p. 493, et 1785,
                                                « Fournissement », p. 263-265</bibl>
                                    </sourceDesc></fileDesc>
                  <encodingDesc>
                        <projectDesc source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
                              <p> Le projet FermGé vise à étudier l’impact d’une organisation
                                    fiscale (1664-1794), discriminante mais rationnelle, sur les
                                    territoires et les sociétés de la France moderne/p </p>
                        </projectDesc>
                  </encodingDesc>
                  <profileDesc>
                        <textClass>
                              <keywords scheme="#fr_RAMEAU">
                                    <list>
                                          <item> Histoire -- Histoire moderne -- Histoire fiscale
                                          </item>
                                    </list>
                              </keywords>
                        </textClass>
                  </profileDesc>
                  <revisionDesc>
                        <change type="AutomaticallyEncoded"> 2022-12-14T10:47:50.844638+2:00
                        </change>
                  </revisionDesc>
            </teiHeader>
            <text>
                  <body>
                        <entry n="98" type="D" xml:id="Dépôt_magasin_général_de_sel">
                              <form type="lemma">
                                    <orth> Dépot (magasin général de sel) </orth>
                              </form>
                              <sense>
                                    <def>
                                        Magasin du roi où est entreposé le sel des gabelles avant d’être acheminé vers les <ref taget="#greniers">greniers</ref>. Ces dépôts sont établis aux embouchures des rivières de la Loire (<ref target="#Nantes">Nantes</ref>), Seine (Honfleur et Rouen), Orne (Caen), et de la Somme (Saint-Valéry-sur-Somme). Leur situation facilitait la distribution du sel dans les pays de <ref target="#grandes_gabelles">grandes gabelles</ref> : le dépôt de Nantes approvisionnait les <ref target="#greniers">greniers</ref> de l’<ref target="#Anjou">Anjou</ref>, du Maine, de la <ref target="#Touraine">Touraine</ref>, du <ref target="#Berry">Berry</ref>, du Bourbonnais et Nivernais, de l’Orléanais et une partie de ceux de <ref target="#Bourgogne">Bourgogne</ref> par la Loire, puis par la Mayenne, la Sarthe, la Vienne, le Cher et l’Allier. Au total, 86 <ref target="#greniers">greniers</ref> pour 3 800 muids de sel distribués en 1788. Le dépôt de Caen approvisionnait les greniers de la Basse-Normandie, soit 13 en tout, pour 1 100 muids. Le dépôt de Saint-Valéry fournissait la <ref target="#Picardie">Picardie</ref>, soit 18 <ref target="#greniers">greniers</ref> pour 1 490 muids. Sur la Seine, le dépôt d’Honfleur n’alimentait directement que 6 <ref target="#greniers">greniers</ref> pour 900 muids car ils servaient de réserve à celui de Rouen, ou plus exactement <ref target="#Dieppedalle">Dieppedalle</ref> situé à deux lieues de la ville. C’est ce dernier dépôt qui approvisionnait la centaine de <ref target="#greniers">greniers</ref> d’Ile-de-France, de <ref target="#Champagne">Champagne</ref>, de <ref target="#Bourgogne">Bourgogne</ref>, du Soissonnais, par la <ref target="#Seine">Seine</ref>, l’Oise et l’Aisne, la Marne et l’Yonne. En 1788, le nombre de muids distribués par Dieppedalle atteignit 8940. En fin, le dépôt de Digoin, en Bourgogne, formait un important centre de redistribution du sel de l’Atlantique. Il desservait 44 greniers pour 2010 muids. Les marchés de <ref target="#voiture">voitures</ref> de sel s’établissaient par dépôt pour des sommes importantes. Il ne faut pas confondre ces dépôts ou magasins avec les dépôts régis par arrondissement situés dans les paroisses limitrophes des pays de <ref target="#grandes_gabelles">grandes gabelles</ref>.
                                    </def>
                                    <listBibl><bibl><idno type="ArchivalIdentifier">AN, G<hi rend="sup">1</hi> 97, dossier 5 : Traité de l’entreprise de la
                                                  voiture des sels pendant le bail Mager
                                                  (1787-1793), Fournissement pour la 2e année
                                                  (1788)</idno>
                                          </bibl>
                                        <bibl type="sources"><hi rend="italic">Encyclopédie méthodique, Finances</hi>,
                                                1784, article « Dépôt », p. 493, et 1785,
                                                « Fournissement », p. 263-265</bibl>
                                    </listBibl></sense>
                        </entry>
                  </body>
            </text>
      </TEI>
      