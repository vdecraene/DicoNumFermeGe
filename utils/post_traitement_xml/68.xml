<?xml version='1.0' encoding='UTF-8'?>
<TEI n="68" xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                  <fileDesc>
                        <titleStmt>
                              <title type="notice">Grenier à sel</title>
                              <author>Marie-Laure Legay</author>
                        </titleStmt>
                        <publicationStmt>
                              <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer
                                    le privilège la Ferme générale dans l'espace français et
                                    européen (1664-1794)Axe 1 : Dictionnaire de la Ferme générale,
                                    objet d'histoire totale </publisher>
                              <pubPlace>
                                    <address>
                                          <addrLine> Maison Européenne des Sciences de l'Homme et de
                                                la Société </addrLine>
                                          <addrLine> 2 rue des cannoniers </addrLine>
                                          <addrLine> 59002 Lille Cedex </addrLine>
                                    </address>
                                    2021-2025
                              </pubPlace>
                              <availability>
                                    <licence>Creative Commons Attribution 3.0 non
                                          transposé (CC BY 3.0)</licence>
                              </availability>
                        </publicationStmt>
                        <seriesStmt>
                              <title> Dictionnaire numérique de la Ferme générale </title>
                              <respStmt>
                                    <resp> Coordinateurs de l'axe : Dictionnaire numérique de la
                                          Ferme générale, objet d'histoire totale. </resp>
                                    <persName> Marie-Laure Legay </persName>
                                    <persName> Thomas Boullu </persName>
                              </respStmt>
                        </seriesStmt>
                        <sourceDesc><bibl><idno type="ArchivalIdentifier">AN, 745AP (fonds
                                                  Turgot), 50, dossier 4</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN, G<hi rend="sup">1</hi> 23, f° 8, f°
                                                  15</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN, G<hi rend="sup">1</hi> 91, dossier
                                                  22 : « Généralité d’Alençon »</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AD
                                                  Bouches-du-Rhône, C 2105, mémoires sur le sel en
                                                  Provence</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AD Calvados, C
                                                  5992, état de répartition du sel par paroisse
                                                  relevantes du grenier à sel de Caen, 1788</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AD Rhône, 1C 248,
                                                  affaire Buinand, juin 1714</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AD Rhône, 6C/23,
                                                  lettre de Mongirod, procureur du roi aux gabelles
                                                  du Lyonnais, à M. Jacques Verchère, juge visiteur,
                                                  4 mai 1735</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AD Rhône, 6C/28,
                                                  requête de François Ganault au juge Visiteur
                                                  général des gabelles, Jacques Verchère, 1719</idno>
                                          </bibl>
                                          <bibl type="sources"><hi rend="italic">Édit du Roy portant établissement
                                                d’un grenier à sel dans le bourg de Danestal, un
                                                dans celui de Neufbourg et un troisième dans celui
                                                de Livarot, et règlement pour l'arrondissement des
                                                ressorts des autres greniers à sel de la direction
                                              de Rouen, Fontainebleau</hi>, octobre 1725</bibl>
                                          <bibl type="sources">Buterne, <hi rend="italic">Dictionnaire de
                                                législation, de jurisprudence et de finances sur
                                              toutes les fermes unies de France</hi>, Avignon, 1763, p.
                                                148 et 191</bibl>
                                          <bibl type="sources">Edme-François Darigrand E.-F.,
                                                <hi rend="italic">L’anti-financier, ou relevé de quelques-unes des
                                                malversations dont se rendent journellement
                                                coupables les Fermiers-Généraux et des vexations
                                                    qu’ils commettent dans les Provinces…</hi>, Paris,
                                                Amsterdam, 1763, La Haye, 1764, p. 57</bibl>
                                          <bibl type="sources">Jean-Louis Moreau de Beaumont,
                                                <hi rend="italic">Mémoires concernant les droits impositions en
                                                    Europe</hi>, tome 3, Paris, Imprimerie royale, 1769, p.
                                                179-194</bibl>
                                        <bibl>Guy Cabourdin, « Gabelle et démographie en France au XVIIe siècle », dans <hi rend="italic">Annales de démographie historique</hi>, 1969. Villes et villages de l'ancienne France, p. 293-314</bibl>
                                          <bibl>Jeanine Recurat, Emmanuel Le Roy Ladurie, « Sur les
                                                fluctuations de la consommation taxée du sel dans la
                                                France du Nord aux XVIIe et XVIIIe siècles », <hi rend="italic">Revue
                                                  du Nord</hi>, t. 54, n°215, 1972, p. 385-398</bibl>
                                          <bibl>Philippe Béchu, « Les officiers du grenier à sel
                                                d'Angers sous l’Ancien Régime », <hi rend="italic">Annales de Bretagne
                                                  et des pays de l'Ouest</hi>, t. 84, 1977, p. 61-74</bibl>
                                    </sourceDesc></fileDesc>
                  <encodingDesc>
                        <projectDesc source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
                              <p> Le projet FermGé vise à étudier l’impact d’une organisation
                                    fiscale (1664-1794), discriminante mais rationnelle, sur les
                                    territoires et les sociétés de la France moderne/p </p>
                        </projectDesc>
                  </encodingDesc>
                  <profileDesc>
                        <textClass>
                              <keywords scheme="#fr_RAMEAU">
                                    <list>
                                          <item> Histoire -- Histoire moderne -- Histoire
                                                administrative </item>
                                          <item> Histoire -- Histoire moderne -- Histoire fiscale </item>
                                          <item> Histoire -- Histoire moderne -- Histoire politique
                                          </item>
                                    </list>
                              </keywords>
                        </textClass>
                  </profileDesc>
                  <revisionDesc>
                        <change type="AutomaticallyEncoded"> 2022-12-15T11:41:30.038529+2:00
                        </change>
                  </revisionDesc>
            </teiHeader>
            <text>
                  <body>
                        <entry n="68" type="G" xml:id="Grenier_à_sel">
                              <form type="lemma">
                                    <orth>Grenier à sel</orth>
                              </form>
                              <sense>
                                    <def> Le grenier à sel était le magasin où la <ref target="#masse"> masse</ref> de sel était
                                          entreposée pour être ensuite débitée, ensacquée et
                                          distribuée aux habitants de son ressort. Après une
                                          tentative malheureuse de faire porter l’impôt sur les
                                          marais salants, François Ier réorganisa les greniers en
                                                1544 dans tout le
                                          royaume, sauf en <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Bretagne"> Bretagne</ref></orgName>, <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Dauphiné"> Dauphiné</ref></orgName>, <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Provence"> Provence</ref>
                                         </orgName> et <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Languedoc"> Languedoc</ref></orgName>. On trouvait à leur tête plusieurs officiers :
                                          un président, un contrôleur, un procureur, un grenetier ou
                                          receveur de grenier, un greffier. Le grenier formait
                                          également un tribunal compétent (ce qui le distinguait du
                                          simple entrepôt nommé « chambre à sel ») pour juger les
                                          causes de gabelle jusqu’à la valeur d’un quart de <ref target="#minot"> minot</ref>, puis jusqu’à un <ref target="#minot"> minot</ref> et au-delà avec appel
                                          à la <ref target="#cour_des_aides">cour des aides</ref>. Les
                                          employés des Fermes générales complétaient le personnel,
                                          notamment pour les opérations d’enregistrement de vente du
                                          sel: le <ref target="#trilleur"> trilleur</ref> ou radeur
                                          de sel mesurait le sel à la <ref target="#trémie"> trémie
                                         </ref> et tenait le <ref target="#registre"> registre
                                         </ref> de <ref target="#délivrance"> délivrance</ref>.
                                          La délimitation des ressorts des greniers subit plusieurs
                                          évolutions, notamment dans les années
1720 au cours desquelles la monarchie
                                          augmenta le nombre de greniers en <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Normandie"> Normandie</ref></orgName>, <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Anjou">Anjou</ref></orgName>, <ref target="#Touraine"> Touraine</ref> et
                                                <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Picardie"> Picardie</ref></orgName>, puis tout au long du XVIIIe siècle. C’était
                                          là un moyen de mieux contrôler le transport du sel. A la
                                          fin de l’Ancien régime, on comptait près de 250 greniers
                                          dans les pays de <ref target="#grandes_gabelles">Grandes gabelles
                                         </ref> et 147 dans les pays de <ref target="#petites_gabelles">petites gabelles</ref>.<emph>Le prix du sel
                                          variait d’un grenier à l’autre.</emph> Ce prix se composait de
                                          plusieurs éléments : du prix primitif fixé par l’<ref target="#ordonnance"> ordonnance</ref> de 1680 (30 à 43 livres le <ref target="#minot"> minot</ref> selon l’éloignement
                                          des marais salants) pour les provinces de <ref target="#grandes_gabelles">grandes gabelles</ref> (en pays de
                                          <ref target="#petites_gabelles">petites gabelles</ref>, le prix
                                          était fixé par le Conseil du roi), des sols pour <ref target="#livre"> livre</ref> qui s’y appliquaient,
                                          de droits manuels rétablis par arrêt du 20 mars 1722 (2 livres et onze sols
                                          pour le <orgName subtype="administrations_juridictions_royales" type="administrations_juridictions"> grenier de
                                                Paris</orgName>, 2 livres et deux sols dans les
                                          autres greniers pour le sel de vente volontaire ; 2 livres
                                          et un sous dans les greniers d’impôt). Ainsi, treize prix
                                          différents existaient en pays de <ref target="#grandes_gabelles">grandes gabelles</ref>. De même en
                                          pays de <ref target="#Bouillon">Quart Bouillon</ref>,
                                          les prix variaient : au grenier à sel de <placeName>Caen</placeName> par exemple,
                                          le receveur attendait que chaque <ref target="#minot">
                                                minot</ref> lui rapportât 60 livres 2 sols trois
                                          deniers au début de l’année 1788, soit le prix de 38 livres le minot, avec les 41
                                          sols six deniers pour les droits manuels, les dix sols
                                          pour livre du principal des dits droits manuels. En pays
                                          de <ref target="#petites_gabelles">petites gabelles</ref>, le
                                          prix du sel variait d’autant plus que des dépenses
                                          publiques particulières nécessitaient d’augmenter le prix
                                          local. Par exemple en <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Dauphiné"> Dauphiné</ref></orgName>, le prix se situait entre 20 livres et 4 sous
                                          et 23 livres et 10 sous le minot selon l’éloignement du
                                          grenier par rapport au lieu d’approvisionnement, mais il
                                          était augmenté des droits manuels (25 sous 6 deniers par
                                          minot selon l’arrêt du 25 avril 1722), de deux sous pour livre (arrêt du 4 juin 1715), de 5 sous par minot
                                          pour la réparation du canal de Launes, de 6 deniers par
                                          minot pour l’entretien des collèges jésuites de <placeName>Vienne</placeName> et
                                          <placeName>Grenoble</placeName>. Le prix au <orgName subtype="administrations_juridictions_royales" type="administrations_juridictions"> grenier
                                                  d’Orange</orgName> était en outre augmenté de
                                                neuf sous par <ref target="#minot"> minot</ref>
                                                pour le règlement de dettes anciennes.
                                          Précisons en outre que les sols pour <ref target="#livre">
                                                livre</ref> étaient à la discrétion du roi: les
                                          quatre sous supplémentaires créés en
1771 par Terray firent réagir les procureurs
                                          de <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Provence"> Provence</ref>
                                         </orgName> par exemple, et Louis XV leur concéda en 1772 une exemption moyennant
                                          une simple indemnité.<emph>Le fonctionnement d’un grenier</emph>
                                          différait selon qu’il se trouvait en pays de <ref target="#grandes_gabelles">grandes gabelles</ref>, en pays de
                                          <ref target="#petites_gabelles">petites gabelles</ref>. Dans le
                                          premier cas, des <ref target="#sextés">registres sextés
                                         </ref> étaient dressés pour enrôler les habitants et
                                          contrôler leur consommation. Dans les pays de <ref target="#petites_gabelles">petites gabelles</ref>, les contraintes
                                          sur les habitants étaient tout à fait différentes : la
                                          vente se faisait au prix marchand, sans obligation de
                                          quantité. Les habitants ressortissants aux greniers
                                          n’étaient pas enrôlés dans des registres fiscaux, sauf
                                          dans les <ref target="#limitrophes">lieues limitrophes
                                         </ref> qui confinaient aux pays de grandes gabelles. Pour
                                          éviter les fraudes entre provinces de régime fiscal
                                          différent, des <ref target="#dépôts"> dépôts</ref> furent
                                          établis aux confins des terroirs. Les marchands avaient la
                                          liberté d’acheter le sel aux greniers et de revendre sans
                                          commission de <ref target="#regrattier"> regrattier</ref>. En pays de <ref target="#grandes_gabelles">grandes gabelles
                                         </ref>, la population devait acheter au moins un <ref target="#minot"> minot</ref> de sel pour quatorze
                                          personnes d’une même famille âgées de plus de huit ans. La
                                          ration d’un minot pour 14 personnes n’est pas attestée. On
                                          peut s’en rendre compte en rapprochant le dénombrement des
                                          feux et les ventes des greniers par généralité. Voici le
                                          résultat pour 1768 :

                                        <figure>
                                            <graphic url="/static/images/grenier_a_sel.jpg"/>
                                            <head/>
                                        </figure><ref target="#minot"> minot</ref> pour 15 personnes et
                                          un quart et que les habitants ressortissants aux greniers
                                          de la <orgName subtype="administrations_juridictions_royales" type="administrations_juridictions"> généralité
                                                d’Alençon</orgName> se trouvaient nettement moins
                                          taxés. Les fermiers avaient d’ailleurs augmenté
                                          successivement en 1761, 1762 et 1763
 de six muids supplémentaires l’impôt des trois
                                          greniers de la généralité (<placeName>Alençon</placeName>, <placeName>Carrouges</placeName> et <placeName>Falaise</placeName>)
                                          qui jusque-là n’étaient que de 60 muids. L’<ref target="#intendant"> intendant</ref> de la
                                          province, Antoine Jullien, sollicité par de multiples
                                          requêtes des contribuables « épuisés », fit valoir en
                                                1768 la nécessité de
                                          réduire le niveau de l’imposition, mais les Fermiers
                                          alléguèrent que la généralité était l’une des moins taxée,
                                          que l’impôt y était à raison d’un minot par an pour 23
                                          personnes, que les rôles d’imposition faisaient voir une
                                          augmentation certaine du nombre de feux. Jullien répondit
                                          qu’à défaut de diminution, il se bornerait à répartir
                                          l’imposition le plus exactement possible. On fera
                                          remarquer ici le rôle déterminant de l’<ref target="#intendant"> intendant</ref> dans la
                                          répartition arithmétique du sel. On distinguait les
                                          greniers de vente volontaire et les greniers d’impôt. Les
                                          premiers étaient désignés « de vente volontaire » car les
                                          contribuables venaient chercher leur sel du devoir à la
                                          date qui leur convenait. Les greniers d’impôt étaient
                                          placés proches des salines ou des frontières. La
                                          réglementation les concernant était plus stricte : dans
                                          leur ressort, chaque communauté élisait un collecteur du
                                          sel, différent du collecteur des tailles, qui, chaque
                                          trimestre, devait prendre au grenier la totalité du sel
                                          imposé à sa communauté et le rapporter dans la journée. Il
                                          procédait ensuite à la distribution aux chefs de famille.
                                          Seuls les sels de salaison échappaient à une répartition
                                          arbitraire. Cette différence de régie avait un impact sur
                                          la consommation de sel, comme le montrèrent Jeanine
                                          Recurat et Emmanuel Le Roy Ladurie : dans le ressort des
                                          greniers de vente volontaire, les habitants pouvaient
                                          restreindre leur consommation pus facilement. En pays de
                                          <ref target="#petites_gabelles">petites gabelles</ref>, dès
                                          lors que les habitants n’étaient pas enrôlés,
                                          s’établissait entre les greniers une sorte de concurrence
                                          pour consommer la <ref target="#masse"> masse</ref> de
                                          sel. En 1719, le receveur du
                                                <orgName subtype="administrations_juridictions_royales" type="administrations_juridictions"> grenier de
                                                Saint-Symphorien</orgName>, François Ganault, se
                                          plaignit de son homologue du <orgName subtype="administrations_juridictions_royales" type="administrations_juridictions"> grenier de
                                                Saint-Etienne</orgName> qui incitait les paroisses
                                          du <ref target="#Lyonnais"> Lyonnais</ref> proches à se
                                          fournir à son grenier. Lorsque fut créé le <orgName subtype="administrations_juridictions_royales" type="administrations_juridictions"> grenier de La
                                           Mure</orgName>, dans le <orgName subtype="province" type="pays_et_provinces"> Dauphiné</orgName> (1759), le receveur de ce
                                          nouvel établissement, le sieur Dupivot, n’eut de cesse
                                          d’obtenir la suppression de celui de Sisteron- <orgName subtype="province" type="pays_et_provinces">
                                                Dauphiné</orgName>. Les officiers avaient pour
                                          objectif en effet d’obtenir la gratification accordée
                                          quand les ventes atteignaient le rendement fixé par le
                                                <ref target="#déchet"> déchet</ref>.<emph>La
                                                distribution des petites mesures</emph> de sel aux <ref target="#regrats"> regrats</ref> était organisée
                                                sous le contrôle des officiers du grenier qui en
                                                fixaient le prix. Selon l’<ref target="#ordonnance"> ordonnance</ref> de 1680, le public devait être
                                          présent lors des opérations de conditionnement, mais tous
                                          les receveurs ne respectaient pas cette disposition. Dans
                                          le <ref target="#Lyonnais"> Lyonnais</ref>, Buinand,
                                                <ref target="#receveur"> receveur</ref> des
                                                <orgName subtype="administrations_juridictions_royales" type="administrations_juridictions"> greniers de
                                                Charlieu</orgName> et <placeName>La Clayette</placeName>, distribuait le
                                          sel n’importe comment : « il ouvre et ferme et distribue
                                          le sel quand il veut au peuple et ne fait pas la mesure
                                          juste et nécessaire, la fesant comme bon lui semble dans
                                          des demys minots et des quarts en son particulier et non
                                          en présence du peuple et fait la distribution dans des
                                          bennes où il les a mises et entreposées, telles qu’il les
                                          a voulu mesurer parce qu’il n’y a aucun officier audits
                                          greniers », constata le fermier des gabelles en 1714. Le rapport incertain
                                          entre mesure et prix fut très contesté au XVIIIe siècle,
                                          pour son incidence évidente sur les plus pauvres. Encore
                                          en 1735, le procureur du roi
                                          aux gabelles du <ref target="#Lyonnais"> Lyonnais</ref>
                                          dénonçait les approximations : les <ref target="#receveurs"> receveurs</ref> « se servent
                                          indifféremment de leurs valets ou de personnes inconnues
                                          pour faire mesurer le sel, lesquels n’ont point prêté le
                                          serment en tel cas requis ; qu’en fin, le public ne trouve
                                          plus depuis quelque temps la même mesure de sel qu’il
                                          prend dans les différents greniers, ce qui ne peut
                                          provenir que de l’altération des minots que les receveurs
                                          négligent de faire echantiller et ce qui peut aussi donner
                                          occasion aux revendeurs de frauder les pauvres pour se
                                          dédommager de la perte qu’ils peuvent souffrir sur le sel
                                          qu’ils lèvent audits greniers ». <ref target="#Darigrand">
                                                Darigrand</ref>, dans son <hi rend="italic">Anti-financier</hi>, dénonça
                                          de même les malversations des grenetiers lors de
                                          l’utilisation de la <ref target="#trémie"> trémie</ref>.
                                        <emph>La suppression des greniers:</emph> Au-delà, le pamphlétaire
                                          défendit la suppression des greniers, à tout le moins,
                                          leur réunion aux <ref target="#élections">élections
                                         </ref> et juges de <ref target="#traites"> traites</ref>
                                          : « Ma plume se refuse à nommer ces tribunaux cimentés par
                                          le sang, les chambres ardentes ! Quelle surcharge immense
                                          pour l’Etat que cette quantité presqu’innombrable de
                                          juridictions ». En réalité, il véhiculait une idée
                                          ancienne. En 1685, la fusion
                                          des tribunaux de première instance compétents en matière
                                          d’impositions indirectes fut testée, et finalement
                                          abandonnée à cause de la guerre.
                                                Turgot reprit le projet, appelant de ses voeux la
                                                réduction du nombre de juridictions et l’attribution
                                                de l’administration du sel aux <ref target="#intendants"> intendants</ref>. Circulait dans la population la rumeur selon
                                          laquelle les officiers des greniers, juges des <ref target="#saisies"> saisies</ref>, abusaient de
                                          leur autorité. Finalement, la réforme Lamoignon vint à
                                          bout de la juridiction des greniers à sel (8 mai 1788). </def>
                                    <listBibl><bibl><idno type="ArchivalIdentifier">AN, 745AP (fonds
                                                  Turgot), 50, dossier 4</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN, G<hi rend="sup">1</hi> 23, f° 8, f°
                                                  15</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN, G<hi rend="sup">1</hi> 91, dossier
                                                  22 : « Généralité d’Alençon »</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AD
                                                  Bouches-du-Rhône, C 2105, mémoires sur le sel en
                                                  Provence</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AD Calvados, C
                                                  5992, état de répartition du sel par paroisse
                                                  relevantes du grenier à sel de Caen, 1788</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AD Rhône, 1C 248,
                                                  affaire Buinand, juin 1714</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AD Rhône, 6C/23,
                                                  lettre de Mongirod, procureur du roi aux gabelles
                                                  du Lyonnais, à M. Jacques Verchère, juge visiteur,
                                                  4 mai 1735</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AD Rhône, 6C/28,
                                                  requête de François Ganault au juge Visiteur
                                                  général des gabelles, Jacques Verchère, 1719</idno>
                                          </bibl>
                                          <bibl type="sources"><hi rend="italic">Édit du Roy portant établissement
                                                d’un grenier à sel dans le bourg de Danestal, un
                                                dans celui de Neufbourg et un troisième dans celui
                                                de Livarot, et règlement pour l'arrondissement des
                                                ressorts des autres greniers à sel de la direction
                                              de Rouen, Fontainebleau</hi>, octobre 1725</bibl>
                                          <bibl type="sources">Buterne, <hi rend="italic">Dictionnaire de
                                                législation, de jurisprudence et de finances sur
                                              toutes les fermes unies de France</hi>, Avignon, 1763, p.
                                                148 et 191</bibl>
                                          <bibl type="sources">Edme-François Darigrand E.-F.,
                                                <hi rend="italic">L’anti-financier, ou relevé de quelques-unes des
                                                malversations dont se rendent journellement
                                                coupables les Fermiers-Généraux et des vexations
                                                    qu’ils commettent dans les Provinces…</hi>, Paris,
                                                Amsterdam, 1763, La Haye, 1764, p. 57</bibl>
                                          <bibl type="sources">Jean-Louis Moreau de Beaumont,
                                                <hi rend="italic">Mémoires concernant les droits impositions en
                                                    Europe</hi>, tome 3, Paris, Imprimerie royale, 1769, p.
                                                179-194</bibl>
                                        <bibl>Guy Cabourdin, « Gabelle et démographie en France au XVIIe siècle », dans <hi rend="italic">Annales de démographie historique</hi>, 1969. Villes et villages de l'ancienne France, p. 293-314</bibl>
                                          <bibl>Jeanine Recurat, Emmanuel Le Roy Ladurie, « Sur les
                                                fluctuations de la consommation taxée du sel dans la
                                                France du Nord aux XVIIe et XVIIIe siècles », <hi rend="italic">Revue
                                                  du Nord</hi>, t. 54, n°215, 1972, p. 385-398</bibl>
                                          <bibl>Philippe Béchu, « Les officiers du grenier à sel
                                                d'Angers sous l’Ancien Régime », <hi rend="italic">Annales de Bretagne
                                                  et des pays de l'Ouest</hi>, t. 84, 1977, p. 61-74</bibl>
                                    </listBibl></sense>
                        </entry>
                  </body>
            </text>
      </TEI>
      