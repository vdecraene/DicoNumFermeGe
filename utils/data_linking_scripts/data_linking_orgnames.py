import requests
import csv


# Fonction pour interroger l'API GeoNames
def get_geonames_id(org_names):
    username = 'fermege'  # Remplacez par votre nom d'utilisateur GeoNames
    base_url = 'http://api.geonames.org/searchJSON'
    params = {
        'q': org_names,
        'maxRows': 1,
        'username': username
    }
    try:
        response = requests.get(base_url, params=params)
        if response.status_code == 200:
            data = response.json()
            if 'geonames' in data and len(data['geonames']) > 0:
                return data['geonames'][0]['geonameId']
    except requests.exceptions.RequestException as e:
        print(f"Exception occurred while fetching data from GeoNames: {e}")
    return None


# Fonction pour interroger l'API Wikidata
def get_wikidata_id(org_name):
    base_url = 'https://www.wikidata.org/w/api.php'
    params = {
        'action': 'wbsearchentities',
        'format': 'json',
        'language': 'fr',  # Remplacez par la langue souhaitée si nécessaire
        'search': org_name,
        'type': 'item',
    }
    try:
        response = requests.get(base_url, params=params)
        if response.status_code == 200:
            data = response.json()
            if data['search']:
                return data['search'][0]['id']
    except requests.exceptions.RequestException as e:
        print(f"Exception occurred while fetching data from Wikidata: {e}")
    return None


# Fonction pour lire le fichier CSV, interroger les API et ajouter les colonnes geonames_id et wikidata_id
def add_ids_to_csv(input_file, output_file):
    with open(input_file, 'r') as csv_file:
        reader = csv.DictReader(csv_file)
        fieldnames = reader.fieldnames + ['geonames_id',
                                          'wikidata_id']  # Ajout de 'geonames_id' et 'wikidata_id' à la liste des champs

        with open(output_file, 'w', newline='') as output_csv:
            writer = csv.DictWriter(output_csv, fieldnames=fieldnames)
            writer.writeheader()

            for row in reader:
                org_name = row['org']
                try:
                    geonames_id = get_geonames_id(org_name)  # Obtenir l'ID GeoNames
                    wikidata_id = get_wikidata_id(org_name)  # Obtenir l'ID Wikidata

                    row['geonames_id'] = geonames_id if geonames_id else ''
                    row['wikidata_id'] = wikidata_id if wikidata_id else ''

                    writer.writerow(row)
                except Exception as e:
                    print(f"An error occurred while processing row: {e}")


# Utilisation de la fonction pour ajouter les colonnes geonames_id et wikidata_id
input_csv_file = 'orgnames.csv'  # Remplacez par le nom de votre fichier CSV d'entrée
output_csv_file = 'orgnames_output.csv'  # Nom du fichier CSV de sortie avec les colonnes ajoutées

add_ids_to_csv(input_csv_file, output_csv_file)
