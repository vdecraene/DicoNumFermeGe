import xml.etree.ElementTree as ET

tree = ET.parse("/DicoNumFermeGe/app/static/xml/corpus_v2.xml")
root = tree.getroot()
for elem in root.findall(".//orgName"):
    if elem.attrib.get("subtype") == "pays" and elem.attrib.get("type") == "pays_et_provinces":
        elem.attrib["subtype"] = "pays_fiscaux"
        elem.attrib["type"] = "administrations_et_juridictions"


for elem in root.findall(".//bibl"):
    text = elem.text
    if text is not None and "[" in text:
        text = text.replace('[', '').strip()
        elem.text = text

for elem in root.findall(".//bibl"):
    text = elem.text
    if text is not None and "]" in text:
        text = text.replace(']', '').strip()
        elem.text = text

for elem in root.findall(".//bibl"):
    text = elem.text
    if text is not None and ";" in text:
        text = text.replace(';', '').strip()
        elem.text = text

for elem in root.findall(".//idno"):
    text = elem.text
    if text is not None and "[" in text:
        text = text.replace('[', '').strip()
        elem.text = text


for elem in root.findall(".//idno"):
    text = elem.text
    if text is not None and "]" in text:
        text = text.replace(']', '').strip()
        elem.text = text

for elem in root.findall(".//idno"):
    text = elem.text
    if text is not None and ";" in text:
        text = text.replace(';', '').strip()
        elem.text = text

for elem in root.findall(".//def"):
    text = elem.text
    if text is not None and "  " in text:
        text = text.replace('  ', ' ')
        elem.text = text

for elem in root.findall(".//def"):
    text = elem.text
    if text is not None:
        text = text.replace(" ,", ",")
        elem.text = text

for elem in root.findall(".//def"):
    text = elem.text
    if text is not None:
        text = text.replace("( ", "(")
        elem.text = text




tree.write("test_clean.xml", encoding="UTF-8", xml_declaration=True)
