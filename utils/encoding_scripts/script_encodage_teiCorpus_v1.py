# I] Installation de l'environnement et des dépendances.

import glob
import shutil
from bs4 import BeautifulSoup
import re
import os
import argparse
from datetime import datetime


# ===============================================================================================================================================================================================


# II] Définition des fonctions d'encodage.

# Fonction de création du <teiHeader> du <teiCorpus> à partir d'une chaîne de caractères.
# La date de création est inséré automatiquement grâce au module datetime.
def build_teiCorpus_header():
    return "".join(["<teiHeader>",
                    "<fileDesc>",
                    "<titleStmt>",
                    f"<title type='corpus'>Dictionnaire numérique de la Ferme générale</title>",
                    "<editor>ANR FermeGé : Administrer le privilège: La Ferme générale dans l'espace"
                    " français et européen (1664-1794)</editor>",
                    "<funder>Agence Nationale de la Recherche, programme 2021-2025</funder>",
                    "<sponsor>GIP Mission de recherche Droit et Justice</sponsor>",
                    "<sponsor>MESHS de Lille</sponsor>",
                    "<sponsor>UMR 8529 IRHIS</sponsor>",
                    "<sponsor>UMR 7354 DRES</sponsor>",
                    "<sponsor>UR 3911 CEPRISCA (université d’Amiens)</sponsor>",
                    "<sponsor>UR 3436 CRESAT (université de Haute Alsace)</sponsor>",
                    "<sponsor> UR 4417 CHAD (université de Nanterre)</sponsor>",
                    "<principal>Madame<persName> Marie-LaureLegay</persName>(Professeur, UMR IRHIS) </principal>",
                    "<principal>Monsieur <persName>Thomas Boullu</persName>(Maître de conférence, CREPRISCA) </principal>",
                    "<respStmt>",
                    "<resp> Coordinateurs de l'axe : Dictionnaire numérique de la Ferme générale, objet d'histoire totale. </resp>",
                    "<persName>Marie-Laure Legay, Orcid: 0000-0002-2818-2681</persName>",
                    "<persName>Thomas Boullu, Orcid:</persName>",
                    "</respStmt>",
                    "<respStmt>",
                    "<resp> Historien/historienne, rédacteur/rédactrice scientifique de la notice. </resp>",
                    "<persName>Nom de l'auteur de la notice (si connu)</persName>",
                    "</respStmt>",
                    "<respStmt>",
                    "<resp> Ingénieure chargée du traitement des données scientifiques </resp>",
                    "<persName>Victoria Le Fourner, Orcid: 0000-0002-8053-0356 </persName>",
                    "</respStmt>",
                    "<respStmt>",
                    "<resp> Ingénieur stagiaire, responsable du traitement automatique.</resp>",
                    "<persName> Valentin De Craene Orcid: 0000-0002-6019-2572</persName>",
                    "</respStmt>",
                    "</titleStmt>",
                    "<editionStmt>",
                    "<edition>Première édition électronique, <date>2022</date></edition>",
                    "</editionStmt>",
                    "<extent>",
                    "<measure unit='MiB' quantity='88'>11 Mo dans sa forme initiale en document.txt, soit 88 Mb</measure>",
                    "</extent>"
                    "<publicationStmt>",
                    "<publisher>MESHS de Lille dans le cadre de l'ANR FermeGé Administrer le privilège"
                    " la Ferme générale dans l'espace français et européen (1664-1794)"
                    "Axe 1 : Dictionnaire de la Ferme générale, objet d'histoire totale"
                    "</publisher>",
                    "<pubPlace><address><addrLine>Maison Européenne des Sciences de l'Homme et de la Société</addrLine>"
                    "<addrLine> 2 rue des cannoniers </addrLine>"
                    "<addrLine> 59002 Lille Cedex</addrLine>"
                    "</address>",
                    "<date>2021-2025</date>"
                    "</pubPlace>",
                    "<availability>",
                    "<licence>[A déterminer. Ex:Creative Commons Attribution 3.0 non transposé (CC BY 3.0)]</licence>",
                    "</availability>",
                    "</publicationStmt>",
                    "<seriesStmt>",
                    "<title>Dictionnaire numérique de la Ferme générale</title>",
                    "<respStmt>",
                    "<resp> Coordinateurs de l'axe : Dictionnaire numérique de la Ferme générale, objet d'histoire totale. </resp>",
                    "<persName> Marie-Laure Legay</persName>",
                    "<persName> Thomas Boullu</persName>",
                    "</respStmt>",
                    "</seriesStmt>",
                    "<sourceDesc>",
                    "<p> Dictionnaire numérique de la Ferme générale</p> </sourceDesc>",
                    "</fileDesc>",
                    "<encodingDesc>",
                    "<projectDesc source='https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3'>",
                    "<p> Le projet FermGé vise à étudier l’impact d’une organisation fiscale (1664-1794), "
                    "discriminante mais rationnelle, sur les territoires et les sociétés de la France moderne. Il cerne les dynamiques de fonctionnement d’une institution ancrée dans "
                    "culture du privilège et donc de l’inégalité, mais tout autant dans une culture administrative éclairée visant l’efficacité."
                    " Véritable « Etat dans l’Etat », omniprésente sur des territoires très différenciés pour collecter près de 50 % des revenus ordinaires de la monarchie, "
                    "dotée de moyens exceptionnels de coercition, mais capable de transactions, la FG a localement renforcé ou affaibli"
                    " le sentiment d’injustice à l’interface avec les sociétés plurielles sur lesquelles elle agit. La confrontation entre une logique gestionnaire éclairée"
                    " par une science et un droit administratifs nouveaux d’une part, et des identités géographiques"
                    " et sociales plurielles généra des réactions qui se déclinèrent en pratiques et discours pluri-sémantiques sur l’inégalité,"
                    " allant jusqu’à la radicalisation violente, mais également en pratiques de conciliation caractéristiques de l’arbitrage administratif "
                    "et gestionnaire. Le projet s’appuie sur une collaboration de quatre laboratoires Histoire/Géographie/Histoire du droit en prenant acte des renouvellements "
                    "heuristiques de chaque discipline. Il vise à restituer des connaissances inédites sur "
                    "cette organisation fiscale et des analyses sur l’interface inégalitaire "
                    "Impôt/territoires/sociétés qui ne se limitent pas au paradigme d’une organisation"
                    "purement coercitive. En effet, l’impôt constitue dès l’époque moderne un outil de "
                    "réduction des inégalités grâce à la rationalité administrative, fonction reprise par"
                    "l’Etat contemporain. Au-delà, l’originalité du projet réside encore dans le"
                    "questionnement d’un binôme notionnel «inégalité/rationalité » que nous élaborons à"
                    "partir d’un modèle de gestion de l’inégalité qui contribua à l’émergence du droit "
                    "administratif français et a été exporté à l’étranger. Nous émettons l’hypothèse que"
                    " ce binôme est opérationnel pour étudier tout type d’organisation fiscale agissant "
                    "globalement sur un territoire. Le projet s’organise autour de 3 axes : 1 — Dictionnaire. Il a pour vocation scientifique de renouveler"
                    " l’historiographie et d’étudier l’apport de la régie des Fermes à la qualification d’un régime inégalitaire;"
                    " il vise l’exhaustivité géographique, gageure que le projet relève en sollicitant les spécialistes de plusieurs régions de France"
                    " et de l’Europe. 2 – Atlas. Il analyse la présence physique de la Ferme sur le territoire par province, mais aussi par"
                    "bassin fluvial et par frontières. Il respecte l’esprit du projet qui souhaite,"
                    "au-delà des données institutionnelles et judiciaires connues, établir des cartes qui"
                    " représentent l’encadrement du privilège. 3 — Une histoire exploratoire et "
                    "transdisciplinaire du binôme notionnel Inégalité/rationalité. L’axe 3 confronte les"
                    " disciplines pour interroger l’administration de l’inégalité spatiale et sociale au"
                    "siècle des Lumières. Un séminaire d’interprétation sera mis en œuvre. L’objectif est"
                    "de croiser les approches pour comprendre ce qui se joue quand la rationalité administrative"
                    " agit sur les inégalités instituées. Partant de l’historiographie classique sur la contrebande,"
                    " le projet passe par l’analyse de la transaction en matière fiscale et de la contrainte du contrôle"
                    " pour apprécier les enjeux du mécontentement. Le premier colloque international sur le sujet (« Administrer le"
                    "privilège : la FG dans l’espace français et européen») est prévu en deux phases :"
                    "l’une approfondira les travaux du séminaire; l’autre extrapolera le modèle de gestion"
                    "du privilège en ouvrant la réflexion à d’autres organisations fiscales historiques."
                    " 4– Un ancrage dans les principes de la science ouverte. Les données et productions"
                    "seront exposées selon les principes de de gestion de données FAIR. Le site du projet "
                    "sera articulé aux plateformes permettant de lier les données.</p>",
                    "</projectDesc>",
                    "<appInfo>",
                    "<application ident='OxygenXMLEditor' version='24.1'>",
                    "<label> Oxygen XML Editor 24.1</label>",
                    "</application>",
                    "</appInfo>",
                    "</encodingDesc>",
                    "<profileDesc>",
                    "<abstract>",
                    "<p>De 'Acquit-à-caution' à Wurtemberg, le Dictionnaire numérique de la Ferme générale"
                    "développe trois approches: - réunir en une seule oeuvre et en un seul site les"
                    " monographies locales et études éparses réalisées sur la Ferme générale. - renouveler"
                    "l'historiographie grâce aux approches inédites en cours. - proposer une exhaustivité géographique"
                    " à travers les entrées de noms de lieux et villes.</p>",
                    "</abstract>",
                    "<creation>",
                    "<date>2022</date>",
                    "</creation>",
                    "<langUsage>",
                    "<language ident='fr-FR'>Français</language>",
                    "</langUsage>",
                    "<textClass>",
                    "<keywords scheme='#fr_RAMEAU'>",
                    "<list>",
                    "<item>Histoire -- 17ème siècle -- Histoire des institutions</item>"
                    "<item>Histoire -- 18ème siècle -- Histoire des institutions</item>"
                    "<item>Histoire -- Dictionnaires</item>"
                    "<item>Histoire -- Sources</item>"
                    "<item>Histoire -- 17e siècle -- Histoire des doctrines</item>"
                    "<item>Histoire -- 18e siècle -- Histoire des doctrines</item>"
                    "<item>Histoire -- Historiographie</item>"
                    "<item>Histoire -- Histoire religieuse</item>"
                    "<item>Histoire -- 17ème siècle -- Histoire économique</item>"
                    "<item>Histoire -- 18ème siècle -- Histoire économique</item>"
                    "<item>Histoire -- Histoire sociale</item>"
                    "<item>Histoire -- Histoire du droit</item>"
                    "<item>Histoire -- Histoire judiciaire</item>"
                    "<item>Histoire -- Histoire financière</item>"
                    "<item>Histoire -- Impôt -- Administration et procédure</item>"
                    "<item>Histoire -- Contestations</item>"
                    "<item>Histoire -- Histoire locale</item>",
                    "</list>",
                    "</keywords>",
                    "</textClass>",
                    "</profileDesc>",
                    "<revisionDesc>",
                    f'<change type="AutomaticallyEncoded">{str(datetime.now()).replace(" ", "T") + "+2:00"}</change>',
                    "</revisionDesc>", "</teiHeader>"])

# Fonction permettant d'ajouter un attribut @n dans les balises <TEI>.
# La valeur de cet attribut @n est ensuite incrémenté à chaque itération afin de numéroter les notices.
def add_n_counters_to_tei(complete_tree):
    i = 1
    for tag in complete_tree.find_all("TEI"):
        tag.attrs["n"] = i
        i += 1
    return complete_tree

# Fonction permettant de supprimer l'espace de noms TEI au sein des balises <TEI> afin de l'intégrer par la suite dans la balise <teiCorpus>
def del_namespace(complete_tree):
    for tag in complete_tree.find_all("TEI"):
        del tag['xmlns']
    return complete_tree

# Fonction permettant d'ajouter un attribut @n dans les balises <entry>.
# La valeur de cet attribut @n est ensuite incrémenté à chaque itération afin de numéroter les entrées de chaque notice.
def add_n_counters_to_entry(complete_tree):
    i = 1
    for tag in complete_tree.find_all("entry"):
        tag.attrs["n"] = i
        i += 1
    return complete_tree


# ===============================================================================================================================================================================================


# III] Fonction principale d'insertion de l'ensemble des arbres dans le conteneur <teiCorpus> général et parsage.
# On ouvre l'ensemble des notices dans le répertoire concerné (au format registre-portatif.xml).
# Ces fichiers sont "fusionnés" dans un seul fichier temporaires (le "corpus intermédiaire").
def build_final_tree():
    filename = glob.glob('output/*registre-portatif.xml')
    outfilename = 'corpus_intermediaire_v2.xml'

    with open(outfilename, 'wb') as outfile:
        for filename in glob.glob('output/*registre-portatif.xml'):
            if filename == outfilename:
                # don't want to copy the output into the output
                continue
            with open(filename, 'rb') as readfile:
                shutil.copyfileobj(readfile, outfile)

# Dans ce corpus intermédiaire on ajoute après le préambule <?xml> la balise <teiCorpus> avec l'espace de noms tei, puis on ré-insère le reste de ce fichier temporaire.
# Ensuite, ce nouveau fichier est parsé
# On supprime les préambules <?xml> non voulus se trouvant avant chaque balise <TEI> (issues des fichiers fusionnés).
# On ajoute ensuite le chemin vers l'ODD du corpus et on ajoute un nouveau préambule <?xml>
    with open('corpus_intermediaire_v2.xml', 'r', encoding='utf-8') as file:
        file = file.read()
        file = file[:38] + "<teiCorpus xmlns='http://www.tei-c.org/ns/1.0' version='1.0'>" + file[39:] + '</teiCorpus>'
        file = BeautifulSoup(file, 'xml')
        file = str(file).replace('<?xml version="1.0" encoding="utf-8"?>', '').replace('<?xml version="1.0" encoding="utf-8"?> ','').replace('<?xml version="1.0" encoding="utf-8"?>','')
        file = str(file).replace('<?xml-model href="file:/F:/Stage%20MESHS%20DE%20CRAENE%20Valentin/TEI%20XSLT/out/ODD_v2.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?><?xml-model href="file:/F:/Stage%20MESHS%20DE%20CRAENE%20Valentin/TEI%20XSLT/out/ODD_v2.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>', '')
        file = '<?xml version="1.0" encoding="utf-8"?>' + file
# Dans un troisième on parse ce nouveau fichier puis on appelle les fonctions requises pour nettoyer et restructurer le <teiCorpus> (en insérant le <teiHeader>).
        complete_tree = BeautifulSoup(file, 'xml')
        complete_tree = add_n_counters_to_tei(complete_tree)
        complete_tree = add_n_counters_to_entry(complete_tree)
        complete_tree = del_namespace(complete_tree)
        header = build_teiCorpus_header()
        header = BeautifulSoup(header, 'xml')
        complete_tree.teiCorpus.insert(0, header)
# On enregistre le résultat du traitement dans un fichier registre-portatif.xml final nommé "corpus.xml"
        with open('corpus_v2.xml', "w", encoding="utf8") as fh:
            fh.write(str(complete_tree))
        # print(complete_tree.prettify())

# On exécute la fonction de création de l'arbre général
build_final_tree()