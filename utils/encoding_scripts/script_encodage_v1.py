# I] Installation de l'environnement et des dépendances :

from bs4 import BeautifulSoup
import re
import os
import argparse
from datetime import datetime

# Définition des constantes pour mémoire. Utilisation des regex avec re.compile(r'') pour les réutiliser par la suite.

PATTERN_BODY = re.compile(r"([^[a-zA-Z0-9-ÀÉàéè_'\r\\(\\) ]])|[a-zA-ZÀ-ÿ-. \d\(\)\s*,;:«»\[\]’œ…&°”“'_?!\–]+")
PATTERN_ENTRY_NOTICE = re.compile(r"(?s)((?:[^\n][\n]?))+")
PATTERN_TITRE_NOTICE = re.compile(r"^([a-zA-Z0-9-ÀÉàéèâ_\'’\\(\\) ,]+\n)")
PATTERN_ISOLATED_WHITESPACES = re.compile(r"^[a-zA-Z0-9-ÀÉàéè_'\r\\(\\) ]\n+$")
PATTERN_DEFINITION_NOTICE_V2 = re.compile(r"^(?!\[.*|[a-zA-Z0-9-ÀÉàéâè_'\r\\(\\) ]+$).*")
PATTERN_REFERENCES_SCIENTIFIQUES = re.compile(r"(\[.*)")
PATTERN_SOURCES_IDNO = re.compile(r"AM.*?;|AN.*?;|An.*?;|AD.*?;|BNF.*?;|(?<!,)( G.*?;)")
PATTERN_SOURCES = re.compile(r" Arrêt.*?;| Dictionnaire.*?.*;| Lettre.*;| Ordonnance.*?;| Carte.*?;| Vue.*?;| Edit.*?;| Déclaration.*?;")
PATTERN_BIBLIO = re.compile(r";\s[A-Z]{1}[a-z\s]+\s{1}[A-Z]{1}[a-zéàê\s]+,\s{1}[A-Z|«](\s|[a-z]).*")
PATTERN_DATES = re.compile(r"\d{4}(-\d\d(-\d\d)\d?)?")
PATTERN_PERSNAME_PLACENAME_ORGNAME = re.compile(r"([ÀÉA-Z][a-z\-àéèê\']*)")
PATTERN_FIGURE = re.compile(r"^\n.*\n$")
PATTERN_LINE_BREAK = re.compile(r"\r\n|\n+")
PATTERN_REF = re.compile(r"[A-Za-zéàèêâç0-9\'\-]+\*")
PATTERN_PROVINCES = re.compile(r"(Bretagne|Franche-Comté|Normandie|Provence|Artois|Lorraine|Dauphiné|Picardie|Languedoc|Anjou|Auvergne|Roussillon|Poitou|Champagne|Hainaut|Comtat-Venaissin|Cambrésis|Bourgogne|Alsace|Navarre|Trois-? ?Evêchés|Trois-? ?Évêchés|Rouergue|Guyenne|Limousin|Béarn|Gascogne|Flandre)")
PATTERN_PAYS_PRINCIPAUTE_ECT = re.compile(r"((Principauté|principauté|Principautés|principautés)+(\s){1}(de|d')?(\s){1}([A-Z]{1}[a-zéàèêâç\-]+)([A-Z]{1}[a-zéàèêâç\-]+)?)|(Comté|comté|Comtés|comtés)+(\s)?(de|d’)?(\s)?([A-Z]{1}[a-zéàèêâç\-]+)|(Duché|duché|Duchés|duchés)+(\s)?(de|d’)?(\s)?([A-Z]{1}[a-zéàèêâç\-]+)|(Seigneurie|seigneurie|Seigneuries|seigneuries)+(\s)?(de|d’)?(\s)?([A-Z]{1}[a-zéàèêâç\-]+)")
PATTERN_PAYS_FISCAUX = re.compile(r"((Pays|pays)+(\s)?(de|d’)?(\s)?([a-zéàèêâç])+)(\s)?([A-Z]?[a-zéàèêâç\-]+)")
PATTERN_PAYS_PRIMITIFS = re.compile(r"((Pays|pays)+(\s)?(de|d’)?(\s)?([a-zéàèêâç])+)(\s)?([A-Z]+[a-zéàèêâç\-]+)")
PATTERN_ADMINISTRATIONS_ET_JURIDICTIONS_ROYALES = re.compile(r"((Généralité|généralité)+(\s)?(de|d’)?(\s)?([A-Z]+[a-zéàèêâ\-ç]+)+)|((Grenier|grenier|Greniers|greniers)+(\s)?(de|d’)?(\s)?([A-Z]+[a-zéàèê\-âç]+)+)|((Cours|Cour|cours|cour)+(\s)?(de|d’)?(\s)?([A-Z]+[a-zéàèê\-âç]+)+)|((Commission|Commissions|commission|commissions)+(\s)?(de|d’)?(\s)?([A-Z]+[a-zéàèê\-âç]+)+)|((Tribunal|Tribunaux|tribunal|tribunaux)+(\s)?(de|d’)?(\s)?([A-Z]+[a-zéàèê\-âç]+)+)|((Election|Elections|élections|élection)+(\s)?(de|d’)?(\s)?([A-Z]+[a-zéàèê\-âç]+)+)")
PATTERN_ADMINISTRATIONS_DES_FERMES = re.compile(r"((Direction|directions|Directions|direction)+(\s)?(de|d’)?(\s)?([A-Z]+[a-zéàèêâ\-ç]+)+)|((Entrepôt|entrepôt|Entrepôts|entrepôts)+(\s)?(de|d’)?(\s)?([A-Z]+[a-zéàèêâ\-ç]+)+)|((Département|département|Départements|départements)+(\s)?(de|d’)?(\s)?([A-Z]+[a-zéàèêâ\-ç]+)+)|((Dépôt|dépôts|Dépôts|dépôt)+(\s)?(de|d’)?(\s)?([A-Z]+[a-zéàèêâ\-ç]+)+)|((Magasin|magasin|Magasins|magasins)+(\s)?(de|d’)?(\s)?([A-Z]+[a-zéàèêâ\-ç]+)+)")
PATTERN_LIEUX_HABITATIONS = re.compile(r"((ville|villes|Villes|Ville)+(\s)?(de|d’)?(\s)?([A-Z]+[a-zéàèêâ\-ç]+)+)|((village|villages|Village|Villages)+(\s)?(de|d’|nommé)?(\s)?([A-Z]+[a-zéàèêâ\-ç]+)+)|((port|ports|Port|Ports)+(\s)?(de|d’|nommé)?(\s)?([A-Z]+[a-zéàèêâ\-ç]+)+)")
PATTERN_LIEUX_CONTROLE = re.compile(r"((Bureau|Bureaux|bureau|bureaux)+(\s)?(de|d’)?(\s)?([A-Z]+[a-zéàèêâ\-ç]+)+)|((Brigade|Brigades|brigade|brigades)+(\s)?(de|d’)?(\s)?([A-Z]+[a-zéàèêâ\-ç]+)+)")
PATTERN_ZONES = re.compile(r"((Limitrophe|limitrophe|Limitrophes|limitrophes)+(\s)?(de|d’|du|au|aux)?(\s)?([A-Z]+[a-zéàèêâ\-ç]+)+)|((Frontière|frontière|frontières|Frontières)+(\s)?(de|d’|du|au|aux|entre)?(\s)?([A-Z]+[a-zéàèêâ\-ç]+)+)")
PATTERN_AUTEURS_BIBL = re.compile(r"((; [A-Z]{1}[a-z\-]+)([A-Z]{1}[a-z]+)? ([A-Z]{1}[a-z]+))")
PATTERN_TITRE = re.compile(r"(, « ([A-Za-z\séàêèâç\-',’\.(0-9)]+))")


# ===============================================================================================================================================================================================


# II] Récupération des données sources.

# Ouverture et lecture du document texte initial (correspondant à une notice) :

with open('txt', encoding="UTF-8") as fp:
    text = fp.read()


# ===============================================================================================================================================================================================


# III] Définition des fonctions d'encodage.

# Création du tei_header à partir d'une chaîne de caractères.
# La date de création est inséré automatiquement grâce au module datetime.
def build_tei_header():
    return "".join(["<teiHeader>",
                    "<fileDesc>",
                    "<titleStmt>",
                    f"<author></author>",
                    "</titleStmt>",
                    "<publicationStmt>",
                    "<publisher>MESHS de Lille dans le cadre de l'ANR FermeGé Administrer le privilège"
                    " la Ferme générale dans l'espace français et européen (1664-1794)"
                    "Axe 1 : Dictionnaire de la Ferme générale, objet d'histoire totale"
                    "</publisher>",
                    "<pubPlace><address><addrLine>Maison Européenne des Sciences de l'Homme et de la Société</addrLine>"
                    "<addrLine> 2 rue des cannoniers </addrLine>"
                    "<addrLine> 59002 Lille Cedex</addrLine>"
                    "</address>",
                    "<date>2021-2025</date>"
                    "</pubPlace>",
                    "<availability>",
                    "<licence>[A déterminer. Ex:Creative Commons Attribution 3.0 non transposé (CC BY 3.0)]</licence>",
                    "</availability>",
                    "</publicationStmt>",
                    "<seriesStmt>",
                    "<title>Dictionnaire numérique de la Ferme générale</title>",
                    "<respStmt>",
                    "<resp> Coordinateurs de l'axe : Dictionnaire numérique de la Ferme générale, objet d'histoire totale. </resp>",
                    "<persName> Marie-Laure Legay</persName>",
                    "<persName> Thomas Boullu</persName>",
                    "</respStmt>",
                    "</seriesStmt>",
                    "<sourceDesc>",
                    "<p> Dictionnaire numérique de la Ferme générale</p> </sourceDesc>",
                    "</fileDesc>",
                    "<encodingDesc>",
                    "<projectDesc source='https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3'>",
                    "<p> Le projet FermGé vise à étudier l’impact d’une organisation fiscale (1664-1794), "
                    "discriminante mais rationnelle, sur les territoires et les sociétés de la France moderne/p>",
                    "</projectDesc>",
                    "</encodingDesc>",
                    "<profileDesc>",
                    "<textClass>",
                    "<keywords scheme='#fr_RAMEAU'>",
                    "<list>",
                    "<item>Histoire -- Histoire moderne -- </item>",
                    "</list>",
                    "</keywords>",
                    "</textClass>",
                    "</profileDesc>",
                    "<revisionDesc>",
                    f'<change type="AutomaticallyEncoded">{str(datetime.now()).replace(" ", "T") + "+2:00"}</change>',
                    "</revisionDesc>", "</teiHeader>"])

# Création et "parsage" du conteneur TEI contenant la notice encodées (dans une balise <TEI>)
# Récupération du chemin de fichier vers l'ODD au format .rng et insertion dans le préambule.
# Après avoir parsé ce conteneur, on ajoute à la suite de la balise <TEI> le <teiHeader> et une nouvelle balise temporaire ("text_blob") qui sera remplacée par la suite.
def build_tei_container():
    container = f"""<?xml-model href="file:/F:/Stage%20MESHS%20DE%20CRAENE%20Valentin/TEI%20XSLT/out/ODD_v2.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="file:/F:/Stage%20MESHS%20DE%20CRAENE%20Valentin/TEI%20XSLT/out/ODD_v2.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns='http://www.tei-c.org/ns/1.0'></TEI>"""
    parsed_container = BeautifulSoup(container, "xml")
    parsed_tei_header = BeautifulSoup(build_tei_header(), "xml").extract()
    parsed_container.TEI.append(parsed_tei_header)
    parsed_container.TEI.append(parsed_container.new_tag("text_blob"))
    return parsed_container

# Création et parsage de l'arbre <body> qui sera ensuite ajouté au conteneur.
def build_tree(text):
    text = '<body>ph</body>'.replace("ph", text)
    tree = BeautifulSoup(text, "xml")
    return tree

# Création et parsage de l'arbre <entry> qui sera ensuite ajouté au conteneur.
def add_entry(text):
    text = '<entry>ph</entry>'.replace('ph', text)
    tree = BeautifulSoup(text, 'xml')
    return tree

# Création et parsage de l'arbre <sense> qui sera ensuite ajouté au conteneur.
def add_sense(text):
    text = '<sense>ph</sense>'.replace('ph', text)
    tree = BeautifulSoup(text, 'xml')
    return tree

# Création et parsage de l'arbre <bibl type="references"> (références bibliographiques et sources primaires) qui sera ensuite ajouté au conteneur.
# Grâce à la regex correspondante, on récupère les passages à encoder qui sont insérés dans la balise <bibl type="references">
# Ce nouvel arbre est ensuite parsé et sera inséré dans le conteneur TEI général.
def add_scientific_references(text):
    text = re.findall(r'\[.+\]$', str(text), re.MULTILINE)
    text = ''.join(text)
    text = '<bibl type="references">ph</bibl>'.replace('ph', str(text))
    tree = BeautifulSoup(str(text), 'xml')
    return tree

# Création et parsage de l'arbre <def> (bloc définitionnel) qui sera ensuite ajouté au conteneur.
# Grâce à la regex correspondante, on récupère les passages à encoder qui sont insérés dans la balise <def>
# Ce nouvel arbre est ensuite parsé et sera inséré dans le conteneur TEI général.
def add_def(text):
    text = re.findall(r'^(?!\[.*|[a-zA-Z0-9ÀÂÄÇÉÈÊËÎÏÔÖÙÛÜŸÆŒàâäçéèêëîïôöùûüÿæœ_\'\r\\(\\) ]+$).*', str(text), re.MULTILINE)
    text = ''.join(text)
    text = '<def>ph</def>'.replace('ph', str(text))
    tree = BeautifulSoup(str(text), 'xml')
    return tree

# Création et parsage de l'ensemble des arbres <bibl> de niveau 2 (correspondant chacun à un item bibliographique ou une source) qui seront ensuite ajoutés au conteneur.
# Grâce à la regex correspondante, on récupère les passages à encoder qui sont insérés dans la balise <bibl>
# Ces nouveaux arbres sont ensuite parsés et seront insérés dans le conteneur TEI général.
def add_scientific_references_single(text):
    list = []
    text = re.findall(r'(^\[.*)', str(text), re.MULTILINE)
    text = ' '.join(text)
    text = text.split(';')
    for i in text:
        text = list.append('<bibl>ph</bibl>'.replace('ph', i+";"))
    return ''.join(list)

# Création et parsage de l'arbre <form> (forme du lemme) qui sera ensuite ajouté au conteneur.
# Grâce à la regex correspondante, on récupère les passages à encoder qui sont insérés dans les balises <form type="lemma"> et <orth> (orthographe du lemme)
# Ce nouvel arbre est ensuite parsé et sera inséré dans le conteneur TEI général.
def add_notice_title(text):
    text = re.findall(r'^[a-zA-Z0-9ÀÂÄÇÉÈÊËÎÏÔÖÙÛÜŸÆŒàâäçéèêëîïôöùûüÿæœ_\-\'\\(\\) ]+$', str(text), re.MULTILINE)
    text = ''.join(text)
    text = '<form type="lemma"><orth>ph</orth></form>'.replace('ph', str(text))
    tree = BeautifulSoup(str(text), 'xml')
    return tree

# Création et parsage de l'arbre <title> (titre de la notice) qui sera ensuite ajouté au conteneur.
# Grâce à la regex correspondante, on récupère les passages à encoder qui sont insérés dans les balises <title type="notice>.
# Ce nouvel arbre est ensuite parsé et sera inséré dans le conteneur TEI général.
def add_title_to_metadata(text):
    text = re.findall(r'^[a-zA-Z0-9ÀÂÄÇÉÈÊËÎÏÔÖÙÛÜŸÆŒàâäçéèêëîïôöùûüÿæœ_\-\'\\(\\) ]+$', str(text), re.MULTILINE)
    text = ''.join(text)
    text = '<title type="notice">ph</title>'.replace('ph', str(text))
    tree = BeautifulSoup(str(text), 'xml')
    return tree

# Création et parsage de l'ensemble des arbres <date> (correspondant aux dates) qui seront ensuite ajoutés au conteneur.
# Grâce à la regex correspondante, on récupère les passages à encoder qui sont insérés dans la balise <date>
# Ces nouveaux arbres sont ensuite parsés et seront insérés dans le conteneur TEI général.
def add_dates(text):
    text = re.findall(r'\d{4}', text)
    text = ' '.join(text)
    text = '<date>ph</date>'.replace('ph', str(text))
    tree = BeautifulSoup(str(text), 'xml')
    return tree

# Fonction de correction des balises <bibl> afin de pouvoir parser l'arbre et assurer la validité de l'encodage.
def fix_bibl_tags(text):
    return text.replace("&lt;bibl&gt;", "<bibl>").replace("&lt;/bibl&gt;", "</bibl>")

# Fonction utilisée dans un second temps qui permet de récupérer les balises <date> uniquement dans la balise <def> et d'ajouter l'attribut @when avec la valeur de la date
def find_dates(complete_tree):
    date_regex = re.compile(r'(\d{4})')
    scope = []
    for elem in complete_tree.entry.find_all(['def']):
        scope.append(elem)
    for elem in scope:
        if elem.string:
            elem.string.replace_with(re.sub(date_regex, r'<date when="\1">\1</date>', elem.string, 0))
    return complete_tree

# Fonction de correction des balises <date> afin de pouvoir parser l'arbre et assurer la validité de l'encodage.
def fix_date_tags(text):
    text = text.replace("&lt;date;", "<date").replace("&lt;/date&gt;", "</date>")
    return text.replace("&gt;", ">").replace("&lt;", '<')

# Fonction permettant d'encoder les <orgName> en accord avec l'ontologie établie dans le cadre du projet.
# Encodage des administrations et juridictions royales dans une balise <orgName type="administrations_juridictions" subtype="administrations_juridictions_royales">
# L'ensemble de ces nouveaux arbres sont parsés et seront réinséré dans le conteneur <TEI> par la suite.
def find_administrations_et_juridictions_royales(complete_tree):
    administrations_regex = re.compile(r"(((Généralité|généralité)+(\s)?(de|d’)?(\s)?([A-Z]+[a-zéàèêâ\-ç]+)+)|((Grenier|grenier|Greniers|greniers)+(\s)?(de|d’)?(\s)?([A-Z]+[a-zéàèê\-âç]+)+)|((Cours|Cour|cours|cour)+(\s)?(de|d’)?(\s)?([A-Z]+[a-zéàèê\-âç]+)+)|((Commission|Commissions|commission|commissions)+(\s)?(de|d’)?(\s)?([A-Z]+[a-zéàèê\-âç]+)+)|((Tribunal|Tribunaux|tribunal|tribunaux)+(\s)?(de|d’)?(\s)?([A-Z]+[a-zéàèê\-âç]+)+)|((Election|Elections|élections|élection)+(\s)?(de|d’)?(\s)?([A-Z]+[a-zéàèê\-âç]+)+))")
    scope = []
    for elem in complete_tree.entry.find_all(['def']):
        scope.append(elem)
    for elem in scope:
        if elem.string:
            ref = elem.string.replace_with(re.sub(administrations_regex, r'<orgName type="administrations_juridictions" subtype="administrations_juridictions_royales"> \1 </orgName>', elem.string, 0))
    return complete_tree

def find_span_normes(complete_tree):
    arret_regex = re.compile(r"(((?<!\[)[A-Za-zééê:;,*()èîçœà&ô\-ùâ’'« 0-9]+(amende |arrêt du Conseil|arrêts du Conseil |arrêts de la Cour |"
                             r"arrêt de la Cour |arrêt de règlement |article |articles |assignation |assignations |"
                             r"commandement |commendements |confiscation |confiscations |contrainte |contraintes |contrôle |contrôles |"
                             r"déclaration |déclarations |délibération |délibérations |édit |édits |enregistrement |enregistrements |inspection |inspections |"
                             r"loi |mandement |mandements |règlement |règlements |réquisition |réquisitions |ordonnance |ordonnances |ordre |procès-verbal |"
                             r"prohibition |recouvrement |recouvrements |saisie |visite )+(\s)?[A-Za-zéœê;,*()èçà ôî:&ù\-â’'«» 0-9]+))")

    scope = []
    for elem in complete_tree.entry.find_all(['def']):
        scope.append(elem)
    for elem in scope:
        if elem.string:
            ref = elem.string.replace_with(re.sub(arret_regex, r'<span type="normes_contestations" subtype="normes">\1</span>', elem.string))
            print(complete_tree)
    return complete_tree


def find_span_contestation(complete_tree):
    arret_regex = re.compile(r"(((?<!\[)[A-Za-zééê:;,*()èîçœà&ô\-ùâ’'« 0-9]+(arme |armes |banditisme |calomnie |conflit |conflits |"
                             r"contestation |contestations |contrebande |émeute |émeutes |fraude |faux |faux-saunage |libelle |libelles |"
                             r"meutre |meutres |pamphlet |pamphlets |opposition |oppositions |rébellion |résistance |résistances |révolte |révoltes |vol )+(\s)?[A-Za-zéœê;,*()èçà ôî:&ù\-â’'«» 0-9]+))")

    scope = []
    for elem in complete_tree.entry.find_all(['def']):
        scope.append(elem)
    for elem in scope:
        if elem.string:
            ref = elem.string.replace_with(re.sub(arret_regex, r'<span type="normes_contestations" subtype="contestation">\1</span>', elem.string))
            print(complete_tree)
    return complete_tree

def find_span_privilege(complete_tree):
    arret_regex = re.compile(r"(((?<!\[)[A-Za-zééê:;,*()èîçœà&ô\-ùâ’'« 0-9]+(apanage |assemblée |autonomie |baronnies |chartes |châtellenie |"
                             r"clergé |comté |comtés |duché |duchés |exempt |exemption |Etats |franc |franc-salé |"
                             r"franche gabelle |franchise |immunité |libertés |noblesse |privilège )+(\s)?[A-Za-zéœê;,*()èçà ôî:&ù\-â’'«» 0-9]+))")

    scope = []
    for elem in complete_tree.entry.find_all(['def']):
        scope.append(elem)
    for elem in scope:
        if elem.string:
            ref = elem.string.replace_with(re.sub(arret_regex, r'<span type="privleges_encadrement" subtype="privileges">\1</span>', elem.string))
            print(complete_tree)
    return complete_tree


def find_span_encadrement(complete_tree):
    arret_regex = re.compile(r"(((?<!\[)[A-Za-zééê:;,*()èîçœà&ô\-ùâ’'« 0-9]+(abonnement |accommodement |accord |arrangement |compromis |concession |"
                             r"convention |coopération |entente |faveur |modération |négociation |réduction )+(\s)?[A-Za-zéœê;,*()èçà ôî:&ù\-â’'«» 0-9]+))")

    scope = []
    for elem in complete_tree.entry.find_all(['def']):
        scope.append(elem)
    for elem in scope:
        if elem.string:
            ref = elem.string.replace_with(re.sub(arret_regex, r'<span type="privleges_encadrement" subtype="encadrement">\1</span>', elem.string))
            print(complete_tree)
    return complete_tree



# Fonction permettant d'encoder les <orgName> en accord avec l'ontologie établie dans le cadre du projet.
# Encodage des provinces dans une balise <orgName type="pays_et_provinces" subtype="province">
# L'ensemble de ces nouveaux arbres sont parsés et seront réinséré dans le conteneur <TEI> par la suite.
def find_provinces(complete_tree):
    # On prévoit le cas où une ref est incluse dans une province, d'où l'étoile
    provinces_regex = re.compile(r"(Bretagne\*?|Franche-Comté\*?|Normandie\*?|Provence\*?|Artois\*?|Lorraine\*?|Dauphiné\*?|Picardie\*?|Languedoc\*?|Anjou\*?|Auvergne\*?|Roussillon\*?|Poitou\*?|Champagne\*?|Hainaut\*?|Comtat-Venaissin\*?|Cambrésis\*?|Bourgogne\*?|Alsace\*?|Navarre\*?|Trois-? ?Evêchés\*?|Trois-? ?Évêchés\*?|Rouergue\*?|Guyenne\*?|Limousin\*?|Béarn\*?|Gascogne\*?|Flandre\*?)")
    scope = []
    for elem in complete_tree.entry.find_all(['def']):
        scope.append(elem)
    for elem in scope:
        if elem.string:
            ref = elem.string.replace_with(re.sub(provinces_regex, r'<orgName type="pays_et_provinces" subtype="province">\1</orgName>', elem.string))
    return complete_tree

# Fonction permettant d'encoder les <orgName> en accord avec l'ontologie établie dans le cadre du projet.
# Encodage des pays primitifs dans une balise <orgName type="pays_et_provinces" subtype="pays">
# L'ensemble de ces nouveaux arbres sont parsés et seront réinséré dans le conteneur <TEI> par la suite.
def find_principautes_comte_duche_seigneurie(complete_tree):
    provinces_regex = re.compile(r"(((((Pays|pays)+(\s)+(de|d’)+(\s)?([A-Z]{1}[a-zéàèêâç]){3,})(\s)?([A-Z]?[a-zéàèêâç\-]+)((\s)?[A-Z]?[a-zéàèêâç\-]+)?)|(Principauté|principauté|Principautés|principautés)+(\s){1}(de|d')?(\s){1}([A-Z]{1}[a-zéàèêâç\-]+)([A-Z]{1}[a-zéàèêâç\-]+)?)|(Comté|comté|Comtés|comtés)+(\s)?(de|d’)?(\s)?([A-Z]{1}[a-zéàèêâç\-]+)|(Duché|duché|Duchés|duchés)+(\s)?(de|d’)?(\s)?([A-Z]{1}[a-zéàèêâç\-]+)|(Seigneurie|seigneurie|Seigneuries|seigneuries)+(\s)?(de|d’)?(\s)?([A-Z]{1}[a-zéàèêâç\-]+))")
    scope = []
    for elem in complete_tree.entry.find_all(['def']):
        scope.append(elem)
    for elem in scope:
        if elem.string:
            ref = elem.string.replace_with(re.sub(provinces_regex, r'<orgName type="pays_et_provinces" subtype="pays">\1</orgName>', elem.string))
    return complete_tree

# Fonction permettant d'encoder les <orgName> en accord avec l'ontologie établie dans le cadre du projet.
# Encodage des administrations et juridictions de la Ferme générale dans une balise <orgName type="administrations_et_juridictions" subtype="administrations_juridictions_fermes">
# L'ensemble de ces nouveaux arbres sont parsés et seront réinséré dans le conteneur <TEI> par la suite.
def find_administrations_fermes(complete_tree):
    administrations_fermes_regex = re.compile(r"(((Direction|directions|Directions|direction)+(\s)?(de|d’)?(\s)?([A-Z]+[a-zéàèêâ\-ç]+)+)|((Entrepôt|entrepôt|Entrepôts|entrepôts)+(\s)?(de|d’)?(\s)?([A-Z]+[a-zéàèêâ\-ç]+)+)|((Département|département|Départements|départements)+(\s)?(de|d’)?(\s)?([A-Z]+[a-zéàèêâ\-ç]+)+)|((Dépôt|dépôts|Dépôts|dépôt)+(\s)?(de|d’)?(\s)?([A-Z]+[a-zéàèêâ\-ç]+)+)|((Magasin|magasin|Magasins|magasins)+(\s)?(de|d’)?(\s)?([A-Z]+[a-zéàèêâ\-ç]+)+))")
    scope = []
    for elem in complete_tree.entry.find_all(['def']):
        scope.append(elem)
    for elem in scope:
        if elem.string:
            ref = elem.string.replace_with(re.sub(administrations_fermes_regex, r'<orgName type="administrations_et_juridictions" subtype="administrations_juridictions_fermes">\1</orgName>', elem.string))
    return complete_tree

# Fonction permettant d'encoder les <placeName> en accord avec l'ontologie établie dans le cadre du projet.
# Encodage des lieux d'habitation dans une balise <placeName type="lieux_habitations">.
# L'ensemble de ces nouveaux arbres sont parsés et seront réinséré dans le conteneur <TEI> par la suite.
def find_lieux_habitations(complete_tree):
    lieux_habitations_regex = re.compile(r"(((ville|villes|Villes|Ville)+(\s)?(de|d’)?(\s)?([A-Z]+[a-zéàèêâ\-ç]+)+)|((village|villages|Village|Villages)+(\s)?(de|d’|nommé)?(\s)?([A-Z]+[a-zéàèêâ\-ç]+)+)|((port|ports|Port|Ports)+(\s)?(de|d’|nommé)?(\s)?([A-Z]+[a-zéàèêâ\-ç]+)+))")
    scope = []
    for elem in complete_tree.entry.find_all(['def']):
        scope.append(elem)
    for elem in scope:
        if elem.string:
            ref = elem.string.replace_with(re.sub(lieux_habitations_regex, r'<placeName type="lieux_habitations">\1</placeName>', elem.string))
    return complete_tree

# Fonction permettant d'encoder les <placeName> en accord avec l'ontologie établie dans le cadre du projet.
# Encodage des lieux de contrôle dans une balise <placeName type="lieux_controle">.
# L'ensemble de ces nouveaux arbres sont parsés et seront réinséré dans le conteneur <TEI> par la suite.
def find_lieux_controle(complete_tree):
    lieux_controle_regex = re.compile(r"(((Bureau|Bureaux|bureau|bureaux)+(\s)?(de|d’)?(\s)?([A-Z]+[a-zéàèêâ\-ç]+)+)|((Brigade|Brigades|brigade|brigades)+(\s)?(de|d’)?(\s)?([A-Z]+[a-zéàèêâ\-ç]+)+)|((Péage|Péages|péage|péages)+(\s)?(de|d’)?(\s)?([A-Z]+[a-zéàèêâ\-ç]+)+))", re.MULTILINE)
    scope = []
    for elem in complete_tree.entry.find_all(['def']):
        scope.append(elem)
    for elem in scope:
        if elem.string:
            ref = elem.string.replace_with(re.sub(lieux_controle_regex, r'<placeName type="lieux_controle">\1</placeName>', elem.string))
    return complete_tree

# Fonction permettant d'encoder les <placeName> en accord avec l'ontologie établie dans le cadre du projet.
# Encodage des zones et territoires géographiques dans une balise <placeName type="zone">.
# L'ensemble de ces nouveaux arbres sont parsés et seront réinséré dans le conteneur <TEI> par la suite.
def find_zones(complete_tree):
    zones_regex = re.compile(r"(((Limitrophe|limitrophe|Limitrophes|limitrophes)+(\s)?(de|d’|du|au|aux)?(\s)?([A-Z]+[a-zéàèêâ\-ç]+)+)|((Frontière|frontière|frontières|Frontières)+(\s)?(de|d’|du|au|aux|entre)?(\s)?([A-Z]+[a-zéàèêâ\-ç]+)+))")
    scope = []
    for elem in complete_tree.entry.find_all(['def']):
        scope.append(elem)
    for elem in scope:
        if elem.string:
            ref = elem.string.replace_with(re.sub(zones_regex, r'<placeName type="zone">\1</placeName>', elem.string))
    return complete_tree

# Fonction de correction des balises <ref> afin de pouvoir parser l'arbre et assurer la validité de l'encodage.
def fix_ref_tags(text):
    text = text.replace("&lt;ref;", "<ref").replace("&lt;/ref&gt;", "</ref>")
    return text.replace("&gt;", ">").replace("&lt;", '<')

# Fonction permettant d'encoder les <bibl> de niveau 2 (sources primaires et imprimées).
# Encodage des sources imprimées dans une balise <bibl type="sources"> et des sources primaires dans les balises <bibl> et <idno type="ArchivalIdentifier">.
# L'ensemble de ces nouveaux arbres sont parsés et seront réinséré dans le conteneur <TEI> par la suite.
def find_sources(complete_tree):
    complete_tree = main('txt', 'test2')
    complete_tree = BeautifulSoup(complete_tree, 'xml')
    regex_sources = re.findall(r'(Arrêt.*?;| Arrest.*?; | Lettre.*;| Ordonnance.*?;| Carte.*?;| Vue.*?;| Edit.*?;| Édit.*?;| Déclaration.*?;| Almanach.*?;| Traité.*?;| Compte.*?;| Encyclopédie.*?;| Règlement.*?;)', text)
    regex_sources = ''.join(regex_sources)
    regex_idno = re.compile(r'(AM .*;|AN,? ?.*;|An .*;|AD,? ?.*;|BNF .*;|(?<!,)( (G\d.*?;)| \dC.*?;))')

    for tag in complete_tree.bibl.find_all("bibl"):
        if tag.string:
            tag.string.replace_with(re.sub(regex_idno, r'<idno type="ArchivalIdentifier">\1</idno>', tag.string, 0))
        if re.search(r'(Arrêt.*?;| Arrest.*?; | Lettre.*;| Ordonnance.*?;| Carte.*?;| Vue.*?;| Edit.*?;| Édit.*?;| Déclaration.*?;| Almanach.*?;| Traité.*?;| Compte.*?;| Encyclopédie.*?;| Règlement.*?;)', str(tag)):
            tag.attrs['type'] = 'sources'
    return complete_tree.prettify()

# Fonction de correction des balises <idno> afin de pouvoir parser l'arbre et assurer la validité de l'encodage.
def fix_idno_tags(text):
    text = text.replace("&lt;idno;", "<idno").replace("&lt;/idno&gt;", "</idno>")
    return text.replace("&gt;", ">").replace("&lt;", '<')


# ===============================================================================================================================================================================================


# IV] Fonction principale d'insertion de l'ensemble des arbres dans le conteneur <TEI> général et parsage.

# Après avoir ouvert le fichier source initial, on créé les arbres requis en appelant les fonctions correspondantes
# Ensuite, on parse le conteneur et on ajoute les arbres aux positions requises
# Finalement on appelle les fonctions de correction des balises pour s'assurer de la validité de l'encodage
# On renvoie l'arbre fusionné correspondant à une première version de la notice encodée.

def main(source, title):
    with open(source, "r", encoding="utf-8") as fh:
        source_as_text = fh.read()

    source_with_entry = add_entry(source_as_text)
    source_with_entry = add_scientific_references(source_with_entry)
    tree_body = build_tree(source_as_text)
    tree_entry = add_entry(text)
    tree_title = add_notice_title(text)
    tree_title_metadata = add_title_to_metadata(text)
    tree_sense = add_sense(text)
    tree_def = add_def(text)
    tree_bibl_1 = add_scientific_references(text)
    tree_bibl_2 = add_scientific_references_single(text)


    complete_tree = build_tei_container()
    complete_tree.text_blob.append(tree_body)
    complete_tree.text_blob.name = "text"
    complete_tree.body.clear()
    complete_tree.body.insert(0, tree_entry)
    complete_tree.entry.clear()
    complete_tree.entry.insert(0, tree_title)
    complete_tree.teiHeader.fileDesc.titleStmt.insert(0, tree_title_metadata)
    complete_tree.entry.insert(1, tree_sense)
    complete_tree.sense.clear()
    complete_tree.sense.insert(0, tree_def)
    complete_tree.sense.insert(1, tree_bibl_1)
    complete_tree.bibl.clear()
    complete_tree.bibl.insert(0, tree_bibl_2)
    complete_tree = find_span_normes(complete_tree)
    complete_tree = find_span_contestation(complete_tree)
    complete_tree = find_span_privilege(complete_tree)
    complete_tree = find_span_encadrement(complete_tree)
    complete_tree = find_dates(complete_tree)
    complete_tree = find_administrations_et_juridictions_royales(complete_tree)
    complete_tree = find_provinces(complete_tree)
    complete_tree = find_principautes_comte_duche_seigneurie(complete_tree)
    complete_tree = find_administrations_fermes(complete_tree)
    complete_tree = find_lieux_habitations(complete_tree)
    complete_tree = find_lieux_controle(complete_tree)
    complete_tree = find_zones(complete_tree)



    complete_tree = fix_ref_tags(fix_date_tags(fix_bibl_tags(complete_tree.prettify())))

    return complete_tree


# ===============================================================================================================================================================================================


# V] Fonctions de nettoyage, ajout et correction des balises.

# Fonction de nettoyage à la fin de la bibliographie du point-virgule
def nettoyage_biblio(complete_tree):
    complete_tree = BeautifulSoup(complete_tree, 'xml')
    regex = re.compile(r'];')
    for tag in complete_tree.find_all('bibl'):
        if tag.string:
            tag.string.replace_with(re.sub(regex, r']', tag.string, 0))
    return complete_tree.prettify()

# Fonction de nettoyage de la bibliographie qui permet d'ajouter dans les <bibl type="sources"> les références antérieures au XXe siècle.
def nettoyage_biblio2(complete_tree):
    complete_tree = BeautifulSoup(complete_tree, 'xml')
    regex_18_19 : re.compile(r'(17\d\d|18\d\d)')
    for tag in complete_tree.bibl.find_all('bibl type="sources"'):
        if re.search(r'(16\d\d|17\d\d|18\d\d|, XVIIe|, XVIIIe|, XIXe)', str(tag)):
            tag.attrs['type'] = 'sources'
    return complete_tree

# Fonction permettant d'ajouter à la balise <entry> un attribut @xml:id correspondant à la valeur de la balise <orth> nettoyée.
def add_attrs_entry(complete_tree):
    complete_tree = BeautifulSoup(str(complete_tree), 'xml')
    xml_id = complete_tree.orth.string.strip().replace(' ', '_').replace('(', '').replace(')', '').replace('’', '_').replace(',', '').replace('-', '_')
    for tag in complete_tree.find_all("entry"):
        mot = tag.orth.get_text()
        mot_clean = mot.replace(' ', '').replace('\n', '')
        lettre = re.findall(r'^[A-Z]{1}', mot_clean)
        tag.attrs['xml:id'] = xml_id
        tag.attrs['type'] = lettre
    return complete_tree

# Fonction qui permet de prendre en compte les <orgName> et <placeName> imbriquées dans une balise <ref> (afin de permettre la double indexation qui demande l'ODD).
def find_ref_imbriques(complete_tree):
    complete_tree = BeautifulSoup(str(complete_tree), 'xml')
    ref_regex = re.compile(r'([A-ZÀÂÄÇÉÈÊËÎÏÔÖÙÛÜŸÆŒa-zàâäçéèêëîïôöùûüÿæœ0-9\'\-]+\*)')
    scope = []
    for elem in complete_tree.entry.find_all(['orgName']):
        scope.append(elem)
    for elem in complete_tree.entry.find_all(['placeName']):
        scope.append(elem)
    for elem in scope:
        if elem.string:
            ref = elem.string.replace_with(re.sub(ref_regex, r'<ref target="#\1">\1</ref>', elem.string))
    return fix_ref_tags(complete_tree.prettify())

# Fonction permettant d'encoder dans des balises <ref> les renvois vers d'autres notices.
def find_ref_generaux(complete_tree):
    complete_tree = str(complete_tree)
    ref_regex = re.compile(r'([A-ZÀÂÄÇÉÈÊËÎÏÔÖÙÛÜŸÆŒa-zàâäçéèêëîïôöùûüÿæœ0-9\'\-]+\*)')
    complete_tree = re.sub('([A-ZÀÂÄÇÉÈÊËÎÏÔÖÙÛÜŸÆŒa-zàâäçéèêëîïôöùûüÿæœ0-9\'\-]+\*)', r'<ref target="#\1">\1</ref>', complete_tree)
    return complete_tree

# Fonction permettant de supprimer l'astérisque de balisage manuel des renvois vers d'autres notices.
def remove_asterisk(complete_tree):
    complete_tree = str(complete_tree)
    complete_tree = complete_tree.replace('*', '')
    return complete_tree

# Fonction permettant de sauvegarder dans un fichier registre-portatif.xml le résultat du processus de traitement des données.
def save_file(complete_tree):
    with open('output/.xml', "w", encoding="utf8") as fh:
        fh.write(complete_tree)


# Fonction générale de nettoyage et ajout des balises imbriquées
# On sauvegarde avec la fonction save_file() le résultat et on renvoie l'arbre complet pour le visualiser.
def ajout_sources_nettoyage():
    complete_tree = main('txt', 'test2')
    complete_tree = find_sources(complete_tree)
    complete_tree = fix_idno_tags(complete_tree)
    complete_tree = add_attrs_entry(complete_tree)
    complete_tree = find_ref_generaux(complete_tree)
    complete_tree = find_ref_imbriques(complete_tree)
    complete_tree = remove_asterisk(complete_tree)
    complete_tree = nettoyage_biblio(complete_tree)
    complete_tree = nettoyage_biblio2(complete_tree)
    save_file(str(complete_tree))
    return complete_tree.prettify()

# Exécution de la fonction.
print((ajout_sources_nettoyage()))