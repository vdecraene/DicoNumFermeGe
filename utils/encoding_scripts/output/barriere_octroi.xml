<?xml version="1.0" encoding="utf-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title type="notice"> Barrière_octroi </title>
    <author>Momcilo Markovic</author>
   </titleStmt>
   <publicationStmt>
    <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer le privilège la Ferme
     générale dans l'espace français et européen (1664-1794)Axe 1 : Dictionnaire de la Ferme
     générale, objet d'histoire totale </publisher>
    <pubPlace>
     <address>
      <addrLine> Maison Européenne des Sciences de l'Homme et de la Société </addrLine>
      <addrLine> 2 rue des cannoniers </addrLine>
      <addrLine> 59002 Lille Cedex </addrLine>
     </address>
     <date> 2021-2025 </date>
    </pubPlace>
    <availability>
     <licence> [A déterminer. Ex:Creative Commons Attribution 3.0 non transposé (CC BY 3.0)]
     </licence>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title> Dictionnaire numérique de la Ferme générale </title>
    <respStmt>
     <resp> Coordinateurs de l'axe : Dictionnaire numérique de la Ferme générale, objet d'histoire
      totale. </resp>
     <persName> Marie-Laure Legay </persName>
     <persName> Thomas Boullu </persName>
    </respStmt>
   </seriesStmt>
   <sourceDesc>
    <p> Dictionnaire numérique de la Ferme générale </p>
   </sourceDesc>
  </fileDesc>
  <encodingDesc>
   <projectDesc
    source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
    <p> Le projet FermGé vise à étudier l’impact d’une organisation fiscale (1664-1794),
     discriminante mais rationnelle, sur les territoires et les sociétés de la France moderne/p&gt;
    </p>
   </projectDesc>
   
  </encodingDesc><profileDesc>
    <textClass>
     <keywords scheme="#fr_RAMEAU">
      <list>
       <item>Histoire -- Histoire moderne -- histoire administrative</item>
       <item>Histoire -- Histoire moderne -- histoire commerciale</item>
       <item>Histoire -- Histoire moderne -- histoire fiscale</item>
       <item>Histoire -- Histoire moderne -- histoire politique</item>


      </list>
     </keywords>
    </textClass>
   </profileDesc>
   <revisionDesc>
    <change type="AutomaticallyEncoded"> 2022-12-12T17:11:22.853210+2:00 </change>
   </revisionDesc>
 </teiHeader>
 <text>
  <body>
   <entry type="B" xml:id="Barrière_octroi">
    <form type="lemma">
     <orth> Barrière_octroi </orth>
    </form>
    <sense>
     <def> <span subtype="privileges" type="privleges_encadrement">Il existe à <ref target="#Paris"> Paris </ref> , depuis la fin du XVIIe siècle, une ligne
      de démarcation qui divise la ville et la banlieue, matérialisée par des bornes ancrées au sol.
      Cette séparation de deux territoires distincts coïncide avec une limite fiscale, symbolisée
      par les barrières d’octroi  où, à
       l’exception des grains qui bénéficient d’une exemption totale à l’entrée des villes, tous les
       produits sont soumis à des taxes indirectes </span> . La monarchie, qui se préoccupe de
      l’accroissement de la ville et craint qu’elle ne devienne tentaculaire et son
      approvisionnement trop difficile, tente de freiner son expansion. De nouvelles limites sont
      fixées entre <date when="1724"> 1724 </date> et <date when="1728"> 1728 </date> . La
      superficie de <placeName type="lieux_habitations">Paris</placeName> s’étend alors sur près de 1 337 hectares. Les commissaires chargés du
      bornage installent environ 300 tables de pierre de liais sur les façades des maisons, balisant
      les limites de la ville et des faubourgs. Pendant une vingtaine d’années, la réglementation
      est globalement respectée, mais de nouveaux quartiers, comme le <placeName type="lieux_habitations">faubourg Saint-Honoré</placeName>, se
      développent, engageant la monarchie à revenir en mai <date when="1765"> 1765 </date> sur la
      pratique déjà expérimentée, celle de poser une borne « dans le mur de ladite dernière maison
      [de la ville] ». A la veille de la Révolution, la question de l’accroissement de la population
      parisienne à l’intérieur des limites fiscales et de ses faubourgs est toujours une question
      d’actualité, sans cesse discutée, mais jamais réglée : doit-on se résoudre à borner d’une
      façon irrévocable les étendues de la capitale ? La ceinture des barrières d’octroi suit
      l’amplitude de la ville. Toutefois, des postes d’observation (les guérites) sont positionnés
      au-delà de la limite fiscale, ce qui permet aux employés de surveiller et empêcher les actions
      frauduleuses avant même que les marchandises soient inspectées par les services de la Ferme
      générale et paient les droits d’ <ref target="#octroi"> octroi </ref> ou droits d’ <ref
       target="#entrée"> entrée </ref> aux barrières de la ville. Les historiens ont pris l’habitude
      de parler de barrières d’octroi. Toutefois, au XVIIIe siècle, le terme barrière suffit
      amplement et aucun autre mot, dans les documents, n’est accolé au premier, le vocable étant
      suffisamment explicite. Les barrières sont des « passages par lesquels arrivent les voitures
      et les marchandises sujettes aux droits, et elles sont traversées par une barre de bois qui
      roule sur un pivot et qui s’ouvre, ou se ferme à la volonté du commis ». Au milieu du
      XVIIIe siècle, les barrières sont essentiellement constituées de « planches d’un aspect très
      désagréable […] », mais « on songe à substituer de nouvelles barrières aux anciennes : on en
      voit déjà plusieurs construites en grillage de fer à quelques-unes des principales entrées de
      Paris ». A la fin des années <date when="1760"> 1760 </date> , il existe une soixantaine de
      barrières, placées « à la tête des faubourgs » : vingt-deux sont considérées comme principales
      et les employés de la Ferme « examinent les lettres de voiture, reçoivent les principaux
      droits veillent aux intérêts des Fermiers généraux de Sa Majesté ». Toutes les <ref
       target="#voitures"> voitures </ref> et « ceux qui sont chargés de denrées comprises dans les
       <ref target="#tarifs"> tarifs </ref> , doivent s’arrêter, souffrir la visite, payer les
      entrées. Les <ref target="#commis"> commis </ref> ont même la permission de visiter les
      carrosses, berlines, chaises, surtout les particuliers, pour voir s’il n’y a point de <ref
       target="#contrebande"> contrebande </ref> cachée, ou de denrées sujettes aux droits ; ce
      qu’ils font pareillement dans les porte-manteaux, valises, coffres dont on doit leur
      représenter les clefs ; saisissant arrêtant tout ce qui n’a point été déclaré, qui
      conformément aux Ordonnances reste confisqué […] ». Craignant une <ref target="#fraude">
       fraude </ref> à outrance, les employés vérifient méticuleusement les voitures avec une sonde
      (objet long en fer avec un manche en bois) ; ils plantent l’instrument dans les ballots,
      suspectant d’autres marchandises cachées sous les paquets. Le foin (autre produit taxé à
      l’entrée), répandu dans l’attelage, est inspecté rigoureusement ; les « commis enfoncent de
      longues aiguilles de fer qui ont huit pieds de long […] ; ils percent même, avec ces
      épouvantables aiguilles, toutes les masses qui leur paraissent suspectes ; alors ils flairent
      le bout de leurs interrogeantes aiguilles ». Les particuliers qui pénètrent dans la ville sont
      pareillement contrôlés car il est aisé de dissimuler sous le manteau une vessie remplie d’
       <ref target="#eau-de-vie"> eau-de-vie </ref>. Les barrières principales sont des bureaux de
      recette ; en effet, les voitures chargées de denrées ne peuvent pas s’engager dans la ville à
      n’importe quelle barrière ou bureau de recette. Ainsi, les animaux à pied-fourché doivent
      obligatoirement passer par une douzaine de barrières, réparties à la périphérie de la ville ;
      au nord, seules les barrières Sainte-Anne, Saint-Denis et Saint-Martin en permettent le
      passage. Il en va de même pour le <ref target="#vin"> vin </ref> qui doit, en dehors de
      quelques portes (Conférence ou Saint-Honoré par exemple), transiter par quatorze barrières (on
      y retrouve au nord les mêmes lieux de perception que pour les bêtes). Les voituriers par eau,
      dont la cargaison est chargée de vin, doivent « arrêter garer leurs bateaux aux ports de la
      Râpée, de Saint-Paul, de la Tournelle de la Conférence ». En amont et aval de la <ref
       target="#Seine"> Seine </ref> , la Ferme a disposé sur le fleuve des
       <ref target="#pataches"> pataches </ref> , dans lesquelles des commis patrouillent et
       inspectent les rives et les bateaux, à la recherche de produits de <ref target="#contrebande"
        > contrebande </ref> ou introduits en fraude de droits  . Toutes les autres
      barrières, au nombre de trente-huit, sont regardées comme des faux-passages avec
      l’interdiction pour les gros attelages d’emprunter ces accès. D’autres lieux, les barrières
      de renvoi et de conserve, sont des entrées secondaires. Les barrières de la porte Blanche près
      desquelles se situe le fameux cabaret de la Grande Pinte sont réputées être des barrières de
      renvoi ; les voitures qui viendraient s’y engager sont refoulées, car les commis n’y laissent
      passer que les piétons et les cavaliers qui n’ont rien à déclarer ou des véhicules légers qui
      transportent d’autres articles, autres que le <ref target="#vin"> vin </ref> ou de la viande.
      D’autres voituriers ne font que traverser Paris et, dans ce cas, ils n’ont pas de droits à
      payer, mais ils ont l’obligation de se rendre dans des bureaux spécifiques dans lesquels ils
      demandent un laisser-passer (ou laissez-passer) où ils présentent les expéditions,
      c’est-à-dire les lettres de congé qui ont été délivrées dans le bureau de départ, là où le
      chargement fut effectué. Les <ref target="#commis"> commis </ref> de la Ferme vérifient la
      conformité de la lettre de congé qui décrit la nature de la marchandise, le nom et l’adresse
      de l’expéditeur et le destinataire de la cargaison.Louis Sébastien Mercier résume parfaitement
      avec sa verve habituelle ce que les habitants de la capitale pensent des barrières et des
      employés de la Ferme : « elles [les barrières] sont communément de sapin, et rarement de fer ;
      mais elles pourraient être d’or massif, si ce qu’elles rapportent avait été employé à les
      faire de ce métal. Aux barrières, un commis en redingote, qui gagne cent misérables pistoles
      par an, l’œil toujours ouvert, ne s’écartant jamais d’un pas, et qui verrait passer une
      souris, se présente à la portière de chaque équipage, l’ouvre subitement, et vous dit,
      n’avez-vous rien contre les ordres du roi ? […] ». <ref target="#Lavoisier"> Lavoisier </ref>
      , le célèbre chimiste et Fermier général, évalue le personnel des Fermes à plus de 28 000
      individus sur tout le territoire. A Paris, le total des employés, dans les années <date
       when="1770"> 1770 </date> , atteint 656 personnes avec, à leur tête, le directeur général des
      entrées de Paris. Un petit nombre est affecté au siège à l’Hôtel de Bretonvilliers où les
      employés supérieurs et subalternes exercent des tâches de direction, de vérification et
      d’inspection. Au bas de l’échelle, les « petits commis forment une classe innombrable : ils ne
      sont pas chers ; leurs appointements sont de huit, douze et quinze cens livres […] ». Ce sont
      ces personnages que le particulier rencontre aux barrières, avec lesquels il a souvent des
      démêlés ; l’habitant essaie de frauder ou se heurte violemment aux employés, au risque de se
      retrouver en prison à la Conciergerie. <span subtype="contestation"
       type="normes_contestations"> Les commis, régulièrement, se déplacent, quittent leurs bureaux
       et effectuent des <ref target="#visites"> visites </ref> domiciliaires au-delà des limites de
       la ville, rentrent dans les tavernes et cabarets à la recherche de produits prohibés ou en
       fraude de droits </span> . Ces employés de base, de terrain, forment plus de 80% de
      l’effectif de l’octroi à Paris : 460 sont affectés directement dans les bureaux de l’octroi.
      Dans chaque bureau, sept individus en moyenne y travaillent, mais ceux-ci sont plus nombreux
      dans les bureaux de recette dans lesquels on trouve un brigadier, un sous-brigadier et de
      simples commis. A leur intégration dans les services de la Ferme, on leur demande (tout du
      moins au <ref target="#brigadier"> brigadier </ref> et au sous-brigadier) de savoir lire et
      écrire ; ils auront, notamment, la tâche essentielle et l’obligation de rédiger les <ref target="#procès-verbaux">
        procès-verbaux </ref> de fraude ou de contrebande ou, plus simplement, de remplir les
       registres entreposés dans les bureaux, car «  tout se fait la plume à la main : dans
      le plus petit état, il faut savoir écrire et chiffrer ; on constate sur un auguste registre,
      l’entrée d’une bouteille de vin et d’un chapon, ainsi que celle d’un tonneau, et d’un troupeau
      de bœufs. On vous en donne quittance : toute la science de ces scribes consiste à savoir faire
      des bordereaux. Ces commis ne savent rien, ne connaissent rien, n’ont idée de rien ; ils
      nivellent des chiffres avec une routine journalière ». Au-dessus d’eux, hiérarchiquement, une
      dizaine d’employés, les contrôleurs <ref target="#ambulants"> ambulants </ref> (à pied ou à
      cheval) patrouillent ; leurs « fonctions sont de parcourir un certain nombre de bureaux, pour
      voir s’il ne s’y passe rien contre l’intérêt public contre celui des droits du roi ».
      Les receveurs et les contrôleurs des barrières viennent compléter le dispositif de
      surveillance et d’inspection. Les premiers tiennent les <ref target="#registres"> registres
      </ref> des recettes, indiquent la nature des marchandises et collectent les fonds
      quotidiennement. <span subtype="contestation" type="normes_contestations"> Les seconds, les
        <ref target="#contrôleurs"> contrôleurs </ref> , ont un rôle primordial dans la recherche de
       la fraude et ils doivent connaître le quartier dont ils ont la charge </span> ; ce sont eux
      qui prescrivent aux employés les lieux à inspecter afin de suivre à la trace les mouvements
      suspects dans les faubourgs et ont un œil sur les préposés de la Ferme qui ne font pas
      toujours preuve d’une scrupuleuse honnêteté, étant de connivence avec certains fraudeurs.
       L’ampleur de cette fraude engagea les autorités à opter pour la construction d’un <ref target="#mur"> mur </ref> à la veille de
       la Révolution. Les droits qui frappaient la consommation à l’entrée des villes furent
      fixés par l’ <ref target="#Ordonnance"> Ordonnance </ref> des <ref target="#Aides"> Aides </ref> de <date when="1680"> 1680 </date> en une seule catégorie,
       simplifiant les règlements complexes. Une quarantaine d’années plus tard (octobre
       <date when="1719"> 1719 </date> ), les droits sur la vente en gros et en détail furent
      rassemblés et devinrent des droits d’entrée. On distinguait : les droits sur les boissons
      (vin, vin de liqueur, eau-de-vie, cidre, poiré, bière, vinaigre, verjus…) ; les droits sur le
      « bestial à pied-fourché » (bœufs, vaches, moutons, veaux, porcs vifs ou morts…) ; ceux perçus
      sur les poissons d’eau douce et de mer ; sur le bois ; sur les suifs et chandelles ; sur les
      cendres gravelées (lie séchée du vin appelée gravelée, utilisée dans les teintures). Les
      droits de domaine et barrage, également encaissés aux entrées de Paris, existaient depuis fort
      longtemps ; le barrage, initialement perçu pour l’entretien des pavés, préfigurait la barrière
      comme obstacle puisque les commis de la Ferme plaçaient des barres au passage des produits
      avant que les droits soient acquittés. Il faut y ajouter le Poids-le-roi qui se levait
      conjointement avec les droits de domaine et barrage ; comme son nom le suggère, le
      Poids-le-roi était « une rétribution payée pour la pesée des marchandises » et cette taxe
      s’appliquait sur les drogueries, les épices et bien d’autres marchandises, en fonction de leur
      poids. Les produits manufacturés ou les marchandises ouvrées (draps, tapisseries, soie filée,
      chapeaux, bas…) étaient concernés par ces taxes ; les divers matériaux n’échappaient pas non
      plus à la taxation (fer, plomb, étain, tuiles, briques, ardoises, plâtre…) ; enfin, les
      produits de première nécessité (œufs, beurre, fromage, sucre, café, légumes, fruits…) étaient
      imposés pareillement. Les impôts perçus sur tous ces objets de consommation sont ensuite
      répartis entre trois institutions : la Ferme générale, la <placeName type="lieux_habitations">
       ville de Paris </placeName> et l’Hôpital général, la Ferme encaissant en moyenne les ¾ du
      total des droits. </def>
     <bibl type="references">
      <bibl> [ <idno type="ArchivalIdentifier"> AN, Q1 1101, Chronologie des lois et opérations sur
        les limites ; </idno>
      </bibl>
      <bibl> <idno type="ArchivalIdentifier">BNF, F-21046 (27), Déclaration…du 26 avril 1672 pour le recouvrement du dixième denier
       de la valeur des clostures et bastiments faits dans les fauxbourgs de Paris, au-delà des
       bornes plantées […], Registrée en Parlement le 30 avril 1672,
       https://gallica.bnf.fr/ark:/12148/btv1b8601553q ; </idno></bibl>
      <bibl> <idno type="ArchivalIdentifier">BNF, GE SH 18 PF 37 DIV 3 P 60, Paris en 1672 : fac-similé du premier plan de Jouvin de
       Rochefort, réduction de ¼ (extrait), https://gallica.bnf.fr/ark:/12148/btv1b53010929s ;</idno> </bibl>
      <bibl> <idno type="ArchivalIdentifier">BNF, F-21097 (21), Déclaration […] qui règle les limites de la ville de Paris […],
       Registrée en Parlement le 4 août 1724, http://catalogue.bnf.fr/ark:/12148/cb338363409 ;</idno> </bibl>
      <bibl><idno type="ArchivalIdentifier"> BNF, F-21171 (21), Déclaration du roi, qui fixe les limites de la ville et faubourgs de
       Paris, donnée à Marli le 16 mai 1765, https://gallica.bnf.fr/ark:/12148/btv1b86137480 ;</idno> </bibl>
      <bibl><idno type="ArchivalIdentifier"> BNF, 8-LB40-1253, Tableau des droits d’entrées, qui se percevaient aux barrières de
       Paris sur les principales denrées, boissons et marchandises, et qui ont été supprimés par les
       décrets de l’Assemblée nationale, sanctionnés par le roi] ; </idno></bibl>
      <bibl><idno type="ArchivalIdentifier"> BNF, LK7-6007, Description de la ville et des faubourgs de Paris en vingt planches,
       dont chacune représente un des vingt quartiers, suivant la division qui en a été faite par la
       déclaration du roi du 12 décembre 1702 […] ; </idno></bibl>
      <bibl type="sources"> Collection de décisions nouvelles et de notions relatives à la […], Volume 7,
       « entrées », p. 709 ; </bibl>
      <bibl type="sources"> Dictionnaire universel ;contenant généralement tous les mots françois […], volume 3, « octroi », édition de
       1727 ; </bibl>
      <bibl type="sources"> Dictionnaire universel françois et latin, vulgairement appelé dictionnaire de Trévoux
       […], édition de 1771, « octroi », p. 294 ; </bibl>
      <bibl type="sources"> Encyclopédie Méthodique. Finances. Volume 1, Panckouke, 1784, « droits », p. 649 et
       suivantes ; </bibl>
      <bibl type="sources"> Encyclopédie Méthodique. Finances. Volume 3, « octroi », p. 240 ; </bibl>
      <bibl> Encyclopédie méthodique. Jurisprudence. Police et municipalités, 1791, tome 10,
       « Paris », p. 615 ; </bibl>
      <bibl type="sources"> Lefebvre De La Bellande, Traité général des droits d’aides, Pierre Prault, 1760 ; </bibl>
      <bibl type="sources"> Robert de Hesseln, Dictionnaire universel de la France, contenant la description
       géographique et historique des provinces, villes… […], Desaint, 1771, volume 5, « Paris », p.
       89, « barrières », p. 110-111 ; </bibl>
      <bibl type="sources"> Jacques Savary des Brûlons, Philémon-Louis Savary, Dictionnaire universel du commerce,
       concernant tout ce qui concerne […], volume 1, Estienne et fils, édition de 1748,
       « barrières », p. 866 ; </bibl>
      <bibl type="sources"> Antoine-Laurent de Lavoisier, Œuvres, t. VI, « Etat des employés attachés à la
       Ferme générale », p. 155-157, http://www.lavoisier.cnrs.fr/ (édition électronique) ; </bibl>
      <bibl type="sources"> Louis Sébastien Mercier, Tableau de Paris, tome 2, 1782, chapitre CXL, « commis » ; </bibl>
      <bibl> Louis-Sébastien Mercier, Tableau de Paris, tome 2, 1782, chapitre CXXIII,
       « barrières » ; </bibl>
      <bibl type="sources"> Louis Sébastien Mercier, Tableau de Paris, tome 10, 1782, chapitre DCCLXXXII, « foin ».
       [Béatrice de Andia (dir.), Les enceintes de Paris, Action artistique de la ville de Paris,
       2001 ; </bibl>
      <bibl> Anne Conchon, Hélène Noizet, Michel Ollion (dir.), Les limites de Paris
       (XIIe-XVIIIe siècles), Septentrion, 2017 ; </bibl>
      <bibl> Marie-Laure Legay (dir.), Les modalités de paiement de l’Etat moderne. Adaptation et
       blocage d’un système comptable, Institut de la gestion publique et du développement
       économique, Paris, 2007 ; </bibl>
      <bibl> Jeanne Pronteau et Isabelle Dérens, Introduction au travail des limites de la ville et
       des faubourgs de Paris (1724-1729), Paris Musées, 1998 ; </bibl>
      <bibl> Momcilo Markovic, Paris brûle ! L’incendie des barrières de l’octroi en juillet 1789,
       L’Harmattan, 2019, p. 75-96 ; </bibl>
      <bibl> Françoise Michaud-Fréjaville, Noëlle Dauphin et Jean-Pierre Guilhembet (dir.), Entrer
       en ville, PUR, 2006 ; </bibl>
      <bibl> Michel Le Moël, Sophie Descat, Béatrice de Andia, L’urbanisme parisien au siècle des
       Lumières, Action artistique de la ville de Paris, 1997 ; </bibl>
      <bibl> Elphège Frémy, « L’Enceinte de Paris construite par les Fermiers généraux et la
       perception des droits d’octroi de la ville, 1784-1791 », dans Bulletin de la Société de
       l’histoire de Paris et de l’île de France, 1912, année 39, p. 115-148 ; </bibl>
      <bibl> Aline Logette, « La Régie générale au temps de Necker et de des successeurs
       (1777-1786) », dans Revue historique de droit français et étranger, vol. 60, n°3, 1982, p.
       415-445 ; </bibl>
      <bibl> Momcilo Markovic, Paris brûle ! L’incendie des barrières de l’octroi en juillet 1789,
       Paris, L’Harmattan, 2019].; </bibl>
     </bibl>
    </sense>
   </entry>
  </body>
 </text>
</TEI>
