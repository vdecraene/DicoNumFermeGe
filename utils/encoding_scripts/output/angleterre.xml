<?xml version="1.0" encoding="utf-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title type="notice">Angleterre</title>
    <author>Marie-Laure Legay</author>
   </titleStmt>
   <publicationStmt>
    <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer le privilège la Ferme
     générale dans l'espace français et européen (1664-1794)Axe 1 : Dictionnaire de la Ferme
     générale, objet d'histoire totale </publisher>
    <pubPlace>
     <address>
      <addrLine> Maison Européenne des Sciences de l'Homme et de la Société </addrLine>
      <addrLine> 2 rue des cannoniers </addrLine>
      <addrLine> 59002 Lille Cedex </addrLine>
     </address>
     <date> 2021-2025 </date>
    </pubPlace>
    <availability>
     <licence> [A déterminer. Ex:Creative Commons Attribution 3.0 non transposé (CC BY 3.0)]
     </licence>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title> Dictionnaire numérique de la Ferme générale </title>
    <respStmt>
     <resp> Coordinateurs de l'axe : Dictionnaire numérique de la Ferme générale, objet d'histoire
      totale. </resp>
     <persName> Marie-Laure Legay </persName>
     <persName> Thomas Boullu </persName>
    </respStmt>
   </seriesStmt>
   <sourceDesc>
    <p> Dictionnaire numérique de la Ferme générale </p>
   </sourceDesc>
  </fileDesc>
  <encodingDesc>
   <projectDesc
    source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
    <p> Le projet FermGé vise à étudier l’impact d’une organisation fiscale (1664-1794),
     discriminante mais rationnelle, sur les territoires et les sociétés de la France moderne/p&gt;
    </p>
   </projectDesc>
  </encodingDesc>
  <profileDesc>
   <textClass>
    <keywords scheme="#fr_RAMEAU">
     <list>
      <item>Histoire -- Histoire moderne -- Histoire commerciale</item>
      <item>Histoire -- Histoire moderne -- Histoire fiscale</item>
     </list>
    </keywords>
   </textClass>
  </profileDesc>
  <revisionDesc>
   <change type="AutomaticallyEncoded"> 2022-12-12T13:37:17.924808+2:00 </change>
  </revisionDesc>
 </teiHeader>
 <text>
  <body>
   <entry type="A" xml:id="Angleterre">
    <form type="lemma">
     <orth> Angleterre </orth>
    </form>
    <sense>
     <def> Les relations commerciales entre la France et l’Angleterre ont été très largement
      étudiées. Au cours des XVIIe et XVIIIe siècles, elles étaient basées sur des régimes
      protectionnistes sur fonds d’hostilité politique. Les deux royaumes se sont engagés dans une
      guerre d’argent particulièrement rude. Les lois prohibitives qui en découlèrent favorisèrent
      le développement de la <ref target="#contrebande"> contrebande </ref> à grande échelle.
      François Crouzet en a tracé les principaux trafics tant de la France vers l’Angleterre que de
      l’Angleterre vers la France. La Ferme générale demeura impuissante à contenir cette économie
      souterraine. <span subtype="contestation" type="normes_contestations"> Comme dans le cas de la
       contrebande d’ <ref target="#indiennes"> indiennes </ref> et de de <ref target="#tabacs">
        tabacs </ref> organisées depuis la <ref target="#Suisse"> Suisse </ref> ou la <ref
        target="#Savoie"> Savoie </ref> , les trafics illégaux qui traversaient les provinces
       septentrionales et animaient la façade maritime depuis <ref target="#Dunkerque"> Dunkerque
       </ref> jusqu’à <ref target="#Bordeaux"> Bordeaux </ref> corrompaient les gardes côtes et les
        <ref target="#commis"> commis </ref> de la Ferme générale </span>. Les deux gouvernements
      furent constants dans leur législation protectionniste ( <ref target="#prohibition">
       prohibition </ref> ou droits de douane exorbitants), hormis une brève période plus détendue à
      la fin de la guerre de Succession d’Espagne ( <date when="1713"> 1713 </date> ). Côté anglais,
      le Parlement de Londres instaura des barrières contre la production française en <date
       when="1678"> 1678 </date> , interdictions renouvelées en <date when="1689"> 1689 </date> et
       <date when="1705"> 1705 </date> . Côté français Colbert imposa des droits prohibitifs contre
      les draps anglais dès <date when="1664"> 1664 </date> . Le régime douanier se stabilisa en
       <date when="1701"> 1701 </date> (arrêt du 6 septembre) en prohibant une grande partie de la
      fabrication anglaise. <span subtype="normes" type="normes_contestations"> La liste établie à
       cette époque évolua sensiblement, mais la loi resta très contraignante vis-à-vis de
       l’importation des produits textiles, des produits métalliques ou stratégiques, des articles
       de mode, des produits concurrençant directement la production française </span> . <span
       subtype="normes" type="normes_contestations"> Voici, par ordre alphabétique, les marchandises
       qui furent concernées par la <ref target="#prohibition"> prohibition </ref> totale (AN, G1
       79, dossier 22) </span> : les bas ( <date when="1701"> 1701 </date> ), la bière en futailles
      (excepté en bouteilles) : arrêt du 11 juillet <date when="1737"> 1737 </date> , les boutons de
      soie, crin et autres matières ( <date when="1701"> 1701 </date> ), les boutons appelés
      pinsbeek (décision du 25 janvier <date when="1740"> 1740 </date> ), le brai ou goudron végétal
      des colonies anglaises (décision du 22 mars <date when="1719"> 1719 </date> et arrêts du 3
      avril et 26 juillet <date when="1723"> 1723 </date> ), le chanvre de toutes espèces (décision
      du 7 avril <date when="1753"> 1753 </date> ), les chapeaux de quelques natures et qualités
      qu’ils fussent (arrêt du 6 septembre <date when="1701"> 1701 </date> ) ; le cidre (ordonnance du 11 juillet 
      <date when="1737"> 1737 </date> ), les cornes claires (décision du 19 mai <date when="1764">
       1764 </date> ) ; les coutelleries de toutes sortes ( <date when="1701"> 1701 </date> ), les
      couvertures de laine ( <date when="1701"> 1701 </date> ), les drogueries et <ref
       target="#épiceries"> épiceries </ref> ( <date when="1701"> 1701 </date> ), l’étain ouvré ou
      laminé (arrêt du 6 septembre <date when="1701"> 1701 </date> , 20 mai <date when="1738"> 1738
      </date> et décision du 12 janvier <date when="1739"> 1739 </date> portant que cette prohibition ne concerne
       que l’Angleterre), les étoffes ( 
      <date when="1701"> 1701 </date> ), les faïences et poteries (décision du 16 août <date
       when="1740"> 1740 </date> , arrêt du 12 mars <date when="1743"> 1743 </date> et confirmation
      du 14 mars <date when="1770"> 1770 </date> ),  les gants ( <date when="1701"> 1701 </date> ),
      les habits vieux et neufs (décision du 17 décembre <date when="1716"> 1716 </date> ), le linge
      de table ouvré et non ouvré (arrêt du 8 janvier <date when="1754"> 1754 </date> et circulaire
      de la compagnie du 7 février suivant), les merceries de toutes sortes ( <date when="1701">
       1701 </date> ), le plomb ouvré ou laminé (arrêt du 6 septembre <date when="1701"> 1701
      </date> , 20 mai <date when="1738"> 1738 </date> et 15 février <date when="1758"> 1758 </date>
      ), la quincaillerie de toutes sortes ( <date when="1701"> 1701 </date> ), les rubans de soie,
      de laine et fil ( <date when="1701"> 1701 </date> ), les serrureries ( <date when="1701"> 1701
      </date> ) ou encore les vins et liqueurs ( <date when="1701"> 1701 </date> ). Pour certaines
      productions stratégiques comme l’ <ref target="#étain"> étain </ref> non ouvré ou l’acier dont
      les manufactures françaises avaient besoin, la législation varia. Encore en <date when="1785">
       1785 </date> , la décision du 5 janvier fixa les marchandises de fer ou d’acier permises et
      les droits qu’elles devaient porter. Le régime prohibitif pouvait concerner également les
      produits qui étaient entièrement à l’imitation des marchandises anglaises comme la faïence de
      la manufacture de Bruges prohibée le 7 mars <date when="1770"> 1770 </date> . Inversement,
      pour contourner la loi, les Anglais faisaient passer leurs marchandises pour hollandaises,
      comme les <ref target="#cuirs"> cuirs </ref> . Dans le cas des harengs saurés et morues
      séchées par exemple, le trafic était si important qu’il fallut systématiquement convertir les
      arrivages hollandais comme pêche anglaise taxée très lourdement ( <date when="1746"> 1746
      </date> ). Le régime douanier de <date when="1701"> 1701 </date> interdisait enfin à tout
      navire anglais d’entrer dans un port français avec des cargaisons qui n’étaient pas du cru de
      l’Angleterre. En <date when="1772"> 1772 </date>
       par exemple, le Conseil de la Ferme fit un
       rappel à l’ordre au <ref target="#directeur"> directeur </ref> de la Ferme à Amiens
       concernant l’entrée de blés du <ref target="#Levant"> Levant </ref> et de Barbarie sur des
       vaisseaux anglais  .Etant donné un tel régime douanier, la <ref target="#contrebande">
       contrebande </ref> qui s’ensuivit fut massive. <span subtype="contestation"
       type="normes_contestations"> Dès le début du XVIIIe siècle, des produits manufacturés anglais
       passaient en France en fraude des droits de la Ferme générale </span> . Les commis saisirent
      en février <date when="1702"> 1702 </date> quatre ballots de bas de laine prohibés et trente
      ballots de peaux de veau aux portes de Grézac en <ref target="#Saintonge"> Saintonge </ref> .
      Les marchandises de <ref target="#contrebande"> contrebande </ref> passaient notamment par les
       <ref target="#Provinces-Unies"> Provinces-Unies </ref> et les <ref target="#Pays-Bas">
       Pays-Bas </ref> , et de là dans les provinces françaises « réputés étrangères ». Les
      marchands de <orgName subtype="province" type="pays_et_provinces">
       <ref target="#Lorraine"> Lorraine </ref>
      </orgName> introduisaient des draps de laine par les <orgName subtype="province"
       type="pays_et_provinces"> Trois-Evêchés </orgName> , ou bien les contrebandiers faisaient
      passer lainages et cotonnades par Ostende, puis dans les <orgName subtype="province"
       type="pays_et_provinces"> Flandre </orgName>
      <ref target="#s"> s </ref> et le <orgName subtype="province" type="pays_et_provinces">
       <ref target="#Hainaut"> Hainaut </ref>
      </orgName> par Bruxelles ou Tournai. <span subtype="privileges" type="privleges_encadrement">
       <ref target="#Dunkerque"> Dunkerque </ref> , port franc de la Manche, devint le théâtre d’une
       activité semi-souterraine notoire </span> . Toute la côte normande fut également animée par
      ces trafics. Ces derniers s’intensifièrent sous l’effet de l’évolution des goûts. <span
       subtype="contestation" type="normes_contestations"> Le <ref target="#tabac"> tabac </ref> de
       la baie de Chesapeake introduit en fraude par mer ou par terre représentait quasiment 20
       % de la consommation française d’après Jacob M. Price.</span> Au total, la contrebande des produits anglais aurait représenté
       un marché de 16 millions de livres tournois juste avant le traité commercial de 
      <date when="1786"> 1786 </date>.
       Dans le sens France-Angleterre, la contrebande des produits coloniaux devint également
       massive  . Le <ref target="#thé"> thé </ref> alimenta un trafic illégal
      particulièrement intense entre <date when="1768"> 1768 </date> (taxe britannique fixée à 64 %
      de sa valeur), <date when="1780"> 1780 </date> (106 %) et <date when="1784"> 1784 </date> ,
      date du Commutation Act qui ramena les droits à 12,5 %. <span subtype="contestation"
       type="normes_contestations"> Cette contrebande était encouragée par les autorités </span> .
      Pour la faciliter, on accorda à la compagnie des <ref target="#Indes"> Indes </ref> un droit
      d’entrepôt pour une partie des thés prévus pour cette destination, sous couvert d’être
      destinés au royaume. Les thés destinés au royaume étaient en effet initialement entreposés à
      Boulogne, Calais, Morlaix et Saint-Malo, mais le droit d’entrepôt fut étendu à <ref
       target="#Dieppe"> Dieppe </ref> , Grandville, Cherbourg, Le <ref target="#Havre"> Havre
      </ref> , Fécamp et Saint-Valéry sur Somme, ces ports s’étant dit pareillement « à portée de faire le commerce de
       contrebande en Angleterre  ». Dans ces entrepôts, on tenait un compte de tous les thés
      qui paraissaient être sortis et l’on demandait aux négociants les droits sur la base du
      compte. <span subtype="encadrement" type="privleges_encadrement"> Pour une plus grande
       fluidité comptable, la compagnie des <ref target="#Indes"> Indes </ref> proposa à la Ferme
       générale une sorte d’abonnement à ces droits </span>. Des relevés furent donc effectués sur
      les six années du bail Bocquillon et les six années du bail Henriet pour connaître le rapport
      des droits sur les thés, à partir des registres des bureaux de la compagnie des Indes de
      Lorient et de <ref target="#Nantes"> Nantes </ref> : Droits perçus année commune du bail
      Bocquillon en temps de paix : 50 555 livres ; droits restitués pour les thés sortis pour
      l’Angleterre : 23 212 livres ; produit net pour la Ferme générale : 27 343 livres ; Pour le
      bail Henriet (temps de guerre) : année commune : 4613 livres, pour
      les restitutions : 5 326 livres, soit une restitution supérieure au produit de 713 livres.
       <span subtype="encadrement" type="privleges_encadrement"> L’abonnement fut convenu le 30
       septembre 
      <date when="1766"> 1766 </date> entre le duc de Duras et les membres de la compagnie des <ref
       target="#Indes"> Indes </ref> d’une part, et les fermiers généraux Etienne Gigault de
      Crisenoy, Roslin, Jacques Roussel d’Erigny, Charles Mazières, Jean-Baptiste Fournier,
      Ganthier, Faventiner : 30 000 livres par an pour les deux dernières années du bail Prévost,
      puis 15 000 livres par an pour le bail suivant.</span> Moyennant quoi, les thés provenant de la
      Compagnie des <ref target="#Indes"> Indes </ref> tirés de l’étranger pour garnir les ventes d’
       <ref target="#Angleterre"> Angleterre </ref> restèrent sujets aux droits de 10 sous par livre
      (arrêt du 6 aout <date when="1726"> 1726 </date> ) à l’arrivée, comme les thés qui venaient
      sans passer par la Compagnie. <span subtype="encadrement" type="privleges_encadrement"> Ce
       principe de l’abonnement fut également utilisé pour le <ref target="#café"> café </ref>
      </span> .Outre le thé, les contrebandiers de l’Essex, du Kent ou du Sussex venaient chercher
      le <ref target="#café"> café </ref> , les étoffes de Rouen ou de <ref target="#Lyon"> Lyon
      </ref> , les batistes de Saint-Quentin ou Cambrai, les <ref target="#eaux-de-vie"> eaux-de-vie
      </ref> , mais aussi les <ref target="#tabacs"> tabacs </ref> de réexportation… <ref
       target="#Bayonne"> Bayonne </ref> , <ref target="#Bordeaux"> Bordeaux </ref> , Lorient, les
      îles anglo-normandes, toute la côte bretonne, normande, picarde et flamande recevaient les
      trafiquants anglais. D’après les commissionnaires des douanes au Parlement de Londres, cette
      activité tripla entre <date when="1780"> 1780 </date> et <date when="1783"> 1783 </date> .
      Elle aurait représenté en valeur à cette date 50 à 70 millions de livres tournois. Elle était
      encouragée par les autorités françaises, notamment, à <ref target="#Dunkerque"> Dunkerque
      </ref> par les principaux représentants à la Chambre de commerce, négociants volontaires pour
      alimenter le trafic des smoogleurs. Les actions de la Ferme générale pour juguler ce commerce
      demeurèrent de faible portée. A <ref
        target="#Dieppe"> Dieppe </ref> , les employés constatèrent par exemple une fraude sur les
       marchandises déclarées pour Portsmouth lors de leur <ref target="#visite"> visite </ref> du
       21 avril 
      <date when="1752"> 1752 </date> sur le navire anglais La Paix appartenant à Samuel Moore.
        Ils procédèrent à la <ref
        target="#saisie"> saisie </ref> du navire et à la confiscation des marchandises restantes
       . Les commis de la Ferme étaient intéressés au partage du produit des <ref
       target="#saisis"> saisis </ref> . Il n’est pas sûr toutefois que cet intéressement fût à la
      hauteur des espoirs de gain par la corruption.Le traité d’Eden-Rayneval (septembre <date
       when="1786"> 1786 </date> ) fut précédé d’un durcissement des relations commerciales entre la
      France et l’ <ref target="#Angleterre"> Angleterre </ref> . Durant la guerre d’Indépendance
      d’Amérique ( <date when="1778"> 1778 </date> - <date when="1783"> 1783 </date>
      <span subtype="contestation" type="normes_contestations"> ), la contrebande atteignit un point
       culminant </span> . L’arrêt du 17 juillet <date when="1785"> 1785 </date>
      <span subtype="normes" type="normes_contestations"> confirma toutes les <ref
        target="#prohibitions"> prohibitions </ref> générales prononcées par les ordonnances et
       règlements rendus depuis </span>
      <date when="1687"> 1687 </date> . <span subtype="normes" type="normes_contestations">
       L’article 2 de cet arrêt prohiba les marchandises anglaises autres que celles annexées qui
       pouvaient entrer en payant les droits fixés par l’arrêt du 6 septembre 
      <date when="1701"> 1701 </date> et suivants</span>. Etaient autorisés à entrer légalement : chevaux,
      laines, cuirs verts, peaux de bœuf, peaux de veau, poils de vache, suifs de toute espèce, cire
      jaune, cire blanche, charbon de terre, chairs salées, bière en bouteille seulement, colle dite
      d’Angleterre, corne ronde ou plate, dents d’éléphant, couperose, drogues servant à la
      teinture, instruments et outils propres aux arts, meules à taillandier, étain non ouvré, bois
      de construction, bois feuillards, bois merrains, futailles (les bois venant d’Angleterre ou
      des colonies anglaises). Une décision du 28 avril <date when="1786"> 1786 </date> défendit à
      tous les employés de la Ferme générale de transiger sur les <ref target="#saisies"> saisies
      </ref> de prohibé. Les propositions d’ <ref target="#accommodement"> accommodement </ref>
      devaient être transmises au <ref target="#directeur"> directeur </ref> et le <ref
       target="#directeur"> directeur </ref> en donnait connaissance à la compagnie.Les marchandises
      du cru ou de la fabrique d’Angleterre autorisées à entrer furent admises dans tous les ports
      et tous les bureaux de la Ferme générale par le traité du 26 septembre <date when="1786"> 1786
      </date> . <span subtype="normes" type="normes_contestations"> L’article 7 du traité portait
       que les marchandises anglaises qui n’y étaient pas énoncées devaient être soumises à leur
       entrée dans le royaume aux mêmes droits qu’acquittaient celles apportées par les autres
       nations européennes </span> . La Ferme générale diffusa ses instructions pour faire face aux
      nouvelles conditions. Dans la <placeName type="lieux_habitations"> ville de Dunkerque
      </placeName> , les marchandises devaient être accompagnées d’un certificat d’origine ou d’un
      acquit de la douane anglaise ; sur cet acquit, la chambre de commerce délivrait un certificat
      visé par l’ <ref target="#intendant"> intendant </ref> ou son subdélégué, lequel le remettait
      au bureau des <ref target="#traites"> traites </ref> établi dans la basse-ville, hors de la
      franchise, pour servir à l’entrée dans le royaume (arrêt du 15 juin <date when="1787"> 1787
      </date> ). Il fallut également prévoir un surcroît d‘employés et les matrices nécessaires pour
      apposer les plombs non seulement à <ref target="#Dunkerque"> Dunkerque </ref> , mais aussi à
      Saint-Valéry-sur-Somme, Lorient, <ref target="#Bayonne"> Bayonne </ref> , <ref
       target="#Marseille"> Marseille </ref> et Toulon. Les marchandises ne devaient pas être
      retardées par les formalités d’entrée plus de quinze jours. Le traité de libre-échange
      Eden-Rayneval ( <date when="1786"> 1786 </date> ) semble voir davantage profité à l’Angleterre
      qu’à la France. Les importations de produits manufacturés anglais vers le continent
      augmentèrent sensiblement en valeur : 16 millions de livres en <date when="1784"> 1784 </date>
      , 64 millions en <date when="1788"> 1788 </date> . </def>
     <bibl type="references">
      <bibl> [ <idno type="ArchivalIdentifier"> AD Somme, 1C 2927, correspondance du directeur des
        Fermes, copie de la lettre reçue de la compagnie 18 septembre 1772 ; </idno>
      </bibl>
      <bibl>
       <idno type="ArchivalIdentifier"> AN, G1 79, dossier 21 : « état des marchandises dont
        l’entrée est fixée par certains bureaux », « état des marchandises d’Angleterre dont
        l’entrée est défendue dans le royaume ; dossiers 22 et 23 ; </idno>
      </bibl>
      <bibl>
       <idno type="ArchivalIdentifier"> AN G1 80, dossier 10, Mémoire sur les thés ; </idno>
      </bibl>
      <bibl>
       <idno type="ArchivalIdentifier"> AN, G7 1147 : procès-verbal de saisie, 6 février 1702 ;
       </idno>
      </bibl>
      <bibl type="sources"> Arrêt du Conseil du roi, 6 septembre 1701 ; </bibl>
      <bibl type="sources"> Arrêt du Conseil d’Etat du roi portant réduction des droits d'entrée sur
       le charbon de terre venant des états de la Grande-Bretagne, 25 août 1716 ; </bibl>
      <bibl type="sources"> Arrêt du Conseil d’Etat qui défend itérativement l'entrée dans le
       royaume des cuirs de la fabrique de la Grande-Bretagne ; fixe les ports par où les cuirs d'autres fabriques étrangères pourront entrer en
       Normandie et Picardie, 26 mars 1718 ; </bibl>
      <bibl type="sources"> Arrêt du Conseil d’Etat qui ordonne qu'il sera perçu 30 sols sur chaque
       barril de charbon de terre venant d’Angleterre, d’Ecosse et d’Irlande et entrant par
       Saint-Vallery, Dunkerque, Boulogne, Calais et autres entrées de la Picardie et de la Flandre,
       6 juin 1741 ; </bibl>
      <bibl type="sources"> Arrêt du Conseil d’Etat concernant l'entrée des harengs saurés et morues
       sèches venant de Hollande, 10 septembre 1746 ; </bibl>
      <bibl type="sources"> Arrêt du Conseil d’Etat qui casse une sentence de l'amirauté de Dieppe
       du 5 juillet précédent, pour avoir fait mainlevée du navire de Londres La Paix, saisi par les
       employés des fermes à Dieppe, sur le capitaine Moore, Anglais et confisque ledit navire, 8
       août 1752 ; </bibl>
      <bibl type="sources"> « Projet de traité de commerce entre la France et l’Angleterre, avec quelques notes de
       Colbert, daté de 1669 », dans Lettres, instructions et mémoires de Colbert, publiées par
       Pierre Clément, t. 2, IIe partie. Industrie, commerce, Paris, Imprimerie impériale, 1863, p.
       803-814 ; </bibl>
      <bibl type="sources"> Louis-Joseph Plumard de Dangeul, Remarques sur les avantages et les désavantages de la
       France et de la Grande Bretagne par rapport au commerce et autres sources de la puissance des
       États. Traduction de l’anglois du chevalier John Nickolls. Seconde édition, Leyde, 1754 ; </bibl>
      <bibl type="sources"> Charles Whitworth, Commerce de la Grande-Bretagne et tableaux de ses importations et
       exportations progressives depuis l'année 1697 jusqu'à la fin de l'année 1773, Paris,
       Imprimerie royale, 1777 ; </bibl>
      <bibl type="sources"> Traité de navigation et de commerce entre la France et la
       Grande-Bretagne, Versailles, le 26 septembre 1786 ; </bibl>
      <bibl type="sources"> Jacques-Philibert Rousselot de Surgy, Encyclopédie méthodique. Finances,
       vol. 1, article « contrebande », Paris, 1784, p. 364-369 ; </bibl>
      <bibl type="sources"> Pierre-Samuel Dupont de Nemours, Lettre à la chambre du commerce de
       Normandie, sur le mémoire qu'elle a publié relativement au traité de commerce avec
       l’Angleterre, Paris, chez Moutard, 12 février 1788 ; </bibl>
      <bibl> Pierre Dardel, Navires et marchandises dans les ports de Rouen et du Havre au XVIIIe
       siècle, Paris, Sevpen, 1963 ; </bibl>
      <bibl> Jacob M. Price, France and the Chesapeake, Ann Arbor, University of Michigan Press,
       1973 ; </bibl>
      <bibl> Robert C. Nash, « The English and Scottish Tobacco Trades in the Seventeenth and
       Eighteenth Centuries : Legal and Illegal Trade », Economic History Review, XXXV, n°3, août
       1982; </bibl>
      <bibl> Antony D. Hippisley Coxe, A Book about Smuggling in the West Country, 1700-1850,
       Padstow, Tabb House, 1984 ; </bibl>
      <bibl> Mary Waugh, Smuggling in Kent and Sussex, 1700-1840, Newbury, Countryside, 1985 ; </bibl>
      <bibl> Christian Pfister-Langanay, Ports, navires et négociants à Dunkerque (1662-1792),
       Dunkerque, Société dunkerquoise, 1985 ; </bibl>
      <bibl> Stan Jarvis, Smuggling in East Anglia 1700-1840, Newbury, Countryside, 1987 ; </bibl>
      <bibl> John V. Nye, « Guerre, commerce, guerre commerciale. L’économie politique des échanges
       franco-anglais réexaminée », Annales ESC, XLVII, mai-juin 1992; </bibl>
      <bibl> Donald C. Wellington, «  The Anglo-French Commercial Treaty of 1786 », Journal of
       European Economic History, 21, n° 2, 1992 ; </bibl>
      <bibl> John Brewer et Roy Porter (dir.), Consumption and the World of Goods, Routledge,
       Londres et New-York, 1993 ; </bibl>
      <bibl> Maxime Berg, « French Fancy and Cool Britannia : the Fashion Market of Early Modern
       Europe », Proceedings of the Istituto Internazionale Di Storia Economica F. Datini, vol. 32,
       Prato, Le Monnier, 2001, p. 87-96 ; </bibl>
      <bibl> François Crouzet, « La contrebande entre la France et les îles britanniques au XVIIIe
       siècle », dans G. Béaur, H. Bonin, C. Lemercier, Fraude, Contrefaçon et contrebande de
       l’Antiquité à nos jours, Librairie Droz, 2007 ; </bibl>
      <bibl> Renaud Morieux, Une mer pour deux royaumes. La Manche, frontière franco-anglaise
       (XVIIe-XVIIIe siècles), Rennes, Presses universitaires de Rennes, 2008 ; </bibl>
      <bibl> « Roscoff, un important centre de contrebande entre la France et l’Angleterre à la fin
       du XVIIIe siècle », dans M. Figeac-Monthus et C. Lastécouères (dir.), Territoires de
       l’illicite : ports et îles, de la fraude au contrôle (XVIe-XXe siècle), Paris, A. Colin,
       2012 ; </bibl>
      <bibl> Gérard Le Bouëdec and Hiroyasu Kimizuka, « Lorient, grand port de dimension mondiale de
       la façade atlantique française (1783-1787) ? », Annales de Bretagne et des Pays de l’Ouest,
       126-1, 2019, p. 103-125] ; </bibl>
     </bibl>
    </sense>
   </entry>
  </body>
 </text>
</TEI>
