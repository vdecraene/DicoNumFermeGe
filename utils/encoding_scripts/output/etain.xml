<?xml version="1.0" encoding="utf-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title type="notice"> Etain </title>
    <author>Marie-Laure Legay</author>
   </titleStmt>
   <publicationStmt>
    <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer le privilège la Ferme
     générale dans l'espace français et européen (1664-1794)Axe 1 : Dictionnaire de la Ferme
     générale, objet d'histoire totale </publisher>
    <pubPlace>
     <address>
      <addrLine> Maison Européenne des Sciences de l'Homme et de la Société </addrLine>
      <addrLine> 2 rue des cannoniers </addrLine>
      <addrLine> 59002 Lille Cedex </addrLine>
     </address>
     <date> 2021-2025 </date>
    </pubPlace>
    <availability>
     <licence> [A déterminer. Ex:Creative Commons Attribution 3.0 non transposé (CC BY 3.0)]
     </licence>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title> Dictionnaire numérique de la Ferme générale </title>
    <respStmt>
     <resp> Coordinateurs de l'axe : Dictionnaire numérique de la Ferme générale, objet d'histoire
      totale. </resp>
     <persName> Marie-Laure Legay </persName>
     <persName> Thomas Boullu </persName>
    </respStmt>
   </seriesStmt>
   <sourceDesc>
    <p> Dictionnaire numérique de la Ferme générale </p>
   </sourceDesc>
  </fileDesc>
  <encodingDesc>
   <projectDesc
    source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
    <p> Le projet FermGé vise à étudier l’impact d’une organisation fiscale (1664-1794),
     discriminante mais rationnelle, sur les territoires et les sociétés de la France moderne/p&gt;
    </p>
   </projectDesc>
  </encodingDesc>
  <profileDesc>
   <textClass>
    <keywords scheme="#fr_RAMEAU">
     <list>
      <item> Histoire -- Histoire moderne -- Histoire fiscale </item>
     </list>
    </keywords>
   </textClass>
  </profileDesc>
  <revisionDesc>
   <change type="AutomaticallyEncoded"> 2022-12-15T09:23:21.565701+2:00 </change>
  </revisionDesc>
 </teiHeader>
 <text>
  <body>
   <entry type="E" xml:id="Etain">
    <form type="lemma">
     <orth> Etain </orth>
    </form>
    <sense>
     <def>
      <span subtype="normes" type="normes_contestations"> L’étain fut d’abord taxé par droit de
       marque ( <date when="1674"> 1674 </date> ), puis imposé par un droit d’entrée de deux sols
       six deniers par livre poids (arrêt du 18 juillet <date when="1676"> 1676 </date> et
       l’ordonnance du 22 juin <date when="1681"> 1681 </date> ) </span> . A cette date et jusqu’en
       <date when="1705"> 1705 </date> , seule la <orgName subtype="province"
       type="pays_et_provinces">
       <ref target="#Bretagne"> Bretagne </ref>
      </orgName> conserva le droit de marque d’étain, de sorte que ce métal paya un droit d’entrée
      dans le royaume au <placeName type="lieux_controle"> bureau d’Ingrandes </placeName> à
      l’arrivée de cette province. A Paris toutefois, le droit de marque sur l’étain ouvragé fut rétabli en <date when="1722"> 1722 </date> , à raison de 9
      deniers par livre. Martin Girard en avait la régie et rétablit donc des bureaux d’essai et de
      marque dans la capitale. Comme l’étain non ouvré était importé de l’étranger, c’est sur cette
      marchandise que se levèrent les droits les plus lucratifs : le tarif de deux sols six deniers
      d’entrée dans le royaume, mais aussi un droit de marque de 12 livres 10 sols par quintal, ce
      qui représentait 13 à 14 % de la valeur de l’étain étranger. Comme beaucoup de marchandises,
      l’étain ouvré et non ouvré d’ <ref target="#Angleterre"> Angleterre </ref> était <ref
       target="#prohibé"> prohibé </ref> (arrêts du 6 septembre <date when="1701"> 1701 </date> et
      du 5 janvier <date when="1716"> 1716 </date> ). Or, la Cornouailles était le principal
      producteur d’Europe. Le minerai venait donc pour l’essentiel d’Asie du Sud-Est par la
      compagnie des Indes hollandaise. <span subtype="normes" type="normes_contestations">
       Toutefois, la prohibition de l’étain anglais n’empêchait pas l’arrivée par les pays
       privilégiés comme <ref target="#Sedan"> Sedan </ref> , principauté à laquelle le roi de
       France reconnaissait la liberté du commerce et qui faisait venir de l’étain anglais depuis
       les <orgName subtype="administrations_juridictions_fermes"
        type="administrations_et_juridictions"> entrepôts de Hollande </orgName>
      </span> . De même, les potiers de Valenciennes tiraient en <ref target="#fraude"> fraude
      </ref> de Mons ou de Gand l’étain nécessaire à leurs manufactures. Surtout, le développement
      des manufactures d’étain nécessita de faire évoluer la législation fiscale. En <date
       when="1738"> 1738 </date> , on autorisa l’entrée de l’étain d’ <ref target="#Angleterre">
       Angleterre </ref> par tous les ports et bureaux en payant 4 livres en plus des droits de
      marque, soit 16 livres dix sols par quintal. L’étain étranger des autres régions du monde
      demeurait favorisé. L’étain en masse ou lingot venant des Indes occidentales sur des vaisseaux
      français ne devait point le droit de deux sols et six deniers pour livre (l’arrêt du 22 août
       <date when="1716"> 1716 </date> assujettit cet étain à 50 pour cent pesant pour tous droits
      d’entrée qui en tenaient lieu, même à la douane de <ref target="#Lyon"> Lyon </ref> ). Enfin,
      à partir de <date when="1762"> 1762 </date> , le droit spécifique de deux sols six deniers sur
      l’étain étranger fut supprimé, toujours en vue « d’être utile aux fabriques dur royaume ». </def>
     <bibl type="references">
      <bibl> [ <idno type="ArchivalIdentifier"> AN, G1 79, projet de modifications des droits de
        traites; </idno>
      </bibl>
      <bibl type="sources"> Arrêt du Conseil d’Etat qui ordonne que l'étain ouvré ou non ouvré
       venant du pays étranger ne pourra entrer dans la ville de Sedan, ni de ladite ville dans
       l'étendue des cinq grosses fermes, 5 mars 1718 ; </bibl>
      <bibl type="sources"> Arrêt du Conseil d'Etat servant de règlement pour la régie et perception
       du droit d'essayage, contrôle et marque sur l'étain ouvré, fin, sonnant et commun, rétabli
       dans Paris, 28 avril 1722 ; </bibl>
      <bibl type="sources"> Arrêt du Conseil d'Etat qui ordonne que les villes de Lille et
       Valenciennes seront ajoutées à celles par lesquelles l'entrée de l'étain dans le royaume a
       été permise par l'ordonnance de 1681, 15 février 1729 ; </bibl>
      <bibl type="sources"> Arrêt du Conseil d'Etat qui permet l'entrée des plombs et étains
       d'Angleterre par tous les ports et bureaux du royaume, en payant 3 liv. par cent pesant de
       plomb et 4 liv. par cent pesant d'étain, outre les droits sur l'étain, fixés par l'ordonnance
       des fermes de 1681, 20 mai 1738 ; </bibl>
      <bibl type="sources"> Arrêt du Conseil d’ Etat du Roi, qui ordonne que les étains en saumons,
       lingots et autres non ouvrés, venant de l'Etranger, seront exempts du droit particulier de
       deux sols six deniers par livre, 22 décembre 1761] </bibl>
     </bibl>
    </sense>
   </entry>
  </body>
 </text>
</TEI>
