<?xml version="1.0" encoding="utf-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title type="notice"> Faux-saunage </title>
    <author>Marie-Laure Legay</author>
   </titleStmt>
   <publicationStmt>
    <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer le privilège la Ferme
     générale dans l'espace français et européen (1664-1794)Axe 1 : Dictionnaire de la Ferme
     générale, objet d'histoire totale </publisher>
    <pubPlace>
     <address>
      <addrLine> Maison Européenne des Sciences de l'Homme et de la Société </addrLine>
      <addrLine> 2 rue des cannoniers </addrLine>
      <addrLine> 59002 Lille Cedex </addrLine>
     </address>
     <date> 2021-2025 </date>
    </pubPlace>
    <availability>
     <licence> [A déterminer. Ex:Creative Commons Attribution 3.0 non transposé (CC BY 3.0)]
     </licence>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title> Dictionnaire numérique de la Ferme générale </title>
    <respStmt>
     <resp> Coordinateurs de l'axe : Dictionnaire numérique de la Ferme générale, objet d'histoire
      totale. </resp>
     <persName> Marie-Laure Legay </persName>
     <persName> Thomas Boullu </persName>
    </respStmt>
   </seriesStmt>
   <sourceDesc>
    <p> Dictionnaire numérique de la Ferme générale </p>
   </sourceDesc>
  </fileDesc>
  <encodingDesc>
   <projectDesc
    source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
    <p> Le projet FermGé vise à étudier l’impact d’une organisation fiscale (1664-1794),
     discriminante mais rationnelle, sur les territoires et les sociétés de la France moderne/p&gt;
    </p>
   </projectDesc>
  </encodingDesc>
  <profileDesc>
   <textClass>
    <keywords scheme="#fr_RAMEAU">
     <list>
      <item> Histoire -- Histoire moderne -- Histoire fiscale </item>
      <item> Histoire -- Histoire moderne -- Histoire judiciaire </item>
     </list>
    </keywords>
   </textClass>
  </profileDesc>
  <revisionDesc>
   <change type="AutomaticallyEncoded"> 2022-12-15T10:20:03.941390+2:00 </change>
  </revisionDesc>
 </teiHeader>
 <text>
  <body>
   <entry type="F" xml:id="Faux_saunage">
    <form type="lemma">
     <orth> Faux-saunage </orth>
    </form>
    <sense>
     <def> Il s’agit à proprement parler de la vente du sel faite au préjudice du
      monopole du roi, donc hors des <ref target="#greniers"> greniers </ref> . Les commentateurs
      des droits de la Ferme générale sont d’accord pour distinguer au moins quatre espèces de
      faux-saunage. Le moins grave était le « faux-saunage de domicile », commis pour son propre
      usage. Le faux-saunage à <ref target="#porte-à-col"> porte-à-col </ref> était commis par ceux
      qui portaient sur eux-mêmes le faux sel ou le sel non gabellé pour le vendre aux particuliers.
      La diversité des moyens utilisés pour porter le sel obligeait les autorités à multiplier les
      interdictions. En <date when="1777"> 1777 </date> , le Conseil dut par exemple lutter contre
      « un nouveau genre de faux-saunage » pratiqué par les <ref target="#Bretons"> Bretons </ref> :
      la fraude de sel caché dans des pains de seigle, de sarrasin ou autre grain. La troisième
      espèce consistait en transport de sel à l’aide de charrettes et bêtes de charge ou par
      bateaux. Le plus grave était le faux-saunage avec port d’armes : il était considéré comme
      attentatoire à l’autorité du roi et valait la peine de mort (article 6 de l’édit de <date
       when="1664"> 1664 </date> et déclaration du 2 août <date when="1729"> 1729 </date> ). Il
      provenait parfois de la <ref target="#contrebande"> contrebande </ref> de sel <ref
       target="#étranger"> étranger </ref> . Les trois premières espèces donnaient lieu à des peines
       <ref target="#afflictives"> afflictives </ref> et/ou des peines pécuniaires ( <ref
       target="#amendes"> amendes </ref> ) selon les circonstances (attroupement, contrebande à
      cheval…) ; en l’absence de port d’armes et d’actes <ref target="#rébellionnaires">
       rébellionnaires </ref> , la Ferme générale était déchargée des formalités de l’ordonnance
      criminelle de <date when="1670"> 1670 </date> . Buterne ajoute dans son Dictionnaire de
      législation une cinquième espèce de faux-saunage qui consistait à tirer du sel des <ref
       target="#salpêtres"> salpêtres </ref> . On peut aussi repérer le faux-saunage par l’emploi de
      l’eau de mer ou de l’eau de sources salées : les habitants des pays de <ref target="#Gabelles"
       > Gabelles </ref> et de <ref target="#dépôts"> dépôts </ref> en avaient l’interdiction
      établie par le titre 14 de l’ordonnance de <date when="1680"> 1680 </date> et renouvelée en
       <date when="1724"> 1724 </date> (déclaration du 22 février). Les Fermiers généraux
      craignaient que les habitants fabriquent du sel. Cette interdiction contrariait les laboureurs
      dont les exploitations étaient proches de la mer car ces derniers souhaitaient lessiver leurs
      blés de semence avec l’eau de mer pour éviter les caries. La société d’agriculture de Caen
      approuva ce procédé en <date when="1772"> 1772 </date> et l’ <ref target="#intendant">
       intendant </ref> de cette ville autorisa les habitants de l’ <ref target="#élection">
       élection </ref> de Bayeux à prélever l’eau de mer. Les habitants de l’ <orgName
       subtype="administrations_juridictions_royales" type="administrations_juridictions"> élection
       d’Arques </orgName> sollicitèrent également des autorisations. Les Fermiers généraux s’y
      refusèrent considérant que les prélèvements occasionnèrent des abus et que l’eau de chaux,
      dont l’usage était général dans le royaume, était incontestablement plus efficace pour ces
      opérations de lessivage. La lutte contre le faux-saunage était l’affaire des <ref
       target="#brigadiers"> brigadiers </ref> de la Ferme, mais celle-ci espérait pouvoir compter
      sur les autorités locales, non seulement les paroisses « tenues de faire sonner le tocsin au
      principal clocher pendant l’espace d’un quart d’heure » au passage des faux-sauniers pour
      alerter les gardes ou encore tenues de remettre des états de feux pour la distribution du sel,
      les villes et bourgs, notamment quand elles étaient franchisées, les tribunaux chargés de
      poursuivre sur les <ref target="#procès-verbaux"> procès-verbaux </ref> des employés, les
      officiers des <ref target="#greniers"> greniers </ref> à sel… L’échec de la collaboration
      institutionnelle explique en grande partie la pérennisation de la fraude. La culture
      administrative de la Ferme générale s’opposait à celle que perpétuaient les officiers de
      justice ou les officiers municipaux. Il était bien illusoire de penser que les juges des
      seigneurs hauts justiciers des paroisses de <orgName subtype="province"
       type="pays_et_provinces">
       <ref target="#Bretagne"> Bretagne </ref>
      </orgName> limitrophes de la <orgName subtype="province" type="pays_et_provinces">
       <ref target="#Normandie"> Normandie </ref>
      </orgName> , du Maine et de l’ <orgName subtype="province" type="pays_et_provinces">
       <ref target="#Anjou"> Anjou </ref>
      </orgName> , allaient « tenir la main » au roi pour empêcher les amas de sel (article 3 de la
      déclaration de décembre <date when="1680"> 1680 </date> ). </def>
     <bibl type="references">
      <bibl> [ <idno type="ArchivalIdentifier"> AN, G1 91, dossier 39 : « Instructions sur les
        notes, faux-sel, faux-saunage et faux-sauniers », 1789 ; dossier 43 : « Faux-saunage. Requête au Conseil, arrêts du Conseil, arrêts de la Cour
       des aides, 1779-1786 » ; dossier 28 : « Chaulage des grains à l’eau de mer en Normandie, 1787 » ; </idno></bibl>
      <bibl type="sources"> Ordonnances sur le fait des gabelles &amp; des aides…, mai et juin 1680,
       titre XVII ; </bibl>
      <bibl type="sources"> Déclaration du Roi laquelle maintient les habitans de la province de
       Bretagne dans l'exemption des droits de gabelles ; </bibl>
      <bibl> et règle l’ordre de procéder contre les faux-sauniers... et l’usage du sel dans les
       paroisses voisines des provinces de Normandie, le Maine et l'Anjou, décembre 1680 ; </bibl>
      <bibl type="sources"> Arrêt du Conseil d’Etat qui déclare commun pour les lieux de
       Basse-Normandie qui sont sous le privilège du sel blanc l'arrêt du 8 décembre 1722 rendu
       contre les faux-sauniers, avec injonction aux habitants des paroisses de faire sonner le
       tocsin sur lesdits faux-sauniers lors de leur passage, 9 février 1723 ; </bibl>
      <bibl type="sources"> Arrest du Conseil du Roy du 19 juin 1777 qui fait défenses d'introduire de Bretagne
       dans les pays de gabelles, du sel déguisé sous la forme de pains de seigle, de sarrazin ou
       d'autres grains, à peine contre les contrevenants d'être poursuivis et punis comme
       faux-sauniers ; </bibl>
      <bibl type="sources"> Buterne, Dictionnaire de législation, de jurisprudence et de finances sur toutes les
       fermes unies de France, Avignon, 1763, p. XX  et p. 15 et p. 161-179; </bibl>
      <bibl type="sources"> Jean-Louis Moreau de Beaumont, Mémoires concernant les droits &amp; impositions en
       Europe, tome 3, Paris, Imprimerie royale, 1769, p. 191] </bibl>
     </bibl>
    </sense>
   </entry>
  </body>
 </text>
</TEI>
