<?xml version="1.0" encoding="utf-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title type="notice"> Café </title>
    <author>Marie-Laure Legay</author>
   </titleStmt>
   <publicationStmt>
    <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer le privilège la Ferme
     générale dans l'espace français et européen (1664-1794)Axe 1 : Dictionnaire de la Ferme
     générale, objet d'histoire totale </publisher>
    <pubPlace>
     <address>
      <addrLine> Maison Européenne des Sciences de l'Homme et de la Société </addrLine>
      <addrLine> 2 rue des cannoniers </addrLine>
      <addrLine> 59002 Lille Cedex </addrLine>
     </address>
     <date> 2021-2025 </date>
    </pubPlace>
    <availability>
     <licence> [A déterminer. Ex:Creative Commons Attribution 3.0 non transposé (CC BY 3.0)]
     </licence>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title> Dictionnaire numérique de la Ferme générale </title>
    <respStmt>
     <resp> Coordinateurs de l'axe : Dictionnaire numérique de la Ferme générale, objet d'histoire
      totale. </resp>
     <persName> Marie-Laure Legay </persName>
     <persName> Thomas Boullu </persName>
    </respStmt>
   </seriesStmt>
   <sourceDesc>
    <p> Dictionnaire numérique de la Ferme générale </p>
   </sourceDesc>
  </fileDesc>
  <encodingDesc>
   <projectDesc
    source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
    <p> Le projet FermGé vise à étudier l’impact d’une organisation fiscale (1664-1794),
     discriminante mais rationnelle, sur les territoires et les sociétés de la France moderne/p&gt;
    </p>
   </projectDesc>
  </encodingDesc>
  <profileDesc>
   <textClass>
    <keywords scheme="#fr_RAMEAU">
     <list>
      <item> Histoire -- Histoire moderne -- Histoire administrative </item>
      <item> Histoire -- Histoire moderne -- Histoire économique </item>
      <item> Histoire -- Histoire moderne -- Histoire fiscale </item>
     </list>
    </keywords>
   </textClass>
  </profileDesc>
  <revisionDesc>
   <change type="AutomaticallyEncoded"> 2022-12-13T11:54:21.752922+2:00 </change>
  </revisionDesc>
 </teiHeader>
 <text>
  <body>
   <entry type="C" xml:id="Café">
    <form type="lemma">
     <orth> Café </orth>
    </form>
    <sense>
     <def> La fiscalisation du café est intéressante à suivre. Comme pour les autres produits
      coloniaux ( <ref target="#tabac"> tabac </ref> , <ref target="#toiles"> toiles </ref> ,
      cacao…), elle se fit à contre-temps de la consommation intérieure. D’abord mues par la
      concurrence commerciale internationale, les décisions du gouvernement ( <ref
       target="#prohibition"> prohibition </ref> , monopole de vente) établirent une situation très
      propice à la fraude. Les choix de libéralisation partielle du commerce du café des îles (
       <date when="1732"> 1732 </date> et <date when="1736"> 1736 </date> ) permirent de fiscaliser
      mieux la consommation intérieure. <span subtype="normes" type="normes_contestations">
       Cependant, en continuant de favoriser fiscalement le commerce de réexpédition, le
       gouvernement mit les Fermiers généraux dans la nécessité d’accroître drastiquement les
       opérations de contrôle dans et autour des entrepôts </span> .Dans le cas du café, la
      fiscalité prit entre <date when="1664"> 1664 </date> et <date when="1789"> 1789 </date>
      diverses formes dont la plus efficace se révéla être le monopole de la vente, à l’instar de la
      régie du <ref target="#tabac"> tabac </ref> . Intégré au commerce confié à la compagnie des
       <ref target="#Indes"> Indes </ref> ( <date when="1664"> 1664 </date> ) et en conséquence
      exempté des droits de <ref target="#traites"> traites </ref> dès lors qu’il fût destiné pour
      l’étranger, le café, comme le chocolat, connut un succès grandissant auprès des Français.
       <span subtype="normes" type="normes_contestations">
       <span subtype="privileges" type="privleges_encadrement"> La production de l’île Bourbon se
        développant et la monarchie souhaitant accroître ses revenus au moment de la guerre de la
        Ligue d’Augsbourg ( <date when="1687"> 1687 </date> - <date when="1698"> 1698 </date> ), le
        gouvernement confia à François Damame un privilège de vente exclusive au sein du royaume
        (édit de mars <date when="1692"> 1692 </date> ) </span>
      </span> . <span subtype="privileges" type="privleges_encadrement"> Ce dernier ne parvenant pas
       toutefois à couvrir les frais de sa régie, le privilège économique fut converti en un droit
       d’entrée (arrêt du 12 mai <date when="1693"> 1693 </date> ) au profit de <ref
        target="#Marseille"> Marseille </ref> seule </span> . Le café fut taxé à 10 sous la livre
      pesant (15 sous pour le cacao, 10 livres pour le thé…). <span subtype="privileges"
       type="privleges_encadrement"> Pour répondre au marché européen, un privilège d’entrepôt fut
       accordé aux négociants, à <ref target="#Marseille"> Marseille </ref> pour le café, à <ref
        target="#Dunkerque"> Dunkerque </ref> , <ref target="#Dieppe"> Dieppe </ref> , Rouen,
       Saint-Malo, <ref target="#Nantes"> Nantes </ref> , La <ref target="#Rochelle"> Rochelle
       </ref> , <ref target="#Bordeaux"> Bordeaux </ref> et <ref target="#Bayonne"> Bayonne </ref>
       pour le cacao </span> . Les négociants de Saint-Malo s’engagèrent dans ce commerce et
      obtinrent la permission pour 300 000 livres pesant dont ils payèrent les droits d’entrée, mais
      encore à cette époque, les Hollandais restaient les maîtres de ce marché. Ils en fixaient les
      prix sur lesquels les négociants français s’alignaient. En <date when="1723"> 1723 </date> ,
      la Compagnie des <ref target="#Indes"> Indes </ref> recouvra le monopole de la vente du café.
       <span subtype="normes" type="normes_contestations"> Par la déclaration du 10 octobre <date
        when="1723"> 1723 </date> , ce monopole fut organisé en 37 articles sur le modèle du
       monopole du <ref target="#tabac"> tabac </ref>
      </span> . Le café devait être ensaqué ( <ref target="#sacs"> sacs </ref> de deux livres poids,
      une livre et une demie livre), plombé, cacheté et emmagasiné. Il ne pouvait être vendu plus de
      100 sols la livre de 16 onces. <ref target="#Marseille"> Marseille </ref> conservait le droit
      de tirer les cafés du <ref target="#Levant"> Levant </ref> , mais les négociants devaient
      alors les entreposer et les vendre soit à la compagnie, soit à l’étranger. <span
       subtype="normes" type="normes_contestations"> S’ils devaient repartir par mer, déclaration
       devait en être faite dans les vingt-quatre heures au bureau des Poids et <ref target="#casse"
        > casse </ref> de la ville, pour éviter le versement frauduleux sur les côtes françaises
      </span> . Tous les autres négociants du royaume avaient interdiction de faire ce commerce. Il
      fallut dès lors organiser le contrôle. Les commis de la compagnie obtinrent des avantages à
      l’instar de ceux de la Ferme générale. Ils pouvaient visiter tout négociant, châteaux,
      princes, seigneurs, couvents… <span subtype="normes" type="normes_contestations"> pour traquer
       la <ref target="#fraude"> fraude </ref> (article 32), dresser <ref target="#procès-verbal">
        procès-verbal </ref> et assigner les fraudeurs devant les juridictions ad hoc ( <ref
        target="#élections"> élections </ref> et juges des <ref target="#traites"> traites </ref> )
      </span> . Concernant la fiscalité, les cafés furent soumis à un droit d’entrée réduit de 3%
       <span subtype="normes" type="normes_contestations"> , selon les dispositions prises en <date
        when="1664"> 1664 </date> (article 44 de l’édit d’ao </span> ût). La régie de Charles <ref
       target="#Cordier"> Cordier </ref> , toujours soucieuse d’augmenter ses profits, réclama en
      sus les 10 sols par livre qui étaient prévus dans l’arrêt du 12 mai <date when="1693"> 1693
      </date> , ce que le gouvernement refusa. Il fut établi par l’arrêt du 10 août <date
       when="1726"> 1726 </date> que par convention, la compagnie des <ref target="#Indes"> Indes
      </ref> règlerait à la Ferme générale pour tout droit une somme forfaitaire de 25 <span
       subtype="encadrement" type="privleges_encadrement"> 000 livres par an, sorte d’abonnement
       qui, au regard de la consommation, constituait un manque à gagner fiscal </span> . <span
       subtype="normes" type="normes_contestations"> Le Conseil du roi ne revint jamais sur cette
        <ref target="#prohibition"> prohibition </ref> , hormis pour le café des îles d’Amérique
       dont l’entrée fut autorisée (déclaration du 27 septembre <date when="1732"> 1732 </date> )
      </span> . <span subtype="encadrement" type="privleges_encadrement"> Sur la demande des
       négociants de la Martinique qui avaient converti leurs terres en caféiers, le gouvernement
       donna son accord pour l’entrepôt en métropole du café des îles françaises </span> . <ref
       target="#Dunkerque"> Dunkerque </ref> , Le <ref target="#Havre"> Havre </ref> , Saint-Malo,
       <ref target="#Nantes"> Nantes </ref> , La <ref target="#Rochelle"> Rochelle </ref> , <ref
       target="#Bordeaux"> Bordeaux </ref> , <ref target="#Bayonne"> Bayonne </ref> et <ref
       target="#Marseille"> Marseille </ref> furent désignés comme ports pour emmagasiner les
      denrées. Quatre ans plus tard (29 mai <date when="1736"> 1736 </date> ), le commerce des cafés
      des îles devint libre, tandis que la compagnie des <ref target="#Indes"> Indes </ref> ne
      conserva plus que le monopole des cafés des autres lieux de production (île Bourbon et île de
      France). Les ports d’entrée passèrent de huit à treize (Calais, <ref target="#Dieppe"> Dieppe
      </ref> , Honfleur, <ref target="#Sète"> Sète </ref> , Rouen vinrent s’ajouter). Surtout, la
      fiscalité à l’entrée dans le royaume fut établie à 10 livres le cent pesant brut, « même pour
      ceux provenant de la Traite des <ref target="#noirs"> noirs </ref> ». Les droits du Domaine d’
       <ref target="#Occident"> Occident </ref> restaient dus aussi. Ainsi, le gouvernement
      multiplia par vingt la fiscalité au poids sur le café par rapport au tarif en vigueur en <date
       when="1693"> 1693 </date> . <span subtype="privileges" type="privleges_encadrement"> En
       revanche, il accorda l’exemption des sous pour livre, bien modeste avantage </span> . De
      même, le droit de 10 livres par quintal remplaça tous les anciens droits qu’acquittait le café
      avant l’arrêt de <date when="1736"> 1736 </date> , comme par exemple les droits de traite de
       <ref target="#Nantes"> Nantes </ref> . Enfin, pour défendre les cafés « nationaux », le
      Conseil taxa les cafés étrangers, tan ceux qui arrivaient à Lorient par la compagnie des <ref
       target="#Indes"> Indes </ref> que ceux qui arrivaient du Levant à <ref target="#Marseille">
       Marseille </ref> ou tout autre café de l’étragner à 25 livres le quintal (janvier <date
       when="1767"> 1767 </date> ).En cas de vente pour l’étranger, les droits de <ref
       target="#transit"> transit </ref> étaient accordés et les droits d’entrée n’étaient pas
      levés. In fine, c’est bien autour de l’entrepôt que les enjeux fiscaux se concentrèrent. <span
       subtype="privileges" type="privleges_encadrement"> D’une part, le privilège d’entrepôt passa
       de six mois à un an (18 décembre <date when="1736"> 1736 </date> ), ce qui laissait le temps
       de faire du trafic illégal </span> . Les magasins étaient à la charge des négociants, mais
      les commis des Fermiers généraux y avaient accès. D’autre part, l’importance de ce commerce
      nécessita des aménagements de ports et des entrepôts provisoires (sous tente) qui facilitaient
      la fraude. A <ref target="#Bordeaux"> Bordeaux </ref> , la Ferme imposa l’entrepôt à plusieurs
      endroits sur les quais, d’abord rue du Chai des Farines puis, à partir de <date when="1741">
       1741 </date> , à la maison Clock. Cependant, faute de place, les négociants bénéficièrent
      d’une certaine liberté de stockage à partir de <date when="1767"> 1767 </date> , une sorte
      d’entrepôt « fictif » <span subtype="contestation" type="normes_contestations"> qui occasionna
       une fraude sans précédent </span> . L’établissement d’un bureau de la Ferme générale à la
      Porte du Chapeau rouge fut l’occasion de revenir sur cette situation et d’envisager un nouvel
      entrepôt pour y enfermer tous les cafés en provenance des îles. <span subtype="contestation"
       type="normes_contestations"> Chaque jour, des contestations naissaient, la fraude grandissait
       et les droits de la Ferme générale diminuaient </span> . L’ <ref target="#intendant">
       intendant </ref> de <ref target="#Bordeaux"> Bordeaux </ref> donna un tableau qui montrait
      « la chute du droit de consommation des caffés » de <date when="1767"> 1767 </date> à <date
       when="1773"> 1773 </date> (par rapport à <date when="1763"> 1763 </date> - <date when="1767">
       1767 </date> ) et dans le même temps, l’augmentation prodigieuse de la masse de l’importation
      des cafés. Toutefois, les négociants de la Chambre de commerce, associés à la <placeName
       type="lieux_habitations"> ville de Bordeaux </placeName> ne voulurent pas établir l’entrepôt
      pour des raisons financières (le coût était estimé à environ 600 000 livres). L’intendant,
      pour concilier les parties, proposa d’établir un droit de 20 sols par quintal qui serait
      indistinctement perceptible sur tous les cafés destinés pour le royaume et pour l’étranger. Il
      estima le produit prévisible à 200 000 livres tournois ; la Ferme générale jugea le projet de
      l’ <ref target="#intendant"> intendant </ref> irréalisable et prit pour exemple le cas de <ref
       target="#Nantes"> Nantes </ref> qui devait servir de modèle aux Bordelais. </def>
     <bibl type="references">
      <bibl> [ <idno type="ArchivalIdentifier"> AN G131, f° 34 v°-35 (20 juillet 1774), entrepôt du café à Bordeaux et Mémoire n° 1226, 9 août 1775 ; </idno>
      </bibl>
      <bibl>
       <idno type="ArchivalIdentifier"> AN, G1 79, dossiers 4 et 16, évolution des droits de
        traites ; </idno>
      </bibl>
      <bibl type="sources"> Arrêt du Conseil d’Etat du roi qui permet aux Capitaines généraux et
       préposés pour la régie du privilège des ventes exclusives du Tabac et du Caffé, de faire des
       visites dans les maisons des Ecclésiastiques, Nobles et Bourgeois &amp; autres pour y faire
       la recherche des faux Tabacs &amp; Caffés, sans permission, 25 janvier 1724 ; </bibl>
      <bibl type="sources"> Arrêt du Conseil d’Etat qui ordonne que tous les cafés venant des
       Echelles du Levant pourront entrer dans la ville de Marseille et en sortir librement par mer,
       ainsi qu'avant l'arrêt du 31 août 1723, 8 février 1724 ; </bibl>
      <bibl type="sources"> Arrêt du Conseil d’Etat concernant les déclarations à fournir pour le
       café qui entre et sort de Marseille, 21 janvier 1731 ; </bibl>
      <bibl type="sources"> Arrêt du Conseil d'Etat qui révoque la permission accordée par celui du
       2 avril 1737, aux négociants de Marseille, d’introduire pour la consommation du royaume, des
       cafés des iles françaises de l'Amérique, 28 octobre 1746 ; </bibl>
      <bibl type="sources"> Arrêt du Conseil d'Etat qui fixe les droits qui seront perçus, tant à
       l'entrée qu'à la sortie du royaume, sur les cafés provenant des iles et colonies françaises
       de l'Amérique et sur ceux apportés du Levant ou provenant du commerce de la Compagnie des
       Indes, 25 janvier 1767 ; </bibl>
      <bibl type="sources"> Joseph Du Fresne de Francheville, Histoire de la Compagnie des Indes avec les titres de
       ses concessions et privilèges, dressée sur les pièces autentiques, Paris, Chez De Bure
       l'aîné, 2 t., 1746, dont tome 2, p. 527 à 579 ; </bibl>
      <bibl type="sources"> Vivent Magnien, Recueil alphabétique des droits de traites uniformes…, 1786, p.
       214-221 ; </bibl>
      <bibl> Anne Radeff, Du café dans le chaudron. Économie globale d'Ancien Régime. Suisse
       Occidentale, Franche-Comté et Savoie, Lausanne, Société d'Histoire de la Suisse Romande, «
       Mémoires et documents », 1996 ; </bibl>
      <bibl> Bernard Michon, « L’introduction du café en France métropolitaine : acteurs, réseaux,
       ports (XVIIe-XIXe siècles), Journée d’études, Nantes, 30 septembre 2021] </bibl>
     </bibl>
    </sense>
   </entry>
  </body>
 </text>
</TEI>
