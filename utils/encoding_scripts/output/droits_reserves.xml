<?xml version="1.0" encoding="utf-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title type="notice"> Droits réservés </title>
    <author>Marie-Laure Legay</author>
   </titleStmt>
   <publicationStmt>
    <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer le privilège la Ferme
     générale dans l'espace français et européen (1664-1794)Axe 1 : Dictionnaire de la Ferme
     générale, objet d'histoire totale </publisher>
    <pubPlace>
     <address>
      <addrLine> Maison Européenne des Sciences de l'Homme et de la Société </addrLine>
      <addrLine> 2 rue des cannoniers </addrLine>
      <addrLine> 59002 Lille Cedex </addrLine>
     </address>
     <date> 2021-2025 </date>
    </pubPlace>
    <availability>
     <licence> [A déterminer. Ex:Creative Commons Attribution 3.0 non transposé (CC BY 3.0)]
     </licence>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title> Dictionnaire numérique de la Ferme générale </title>
    <respStmt>
     <resp> Coordinateurs de l'axe : Dictionnaire numérique de la Ferme générale, objet d'histoire
      totale. </resp>
     <persName> Marie-Laure Legay </persName>
     <persName> Thomas Boullu </persName>
    </respStmt>
   </seriesStmt>
   <sourceDesc>
    <p> Dictionnaire numérique de la Ferme générale </p>
   </sourceDesc>
  </fileDesc>
  <encodingDesc>
   <projectDesc
    source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
    <p> Le projet FermGé vise à étudier l’impact d’une organisation fiscale (1664-1794),
     discriminante mais rationnelle, sur les territoires et les sociétés de la France moderne/p&gt;
    </p>
   </projectDesc>
  </encodingDesc>
  <profileDesc>
   <textClass>
    <keywords scheme="#fr_RAMEAU">
     <list>
      <item> Histoire -- Histoire moderne -- Histoire fiscale </item>
      <item> Histoire -- Histoire moderne -- Histoire financière </item>
     </list>
    </keywords>
   </textClass>
  </profileDesc>
  <revisionDesc>
   <change type="AutomaticallyEncoded"> 2022-12-14T11:21:45.432707+2:00 </change>
  </revisionDesc>
 </teiHeader>
 <text>
  <body>
   <entry type="D" xml:id="Droits_réservés">
    <form type="lemma">
     <orth> Droits réservés </orth>
    </form>
    <sense>
     <def> Droits perçus par le roi sur certains offices créés aux XVIe et XVIIe siècles, mais
      supprimés pour partie en <date when="1716"> 1716 </date> . Pour permettre le remboursement des
      offices en question (par exemple « receveurs et contrôleurs des amendes », « jurés mouleurs de
      bois », « mesureurs de grains », « auneurs et contrôleurs de toiles »…), il fallut
      « réserver » les droits qui y étaient attachés au profit du roi et les affermer. Eux-mêmes
      supprimés en <date when="1719"> 1719 </date> , les droits réservés furent rétablis en <date
       when="1722"> 1722 </date>, réunis au <ref target="#Domaine"> Domaine </ref> et finalement
      mis en régie. En <date when="1768"> 1768 </date> , la régie des droits réservés, confiée à
      François Noël, continua de lever les droits des offices supprimés là où ils avaient été
      établis, mais aussi le « don gratuit » sollicité des villes, bourgs et faubourgs depuis <date when="1758"> 1758 </date> (édit d’ao
      les boissons) additionnellement aux octrois municipaux. Le nouveau régisseur avait d’ailleurs
      la possibilité, pour exercer sa propre régie, de se servir des commis et gardes des <ref
       target="#aides"> aides </ref> de la Ferme générale. La partie « don gratuit » des droits
      réservés avait nécessité d’établir des commis dans des localités dont le rattachement
      juridique à telle ou telle ville demeurait incertain. Pour limiter les contestations, une
      centaine de lieux-dits furent donc exemptés de la perception en <date when="1770"> 1770
      </date> . Lorsque Jean-Baptiste Bossuat reprit cette régie ( <date when="1773"> 1773 </date>
      ), il y fut adjoint les sols pour <ref target="#livre"> livre </ref> régulièrement créés
      depuis <date when="1760"> 1760 </date> et encore augmentés par l’abbé Terray en <date
       when="1771"> 1771 </date> . On admit encore les abonnements aux droits réservés dans de
      nombreux cas où la perception était moins fondée. Les abonnements de la plupart des pays d’
       <ref target="#Etats"> Etats </ref> ou pays d’impositions furent renouvelés par Henri Clavel,
      chargé de la régie générale des <ref target="#aides"> aides </ref> , en <date when="1784">
       1784 </date> . </def>
     <bibl type="references">
      <bibl> [ <idno type="ArchivalIdentifier"> AN G2 1811 et 1812 ; </idno>
      </bibl>
      <bibl type="sources"> Arrêt du conseil d'état qui ordonne que François Noël, subrogé à
       François Tessier par arrêt du Conseil du 5 mai 1768, sera mis en possession de la régie et
       perception des droits réservés, 15 mai 1768 ; </bibl>
      <bibl type="sources"> Arrêt du Conseil d'état qui commet Jean-Baptiste Bossuat, pour faire
       pendant 6 années, la régie des droits réservés et des sous pour livre établis en sus desdits
       droits, 18 avril 1773 ; </bibl>
      <bibl type="sources"> Arrêt du Conseil d'Etat portant règlement pour la perception des droits
       réservés, 20 janvier 1774 ; </bibl>
      <bibl> Aline Logette, « La Régie générale au temps de Necker et de ses successeurs,
       1777-1786», Revue historique de droit français et étranger, 1982, n°3, vol. 60, p. 415-445]
      </bibl>
     </bibl>
    </sense>
   </entry>
  </body>
 </text>
</TEI>
