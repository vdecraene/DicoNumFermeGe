<?xml version="1.0" encoding="utf-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title type="notice"> Berry </title>
    <author>Marie-Laure Legay</author>
   </titleStmt>
   <publicationStmt>
    <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer le privilège la Ferme
     générale dans l'espace français et européen (1664-1794)Axe 1 : Dictionnaire de la Ferme
     générale, objet d'histoire totale </publisher>
    <pubPlace>
     <address>
      <addrLine> Maison Européenne des Sciences de l'Homme et de la Société </addrLine>
      <addrLine> 2 rue des cannoniers </addrLine>
      <addrLine> 59002 Lille Cedex </addrLine>
     </address>
     <date> 2021-2025 </date>
    </pubPlace>
    <availability>
     <licence> [A déterminer. Ex:Creative Commons Attribution 3.0 non transposé (CC BY 3.0)]
     </licence>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title> Dictionnaire numérique de la Ferme générale </title>
    <respStmt>
     <resp> Coordinateurs de l'axe : Dictionnaire numérique de la Ferme générale, objet d'histoire
      totale. </resp>
     <persName> Marie-Laure Legay </persName>
     <persName> Thomas Boullu </persName>
    </respStmt>
   </seriesStmt>
   <sourceDesc>
    <p> Dictionnaire numérique de la Ferme générale </p>
   </sourceDesc>
  </fileDesc>
  <encodingDesc>
   <projectDesc
    source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
    <p> Le projet FermGé vise à étudier l’impact d’une organisation fiscale (1664-1794),
     discriminante mais rationnelle, sur les territoires et les sociétés de la France moderne/p&gt;
    </p>
   </projectDesc>
  </encodingDesc>
  <profileDesc>
   <textClass>
    <keywords scheme="#fr_RAMEAU">
     <list>
      <item> Histoire -- Histoire moderne -- Histoire administrative </item>
      <item> Histoire -- Histoire moderne -- Histoire économique </item>
      <item> Histoire -- Histoire moderne -- Histoire fiscale </item>
     </list>
    </keywords>
   </textClass>
  </profileDesc>
  <revisionDesc>
   <change type="AutomaticallyEncoded"> 2022-12-12T17:28:50.413045+2:00 </change>
  </revisionDesc>
 </teiHeader>
 <text>
  <body>
   <entry type="B" xml:id="Berry">
    <form type="lemma">
     <orth> Berry </orth>
    </form>
    <sense>
     <def> Pays de grandes <ref target="#gabelles"> gabelles </ref> et du ressort des Cinq grosses
       <ref target="#fermes"> fermes </ref> . Le bas Berry, confiné à la Marche, au <orgName
       subtype="province" type="pays_et_provinces">
       <ref target="#Limousin"> Limousin </ref>
      </orgName> et au <orgName subtype="province" type="pays_et_provinces">
       <ref target="#Poitou"> Poitou </ref>
      </orgName> , c’est-à-dire aux pays <ref target="#rédimés"> rédimés </ref> , connut une intense
       <ref target="#contrebande"> contrebande </ref> de sel. Les faux-sauniers allaient le chercher
      du côté de La Souterraine dans la Marche, passaient la Creuse et le revendaient en Berry. La
      mise en œuvre de <placeName type="zones">cinq lieues <ref target="#limitrophes"> limitrophes </ref></placeName> nécessita un
      arpentage effectué en <date when="1667"> 1667 </date> . Les paroisses intégrées dans la
      zone-tampon, comme Lourdoueix Saint-Pierre ou Saint-Plantaire, réagirent à la suppression de
      liberté du commerce du sel. <span subtype="contestation" type="normes_contestations"> L’émeute
       de décembre <date when="1667"> 1667 </date>, violente, se concentra sur Chéniers et fut
       réprimée sévèrement </span>. Les <ref target="#brigades"> brigades </ref> tant à pied qu’à
      cheval furent renforcées, le long de la Creuse (<placeName type="lieux_habitations">Fontgombault</placeName>, <placeName type="lieux_habitations">Blanc</placeName>, <placeName type="lieux_habitations">Ciron</placeName>, <placeName type="lieux_habitations">Chitray</placeName>, Saint
      Gaultier, <placeName type="lieux_habitations">Argenton</placeName>, <placeName type="lieux_habitations">Cuzion</placeName>, <placeName type="lieux_habitations">Flesselines</placeName>, <placeName type="lieux_habitations">La Celle-Dunois</placeName>, <placeName type="lieux_habitations">Glénic</placeName>) et dans les cinq lieues <ref
       target="#limitrophes"> limitrophes </ref> (Orsennes, Aigurande, La Celette). La rivière
      servit également de ligne de démarcation pour les <ref target="#traites"> traites </ref> .
      Trois bureaux étaient établis au XVIIIe siècle : Saint-Gaultier, Argenton et Aigurande.Au cœur
      de cette province, la <orgName subtype="pays" type="pays_et_provinces"> principauté de
       Boisbelle-Henrichemont </orgName> , alleu souverain qui appartenait à la famille de
      Béthune-Sully depuis <date when="1605"> 1605 </date> , était située entre les <orgName subtype="administrations_juridictions_royales"
       type="administrations_juridictions"><ref
       target="#greniers"> greniers </ref> d’Aubigny</orgName>, de Bourges, de Sancerre et de Vierzon. Elle
      jouissait du droit exclusif de la vente du sel et du tabac. Pour éviter tout préjudice
      financier, la Ferme générale se rendit adjudicataire de ce petit bail, même si ses conditions
      n’étaient pas très favorables. En <date when="1716"> 1716 </date> , le bail des gabelles
      d’Henrichemont, qui n’était que de 16 000 livres fut porté à 20 000 livres ; le bail du <ref
       target="#tabac"> tabac </ref> montait quant à lui à 2 000 livres en <date when="1738"> 1738
      </date> , 5 000 livres en <date when="1757"> 1757 </date> . Par contrat d’échange du 24 sept
       <date when="1766"> 1766 </date> , le roi acquit la principauté du duc de Sully, Maximilien
      Antoine Armand de Béthune. La Ferme générale fit valoir les pertes qu’elle subissait sur ce
      bail de 25 000 livres, auxquelles il fallait ajouter le coût des douze minots de sel en nature
      assignés au prince sur le <orgName subtype="administrations_juridictions_royales"
       type="administrations_juridictions"> grenier de Paris </orgName> : le tabac se vendait sur le
      même pied que dans les autres entrepôts de la Ferme générale, tandis que le prix du sel était
      fixé à 30 livres le minot, le produit de la consommation de ces deux denrées s’élevait à
      24 930 livres. Toutefois, les charges d’approvisionnement, de voiture, d’entrepôt et
      d’appointement s’élevaient à 5 521 livres, soit une recette nette de 19 408 livres seulement. </def>
     <bibl type="references">
      <bibl> [ <idno type="ArchivalIdentifier"> AN, G1 88, dossier 10 : Mémoire sur établissement
        des droits de gabelle dans la principauté de Boisbelle et Henrichemont, 1773 ; </idno>
      </bibl>
      <bibl type="sources"> De Miremont, Carte générale de la province du Berry, qui comprend les
       greniers à sel, les paroisses et lieux imposés de leur ressort, les contrôles establis sur
       les limites du Poitou, les bureaux des traites et postes des brigades tant de pied que de
       cheval, etc, s.l.n.d., XVIIIe siècle ; </bibl>
      <bibl type="sources"> Bertrand Barrère de Vieuzac, Rapport fait au nom du comité des domaines, sur l’échange
       de la ci-devant principauté d'Henrichemont et de Boisbelles, lors de la séance du 27
       septembre 1791, Archives Parlementaires de 1787 à 1860, Première série (1787-1799), t. XXXI,
       Librairie Administrative P. Dupont, 1888, p. 399-403 ; </bibl>
      <bibl> Jacques Métrich. L'émeute de Chéniers, un exemple de résistance à la fiscalité au XVIIe
       siècle (mémoire de la Société des Sciences Naturelles de la Creuse), 1965 ; </bibl>
      <bibl> Jean-Pierre Surrault, Gabelle, gabelous et faux-sauniers en Bas Berry, Revue de
       Académie du Centre, 1987, p. 92-127 ; </bibl>
      <bibl> Isabelle Formont, « Les derniers ducs de Sully », dans Sully, tel qu’en lui-même,
       Paris, CHEFF, 2004, p. 53-59] </bibl>
     </bibl>
    </sense>
   </entry>
  </body>
 </text>
</TEI>
