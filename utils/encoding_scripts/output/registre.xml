<?xml version="1.0" encoding="utf-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title type="notice"> Registre </title>
    <author>Marie-Laure Legay</author>
   </titleStmt>
   <publicationStmt>
    <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer le privilège la Ferme
     générale dans l'espace français et européen (1664-1794)Axe 1 : Dictionnaire de la Ferme
     générale, objet d'histoire totale </publisher>
    <pubPlace>
     <address>
      <addrLine> Maison Européenne des Sciences de l'Homme et de la Société </addrLine>
      <addrLine> 2 rue des cannoniers </addrLine>
      <addrLine> 59002 Lille Cedex </addrLine>
     </address>
     <date> 2021-2025 </date>
    </pubPlace>
    <availability>
     <licence> [A déterminer. Ex:Creative Commons Attribution 3.0 non transposé (CC BY 3.0)]
     </licence>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title> Dictionnaire numérique de la Ferme générale </title>
    <respStmt>
     <resp> Coordinateurs de l'axe : Dictionnaire numérique de la Ferme générale, objet d'histoire
      totale. </resp>
     <persName> Marie-Laure Legay </persName>
     <persName> Thomas Boullu </persName>
    </respStmt>
   </seriesStmt>
   <sourceDesc>
    <p> Dictionnaire numérique de la Ferme générale </p>
   </sourceDesc>
  </fileDesc>
  <encodingDesc>
   <projectDesc
    source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
    <p> Le projet FermGé vise à étudier l’impact d’une organisation fiscale (1664-1794),
     discriminante mais rationnelle, sur les territoires et les sociétés de la France moderne/p&gt;
    </p>
   </projectDesc>
   
  </encodingDesc><profileDesc>
    <textClass>
     <keywords scheme="#fr_RAMEAU">
      <list>
       <item> Histoire -- Histoire moderne -- Histoire administrative </item>
      </list>
     </keywords>
    </textClass>
   </profileDesc>
   <revisionDesc>
    <change type="AutomaticallyEncoded"> 2022-12-20T14:02:49.315477+2:00 </change>
   </revisionDesc>
 </teiHeader>
 <text>
  <body>
   <entry type="R" xml:id="Registre">
    <form type="lemma">
     <orth> Registre </orth>
    </form>
    <sense>
     <def> La compagnie des Fermiers généraux était une entreprise paperassière particulièrement
      efficace. Elle donna des directives strictes à ses employés pour régir les droits à l’aide de
      registres et rendre compte des recettes et dépenses à l’administration centrale. Tous ces
      registres avaient une valeur juridique qui courrait encore dix ans après la régie ; pour cette raison, ils étaient paraphés par
       un <ref target="#Elu"> Elu </ref> ou par le juge local qui visait les contraintes prononcées
       localement par les <ref target="#directeurs"> directeurs </ref>
       <span subtype="contestation" type="normes_contestations"> La production de faux registres ou
       la contrefaçon de la signature des juges était punie de mort </span> . Toutefois, les <ref
       target="#adjudicataires"> adjudicataires </ref> n’étaient pas tenus de déposer les registres
      qui avaient servi à la régie pendant le bail auprès d’une juridiction royale. Sur ce point, la
      demande formulée en <date when="1780"> 1780 </date> par les Magistrats du baillage de
      Mirecourt en <orgName subtype="province" type="pays_et_provinces">
       <ref target="#Lorraine"> Lorraine </ref>
      </orgName> fut déboutée. Celle-ci révélait néanmoins les rapports tendus qui se nouaient à
      cette époque autour de la publicité des <ref target="#comptes"> comptes </ref> .Au <ref
       target="#grenier"> grenier </ref> ou chambre à sel, l’ <ref target="#ordonnance"> ordonnance
      </ref> des gabelles de <date when="1680"> 1680 </date> exigeait quatre registres pour suivre
      la distribution journalière : un par le grenetier, un par le contrôleur, un par le <ref
       target="#receveur"> receveur </ref> et un par le greffier, mais ces dispositions n’étaient
      pas toujours suivies, comme l’indiqua l’inspecteur Languérat à propos du <orgName
       subtype="administrations_juridictions_royales" type="administrations_juridictions"> grenier
       de Saint-Quentin </orgName> en <date when="1708"> 1708 </date> : dans ce grenier, le greffier
      ne tenait pas de registre. Les registres sextés n'étaient pas toujours conformes non plus aux
      modèles attendus, car ils étaient établis sur la base des rôles de taille ou capitation ou
      états de dénombrement eux-mêmes défectueux. S’y ajoutaient le registre de recette de l’impôt
      du sel, le registre des <ref target="#procès-verbaux"> procès-verbaux </ref> de captures, et
      les registres comptables. Le receveur des <ref target="#tabacs"> tabacs </ref> tenait quant à
      lui, avec son contrôleur, sept registres <span subtype="normes" type="normes_contestations"> :
       un registre de réception des <ref target="#tabacs"> tabacs </ref> , un pour la déclaration et
       dépôts des tabacs <ref target="#étrangers"> étrangers </ref> , un pour la recette de la
       vente, un pour les dépenses, un pour les <ref target="#saisies"> saisies </ref> , un registre
       des numéros, un registre d’ordre et en plus, le <ref target="#registre-journal">
        registre-journal </ref> exigé du receveur seul depuis <date when="1717"> 1717 </date>
      </span> . Dans l’administration des <ref target="#aides"> aides </ref> , le <ref
       target="#receveur"> receveur </ref> avait également fort à faire pour établir ses registres
      sommiers de recettes dans lesquels on reportait les quittances de paiement : il ne pouvait le
      faire qu’à partir des états collationnés des nombreux registres <ref target="#portatifs">
       portatifs </ref> des commis aux exercices et commis aux entrées des villes. Brunet de
      Granmaison conseilla sur ce point de reporter les articles des portatifs dans le sommier sur le champ, de sorte que lorsque le
        <ref target="#cabaretier"> cabaretier </ref> venait régler au bureau, le <ref
        target="#receveur"> receveur </ref> p  ût reporter tout aussi rapidement sa
      quittance, puis faire un dernier report dans le <ref target="#registre-journal">
       registre-journal </ref> . Dans l’administration des douanes, le registre des recettes des
      droits d’entrée et de sortie selon les tarifs, le registre de déclarations, le registre de
       <ref target="#saisies"> saisies </ref> et confiscations, celui des <ref
       target="#acquit-à-caution"> acquit-à-caution </ref> et décharges de marchandises… formaient
      le quotidien des receveurs et contrôleurs. Le plus souvent, les <ref target="#inspecteurs">
       inspecteurs </ref> des fermes déploraient le retard des décharges sur les registres d’ <ref
       target="#acquit-à-caution"> acquit-à-caution </ref> : « j’ai trouvé environ cent
      acquits-à-caution qui n’est esté rapportées, quoyqu’elles ayent esté délirées depuis huit à
      dix mois », écrivit Languérat. Les <ref target="#directeurs"> directeurs </ref> tenaient
      également registre des ordres, de leurs correspondances, plus tard les registres des employés,
      reprenant noms, prénoms, domicile, âge, fonction de chaque commis… Les <ref
       target="#directeurs"> directeurs </ref> avaient en outre un devoir de surveillance des
      registres des receveurs du ressort de leur direction. Ces registres avaient de multiples
      vocations, administrative, judiciaire, comptable et financière. La compagnie sollicitait
      chaque mois un état des recettes et charges établi soit par simple bordereau, soit par extrait
      du registre-journal, état qui devait être remis à bureaux centraux à Paris. Pour que cet état
      mensuel fût exact, il devait reprendre les états des registres ordinaires et portatifs que le
      receveur vérifiait également chaque mois en reportant les informations dans son registre
      sommier et les quittances s’y rapportant. Ces documents permettaient également d’établir le débet de chaque comptable envers la Ferme
       générale et de procéder ainsi au recouvrement avant la fin de l’année x du bail  . </def>
     <bibl type="references">
      <bibl> [ <idno type="ArchivalIdentifier"> AN, G1 115, dossier 4 : commission de receveur du
        tabac ; </idno>
      </bibl>
      <bibl>
       <idno type="ArchivalIdentifier"> AN, G7 1170, procès-verbal de la tournée faite par
        l’inspecteur Languérat dans la direction des fermes de Saint-Quentin, 1708 ; </idno>
      </bibl>
      <bibl type="sources"> Ordre qui sera observé par les directeurs establiz pour la régie, recette et dépense
       des droits de quatrième et autres droits y joints, papier et parchemin timbré, de l'élection
       de…, à commencer du premier jour d'octobre 1680 (arrêté par le Bureau de la ferme des aides),
       16 septembre 1680 ; </bibl>
      <bibl type="sources"> Arrêt du Conseil d’Etat qui ordonne qu'à commencer du 1er janvier 1718
       les receveurs des 5 grosses fermes et ceux des gabelles tiendront des registres journaux en
       exécution de l'édit de juin 1716 et de l'arrêt du 24 juillet 1717, 13 décembre 1717 ; </bibl>
      <bibl type="sources"> Arrêt du Conseil d’Etat portant que les grenetiers, contrôleurs,
       greffiers et receveurs des greniers auront seuls le droit de tenir, arrêter et signer les
       registres de ventes, distributions, descentes, mesurages et emplacements des sels ; permet aux présidents d’y assister, mais sans préséance et sans pour raison de ce
       prétendre aucun droit ni émolument, 12 mai 1739 ; </bibl>
      <bibl type="sources"> Arrêt du Conseil d’État du roi concernant le paraphe des registres des
       employés des fermes, 28 juin 1758 ; </bibl>
      <bibl type="sources"> Arrêt du Conseil d’Etat qui annulle une sentence du bailliage de
       Mirecourt, du 30 septembre 1780, et dispense l'adjudicataire des fermes générales de déposer
       dans les greffes des bailliages de la Lorraine les registres qui ont servi ou serviront à la
       régie des droits de traite foraine, haut-conduit, impôt sur les toiles et autres de même
       nature, 3 juillet 1781 ; </bibl>
      <bibl type="sources"> Pierre Brunet de Grandmaison, Dictionnaire des Aydes ou les dispositions tant des
       ordonnances de 1680 et 1681 que les règlemens rendus en interprétation jusqu’à présent, t.2,
       Paris, 1750, p. 148] </bibl>
     </bibl>
    </sense>
   </entry>
  </body>
 </text>
</TEI>
