<?xml version="1.0" encoding="utf-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title type="notice"> Douane de Lyon </title>
    <author>Nicolas Soulas</author>
   </titleStmt>
   <publicationStmt>
    <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer le privilège la Ferme
     générale dans l'espace français et européen (1664-1794)Axe 1 : Dictionnaire de la Ferme
     générale, objet d'histoire totale </publisher>
    <pubPlace>
     <address>
      <addrLine> Maison Européenne des Sciences de l'Homme et de la Société </addrLine>
      <addrLine> 2 rue des cannoniers </addrLine>
      <addrLine> 59002 Lille Cedex </addrLine>
     </address>
     <date> 2021-2025 </date>
    </pubPlace>
    <availability>
     <licence> [A déterminer. Ex:Creative Commons Attribution 3.0 non transposé (CC BY 3.0)]
     </licence>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title> Dictionnaire numérique de la Ferme générale </title>
    <respStmt>
     <resp> Coordinateurs de l'axe : Dictionnaire numérique de la Ferme générale, objet d'histoire
      totale. </resp>
     <persName> Marie-Laure Legay </persName>
     <persName> Thomas Boullu </persName>
    </respStmt>
   </seriesStmt>
   <sourceDesc>
    <p> Dictionnaire numérique de la Ferme générale </p>
   </sourceDesc>
  </fileDesc>
  <encodingDesc>
   <projectDesc
    source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
    <p> Le projet FermGé vise à étudier l’impact d’une organisation fiscale (1664-1794),
     discriminante mais rationnelle, sur les territoires et les sociétés de la France moderne/p&gt;
    </p>
   </projectDesc>
  </encodingDesc>
  <profileDesc>
   <textClass>
    <keywords scheme="#fr_RAMEAU">
     <list>
      <item> Histoire -- Histoire moderne -- Histoire administrative </item>
      <item> Histoire -- Histoire moderne -- Histoire commerciale </item>
      <item> Histoire -- Histoire moderne -- Histoire fiscale </item>
     </list>
    </keywords>
   </textClass>
  </profileDesc>
  <revisionDesc>
   <change type="AutomaticallyEncoded"> 2022-12-14T11:11:50.718209+2:00 </change>
  </revisionDesc>
 </teiHeader>
 <text>
  <body>
   <entry type="D" xml:id="Douane_de_Lyon">
    <form type="lemma">
     <orth> Douane de Lyon </orth>
    </form>
    <sense>
     <def> La douane de Lyon trouve son origine dans la politique protectionniste de la monarchie
      française qui tente de faire de la ville une plaque tournante du commerce international.
      Louis XI et Charles VIII imposèrent une taxe de 5% sur les importations de soies italiennes
      afin de protéger et stimuler la production locale. Quelques années plus tard, la municipalité
      lyonnaise ayant besoin de liquidités pour financer les réparations des fortifications de la
      ville, proposa à la monarchie de taxer, sous formes d’octroi, toutes les marchandises entrant
      dans Lyon. Pendant huit ans, la ville perçut un octroi de 2,5% de la valeur de toutes les
      marchandises de passage. À l’expiration du bail, Henri II prorogea l’octroi au profit de la
      monarchie en élargissant la liste des produits taxés : la douane de Lyon était née.Toutes les
      soieries provenant de l’ <ref target="#Espagne"> Espagne </ref> ou de l’Italie devaient passer
      par <ref target="#Lyon"> Lyon </ref> . Par la suite, les textiles provenant de <orgName
       subtype="province" type="pays_et_provinces"> Flandre </orgName> s, des espaces germaniques,
      anglais ou du Lela douane. Celle-ci s’étendit rapidement à d’autres produits tels
      qu’épiceries, drogueries et toute autre denrée passant par Lyon. Fixés à hauteur de 5% lorsque
      les marchandises importées étaient destinées au marché français, les droits s’élèvaient à 21,5
      % lorsque les produits traversaient le royaume. Dès sa fondation, un usage inéquitable fut
      instauré dans la pesée des marchandises : les productions étrangères étaient pesées au poids
      de marc tandis que les produits français l’étaient au poids de Lyon, plus faible de 16 %, ce
      qui favorisait les marchandises étrangères. Par la suite, cette anomalie fut corrigée et le
      poids de marc uniformément utilisé.La municipalité de <ref target="#Lyon"> Lyon </ref> profita
      de la manne financière exceptionnelle représentée par la douane de Lyon. En effet, elle obtint
      du pouvoir royal un octroi de 45 000 livres – porté ensuite à 60 000 livres – sur le produit
      de la douane. En outre, la ville perçut un droit supplémentaire (le tiers-sur-taux, un tiers
      en sus du produit de la douane). En <date when="1632"> 1632 </date> , l’augmentation des
      droits et la possibilité laissée aux commis de les fixer de manière arbitraire sur certaines
      marchandises déclenchèrent une insurrection populaire bien connue. Les bureaux de la douane
      furent envahis, les archives détruites et le sous-fermier, Jean de Grange, échappa de peu au
      lynchage. Tout au long du siècle, la monarchie, régulièrement à court d’argent, intervint pour
      fixer le montant – toujours plus élevé – des droits perçus sur les soieries étrangères ( <date
       when="1667"> 1667 </date> , <date when="1687"> 1687 </date> , <date when="1692"> 1692 </date>
      , <date when="1711"> 1711 </date> ).Au début du XVIIIe siècle, la douane de Lyon contrôlait le
      marché national des soieries. Pour lutter contre la concurrence étrangère, le pouvoir royal
      interdit, dans un premier temps, l’importation de soie asiatique. C’est pour veiller à une
      stricte application de cet embargo que la douane de Lyon fut autorisée à déployer des bureaux
      dans les <placeName type="lieux_habitations"> ports de Nantes </placeName> et de Rouen pour
      contrôler les navires et lutter contre la contrebande. Cependant, jusqu’en <date when="1743">
       1743 </date> , le pouvoir royal conduisit une politique économique très erratique. <span
       subtype="normes" type="normes_contestations"> Après une timide tentative de libéralisation
       économique sous la Régence, au cours de laquelle l’entrée des soies étrangères fut tolérée
       dans tous les ports du royaume ( <date when="1720"> 1720 </date> - <date when="1722"> 1722
       </date> ), le gouvernement revint à un strict protectionnisme et les productions étrangères
       furent de nouveau contraintes de passer par Lyon où elles étaient lourdement taxées </span> .
      En revanche, la perception fut rationalisée : 14 sous par livre pour les soies étrangères, à
      l’exception des soies indiennes, pour lesquelles la taxe fut fixée à 6 sols. La progression
      des idées physiocratiques poussa le pouvoir central à revoir sa politique économique. En <date
       when="1755"> 1755 </date> , les soieries françaises furent exemptées des droits de douane
      internes et, surtout, obtinrent le droit de transiter dans tout le royaume sans passer par
      Lyon. Reste qu’en <date when="1772"> 1772 </date> , la municipalité de Lyon, à court de
      liquidité, obtint le rétablissement d’un droit de 3 sols six deniers par livre. Cette nouvelle
      mesure, jugée très nocive pour le commerce, fut abrogée en <date when="1775"> 1775 </date> .À
       <ref target="#Lyon"> Lyon </ref> , longtemps la douane fut établie dans le quartier de
      Saint-Paul, « assise sur le bord de la Saône avec un port suffisant et commode ». Là  se
      déroulaient les opérations de reconnaissance et de retrait des marchandises, l’acquittement
      des droits, ou  la prise des acquits à <ref target="#caution"> caution </ref> pour les
      marchandises en expédition. Les bureaux furent transférés dans l’hôtel des fermes bâti en
       <date when="1786"> 1786 </date> sur l’emplacement du bicêtre de l’hôpital de la Charité. Les
      travaux d’aménagement, supervisés par Jean Dupoux, s’élevèrent à 278 000 livres. Le transfert
      eut lieu précipitamment en février <date when="1789"> 1789 </date> à la suite des gels de
      l’hiver : le pont d’Alincourt fut emporté ; ceux de Saint-Vincent et Saint-Georges endommagés.
       <span subtype="encadrement" type="privleges_encadrement"> Le conseil de la Ferme générale
       donna son accord pour le transfert d’une partie des services de la Douane de Lyon vers la
       nouvelle douane, non sans inquiétude  : elle ne se dissimula pas les dangers d’une
      pareille scission des opérations mais la crainte des abus (engorgement, encombrement de la
      douane et fraude) lui parut devoir céder à l’emprise des circonstances… « Elle arrêta en
      conséquence qu’on détacheroit à la nouvelle douane une partie des préposés de l’ancienne et
      qu’on y feroit l’expédition de toutes les marchandises qui venant des Provinces méridionales
      en entrant par la Porte du Rhône se trouveroient à l’instant rendues sur le terrein de la
      charité et par conséquent à la nouvelle douane ».</span> En juillet <date when="1789"> 1789 </date>, l’ensemble des
       opérations, recettes, déclarations et visites, fut transféré à la nouvelle douane 
      . En août, Jean Dupoux eut encore le temps de faire ériger une barrière pour fermer la
      nouvelle douane, mais elle fut jugée trop petite.  A cette époque, comme l’écrivit le
      Contrôleur général Lambert à l’intendant de la généralité, Antoine-Jean Terray : « la
      suppression de la gabelle et les changements qui peuvent survenir dans le régime du tabac et
      des traites est susceptibles de donner lieu à de nouvelles dispositions relativement à
      l’étendue et la distribution des bâtiments ». En dehors de Lyon même, il fut permis au fermier
      de la douane d’établir des bureaux pour contrôler plus étroitement les flux commerciaux.
      L’inscription géographique de la douane de Lyon est difficile à établir avec précision mais
      selon Marcel Marion, le territoire est très bien quadrillé : cinquante-trois bureaux en
       <orgName subtype="province" type="pays_et_provinces">
       <ref target="#Dauphiné"> Dauphiné </ref>
      </orgName> , cinquante-six autour de <ref target="#Lyon"> Lyon </ref> , cinquante-huit vers la
       <orgName subtype="province" type="pays_et_provinces">
       <ref target="#Provence"> Provence </ref>
      </orgName> et le <orgName subtype="province" type="pays_et_provinces">
       <ref target="#Languedoc"> Languedoc </ref>
      </orgName> , et bien d’autres en <orgName subtype="province" type="pays_et_provinces">
       <ref target="#Bourgogne"> Bourgogne </ref>
      </orgName> , <orgName subtype="province" type="pays_et_provinces">
       <ref target="#Champagne"> Champagne </ref>
      </orgName> . En plus des différents commis
       qui officiaient dans chaque bureau, la douane de Lyon possèdait, depuis un édit de Charles IX
       de <date when="1563"> 1563 </date>, une juridiction très étoffée  : un bureau
      judiciaire, formé par les trésoriers de France, le sénéchal et son lieutenant, le maître des
       <ref target="#ports"> ports </ref> et son lieutenant, le procureur du roi, et divers avocats,
      ayant pour fonction première de régler les différends et les contraventions liées à la douane.
      En outre, les juges de la douane avaient le pouvoir de saisir une marchandise dans tout le
      ressort du royaume. Cette organisation fut complétée par deux édits royaux de <date when="1691"> 1691 </date> et <date when="1692"> 1692
       </date> qui confiaient la direction de la juridiction à un président juge . La douane
      de Lyon souffrait de la concurrence exercée par la douane de <ref target="#Valence"> Valence
      </ref> , établie à la fin du XVIe siècle, et avec laquelle les autorités lyonnaises
      entretiennent une intense rivalité tout au long de l’époque moderne. Dès <date when="1598">
       1598 </date> , les échevins de <ref target="#Lyon"> Lyon </ref> s’opposèrent à
      l’établissement de la douane de Valence et tentèrent de la court-circuiter auprès des
      trésoriers de France de <orgName subtype="province" type="pays_et_provinces">
       <ref target="#Dauphiné"> Dauphiné </ref>
      </orgName> ou des <ref target="#États"> États </ref> de la province. Au début du XVIIe siècle,
      ils remportèrent une victoire en obtenant que de nombreuses marchandises transitant par Lyon
      fussent déchargées des droits de la douane de Valence ( <date when="1623"> 1623 </date> ).
      Surtout, ils obtinrent de Louis XIII la suppression pure et simple de leur concurrente en
       <date when="1624"> 1624 </date> . Reste que cette victoire fut éphémère : Le duc de
      Lesdiguières la rétablit en <date when="1626"> 1626 </date> . Les marchands et les édiles
      lyonnais eurent beau se pourvoir devant le parlement de Grenoble, le gouvernement confirma le
      rétablissement. En <date when="1657"> 1657 </date> , soutenus par le Parlement de Dijon, les
      fermiers de la douane de Lyon parvinrent toutefois à limiter l’ingérence des agents de la
      douane de Valence sur la Saône. L’existence de deux douanes, géographiquement proches l’une de
      l’autre, percevant chacune des droits élevés, aurait, selon les contemporains comme
      Forbonnais, provoqué une lente asphyxie du commerce lyonnais. En effet, les marchands
      étrangers préféraient allonger la durée de leur trajet en contournant la vallée du Rhône par
      la <ref target="#Suisse"> Suisse </ref> , la <ref target="#Savoie"> Savoie </ref> et l’espace
      germanique. Les travaux récents montrent au contraire que les circulations dans le sillon
      rhodanien augmentèrent fortement entre le XVIIe et le XVIIIe siècle. Ce sont avant tout les
      carences en matière de communications, notamment fluviales, qui entravent le grand commerce.
       <span subtype="normes" type="normes_contestations"> Institution particulièrement détestée,
       critiquée dans les cahiers de doléance de la ville, la douane de Lyon est supprimée, comme
       toutes les douanes internes, par la loi du 30 octobre <date when="1790"> 1790 </date>
      </span> . </def>
     <bibl type="references">
      <bibl> [ <idno type="ArchivalIdentifier"> AN, G1 15, Délibération du 11 aout 1760 ; </idno>
      </bibl>
      <bibl>
       <idno type="ArchivalIdentifier"> AD Rhône, 1C 65, élection de Lyon, Mémoire de la Ferme
        générale, à Paris, en l’hôtel des fermes, 6 juillet 1789 ; lettre de Lambert à Terray, 7 juillet 1789 ; </idno></bibl>
      <bibl type="sources"> Tarif de la douane de Lyon pour le Roy, Lyon, A. Jullierond, 1680, 103 f° ; </bibl>
      <bibl type="sources"> François de Forbonnais, Recherches et considérations sur les finances de la France
       depuis l’année 1595 jusqu’à l’année 1721, 1758 ; </bibl>
      <bibl type="sources"> Fayard, « Douane de Lyon et de Valence », Bulletin de la société départementale
       d’archéologie et de statistique de la Drôme, Tome 2, 1867, p. 58-73 ; </bibl>
      <bibl> Marcel Marion, « Douane de Valence », Dictionnaire des institutions de la France aux
       XVIIe et XVIIIe siècle, Paris, Picard, 1923, p. 188-189 ; </bibl>
      <bibl> L’administration des douanes en France sous l’ancien régime, Paris, Association Pour
       L’histoire De L’administration Des Douanes, 1976] </bibl>
     </bibl>
    </sense>
   </entry>
  </body>
 </text>
</TEI>
