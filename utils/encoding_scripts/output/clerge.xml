<?xml version="1.0" encoding="utf-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title type="notice"> Clergé </title>
    <author>Marie-Laure Legay</author>
   </titleStmt>
   <publicationStmt>
    <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer le privilège la Ferme
     générale dans l'espace français et européen (1664-1794)Axe 1 : Dictionnaire de la Ferme
     générale, objet d'histoire totale </publisher>
    <pubPlace>
     <address>
      <addrLine> Maison Européenne des Sciences de l'Homme et de la Société </addrLine>
      <addrLine> 2 rue des cannoniers </addrLine>
      <addrLine> 59002 Lille Cedex </addrLine>
     </address>
     <date> 2021-2025 </date>
    </pubPlace>
    <availability>
     <licence> [A déterminer. Ex:Creative Commons Attribution 3.0 non transposé (CC BY 3.0)]
     </licence>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title> Dictionnaire numérique de la Ferme générale </title>
    <respStmt>
     <resp> Coordinateurs de l'axe : Dictionnaire numérique de la Ferme générale, objet d'histoire
      totale. </resp>
     <persName> Marie-Laure Legay </persName>
     <persName> Thomas Boullu </persName>
    </respStmt>
   </seriesStmt>
   <sourceDesc>
    <p> Dictionnaire numérique de la Ferme générale </p>
   </sourceDesc>
  </fileDesc>
  <encodingDesc>
   <projectDesc
    source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
    <p> Le projet FermGé vise à étudier l’impact d’une organisation fiscale (1664-1794),
     discriminante mais rationnelle, sur les territoires et les sociétés de la France moderne/p&gt;
    </p>
   </projectDesc>
  </encodingDesc>
  <profileDesc>
   <textClass>
    <keywords scheme="#fr_RAMEAU">
     <list>
      <item> Histoire -- Histoire moderne -- Histoire fiscale </item>
      <item> Histoire -- Histoire moderne -- Histoire judiciaire </item>
      <item> Histoire -- Histoire moderne -- Histoire politique </item>
     </list>
    </keywords>
   </textClass>
  </profileDesc>
  <revisionDesc>
   <change type="AutomaticallyEncoded"> 2022-12-13T13:11:13.407348+2:00 </change>
  </revisionDesc>
 </teiHeader>
 <text>
  <body>
   <entry type="C" xml:id="Clergé">
    <form type="lemma">
     <orth> Clergé </orth>
    </form>
    <sense>
     <def> Comme agents spirituels du pouvoir, on attendait des prêtres qu’ils soutinssent la lutte
      contre la <ref target="#contrebande"> contrebande </ref> grâce à leurs exhortations. Dans ce
      sens, le ministre Michel Chamillart rappela les devoirs des curés à l’évêque de Châlons en
       <date when="1705"> 1705 </date> : « l’usage de frauder les droits du Roy, particulièrement
      pour ce qui regarde la gabelle, s’est tellement introduit dans le Royaume qu’il n’est pas
      possible de croire que les curez et les confesseurs fassent leur devoir lorsque ceux qui font
      ce mauvais commerce se présentent à la confession ». Comme acteurs de la société, les membres
      du clergé, tant séculiers que réguliers, pratiquaient cependant toutes sortes de <ref
       target="#fraudes"> fraudes </ref> , bien qu’ils fussent soumis, comme les autres sujets du
      roi, aux grandes <ref target="#ordonnances"> ordonnances </ref> sur les <ref
       target="#gabelles"> gabelles </ref> , les <ref target="#aides"> aides </ref> , les <ref
       target="#traites"> traites </ref> ou le <ref target="#tabac"> tabac </ref> . <span
       subtype="normes" type="normes_contestations"> Les membres du premier ordre pris
       individuellement n’étaient pas considérés a priori comme <ref target="#privilégiés">
        privilégiés </ref> vis-à-vis des impôts indirects </span>. Le <ref target="#privilège"> privilège </ref> concernait les
       corps constitués et communautés et non l’ordre stricto sensu. Par exemple, l’ <ref
       target="#ordonnance"> ordonnance </ref> sur les gabelles de <date when="1680"> 1680 </date>
      listait dans son titre XIII les communautés qui bénéficiaient du <ref target="#franc-salé">
       franc-salé </ref> comme le chapitre de Notre-Dame de Paris, les Minimes de Vincennes pour
      lesquelles le <ref target="#minot"> minot </ref> était fixé à 50 sous, tandis que les autres
      monastères du <ref target="#grenier"> grenier de Paris </ref>
      ressortissaient à quatre livres et dix sous… Le clergé comme ordre n’était pas exempté de gabelle. Ses membres étaient inscrits dans les registres <ref target="#sextés"> sextés </ref>
      et si la distribution du sel de vente volontaire ne se faisait pas correctement auprès d’eux,
      le receveur du <ref target="#grenier"> grenier </ref> était blamé. Ainsi, l’inspecteur
      Languérat repéra en <date when="1708"> 1708 </date> que les quarante personnes de l’abbaye de
      <placeName type="lieux_habitations">Fervaques</placeName>, située à <placeName type="lieux_habitations">Saint-Quentin</placeName>, devaient prendre au <ref target="#grenier"> grenier </ref>
      trois <ref target="#minots"> minots </ref> de sel, et non un demi.Les caves et celliers des
      couvents conservaient des barils de <ref target="#vins"> vins </ref> tirés des vendanges
      soumis aux <ref target="#inventaires"> inventaires </ref> , droits de <ref target="#gros">
       gros </ref> , comme aux droits <ref target="#rétablis"> rétablis </ref> des inspecteurs aux
       <ref target="#boissons"> boissons </ref>. Les moines brasseurs devaient faire leur déclaration aux commis des fermes et accepter les
       visites  . Les ecclésiastiques firent naturellement valoir la dignité de leur état et
      l’inviolabilité de la clôture des abbayes et couvents pour lutter contre les nouvelles formes
      de l’inquisition fiscale dans leurs établissements. Vincent Delarue, sous-fermier des
         <ref target="#aides"> aides </ref> dans la <orgName
         subtype="administrations_juridictions_royales" type="administrations_juridictions">
         généralité d’Amiens </orgName> , en charge du doublement des droits des inspecteurs aux
         <ref target="#boissons"> boissons </ref> se plaignit à l’ <ref target="#intendant">
         intendant </ref> de la résistance des communautés religieuses en juillet <date when="1713">
         1713 </date> et requit la possibilité de visiter les enclos comme le rendait possible
        l’édit d’octobre <date when="1705"> 1705 </date> qui rétablit ces droits. Plus tard, Martin Girard,
       régisseur des mêmes droits des inspecteurs aux boissons depuis <date when="1722"> 1722
       </date> demanda expressément que l’édit de <date when="1705"> 1705 </date> f  ût
      respecté et que ses commis puissent inspecter les communautés religieuses, tant d’hommes que
      de femmes.La Ferme générale constatait non seulement la <ref target="#fraude"> fraude </ref>
      de ses droits, mais aussi la <ref target="#contrebande"> contrebande </ref> de marchandises
      prohibées comme le <ref target="#tabac"> tabac </ref> , de sorte qu’elle n’eut de cesse
      d’obtenir du roi les moyens d’amplifier les visites. En décembre <date when="1707"> 1707 </date>, une déclaration
       royale interdit toute plantation de tabac dans l’enceinte des communautés religieuses et les
       contraignit d’accepter les inspections. Les plaintes redoublèrent. En <date when="1721"> 1721 </date>, l’évêque
       de Chalon se plaignit de la visite des employés du tabac chez les Jacobines de la ville
       . <span subtype="normes" type="normes_contestations"> D’après l’arrêt du 25 janvier
        <date when="1724"> 1724 </date> , les <ref target="#capitaines"> capitaines </ref> des
       Fermes étaient autorisés à faire des inspections chez les <ref target="#privilégiés">
        privilégiés </ref> sans permission du juge </span> . Les Jacobins de la ville du Mans
      refusèrent néanmoins la perquisition « declarans qu’ils n’estoient point sujets à aucuns
      droits, qu’ils n’en avoient jamais payé n’y n’en vouloient payer ». Le prieur sonna la cloche
      du réfectoire en criant « à moy toute la communauté, venez nous aider à assommer les
      maltôtiers qui sont ici » ; les religieux s’armèrent de bâtons et les commis des aides durent
      fuir ( <date when="1725"> 1725 </date> ). Cet acte de <ref target="#rébellion"> rébellion </ref> leur valut l’amende de 500 livres, en
       sus de celle ordonnée pour refus d’ouverture des caves et celliers. Les cas de <ref
       target="#rébellion"> rébellion </ref> de couvents de <ref target="#femmes"> femmes </ref>
      sont également avérés comme celle des religieuses de Saint-François de La Flèche ( <date
       when="1724"> 1724 </date> ). En <date when="1734"> 1734 </date> , le Conseil d’Etat
      prescrivit la forme que devaient prendre les visites dans les abbayes et couvents de <ref
       target="#filles"> filles </ref> : les commis devaient avoir l’autorisation préalable de
      l’évêque diocésain ou d’un Grand-Vicaire et être accompagnés soit d’un officier du grenier,
      soit d’un juge des traites… selon la nature de la contrebande soupçonnée. D’autres cas furent signalés : le couvent des
      Capucins de <ref target="#Rethel-Mazarin"> Rethel-Mazarin </ref> fut condamné à 1 000 livres d’amende et à la <ref
        target="#saisie"> saisie </ref> de 60 livres-poids et dix onces de <ref target="#faux-tabac"
        > faux-tabac </ref> en <date when="1747"> 1747 </date>; pour avoir refusé l’ouverture des
       armoires et coffres de leur église, les doyen et chanoines de l’Eglise collégiale de
       Saint-Evroult furent condamnés à 500 livres d’amende en <date when="1755"> 1755 </date>
       ; de même, le curé de Saint-Vaast en <orgName subtype="province" type="pays_et_provinces"> Normandie </orgName> fut condamné en <date when="1768"> 1768
      </date> après la découverte de 492 livres de faux-tabac dans son presbytère… etc. Ces cas, et
      bien d’autres listés de son côté par Jean Nicolas, témoignaient de la volonté du législateur
      de faire porter les droits des Fermes sur les sujets du roi indistinctement. A l’échelle du royaume, les relations du
       clergé avec la Ferme générale, outre les liens familiaux qui unissaient les élites
       traditionnelles et nouvelles, prenaient la forme de versements financiers pour le compte du
       roi. L’édit du mois d’août <date when="1780"> 1780 </date> aliéna un million par an pendant quatorze ans sur le
      produit annuel du bail de la Ferme générale au profit du clergé. Ce « contre-don »
      s’expliquait: outre le don gratuit de 30 millions fourni au roi cette année-là, le clergé de France lançait des emprunts pour le
       compte du souverain  . La Ferme générale était alors mobilisée pour rembourser les
      sommes dues au clergé. Un double du bail des Fermiers généraux fut donné aux agents du clergé et la somme fut inscrite dans les
       états de dépense de la Ferme  . <span subtype="normes" type="normes_contestations">
       Cette somme fut portée à 1,2 million en <date when="1782"> 1782 </date> (édit de novembre)
      </span> . </def>
     <bibl type="references">
      <bibl> [ <idno type="ArchivalIdentifier"> AD Rhône, 5C/4, registre d’ordres de la Douane de
        Lyon, f° 152, 29 octobre 1755 ; </idno>
      </bibl>
      <bibl>
       <idno type="ArchivalIdentifier"> AN, F4 1012 : édit d’août 1780 ; </idno>
      </bibl>
      <bibl>
       <idno type="ArchivalIdentifier"> AN, G1 60, acquits de paiements au clergé de France ;
       </idno>
      </bibl>
      <bibl>
       <idno type="ArchivalIdentifier"> AN, G7 1170, procès-verbal de la tournée faite par
        l’inspecteur Languérat, 1708 ; </idno>
      </bibl>
      <bibl>
       <idno type="ArchivalIdentifier"> AN, G7 599, lettre de l’évêque de Chalon, décembre 1721 ; Requête de Vincent Delarue, fermier des aides, à Monseigneur de Bernage, intendant de
       la généralité d’Amiens, 22 juillet 1713 ; </idno></bibl>
      <bibl><idno type="ArchivalIdentifier"> BNF, Ms 23209, lettre de Chamillart à Gaston de Noailles, 4 novembre 1705 ;</idno> </bibl>
      <bibl type="sources"> Arrêt du Conseil d’Etat qui ordonne que les ecclésiastiques payeront les
       droits de subvention, jauge et courtage, anciens et nouveaux 5 sols et inspecteurs des
       boissons, pour les boissons qu'ils feront entrer tant d’achat que du crû de leur propre, 9
       février 1715 ; </bibl>
      <bibl type="sources"> Ordonnance de Mr l’Intendant de la généralité de Picardie et Artois qui
       fait défenses à toutes les communautés de religieux et religieuses de la province de
       Picardie, de brasser ni faire faire aucunes boissons dans leurs enclos ou couvents, sans en
       avoir fait déclaration aux plus prochains bureaux établis par Martin Girard, avant de faire
       mettre le feu sous leurs chaudières, 2 octobre 1723 ; </bibl>
      <bibl type="sources"> Arrêt du Conseil d’Etat du Roy qui condamne les religieux Jacobins de la
       ville du Mans, en l'amende de trois cens livres, pour refus par eux fait aux commis de Martin
       Girard, de l'ouverture des caves et celliers de leur maison conventuelle, et du payement des
       droits d'inspecteurs aux boissons, pour les vins qu'ils y ont fait entrer, et en une autre
       amende de cinq cens livres pour rebellion et voies de fait qu'ils ont commises contre lesdits
       employez, pour les empescher de faire leurs fonctions dans leur convent , 11 décembre 1725 ; </bibl>
      <bibl type="sources"> Arrêt du Conseil d’Etat du Roy qui prescrit la manière &amp; forme dans
       lesquelles les Commis des Fermes du Roi pourront faire des visites dans les Abbayes &amp;
       autres couvens de filles, 19 octobre 1734, dans Chambon (receveur des Fermes), Le commerce de
       l’Amérique par Marseille, Avignon, t. 1, 1764, p. 541-542 ; </bibl>
      <bibl type="sources"> Arrêt de la Cour des Aides du 2 septembre 1739, portant que les curés
       seront tenus de payer les droits de gros pour la vente des produits des dîmes ; </bibl>
      <bibl type="sources"> Arrêt du Conseil d’Etat qui casse un arrêt de la cour des Aides de
       Rouen, du 7 août 1767 et déclare bonne et valable la saisie de quatre cent quatre-vingt-douze
       livres de faux tabac ; trouvées dans le presbytère du sieur Sevestre, curé de la paroisse de Saint-Vaast en
       Normandie, 14 juin 1768 ; </bibl>
      <bibl type="sources"> Sentence de l’élection de Reims, 2 décembre 1747 ; </bibl>
      <bibl type="sources"> Édit du Roi portant aliénation au profit du Clergé, pendant 14 ans, d'un
       million sur le produit annuel du bail des fermes, Versailles, août 1780 ; </bibl>
      <bibl> L. Lembeye, Les vins de Bayonne et la franchise du Clergé sous l’Ancien régime, Pau,
       Lescher-Moutoué, 1937 ; </bibl>
      <bibl> Jean Nicolas, La rébellion française 1661-1789, Paris, Folio Histoire, 2008, p.
       139-144] </bibl>
     </bibl>
    </sense>
   </entry>
  </body>
 </text>
</TEI>
