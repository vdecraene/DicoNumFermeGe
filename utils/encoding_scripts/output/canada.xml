<?xml version="1.0" encoding="utf-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title type="notice"> Canada </title>
    <author>Marie-Laure Legay</author>
   </titleStmt>
   <publicationStmt>
    <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer le privilège la Ferme
     générale dans l'espace français et européen (1664-1794)Axe 1 : Dictionnaire de la Ferme
     générale, objet d'histoire totale </publisher>
    <pubPlace>
     <address>
      <addrLine> Maison Européenne des Sciences de l'Homme et de la Société </addrLine>
      <addrLine> 2 rue des cannoniers </addrLine>
      <addrLine> 59002 Lille Cedex </addrLine>
     </address>
     <date> 2021-2025 </date>
    </pubPlace>
    <availability>
     <licence> [A déterminer. Ex:Creative Commons Attribution 3.0 non transposé (CC BY 3.0)]
     </licence>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title> Dictionnaire numérique de la Ferme générale </title>
    <respStmt>
     <resp> Coordinateurs de l'axe : Dictionnaire numérique de la Ferme générale, objet d'histoire
      totale. </resp>
     <persName> Marie-Laure Legay </persName>
     <persName> Thomas Boullu </persName>
    </respStmt>
   </seriesStmt>
   <sourceDesc>
    <p> Dictionnaire numérique de la Ferme générale </p>
   </sourceDesc>
  </fileDesc>
  <encodingDesc>
   <projectDesc
    source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
    <p> Le projet FermGé vise à étudier l’impact d’une organisation fiscale (1664-1794),
     discriminante mais rationnelle, sur les territoires et les sociétés de la France moderne/p&gt;
    </p>
   </projectDesc>
  </encodingDesc>
  <profileDesc>
   <textClass>
    <keywords scheme="#fr_RAMEAU">
     <list>
      <item> Histoire -- Histoire moderne -- Histoire économique </item>
      <item> Histoire -- Histoire moderne -- Histoire financière </item>
      <item> Histoire -- Histoire moderne -- Histoire fiscale </item>
     </list>
    </keywords>
   </textClass>
  </profileDesc>
  <revisionDesc>
   <change type="AutomaticallyEncoded"> 2022-12-13T12:01:10.671501+2:00 </change>
  </revisionDesc>
 </teiHeader>
 <text>
  <body>
   <entry type="C" xml:id="Canada">
    <form type="lemma">
     <orth> Canada </orth>
    </form>
    <sense>
     <def> Le prélèvement fiscal sur les activités commerciales canadiennes ne doit pas être
      uniquement apprécié selon l’angle d’approche métropolitain, c’est-à-dire en ne considérant que
      les exportations vers la métropole. Il existait une fiscalité locale importante : on levait 10
      % sur les <ref target="#vins"> vins </ref> , <ref target="#eaux-de-vie"> eaux-de-vie </ref> et
       <ref target="#tabacs"> tabacs </ref> entrants (sauf pour les denrées prévues pour le
      ravitaillement des vaisseaux du roi), un droit de quart sur les castors que les habitants
      livraient au magasin du Fermier à Québec, soit 6 livres ( <date when="1686"> 1686 </date> ),
      puis 9 livres ( <date when="1687"> 1687 </date> ) sur les castors, un droit de dixième sur les
      peaux d’orignaux et des droits domaniaux et seigneuriaux (lods et ventes, confiscation…). La
      perception de l’ensemble des droits, ainsi que ceux des îles, fut confiée à la ferme du
      Domaine d’ <ref target="#Occident"> Occident </ref> . Celle-ci fut baillée à des cautions
      indépendantes puis intégrée en <ref target="#sous-fermes"> sous-fermes </ref> à la Ferme
      générale à partir du bail Fauconnet ( <date when="1685"> 1685 </date> ). Articulée à d’autres
      systèmes d’exploitation coloniale et notamment aux monopoles de traite (traite des castors,
      traite de <ref target="#Tadoussac"> Tadoussac </ref> ), la Ferme du Domaine d’ <ref
       target="#Occident"> Occident </ref> se scinda en <date when="1732"> 1732 </date> : d’une
      part, elle prit sur place la forme d’une régie sous l’autorité de la Marine, et d’autre part
      en métropole, elle continua le prélèvement des taxes à l’arrivée pour le compte de la Ferme
      générale. A cette date, on comptait à l’inventaire dans les <placeName type="lieux_controle">
       bureaux de Québec </placeName> dirigés par Etienne-François Cugnet, 24 registres dressés pour
      les formalités de déclarations, acquits, débarquement et embarquement, recettes des droits
      d’entrée, recettes des droits de sortie, achats pour les traites, ventes, contrôles, <ref
       target="#saisies"> saisies </ref> pour fraude… L’activité de la ferme était donc bien
      établie. Pour autant, la régie fit faillite ( <date when="1742"> 1742 </date> ). Outre le
      caractère hasardeux des montages financiers de son directeur (Cugnet fut maintenu à la tête du
      bureau), elle fut mise en difficulté par le caractère aléatoire des arrivages de boissons, par
      la concurrence des postes anglais pour capter la traite intérieure, par l’augmentation très
      sensible des dépenses. L’accroissement de la population, des frais d’aménagement, des frais de
      justice et des frais militaires liés à la guerre de Succession d’Autriche ( <date when="1744">
       1744 </date> - <date when="1748"> 1748 </date> ) engendrèrent un déséquilibre financier
      important avant même le début de la guerre de la Conquête ( <date when="1754"> 1754 </date> -
       <date when="1760"> 1760 </date> ).Le fermier du Domaine d’Occident jouissait des droits
      domaniaux, ordinaires et casuels, selon la coutume de Paris. Tant que le papier terrier ne fut
      pas entièrement confectionné, la perception demeura néanmoins difficile. <span
       subtype="normes" type="normes_contestations"> L’ <ref target="#intendant"> intendant </ref>
       Michel Bégon ordonna aux seigneurs et propriétaire en censives de faire leurs déclarations,
       aveux et dénombrements (déclaration du 24 mai <date when="1724"> 1724 </date> ), mais les
       droits ne furent payés que par ceux qui se présentaient volontairement </span> . De même, les
      droits de quint, lods et ventes, droits de relief ne furent perçus que lorsque les mutations
      étaient connues. A partir de <date when="1737"> 1737 </date> , la régie du Domaine prit en
      charge le coût de la confection du Papier terrier à hauteur de 1 200 livres par an. On établit
      en outre un receveur des droits à Montréal ( <date when="1739"> 1739 </date> ). Dans
      l’ensemble néanmoins, la recette des droits seigneuriaux et domaniaux ne s’éleva guère au-delà
      de 3 000 livres ( <date when="1741"> 1741 </date> ).Le prélèvement de 10 % sur les boissons à
      l’arrivée ne fut pas maintenu car les prix variaient considérablement selon les arrivages, ce
      qui entravait la gestion. L’intendant Michel Bégon fixa donc les droits à 9 livres la barrique
      de <ref target="#vin"> vin </ref> , 20 livres et 10 sous la barrique d’ <ref
       target="#eau-de-vie"> eau-de-vie </ref> et à 15 livres la barrique de guildive en provenance
      des îles. Les droits sur les <ref target="#tabacs"> tabacs </ref> , qui venaient pour
      l’essentiel du Brésil, furent fixés à la même époque à cinq sols par livre pesant. La gestion
      pour les droits d’entrée fut réorganisée par l’arrêt du 9 juin <date when="1722"> 1722 </date>
      qui insista sur la nécessité de placer, dès le mouillage dans la baie de Québec, un garde du
      Domaine d’Occident sur chaque bâtiment venant de France, des îles ou de l’Ile Royale. Les formalités de déclaration et de
       débarquement furent calquées sur celle de l’Ordonnance pour les Fermes de février <date
        when="1687"> 1687 </date>
      les congés et certificats délivrés par les commis aux écritures (4 en <date when="1748"> 1748
      </date> ) aux capitaines de vaisseaux… Rarement les droits d’entrée étaient réglés argent
      comptant. Les redevables ne payaient au bureau de la Ferme qu’au moment du départ des navires,
      en octobre, une fois les marchandises troquées en pelleterie ou en denrées du pays. La ferme du Domaine d’Occident faisait donc
       crédit pendant les campagnes de traite  . Elle accordait des délais de paiement, comme
      pour ce Duquesnel qui fit sa soumission le 17 octobre <date when="1737"> 1737 </date> pour
      1 793 livres qu’il promit de régler soit à Québec, soit en France. En dehors de ces denrées, les marchandises étaient libres de
       droit, mais elles faisaient tout de même l’objet de déclarations de quantités sous le titre
       de «  marchandises sèches ». Cugnet en estima la valeur en <date when="1730"> 1730
      </date> à 1,6 millions (contre 250 000 livres pour la valeur des boissons).Comme en métropole,
      ces droits faisaient l’objet de <ref target="#fraude"> fraude </ref> . On relève dans les
      archives d’Outre-Mer des saisies tant sur les bateaux que chez les <ref target="#cabaretiers">
       cabaretiers </ref> de Québec. La fraude provenait aussi de la différence de jauge entre les barriques selon leur provenance
       ; la plus grande (celle de <ref target="#Bordeaux"> Bordeaux </ref> ) était également
      employée par les négociants de La <ref target="#Rochelle"> Rochelle </ref> ou Nantes. Par
      ailleurs, le transbordement de bord à bord lorsque les marchandises étaient destinées à
      Trois-Rivières ou à Montréal occasionnait des abus. Enfin, des toiles d’ <ref target="#indiennes"> indiennes </ref>
       , prohibées en métropole, furent également passées en fraude au Canada  .A la sortie,
      seules les peaux d’orignaux étaient taxées, soit en nature, soit en argent (40 sols par peau
      verte, 20 sols par peau blanche). Depuis <date when="1717"> 1717 </date> , les castors
      faisaient l’objet du monopole de la compagnie d’Occident, intégrée à la compagnie des Indes (
       <date when="1719"> 1719 </date> ) et ne payaient plus de droits à la sortie.La fiscalité sur
      le commerce du Canada évolua tardivement. En avril <date when="1737"> 1737 </date> , le
      Conseil royal exempta les marchandises des îles destinées au Canada ou à l’Ile Royale de tout
      droit, mais maintint les 15 livres par barrique de guildive. Les droits sur les boissons
      constituaient donc toujours l’essentiel des recettes (autour de 50 à 60 000 livres) avec la
      traite de <ref target="#Tadoussac"> Tadoussac </ref> qui rapportait à peu près l’équivalent
      (voir tableau). Les arrivages de boissons conditionnaient pour partie les rapports avec les
      Indiens avec qui les colons négociaient la traite intérieure pelletière et huilière. La
      diminution de la distribution de l’eau-de-vie entrainait celle de la traite à Québec, Montréal
      et dans les postes de Frontenac, de Niagara et de <ref target="#Tadoussac"> Tadoussac </ref> ;
      cet affaiblissement engendrait à son tour une baisse des recettes fiscales sur les castors et
      autres orignaux. C’est pourquoi le gouvernement hésita à taxer trop ces denrées. Les droits
      furent néanmoins augmentés en <date when="1747"> 1747 </date> . Surtout, le gouvernement se
      décida à imposer les autres marchandises à l’entrée et à la sortie du Canada. Par l’édit de février <date when="1748"> 1748
       </date> , la régie du Domaine d’Occident fut autorisée à lever les 3 % de la valeur,
      sauf sur les denrées déjà taxées (boissons, tabacs et orignaux). Un tarif fut publié. Les
      négociants se plaignirent dès lors de la lourdeur des formalités de pesage, jauge,
      vérifications…, mais de fait, les recettes à l’entrée augmentèrent sensiblement pour atteindre
      316 018 livres brut en <date when="1749"> 1749 </date> .Il faut distinguer ici la traite à
      l’intérieur des terres et la vente exclusive vers la métropole. A l’intérieur, la Ferme avait depuis <date when="1677"> 1677
       </date> le privilège de la traite dans le domaine du roi (délimité définitivement en <date
        when="1733"> 1733 </date> ), traite dite de <ref target="#Tadoussac"> Tadoussac </ref>
      les peaux aux postes et bureaux de la Ferme. Celle-ci prélevait un quart pour son compte et
      achetait le reste qu’elle transportait en France.Comme celle du <ref target="#café"> café
      </ref> , l’organisation du commerce des castors prit plusieurs formes. Ces tâtonnements
      révélaient les difficultés capitalistiques dans un contexte concurrentiel élevé, aggravé par
      les guerres. La gestion des stocks importants, l’investissement dans la transformation des
      peaux, l’encadrement des prix… rendaient les profits incertains, non seulement pour les
      colons, mais aussi pour les Fermiers et, in fine, pour le fisc. Le gouvernement céda le <ref
       target="#privilège"> privilège </ref> de ce trafic à diverses compagnies : celle de la
      Nouvelle France en <date when="1628"> 1628 </date> , puis celle des Indes occidentales de
       <date when="1664"> 1664 </date> à <date when="1674"> 1674 </date> , date à laquelle le Canada
      fut réuni à la Couronne et le commerce de castors confié à la Ferme du Domaine d’ <ref
       target="#Occident"> Occident </ref> . Le marché était somme toute assez simple : la Ferme
      procédait aux achats, au transport et à la vente en métropole en se réservant le quart des
      castors au titre des bénéfices. Les adjudicataires successifs (voir notice Domaine d’ <ref
       target="#Occident"> Occident </ref> ) renouvelèrent ce fructueux marché de pelleteries. Ils
      établirent leurs bureaux à Québec. Là, les commis procédaient au pesage et au conditionnement
      dans des ballots. La traite s’étalait du 1er juillet au 20 octobre. En dehors de cette
      période, les castors étaient entreposés pour l’année suivante. Les fermiers de Québec
      recevaient également les pelleteries de la compagnie de l’Acadie ( <date when="1683"> 1683
      </date> - <date when="1703"> 1703 </date> ), compagnie qui pouvait négocier ses peaux avec la
      Ferme soit à Québec, soit à l’arrivée en métropole. Les prix étaient encadrés, tant à l’achat
      qu’à la vente. Au temps du bail Louis Guigues ( <date when="1697"> 1697 </date> - <date
       when="1700"> 1700 </date> ), le « gras d’hiver » s’achetait 6 livres, le demi-gras 3 livres
      et le sec (« sec d’hiver », « sec de l’Illinois », « sec d’été ») une livre. Ces castors,
      transformés sur place, pouvaient entrer par tous les ports de la métropole. La Ferme disposait du privilège d’entrepôt
      ordinaires des Cinq grosses <ref target="#fermes"> fermes </ref> .Pour favoriser le commerce
      de la Ferme, le castor « étranger » (des habitations anglaises) fut taxé lourdement (un écu
      pour chaque livre pesant de peaux, deux écus pour chaque livre pesant de poils) par arrêt du
      24 mars <date when="1685"> 1685 </date> . De plus, la traite étrangère des castors ne pouvait
      entrer en France que par les <placeName type="lieux_habitations"> ports de Rouen </placeName>
      , <ref target="#Dieppe"> Dieppe </ref> , Le <ref target="#Havre"> Havre </ref> et La <ref
       target="#Rochelle"> Rochelle </ref> . Malgré ces précautions, l’abondance des peaux de
      castors débarquées en Europe n’empêcha pas la fraude. Lorsque Louis Guigues succéda à la ferme
      Pointeau ( <date when="1697"> 1697 </date> ), il reçut de son prédécesseur 960 185 livres de
      castors qu’il voulut transformer pour en accélérer l’écoulement. Il établit une manufacture de
      chapeaux à Paris, faubourg Saint-Antoine, mais les chapeliers, malgré les conventions passées,
      se fournirent préférentiellement en peaux illicites. L’entrée des castors « étrangers » fut
      donc réduite aux <placeName type="lieux_habitations"> ports de Rouen </placeName> et La
      Rochelle (arrêt du 26 août <date when="1698"> 1698 </date> ) pour tenter de limiter la fraude.
      La concurrence des castors « anglais » d’une part et les circonstances de la guerre d’autre
      part, plaçaient la colonie de la Nouvelle France dans un situation misérable. <span
       subtype="encadrement" type="privleges_encadrement"> Leur mésentente avec le directeur de la
       Ferme du Domaine d’Occident, Daubenton de Villebois, amena le roi à accorder aux colons du
       Canada, de la baie du Nord, et des autres lieux de la Nouvelle-France la liberté de faire le
       commerce des castors tant en France qu’à l’étranger, en réglant les droits du quart à la
       Ferme (traités de janvier et février <date when="1700"> 1700 </date> ) </span> . Ces traités
      conservaient les privilèges reconnus à la compagnie de l’Acadie. Finalement, par le traité du
      3 juin <date when="1700"> 1700 </date> , Louis Guigues céda à la colonie, outre le droit de
      vente exclusif de castors, la jouissance des fermes de Canada, soit le droit de quart des
      castors et du dixième des orignaux et la ferme générale du droit de marque des chapeaux pour
      les mêmes douze années.Après la guerre de Succession d’Espagne ( <date when="1703"> 1703
      </date> - <date when="1714"> 1714 </date> ) et la perte de l’Acadie, la traite des castors
      s’organisa comme suit : la vente exclusive fut confiée à la compagnie d’Occident ( <date
       when="1717"> 1717 </date> ), puis à la compagnie des Indes ( <date when="1719"> 1719 </date>
      ). Néanmoins, les habitants du Canada conservèrent toujours la liberté de commercer dans la
      colonie elle-même avec les nations premières (Anishinabés et Hurons-Wendat). La Ferme du
      Domaine disposait du monopole de cette traite dans les limites du Domaine dit de Tadoussac.
      Celui-ci comprenait les postes de Tadoussac (1 commis), Chicoutimi (1 commis) ; le poste et
      magasin des Ilets de Jérémie (avec l’étendue de Manikouagan) ; le poste de la rivière Moïsie
      (1 commis). Du poste de Chicoutimi dépendaient les postes du lac Saint-Jean à 30 lieues en
      remontant la rivière du Saguenay (1 commis), les poste du Grand et des petits Mistassini (1
      commis pour ces 2 postes). L’intendant disposa seul de la connaissance des contraventions
      concernant cette traite (arrêt du 4 juin <date when="1719"> 1719 </date> ).Lorsque la ferme du Domaine d’Occident fut mise
       en régie sous le contrôle de la Marine en <date when="1732"> 1732 </date> , la traite de
       Tadoussac fut confiée à bail au directeur de ladite régie, Etienne-François Cugnet, qui
       portait donc une double casquette  .La principale difficulté sur place consistait à
      éviter d’une part que les habitants des abords du Domaine empiètent sur le monopole de
      Tadoussac, et d’autre part à éviter l’introduction des castors de la Nouvelle-Angleterre. Gouverneurs et <ref
        target="#intendants"> intendants </ref> soutenaient la lutte contre la fraude aux droits en
       missionnant des détachements au lac Champlain pour arrêter les castors anglais (2 décembre
        <date when="1721"> 1721 </date> ), ou en missionnant le subdélégué de Montréal aux visites
       des maisons de la ville pour la recherche des peaux (9 septembre <date when="1722"> 1722
       </date> ). Pour mieux assoir le contrôle, la
        Ferme du Domaine basée à Québec et la compagnie des Indes basée à Montréal, signèrent une
        convention (8 novembre <date when="1724"> 1724 </date> ) pour lutter contre la fraude aux
        castors. Les deux directeurs, Cugnet d’un côté, Eustache Chartier de Lotbinière de l’autre,
      établir les responsabilités respectives des corps de gardes et les modalités de partage en cas
      de <ref target="#saisies"> saisies </ref>. La convention fut renouvelée en <date when="1729"> 1729 </date>
       .La régie du Domaine d’Occident au Canada ( <date when="1733"> 1733 </date> - <date
       when="1759"> 1759 </date> )En <date when="1732"> 1732 </date> , le Domaine d’Occident fut
      réuni au Secrétariat d’Etat à la Marine. Son directeur, François-Etienne Cugnet, fut maintenu
      à sa direction.Recettes et dépenses de la régie du Domaine d’Occident au Canada ( <date
       when="1733"> 1733 </date> - <date when="1743"> 1743 </date> )On mesure à travers ce tableau
      l’importance des droits d’entrée sur les boissons et la faiblesse des autres droits. La traite
      de Tadoussac rapportait brut une cinquantaine de milliers de livres, mais les
      approvisionnements des postes en marchandises pour traiter avec les Indiens grevaient ces
      recettes. La régie décida donc de mettre la traite en affermage et la confia à Cugnet pour
      4 500 livres annuelles ( <date when="1737"> 1737 </date> ). Le produit net du Domaine
      d’Occident entrait en recette dans le compte du Trésorier général de la Marine, Marcelin
      François Zacharie de Selle. Son commis sur place, Thomas-Jacques Taschereau, comptait en outre
      les lettres de change tirées sur le Domaine d’Occident trésorier, soit en:Le Domaine
      d’Occident contribuait bel et bien aux dépenses de la Marine, mais l’on doit rapporter le
      total des lettres de change tirées sur le Domaine à celui de l’ensemble des lettres tirées sur
      le Trésorier au Canada. En <date when="1745"> 1745 </date> , pendant la guerre de Succession
      d’Autriche, ce ratio monta à 7,2 % (56 727 livres sur 778 327). En outre, les dettes de Cugnet
      vis-à-vis du Domaine, évaluées en <date when="1742"> 1742 </date> à 64 302 livres, pesèrent
      sur la gestion publique de la colonie. Engagés avec ses associés dans l’exploitation des
      forges du Saint-Maurice, il subit des revers importants. Le roi prit en paiement les forges
      elles-mêmes, les réunit au Domaine ( <date when="1743"> 1743 </date> ) et déchargea Cugnet.
      Malgré tout, la régie du Domaine d’ <ref target="#Occident"> Occident </ref> enregistra au
      Canada en <date when="1752"> 1752 </date> une recette de 376 394 livres et une dépense de
      188 220 livres, soit un débet clair de 188 174 livres.Il est certain que l‘organisation du
      Domaine d’Occident constitua une réponse hybride aux défis que l’exploitation coloniale
      soulevait. Dans le cas du Canada, en concédant le monopole de la traite pelletière et huilière
      dans le Domaine du roi à la Ferme, l’Etat en faisait un agent commercial (traites des castors)
      tout autant que fiscal (droits domaniaux, droits sur les boissons, tabacs et sur les
      orignaux), articulant ainsi les responsabilités politiques aux engagements d’exploitation. Inversement, la compagnie des
         Indes, clairement identifiée comme agent commercial, participa par convention à la lutte
         contre la fraude aux droits en mettant ses employés de Montréal au service du contrôle
         fiscal ( <date when="1724"> 1724 </date> ). Ces modes opératoires révèlent une politique coloniale pragmatique caractéristique
      de l’entreprise ultramarine. <span subtype="encadrement" type="privleges_encadrement">
       Celle-ci nécessitait la concession de privilèges économiques sous forme de monopoles et de
       privilèges financiers sous forme de fermes fiscales </span> . Comme dans le royaume, la
      gestion publique coloniale engageait l’Etat à faire appel à des affairistes, à l’instar de
      Jean Oudiette, Louis Guigues, ou de François-Etienne Cugnet. Cet arrimage de l’Etat au
      business renvoie au temps de l’elusive empire défini par James Pritchard. <span
       subtype="normes" type="normes_contestations"> Pour autant, la logique gestionnaire (fondée
       sur les opérations de prévision, exécution et contrôle des états du roi) fut mise en œuvre
       avant même que l’adjudicataire de la Ferme générale remit les effets de la Ferme du Domaine
       d’Occident sur place au Trésorier de la Marine ( <date when="1732"> 1732 </date> ) </span> .
      Les agents de la Ferme générale, des sous-fermes ou des compagnies, tenaient les registres,
      maîtrisaient les arcanes de la comptabilité et, comme à Paris, les opérations utiles à
      l’exploitation des droits. </def>
     <bibl type="references">
      <bibl> [ <idno type="ArchivalIdentifier"> ANOM, fonds du Secrétariat d’Etat à la Marine et aux
        colonies COL C11A, 15 à 121, numérisés sur nouvelle-France.org ; dont 6, f° 469-473, « Mémoire concernant le commerce des pelleteries et la ferme du
       Domaine d’Occident », 1684 ; 11, f° 171-176 v : « Mémoire sur la ferme du Domaine d’Occident », 1690 ; 13, f° 420-430 v : « Mémoire sur les obligations et privilèges du fermier du Domaine
       d’Occident », 1695 ; 14, f° 331-352, « Mémoire concernant la ferme du Domaine d’Occident au Canada », 1er
       mai 1696 ; 16, f° 206-217 v, « Mémoire sur la ferme du Domaine d’Occident », 28 décembre 1698 ;  36, f° 359-362, « Etat du produit de la Ferme du Domaine d’Occident », 1er janvier
       1716 ;44, f° 408, 2 mai 1722 ; 47, f° 408-411, convention entre le Domaine d’Occident et la compagnie des Indes, 8
       novembre 1724 ; 48, f° 445-446, « Mémoire des fermiers du Domaine d’Occident à Maurepas », 1er avril
       1726 ; 51, f° 339-341, Convention entre le Domaine d’Occident et la compagnie des Indes, 18
       octobre 1729 ;53, f° 113-127, Lettre de Gilles Hocquart, 17 octobre 1730 ;  81, f° 12-37, 1744 ; 96, f° 101-103, Concernant la veuve Fornel, 1750 ;  121, f° 218-236, Régie du Domaine d’Occident, février 1748 ; </idno></bibl>
      <bibl>
       <idno type="ArchivalIdentifier"> AN G1 80, dossier 14, tableau des pelleteries importées en
        1784 ; </idno>
      </bibl>
      <bibl type="sources">
       <idno type="ArchivalIdentifier"> AN, G7 1314, Traité du 3 juin 1700 ; </idno>
      </bibl>
      <bibl>
       <idno type="ArchivalIdentifier"> G7 1314, dossier 18 : compte que la colonie rend au fermier
        Louis Guigue, année après année : la colonie doit 94 423 livres ; mais le fermier doit 31 898 livres, donc la colonie doit au fermier 65 525 ; </idno> </bibl>
      <bibl type="sources"> Arrêt du Conseil d’Etat qui ordonne que les pelleteries et denrées
       provenant du crû et fabrique de Canada, de quelque nature qu'elles puissent être, à
       l'exception du Castor, jouiront du bénéfice du transit, 21 mai 1721 ; </bibl>
      <bibl type="sources"> Arrêt du Conseil d'Etat qui casse deux sentences rendues par les maire
       et échevins du Hâvre, les 9 juin et 4 juillet 1733, décharge le Sr Le Vaillant, négociant de
       ladite ville, des sommes de 1243 livres 4 sols et de 295 liv. 6 sols 3 deniers auxquelles il
       a été condamné par lesdites sentences pour les droits d'octrois du Hâvre sur des vins et
       eaux-de-vie venant de Bordeaux et de La Rochelle destinés à être transportés au Canada. Et
       qui ordonne que tous les armateurs ou négociants qui armeront dans la ville du Havre des
       vaisseaux destinés pour les îles françaises de l'Amérique jouiront de l'exemption des droits
       d'octrois de ladite ville, 25 mai 1734 ; </bibl>
      <bibl type="sources"> Joseph Du Fresne de Francheville, Histoire de la Compagnie des Indes avec les titres de
       ses concessions et privilèges, dressée sur les pièces autentiques, Paris, Chez De Bure
       l'aîné, 2 t., 1746, dont tome 1, p. 96-106 ; </bibl>
      <bibl> Lucien Campeau, Les finances publiques de la Nouvelle-France sous Les Cent-Associés,
       Montréal, Bellarmin, 1975 ; </bibl>
      <bibl> Laurier Turgeon, Les échanges franco-canadiens de 1713 à 1758: Bayonne, les ports
       basques et Louisbourg, Île Royale, Pau, 1977 ; </bibl>
      <bibl> Josette Louise Dechêne et Richard C. Harris (éd.), Atlas historique du Canada,
       Montréal, PUM, 1987 ; </bibl>
      <bibl> Craig Brown (éd.), Histoire générale du Canada. Éd. française de P.-A. Linteau, Boréal,
       1990 ; </bibl>
      <bibl> Bruce G. Trigger, Les indiens, la fourrure et les blancs. Français et
       Amérindiens en Amérique du Nord, trad. G. Kahl, Montréal, Editions du Boréal, 1992 ; </bibl>
      <bibl> Gilles Havard, Cécile Vidal, Histoire de l’Amérique française, Paris, Flammarion,
       2003 ; </bibl>
      <bibl> James Pritchard, In search of empire : the French in the Americas, 1670-1730,
       Cambridge, Cambridge University Press, 2004 ; </bibl>
      <bibl> Michel Lavoie : Michel Lavoie, Le Domaine du roi, 1652-1859 : souveraineté, contrôle,
       mainmise, propriété, possession, exploitation, Québec, Septentrion, 2010 ; </bibl>
      <bibl> Catherine Desbarats et Thoams Wien (dir.), « La Nouvelle-France et l’Atlantique »,
       numéro spécial, Revue d’histoire de l’Amérique française, vol. 64, n° 3-4, 2011, dont la
       contribution de Catherine Desbarats et Allan Greer, « Où est la Nouvelle-France ?», p. 31-62]
      </bibl>
     </bibl>
    </sense>
   </entry>
  </body>
 </text>
</TEI>
