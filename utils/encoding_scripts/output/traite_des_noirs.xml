<?xml version="1.0" encoding="utf-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title type="notice"> Traite des noirs </title>
    <author>Marie-Laure Legay</author>
   </titleStmt>
   <publicationStmt>
    <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer le privilège la Ferme
     générale dans l'espace français et européen (1664-1794)Axe 1 : Dictionnaire de la Ferme
     générale, objet d'histoire totale </publisher>
    <pubPlace>
     <address>
      <addrLine> Maison Européenne des Sciences de l'Homme et de la Société </addrLine>
      <addrLine> 2 rue des cannoniers </addrLine>
      <addrLine> 59002 Lille Cedex </addrLine>
     </address>
     <date> 2021-2025 </date>
    </pubPlace>
    <availability>
     <licence> [A déterminer. Ex:Creative Commons Attribution 3.0 non transposé (CC BY 3.0)]
     </licence>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title> Dictionnaire numérique de la Ferme générale </title>
    <respStmt>
     <resp> Coordinateurs de l'axe : Dictionnaire numérique de la Ferme générale, objet d'histoire
      totale. </resp>
     <persName> Marie-Laure Legay </persName>
     <persName> Thomas Boullu </persName>
    </respStmt>
   </seriesStmt>
   <sourceDesc>
    <p> Dictionnaire numérique de la Ferme générale </p>
   </sourceDesc>
  </fileDesc>
  <encodingDesc>
   <projectDesc
    source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
    <p> Le projet FermGé vise à étudier l’impact d’une organisation fiscale (1664-1794),
     discriminante mais rationnelle, sur les territoires et les sociétés de la France moderne/p&gt;
    </p>
   </projectDesc>
   
  </encodingDesc><profileDesc>
    <textClass>
     <keywords scheme="#fr_RAMEAU">
      <list>
       <item> Histoire -- Histoire moderne -- Histoire commerciale </item>
       <item> Histoire -- Histoire moderne -- Histoire financière </item>
       <item> Histoire -- Histoire moderne -- Histoire fiscale </item>
      </list>
     </keywords>
    </textClass>
   </profileDesc>
   <revisionDesc>
    <change type="AutomaticallyEncoded"> 2022-12-20T16:05:55.745266+2:00 </change>
   </revisionDesc>
 </teiHeader>
 <text>
  <body>
   <entry type="T" xml:id="Traite_des_noirs">
    <form type="lemma">
     <orth> Traite des noirs </orth>
    </form>
    <sense>
     <def> La fiscalité royale favorisa le commerce des noirs de trois façons : en exemptant de la
      moitié des droits du Domaine d’ <ref target="#Occident"> Occident </ref> les marchandises « de
      toutes sortes » apportées des concessions africaines vers les colonies américaines, en
      exemptant de droits de sortie du royaume les vivres et munitions embarquées pour l’Afrique et
      en exemptant de la moitié des droits de traites les marchandises des îles issues du commerce
      triangulaire à l’entrée du royaume (arrêt du 30 mai <date when="1664"> 1664 </date> ). Pour
      les commerçants, ces avantages fiscaux s’ajoutaient à la prime de 13 livres par esclave
      transporté dans les <ref target="#colonies"> colonies </ref> , prime concédée par le Trésor
      royal.Après l’échec des premières compagnies à monopole (Compagnie du Cap Vert et du Sénégal
      en activité entre <date when="1658"> 1658 </date> et <date when="1664"> 1664 </date> ,
      Compagnie des Indes occidentales entre <date when="1664"> 1664 </date> et <date when="1672">
       1672 </date> ), la traite fut confiée à une nouvelle compagnie du Sénégal qui s’engagea à
      transporter 2 000 noirs par an pendant huit ans aux Antilles moyennant une prime de 13 livres
      par tête. A cette époque, Colbert souhaitait encourager la traite nationale qui ne couvrait
      pas suffisamment les besoins des plantations antillaises. <span subtype="encadrement"
       type="privleges_encadrement"> Un premier traité avait été conclu pour la Guinée entre
       Oudiette, fermier des droits du Domaine d’ <ref target="#Occident"> Occident </ref> et les
       directeurs de cette administration pour faire passer 800 noirs (16 octobre <date when="1675">
        1675 </date> ), mais cet accord ne fut pas exécuté </span>. La prime de 13 livres fut conservée en faveur des compagnies
       suivantes, celle de Guinée (création en janvier <date when="1685"> 1685 </date> ) et celle du
       Sénégal (refondation en <date when="1684"> 1684 </date> et <date when="1694"> 1694 </date> ,
       lettres patentes de <date when="1696"> 1696 </date> ), puis la compagnie des <ref
        target="#Indes"> Indes </ref> reconstituée par John Law (arrêt du 27 septembre <date
        when="1720"> 1720 </date> )  . D’après les certificats délivrés par l’intendant à
      Cap-Français (Saint-Domingue), 2 635 têtes furent débarquées entre le 17 avril <date
       when="1714"> 1714 </date> et le 27 août <date when="1716"> 1716 </date> (le montant de la
      prime s’éleva à 34 374 livres et sept sols), et 1 151 têtes entre le 2 février <date
       when="1717"> 1717 </date> et le 22 février <date when="1718"> 1718 </date> (14 963 livres de
      prime réglée par le Trésor royal), et ainsi durant le XVIIIe siècle. La compagnie des <ref
       target="#Indes"> Indes </ref> se déchargea par la suite du commerce des noirs sur les
      négociants. En <date when="1767"> 1767 </date> , la traite devint libre, à charge pour les
      maisons de commerce de verser 10 livres par tête directement dans les caisses du roi. Cette
      prime ne concernait pas la Ferme générale. En revanche, la Ferme générale fut impliquée dans les choix de
       politique fiscale en faveur de la traite. Les compagnies de commerce bénéficiaient de l’exemption des
       droits de sortie sur les vivres et munitions destinées aux voyages vers les côtes africaines
       (arrêt des 18 septembre et 25 novembre <date when="1671"> 1671 </date> ), et d’une manière
       générale de l’exemption des droits (octrois, péages, passages, traites <ref
        target="#domaniales"> domaniales </ref>
      devaient présenter à la Ferme générale les soumissions certifiant que les marchandises étaient
      destinées au commerce avec l’Afrique. Le ministre Chamillart ordonna le 17 juillet <date
       when="1704"> 1704 </date> aux Fermiers de respecter les privilèges de la compagnie royale du
      Sénégal. Desmaretz agit de même le 14 janvier <date when="1714"> 1714 </date> , exhortant les
      commis des droits de Convoi et <ref target="#Comptablie"> Comptablie </ref> de <ref
       target="#Bordeaux"> Bordeaux </ref> de laisser sortir les <ref target="#vins"> vins </ref>
      destinés au <ref target="#Havre"> Havre </ref> et à Honfleur pour être transborder dans les
      navires affrétés pour l’Afrique. <span subtype="privileges" type="privleges_encadrement">
       Lorsque le commerce avec la Guinée devint libre (de mars <date when="1716"> 1716 </date> à
       janvier <date when="1720"> 1720 </date> ), l’exemption des droits de sortie fut également
       acquise pour les marchandises du cru du royaume ( <ref target="#toiles"> toiles </ref> ,
       quincaillerie, mercerie, verroterie, fusils </span> …).De même, les compagnies de commerce,
      et plus tard les négociants, furent exemptés de la moitié des droits d’entrée sur les
      marchandises rapportées soit des côtes d’Afrique soit des îles d’Amérique. Cet avantage fut
      rappelé pour la compagnie de Guinée en <date when="1688"> 1688 </date> (arrêt du 20 mars) et
      pour la compagnie du Sénégal en <date when="1690"> 1690 </date> (arrêt du 16 décembre). Dès lors, la Ferme générale dut mettre en œuvre
       les contrôles nécessaires à la jouissance de ce <ref target="#privilège"> privilège </ref>
       . <span subtype="privileges" type="privleges_encadrement"> En <date when="1702"> 1702
       </date> par exemple, le fermier général Thomas Templier s’opposa aux marchands de <ref
        target="#Nantes"> Nantes </ref> et d’Angers, Goujon et Henriot, qui réclamaient la
       restitution de 548 livres payés au <placeName type="lieux_controle"> bureau d’Ingrandes
       </placeName> pour neuf barriques de <ref target="#sucre"> sucre </ref> qu’ils avaient
       achetées à la compagnie de Guinée, au titre de l’exemption des droits </span> . Pour Templier
      toutefois, le <ref target="#privilège"> privilège </ref> accordé à la compagnie de Guinée
      était personnel et ne pouvait s’étendre à ceux qui achetaient les marchandises. <span
       subtype="privileges" type="privleges_encadrement"> Pour bénéficier de cette exemption à
       l’entrée, les capitaines de vaisseaux devaient se munir des factures issues de la vente des
       noirs dans les <ref target="#colonies"> colonies </ref> car de la valeur de cette vente
       dépendait le montant de l’exemption (la déduction se faisait jusqu’à concurrence du montant
       de la facture) </span> . Sur place, ces factures faisaient l’objet d’un visa des employés du
      Domaine d’ <ref target="#Occident"> Occident </ref> et d’un certificat de l’administration de
      l’intendant. Ces certificats étaient présentés à l’arrivée en métropole. La Ferme générale
      tenait les comptes de tous les certificats à Paris.Notons que seuls les droits de <ref
       target="#traites"> traites </ref> pouvaient être réduits au retour ; ceux du Domaine d’ <ref
       target="#Occident"> Occident </ref> (3% de la valeur des marchandises acquittés à l’entrée
      dans les <placeName type="lieux_habitations"> ports de France </placeName> , quarante sols sur
      les <ref target="#sucres"> sucres </ref> ) restaient dus. Les armateurs de <ref
       target="#Bordeaux"> Bordeaux </ref> tentèrent sans succès d’échapper à ces droits de nature
      domaniale. La Ferme d’Occident fit régulièrement les représentations nécessaires pour défendre
      ses intérêts, au temps d’Oudiette, de Jean Fauconnet et pendant tout le XVIIIe siècle. <span
       subtype="encadrement" type="privleges_encadrement"> La question fut âprement discutée devant
       la <ref target="#Cour"> Cour </ref> des <ref target="#aides"> aides </ref> car initialement,
       le <ref target="#privilège"> privilège </ref> des compagnies à monopole reconnaissait la
       réduction de moitié des droits du Domaine d’ <ref target="#Occident"> Occident </ref> ,
       lorsque celui-ci se prélevait sur place en Amérique </span> . Puis la Ferme d’Occident
      organisa la levée au retour en métropole, ce qui incita les négociants à les confondre avec
      les droits de traites. <span subtype="normes" type="normes_contestations"> Le conflit portait également sur le
          <ref target="#privilège"> privilège </ref> d’entrepôt, privilège qui entrait en
         contradiction avec l’esprit de la loi de <date when="1717"> 1717 </date> , mais qui, s’il
         était accordé (comme ce fut le cas pour la compagnie du Sénégal pendant dix ans par les
         lettres patentes de <date when="1696"> 1696 </date> ), privait en effet la Ferme d’Occident
         des droits en question
      </span> . Les arrêts des 26 mars <date when="1722"> 1722 </date> et 14 août <date when="1725">
       1725 </date> conclurent en faveur de cette dernière  : les 3% d’imposition sur les marchandises (et quarante sols sur les <ref
       target="#sucres"> sucres </ref> ) provenant de la traite des noirs devaient être réglés
      intégralement. Les négociants cherchèrent à tirer profit du privilège commercial en fraudant de diverses manières sur les
       formalités. « Les fermiers généraux ont exposé
         plusieurs fois au Conseil la fraude qui se faisait de moitié des droits d’entrée imposés
         sur les marchandises venant des îles et colonies françaises dans les ports du royaume à la
         faveur de l’exemption accordée aux marchandises provenant du troc des nègres introduits aux
         îles ». <span subtype="normes" type="normes_contestations"> Le roi ordonna donc, par la
       déclaration du 6 juillet <date when="1734"> 1734 </date> , de suivre un modèle de certificat
       contenant d’abord la facture qui devait être certifiée par l’agent de la cargaison, ensuite
       un bordereau contenant le prix des nègres et les notes par extrait des marchandises expédiées
       pour la France à compte de de ce prix et enfin le certificat de l’intendant </span> . Le
      certificat ne devait être livré qu’aux seuls armateurs qui géraient la cargaison et les commis
      du Domaine d’ <ref target="#Occident"> Occident </ref> certifiaient par leur « vu »
      l’embarquement aux îles des marchandises énoncées en la facture qui devait être mise en tête
      du certificat. Les négociants continuèrent à frauder néanmoins. Un mémoire de <date
       when="1741"> 1741 </date> en donne un exemple : Deluynes, négociant à <ref target="#Nantes">
       Nantes </ref> introduisit à Léogâne le 19 août <date when="1739"> 1739 </date> 265 nègres
      dont la vente avait produit 251 925 livres. Il associa à cette vente plusieurs bordereaux, ce qui lui
       permit de faire passer davantage de marchandises en exemption des droits à <ref
        target="#Nantes"> Nantes </ref>. Au pied de ces bordereaux, il se faisait donner le certificat par les commis de
      l’intendant ordonnateur qui n’était pas à portée de les vérifier. La Ferme générale donna des ordres au <ref target="#Directeur">
        Directeur </ref> de Nantes pour poursuivre la confiscation des marchandises et mettre
       Deluynes à l’amende. Lorsqu’il fut décréter en <date
         when="1741"> 1741 </date> (permission du Conseil en date du 30 septembre) que le commerce
        de Guinée pouvait se faire dans tous les ports qui armaient pour les îles, la Ferme générale
        craignit une fraude plus grande encore et sollicita un règlement pour que les capitaines de
        vaisseaux fissent à l’arrivée dans les îles déclaration du nombre de noirs au greffe de
        l’intendant et que les agents fissent de leurs côtés
       une déclaration exacte du prix total des esclaves vendus, lesquelles déclarations seraient
       portées sur une même feuille. Les agents seraient obligés, lorsqu’ils voudraient
      faire un envoi de marchandises, d’apporter au greffe une facture des marchandises avec le
      montant des précédents envois dans la forme des modèles prescrits en <date when="1734"> 1734
      </date> . Par ailleurs, les fermiers généraux souhaitaient astreindre les armateurs de
      vaisseaux qui allaient à la traite des noirs de faire faire le retour des marchandises dans le
      port de départ, suivant l’esprit des lettres patentes d’avril <date when="1717"> 1717 </date>
      rendues pour les <ref target="#colonies"> colonies </ref> . </def>
     <bibl type="references">
      <bibl> [ <idno type="ArchivalIdentifier"> AN, G1 31, Traites,
        direction de Bordeaux, Mémoire n°6034 : 1773 ; </idno>
      </bibl>
      <bibl type="sources"> Mémoire n° 386, 25 janvier 1775 concernant M. Lafon de La Debat, armateur à Bordeaux; </bibl>
      <bibl>
       <idno type="ArchivalIdentifier"> AN G1 80, dossier 15, Mémoire sur le commerce de Guinée, 14
        décembre 1741 ; </idno>
      </bibl>
      <bibl>
       <idno type="ArchivalIdentifier"> AN G7 1147, requête de Thomas Templier, fermier général, 11
        mars 1702 ; </idno>
      </bibl>
      <bibl type="sources"> Lettres patentes du Roy portant règlement pour le commerce des colonies
       françoises, avril 1717 ; </bibl>
      <bibl type="sources"> Arrêt du Conseil d’Etat pour le paiement de la gratification de 13
       livres par tête de nègre et de 20 liv. par chaque marc on matière de poudre d'or que la
       Compagnie du Sénégal ferait entrer en France venant des pays de sa concession , 22 aout
       1724 ; </bibl>
      <bibl type="sources"> Recueil des édits, déclarations, arrêts et lettres patentes, concernant les compagnies
       de Guinée &amp; du Sénégal, Paris, Imprimerie Antoine Boudet, 1754 ; </bibl>
      <bibl type="sources"> Arrêt du Conseil d'Etat concernant le commerce des noirs au Sénégal, 31
       juillet 1767 ; </bibl>
      <bibl type="sources"> Joseph Dufresne de Francheville, Histoire de la Compagnie des Indes, avec les titres de
       ses concessions et privilèges dressée sur les pièces authentiques, Paris, De Bure, 2 t.,
       1746, dont tome 2, p. 477-480 : Mémoire du Fermier du Domaine d’Occident sur les privilèges
       de la compagnie du Sénégal, 1er juillet 1715 ; </bibl>
      <bibl> Henry A. Gemery et Jan S. Hogendorn (éd.), The Uncommon Market : Essays in the Economic
       History of the Atlantic Slave Trade, New York, Academic Press, 1979 ; </bibl>
      <bibl type="sources"> Philip P. Boucher, Les Nouvelles Frances, France in America, 1500-1815,
        <idno type="ArchivalIdentifier"> An Imperial Perspective, Providence, The John Carter Brown
        Library, 1989 ; </idno>
      </bibl>
      <bibl> Philip D. Curtin, The Atlantic slave trade. A census, Madison, Wisconsin University
       Press, 1969 ; </bibl>
      <bibl> id., The rise and fall of the plantation complex. Essays in Atlantic history,
       Cambridge, Cambridge University Press, 1998 (1re éd. 1990), p. 26 ; </bibl>
      <bibl> Olivier Pétré-Grenouilleau, Les traites négrières. Essai d’histoire globale, Paris,
       Gallimard, 2004 </bibl>
     </bibl>
    </sense>
   </entry>
  </body>
 </text>
</TEI>
