<?xml version="1.0" encoding="utf-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title type="notice"> Bourgneuf (baie de) </title>
    <author>Marie-Laure Legay</author>
   </titleStmt>
   <publicationStmt>
    <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer le privilège la Ferme
     générale dans l'espace français et européen (1664-1794)Axe 1 : Dictionnaire de la Ferme
     générale, objet d'histoire totale </publisher>
    <pubPlace>
     <address>
      <addrLine> Maison Européenne des Sciences de l'Homme et de la Société </addrLine>
      <addrLine> 2 rue des cannoniers </addrLine>
      <addrLine> 59002 Lille Cedex </addrLine>
     </address>
     <date> 2021-2025 </date>
    </pubPlace>
    <availability>
     <licence> [A déterminer. Ex:Creative Commons Attribution 3.0 non transposé (CC BY 3.0)]
     </licence>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title> Dictionnaire numérique de la Ferme générale </title>
    <respStmt>
     <resp> Coordinateurs de l'axe : Dictionnaire numérique de la Ferme générale, objet d'histoire
      totale. </resp>
     <persName> Marie-Laure Legay </persName>
     <persName> Thomas Boullu </persName>
    </respStmt>
   </seriesStmt>
   <sourceDesc>
    <p> Dictionnaire numérique de la Ferme générale </p>
   </sourceDesc>
  </fileDesc>
  <encodingDesc>
   <projectDesc
    source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
    <p> Le projet FermGé vise à étudier l’impact d’une organisation fiscale (1664-1794),
     discriminante mais rationnelle, sur les territoires et les sociétés de la France moderne/p&gt;
    </p>
   </projectDesc>
  </encodingDesc><profileDesc>
    <textClass>
     <keywords scheme="#fr_RAMEAU">
      <list>
       <item> Histoire -- Histoire moderne -- Histoire économique </item>
      </list>
     </keywords>
    </textClass>
   </profileDesc>
   <revisionDesc>
    <change type="AutomaticallyEncoded"> 2022-12-12T17:46:19.541452+2:00 </change>
   </revisionDesc>
 </teiHeader>
 <text>
  <body>
   <entry type="B" xml:id="Bourgneuf_baie_de">
    <form type="lemma">
     <orth> Bourgneuf (baie de) </orth>
    </form>
    <sense>
     <def> Les marais salants de la baie de <placeName type="lieux_habitations">Bourgneuf</placeName>, situés géographiquement entre ceux de la
      presqu’île <ref target="#guérandaise"> guérandaise </ref> et ceux de l’Aunis et de la
      Saintonge, étaient exploités depuis le Moyen Âge. Leur production atteignit leur apogée au
      XVIe siècle d’après les historiens Fernand Guilloux, Jean Tanguy, et plus récemment Bernard
      Michon. Aux XVIIe et XVIIIe siècles, les prix du sel de la Baie étaient contrôlés par la Ferme
      générale, l’un des principaux clients des exploitants. Le sel était destiné au <placeName
       type="lieux_habitations"> port de Nantes </placeName> et de là, remontaient la Loire pour
      alimenter les <ref target="#greniers"> greniers </ref> le long des rivières de Vienne, Creuse
      et Claise. D’après Bernard Michon, sur les 395 embarcations, totalisant plus de 8 000
      tonneaux, arrivant à Nantes chargées de sel en <date when="1702"> 1702 </date> , 239,
      représentant 5 100 tonneaux environ, venaient de la baie de Bourgneuf, contre 146 de <ref
       target="#Guérande"> Guérande </ref> . A cette époque, on estime la production à 16-17 000
      muids (muid de Nantes, estimé à 900 kilogrammes) par an. Elle ne fut jamais aussi forte que
      celle de <ref target="#Brouage"> Brouage </ref> ou de la presqu’île de <ref target="#Guérande"
       > Guérande </ref> , mais les liens avec la Ferme générale se renforcèrent. Claude Bouhier a
      montré notamment comment les propriétaires de Noirmoutier investirent dans les techniques
      guérandaises pour accroître leurs chances de passer les marchés avec la Ferme. </def>
     <bibl type="references">
      <bibl> [Jean Tanguy, Le commerce nantais dans la seconde moitié du XVIe et au début du XVIIe
       siècle, thèse de troisième cycle, Rennes, 1967 ; </bibl>
      <bibl> Dominique Guillemet, « Les marais salants de l’Ouest français du XVIe au XVIIIe
       siècle : recherches récentes », dans Aux rives de l’incertain. Histoire et représentation des
       marais occidentaux du Moyen Âge à nos jours, Paris, Somogy éditions d’art, 2002, p. 189-200 ; </bibl>
      <bibl> Bernard Michon, « Les activités des ports de la baie de Bourgneuf au XVIIIe siècle »,
       dans Enquêtes et documents (revue du Centre de Recherches en Histoire Internationale et
       Atlantique), numéro 29, Rennes, PUR, 2004, p. 187-210 ; </bibl>
      <bibl> Jean-Claude Hocquet et Jean-Luc Sarrazin, Le sel de la Baie : Histoire, archéologie,
       ethnologie des sels atlantiques [en ligne]. Rennes, Presses universitaires de Rennes, 2006,
       dont les chapitres rédigés par Claude Bouhier (« le transfert de technologie saunière de
       Guérande à Noirmoutier au début du XVIIIe siècle ») et de Bernard Michon (« Les débouchés du
       sel de la baie de Bourgneuf au milieu du XVIIe siècle ») ; </bibl>
      <bibl> Gérard Le Bouëdec, « Le cabotage sur la façade atlantique française (XVe-XVIIIe
       siècles) », Revue d’histoire maritime, n° 8, 2008, p. 9-37] ; </bibl>
     </bibl>
    </sense>
   </entry>
  </body>
 </text>
</TEI>
