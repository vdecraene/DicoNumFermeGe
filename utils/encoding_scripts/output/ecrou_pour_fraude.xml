<?xml version="1.0" encoding="utf-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title type="notice"> Ecrou pour fraude MM </title>
    <author>Momcilo Markovic</author>
   </titleStmt>
   <publicationStmt>
    <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer le privilège la Ferme
     générale dans l'espace français et européen (1664-1794)Axe 1 : Dictionnaire de la Ferme
     générale, objet d'histoire totale </publisher>
    <pubPlace>
     <address>
      <addrLine> Maison Européenne des Sciences de l'Homme et de la Société </addrLine>
      <addrLine> 2 rue des cannoniers </addrLine>
      <addrLine> 59002 Lille Cedex </addrLine>
     </address>
     <date> 2021-2025 </date>
    </pubPlace>
    <availability>
     <licence> [A déterminer. Ex:Creative Commons Attribution 3.0 non transposé (CC BY 3.0)]
     </licence>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title> Dictionnaire numérique de la Ferme générale </title>
    <respStmt>
     <resp> Coordinateurs de l'axe : Dictionnaire numérique de la Ferme générale, objet d'histoire
      totale. </resp>
     <persName> Marie-Laure Legay </persName>
     <persName> Thomas Boullu </persName>
    </respStmt>
   </seriesStmt>
   <sourceDesc>
    <p> Dictionnaire numérique de la Ferme générale </p>
   </sourceDesc>
  </fileDesc>
  <encodingDesc>
   <projectDesc
    source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
    <p> Le projet FermGé vise à étudier l’impact d’une organisation fiscale (1664-1794),
     discriminante mais rationnelle, sur les territoires et les sociétés de la France moderne/p&gt;
    </p>
   </projectDesc>
  </encodingDesc>
  <profileDesc>
   <textClass>
    <keywords scheme="#fr_RAMEAU">
     <list>
      <item> Histoire -- Histoire moderne -- Histoire fiscale </item>
      <item> Histoire -- Histoire moderne -- Histoire judiciaire </item>
      <item> Histoire -- Histoire moderne -- Histoire politique </item>
     </list>
    </keywords>
   </textClass>
  </profileDesc>
  <revisionDesc>
   <change type="AutomaticallyEncoded"> 2022-12-15T09:30:20.566629+2:00 </change>
  </revisionDesc>
 </teiHeader>
 <text>
  <body>
   <entry type="E" xml:id="Ecrou_pour_fraude_MM">
    <form type="lemma">
     <orth> Ecrou pour fraude </orth>
    </form>
    <sense>
     <def> Trop souvent, la prison de la Conciergerie à Paris est associée au Tribunal
      révolutionnaire de l’an II, véhiculant depuis plus de deux siècles une image macabre.
      Pourtant, tout au long de la période moderne, au XVIIIe siècle notamment, la Conciergerie
      demeure une « prison ordinaire » qui accueille des prisonniers qui sont en attente d’un
      jugement ou de l’exécution d’une peine criminelle. Ce sont des détenus de droit commun, jugés
      en premier ressort dans leur généralité respective avant qu’une sentence définitive soit
      prononcée en appel. La Conciergerie conserve cependant une certaine particularité, car, à côté
      de cette masse d’individus, une autre catégorie de personnes subit les affres de
      l’emprisonnement où la geôle voit défiler chaque jour des personnes inculpées pour fraude,
      emmenées en prison par les employés de la Ferme. Le plaisir de boire n’est pas défendu. Ce qui
      l’est, en revanche, c’est la fraude sur les boissons ou la <ref target="#contrebande">
       contrebande </ref> de <ref target="#tabac"> tabac </ref> et de faux sel, car de nombreux
      Parisiens évitent de payer les lourdes taxes afférentes aux boissons. Le commerce frauduleux
      est important en cette fin du XVIIIe siècle à Paris et la population profite des lieux qui
      sont insuffisamment contrôlés par les employés de la Ferme. La clôture fiscale, le <ref
       target="#Mur"> Mur  des Fermiers généraux</ref>, encore en construction au début de <date
       when="1789"> 1789 </date> , est en partie responsable de cette situation, car les passages,
      non encore édifiés, permettent aux habitants de faire circuler, en toute discrétion et en
      toute illégalité, du <ref target="#vin"> vin </ref> et d’autres boissons. Les procédés sont
      multiples et dévoilent l’ingéniosité des trafiquants : pendant qu’un individu transporte une
      vessie remplie d’ <ref target="#eau-de-vie"> eau-de-vie </ref> , cachée sous le manteau,
      d’autres hommes, du côté de la banlieue, balancent par-dessus le <ref target="#Mur"> Mur
      </ref> des Fermiers des vessies pleines qui sont vite réceptionnées par les fraudeurs, du côté
      de la ville. Certains, profitant de la nuit, transportent sur leurs épaules des barils remplis
      de <ref target="#vin"> vin </ref> qu’ils emportent discrètement dans un <ref target="#cabaret"
       > cabaret </ref> à l’intérieur de la Ville-Lumière. Les fraudeurs exploitent ces rues
      tortueuses, ces jardins et bâtiments qui communiquent pour apporter des quantités importantes
      d’alcool ; on construit même des tuyaux souterrains, installés sous la chaussée, dans lesquels
      on fait passer les liquides alcoolisés. Ces actions frauduleuses comportent des risques et nul
      n’est à l’abri d’une interpellation menée par les employés de la Ferme. Débute alors pour le
      fraudeur l’enfermement carcéral qui le conduit tout droit à la Conciergerie, la prison de
      l’île de la Cité. Cependant, avant la fatidique incarcération, les règles de procédure sont
      scrupuleusement respectées par les commis de la Ferme qui ont déjà dressé un procès-verbal de
      fraude ou de rébellion, en cas de résistance. L’individu est ensuite conduit par quelques
      hommes de la Ferme (la plupart du temps, le brigadier, le sous-brigadier et des subalternes)
      où il est écroué. Ainsi, « écrouer quelqu’un, c’est le constituer prisonnier et en faire
      mention sur le registre des prisons ». L’action d’écrouer constitue donc une « opération
      d’enregistrement du prisonnier, sans laquelle la contrainte par corps exercée sur un individu
      est illégale ». Conservés aux Archives de la Préfecture de Police de Paris, les registres des
      prisonniers de la Conciergerie constituent des séries quasi continues entre <date when="1564">
       1564 </date> et <date when="1794"> 1794 </date> et demeurent une source riche et
      incontournable pour les historiens qui cherchent à mieux cerner la politique criminelle et
      répressive du Parlement. L’ <ref target="#ordonnance"> ordonnance </ref> criminelle de <date
       when="1670"> 1670 </date> précise, dans son titre XIII, les règles de l’enregistrement des
      détenus qui se vérifient à la lecture des registres. Le greffier partage chaque feuillet en
      deux colonnes : sur celle de droite, il y porte les informations relatives qui président à
      l’incarcération de l’individu ; la colonne de gauche indique la sentence rendue. Toutefois,
      les personnes écrouées pour fraude ou contrebande ne connaissent pas le même traitement
      scriptural que les autres prévenus ; l’écrou affiche ainsi une volonté délibérée de dissocier
      les individus incarcérés. Le premier type d’écrou, que nous nommerons « l’écrou classique »,
      se distingue du deuxième, « l’écrou pour fraude ». Pour le premier écrou, le format de la date
      est classique avec le jour (en chiffres) et, en lettres, le mois et l’année ; l’acte
      mentionne, avec des caractères plus grands et en gras, l’état civil de l’accusé, suivi de son
      âge, son activité, son domicile et son lieu de naissance ; ensuite, le greffier indique la
      condamnation prononcée par la juridiction d’origine et la nature du délit ou du crime. Sur la
      colonne de gauche, le greffier note l’arrêt définitif prononcé en appel par le Parlement de
      Paris. L’écrou pour fraude (ou <ref target="#contrebande"> contrebande </ref> ) diffère
      sensiblement ; là, le greffier inscrit la date entièrement en lettres en commençant par
      l’année, suivi de l’heure à laquelle l’individu est présenté à la Conciergerie. Le greffier
      désigne la juridiction pour le compte de laquelle l’arrestation est effectuée ; dans les cas
      qui nous intéressent, il s’agit toujours de l’ <ref target="#adjudicataire"> adjudicataire
      </ref> général (l’homme de paille) des Fermes du roi. Le patronyme (quelquefois le surnom
      uniquement) apparaît plus bas et, assez régulièrement, le nom de la personne incarcérée est
      souligné. L’écrou se termine par une observation sommaire de la nature du délit de fraude.
      Enfin, le fraudeur est « écroué sur le registre de la geôle des prisons de la Conciergerie, et
      laissé à la charge et garde du sieur Hubert, concierge d’icelle qui s’en est chargé aux peines
      de droit, dont acte ». À ce moment, les employés qui conduisent l’inculpé signent sur le petit
      registre (de nuit ou de jour).APP, AB 128, vue d’ensemble du registre de la Conciergerie,
      entre le 6 et le 9 janvier <date when="1786"> 1786 </date> (cliché de l’auteur).On distingue
      nettement sur chaque feuillet les deux colonnes. Pour les détenus de droit commun, sous la
      date de l’écrou, apparaît clairement en caractères gras l’état civil de la personne ; pour les
      individus incarcérés pour fraude, c’est l’année qui ressort en premier lieu. « L’écrou pour
      fraude » (cliché de l’auteur). APP, AB 128, écrou de Pierre Degrè, le 9 janvier <date
       when="1786"> 1786 </date> , pour fraude et rébellionN’apparaît ici que la colonne de droite,
      à savoir l’écrou]. [L’homme est remis en liberté 15 mois plus tard, le 5 avril <date
       when="1787"> 1787 </date> . La durée d’incarcération est exceptionnellement longue ;
      généralement, la détention excède rarement 3-4 mois. En réalité, le parcours judiciaire de
      Pierre Degrè (ou Desgrais) fut tumultueux : à peine écroué, l’homme subit un interrogatoire
      devant le premier président de l’Élection. Le 25 janvier <date when="1786"> 1786 </date> , un
      décret de prise corps est décerné ; la <ref target="#Cour"> Cour </ref> des <ref
       target="#Aides"> Aides </ref> , prenant le relais de l’instruction, condamna Pierre Desgrais
      le 9 janvier <date when="1787"> 1787 </date> « à être attaché trois jours consécutifs au
      carcan à un poteau qui sera planté le premier jour à la barrière Saint-Denis, le deuxième jour
      à la barrière Saint-Martin et le troisième jour à la barrière du Temple ; et y rester chacun
      des trois jours de midi à deux heures de relevée ». Après avoir subi cette peine infamante,
      typique de l’Ancien Régime, l’individu retourne à la Conciergerie avant d’être définitivement
      relâché en avril <date when="1787"> 1787 </date> . Le <ref target="#procès-verbal">
       procès-verbal </ref> délivré à l’encontre du suspect, à l’occasion de l’interpellation,
      précise les formes prescrites de l’incarcération où les employés doivent obligatoirement
      amener l’individu « entre les deux guichets » (endroit entre les deux portes où se tiennent
      les guichetiers, les préposés du geôlier) ; à cet instant, l’homme n’est pas encore à
      proprement parler un prisonnier et il est censé encore conserver sa liberté. Les employés de
      la Ferme rédigent alors l’acte en sa présence, le lisent, puis demandent au prévenu de le
      signer (ce que ce dernier refuse très souvent), et lui remettent une copie sur du papier
      timbré. Enfin, l’individu est confié à la charge du concierge et il intègre réellement la
      prison. On ne peut qu’être frappé par la tenue remarquable des registres par les greffiers.
      L’écriture cursive, fluide et lisible, est celle d’un professionnel qui manie la plume avec
      dextérité et la lecture est aisée au contraire de la rédaction de la plupart des <ref
       target="#procès-verbaux"> procès-verbaux </ref> pour fraude. Deux greffiers, au moins, sont
      désignés pour la tenue des registres d’écrou comme en témoigne la différence d’écriture : de
      manière générale, l’un écroue alors que l’autre, le greffier des geôles, élargit le prévenu.
      Encore faut-il nuancer ces propos, car, là aussi, celui qui écrou un individu jugé en appel
      n’est pas le même greffier qui écroue en cas de fraude. L’ordonnance de <date when="1670">
       1670 </date> ne fait nulle mention de ces pratiques normatives de l’écrou, suggérant un
      traitement distinct des détenus à la Conciergerie, en identifiant clairement ceux qui sont des
      fraudeurs et ceux qui ont interjeté appel d’une sentence. Les législations royales
      (ordonnances, édits, arrêts) sont muettes sur ces variétés de l’écrou. Ces transformations,
      plutôt empiriques, témoignent, sans doute, d’une volonté de classement, afin de mieux
      identifier les individus. Si la très grande majorité des individus incarcérés sont pris en
      flagrant délit de fraude, d’autres sont emprisonnés sur une décision de justice, acte qui se
      traduit en amont par un décret de prise de corps. Celui-ci est par conséquent un jugement
      rendu par une juridiction. Cette forme de décret est rigoureuse puisqu’elle prive une personne
      de liberté. Il existe deux types de décrets délivrés par la cour qui précèdent la prise de
      corps : le décret d’assigné pour être ouï, et le décret d’ajournement personnel. Le premier
      oblige une personne à se présenter devant le juge pour y subir une audition, sans qu’il existe
      des charges formelles à l’encontre de l’intéressé ; ce dernier peut cependant écoper d’une
      condamnation pécuniaire. La deuxième situation, l’ajournement personnel, est très souvent
      délivrée lorsque la personne assignée ne se rend pas à la convocation de la cour. Enfin,
      le décret de prise de corps (l’équivalent du mandat de dépôt ou d’arrêt) entraîne
      l’incarcération. Lorsque le tribunal de l’ <ref target="#Élection"> Élection </ref> convertit
      un décret d’ajournement personnel en prise de corps, l’individu est appréhendé et entendu par
      le magistrat. Ces situations se retrouvent régulièrement dans les écrous de la
      Conciergerie lorsque des particuliers portent plainte contre des employés de la Ferme pour
      mauvais traitements. L’huissier de justice, à verge ou à cheval, muni du décret de prise de
      corps, procède à l’arrestation de l’employé qui est interrogé par un magistrat ; le décret de
      prise de corps rétrograde à un niveau inférieur, redevenant un décret d’ajournement personnel,
      mais ne constitue pas, pour autant, une décharge des accusations portées. Quand l’individu se
      constitue volontairement prisonnier, sachant qu’un décret de prise de corps est décerné, il
      voit son statut se transformer en simple décret d’assigné. Régulièrement, l’ <ref
       target="#Élection"> Élection </ref> rend une sentence qui équivaut à une condamnation.
      Cependant, l’emprisonnement n’est pas immédiat et il faut attendre quelques semaines, voire
      plusieurs mois avant que l’huissier ne délivre le décret de prise de corps à l’intéressé.
      L’incarcération prend du temps, car il faut retrouver les individus qui ont pu, entre-temps,
      déménager ou donner une fausse adresse ; l’huissier de justice devient alors enquêteur.« La
      levée d’écrou » (cliché de l’auteur).APP, AB 128, ordonnance rendue le 28 janvier <date
       when="1786"> 1786 </date> par Marye, premier président de l’Élection, qui met en liberté
      provisoire Alexandre Lecoeuvre, et délivre un décret d’ajournement personnel. [L’homme, un
      ancien employé des Fermes, fut écroué le 10 janvier <date when="1786"> 1786 </date> , en vertu
      d’un décret de prise de corps décerné le 17 décembre <date when="1785"> 1785 </date> par
      l’Élection de Paris. Tôt ou tard, la Conciergerie se vide de ses prisonniers. Ne nous
      attendons pas à ce qu’il y ait des règles strictes dans le domaine des libérations, car tout
      dépend de la fraude constatée et surtout de la résistance opposée aux forces de la Ferme. Il
      existe, principalement, trois cas de figure à la levée d’écrou d’un individu emprisonné pour
      fraude : en premier lieu, l’élargissement du prisonnier est effectué « en vertu de la
      mainlevée par Monsieur Delaître [Bernard Delaître est en poste à l’hôtel de Bretonvilliers, le
      siège de la Ferme ; il sera assassiné en <date when="1792"> 1792 </date> dans son château de
      Charonne, à Paris], directeur général des entrées de Paris [et du plat-pays] ».
      Cette mainlevée (ou main-levée) s’oppose évidemment à la main de justice qui « ordonne en
      contraignant les personnes et procédant sur leurs biens ». Dès lors, les dépositaires de
      l’ordre ont le pouvoir de saisir les biens d’une personne et, le cas échéant, de les
      emprisonner ; souvent, les deux aspects vont de pair, puisque les employés confisquent l’objet
      de la fraude et écrouent l’individu. La mainlevée est la situation qui se rencontre
      majoritairement dans les registres de la prison. Les écrous signalent deux types de
      mainlevées qui permettent soit une libération sous caution, soit un élargissement définitif.
      Dans le premier cas, la liberté est provisoire, à la charge de l’inculpé de se représenter à
      tous les actes de la procédure dès qu’il en sera requis. Le deuxième cas est favorable au
      prévenu puisque la mainlevée n’est susceptible d’aucune opposition et elle est délivrée sans
      cautionnement puisqu’elle rend la liberté à l’individu. À la Conciergerie, la mention d’une
      mainlevée sous caution ou définitive n’apparaît jamais. On peut estimer qu’une incarcération
      de plusieurs semaines est en soi une punition suffisante dans le cas d’une fraude qui
      n’entraîne pas un acte de rébellion ; la Ferme se résout alors à libérer l’individu sans
      attendre une quelconque contrepartie financière, la saisie des boissons, en cas de fraude,
      ayant déjà eu lieu au moment de l’arrestation par les employés de la Ferme. Le deuxième cas de
      figure est à chercher dans la coutume qui joue un rôle non négligeable. Dans cette société
      encore imprégnée de catholicisme, le pardon des fautes demeure un acte important. En dehors
      des grâces royales, où le roi pouvait casser le jugement des cours souveraines, les
      juridictions financières possédaient ce pouvoir régalien, délégué par le monarque. Il n’est
      donc pas surprenant que les grandes fêtes religieuses soient l’occasion d’un pardon en masse.
      Lors des quatre grandes fêtes religieuses (Noël, Pâques, la fête de la Vierge Marie et la
      Toussaint), les officiers de la <ref target="#Cour"> Cour </ref> des <ref target="#aides">
       aides </ref> descendaient dans le préau de la Conciergerie et graciaient un nombre assez
      conséquent de prisonniers. Ainsi, pour l’année <date when="1786"> 1786 </date> , plus de 100
      personnes bénéficient de ces mesures de clémence pour les diverses célébrations catholiques où
      l’accusé est « mis en liberté et hors des prisons de céans par messieurs de la <ref
       target="#Cour"> Cour </ref> des <ref target="#aides"> aides </ref> y tenant leurs séances à
      cause de la révérence des fêtes [de Pâques] et en vertu de la mainlevée de ce jourd’hui ;
      signé de monsieur Delaitre, directeur général des entrées de Paris ». Enfin, d’autres écrous,
      minoritaires, attestent d’un troisième cas de figure où le premier président de l’ <ref
       target="#Élection"> Élection </ref> possède la faculté de rendre la liberté à l’issue d’une
      ordonnance de justice, toujours suivie de la mainlevée (ou du consentement) de l’adjudicataire
      des Fermes. Ici, les attributions de l’Élection sont composées essentiellement d’affaires de
      contentieux des taxes indirectes. Les magistrats peuvent également traduire devant leur
      juridiction les commis de la Ferme dans l’exercice de leurs fonctions ; au criminel, comme en
      témoignent abondamment les écrous, les magistrats de l’Élection se chargent des rébellions
      contre les employés. Entre le début de l’édification du <ref target="#Mur"> Mur  des
      Fermiers généraux</ref> en <date when="1785"> 1785 </date> et la suppression de l’octroi aux entrées
      des villes en <date when="1791"> 1791 </date> , votée par l’Assemblée, la <ref
       target="#fraude"> fraude </ref> et la <ref target="#contrebande"> contrebande </ref> à Paris
      étalent la répression exercée par la Ferme et les juridictions financières. Il n’est pas
      excessif de dire que la Conciergerie demeure la prison des fraudeurs. La punition est réelle,
      la sanction est forte. En effet, en 6 ans, ce sont <date when="1553"> 1553 </date> individus
      qui sont écroués, dont les trois-quarts sont des hommes. Chaque année, entre <date when="1785"
       > 1785 </date> et <date when="1790"> 1790 </date> , près de 260 personnes sont incarcérées.
      Les arrestations sont d’un niveau égal entre <date when="1785"> 1785 </date> et <date
       when="1787"> 1787 </date> (une moyenne de 270), mais explosent en <date when="1788"> 1788
      </date> , avec près de 500 personnes, soit deux fois plus que les années précédentes. L’année
      reste un moment sombre pour les supposés fraudeurs : avec une moyenne de 40 personnes écrouées
      chaque mois, la prison héberge autant de détenus de droit commun que de fraudeurs durant l’été
       <date when="1788"> 1788 </date> . Les captifs pour <ref target="#fraude"> fraude </ref>
      atteignent alors 40% de l’effectif total. Puis, le nombre baisse fortement, tombant à moins de
      200 arrestations en <date when="1789"> 1789 </date> . Encore faut-il relativiser les chiffres
      bas de <date when="1789"> 1789 </date> : entre le 8 juillet et le 11 août <date when="1789">
       1789 </date> , aucune arrestation n’est opérée et le deuxième semestre de l’année
      n’enregistre que 11 écrous. Une remarque similaire s’impose pour l’année <date when="1790">
       1790 </date> où seulement 77 personnes sont écrouées pour fraude. Cela coïncide avec les
      événements révolutionnaires parisiens, combinés avec la mise en place de nouvelles règles de
      procédure de justice qui se traduisent par un net assouplissement répressif, malgré la
      continuité juridique de l’administration de la Ferme et des anciennes juridictions
      financières. Les écrous pour l’année <date when="1791"> 1791 </date> sont insignifiants, ne
      comptant que 6 détenus en janvier à la Conciergerie. Quelques semaines plus tard, l’Assemblée
      nationale supprimait définitivement les taxes aux entrées des villes ; il n’y a donc plus
      aucune barrière d’octroi à surveiller ni d’arrestations à réaliser en ce début d’hiver <date
       when="1791"> 1791 </date> . APP 128-129-130, les détenus pour fraude à la Conciergerie (
       <date when="1785"> 1785 </date> - <date when="1791"> 1791 </date> ).Du plus petit délit – le
      refus de subir un contrôle des employés – jusqu’à la rébellion avec violences, en passant par
      la fraude d’alcool, la prison conserve cette large panoplie des violations aux règles et lois,
      où les pratiques illicites sont sévèrement réprimées. Outre l’intérêt indéniable à déchiffrer
      ces « papiers de justice » où une histoire quantitative peut s’appliquer à une catégorie de
      délinquants, donnant à voir une politique répressive de la Ferme et des cours de l’ <ref
       target="#Élection"> Élection </ref> et des Aides, les écrous pour fraude à la Conciergerie
      déploient d’autres dimensions, culturelles et sociales, où la description des faits et des
      actes juridiques répond à un besoin croissant d’un enregistrement particulier, en
      individualisant, classant et hiérarchisant les individus écroués. Ainsi, on ne doit pas
      négliger cette « histoire matérielle du droit de punir » qui participe autant au savoir sur la
      violence et la délinquance à la Conciergerie. </def>
     <bibl type="references">
      <bibl type="sources"> [Ordonnance de Louis XIV. Roy de France et de Navarre. Donnée à Saint-Germain en Laye
       au mois d’août 1670. Pour les matières criminelles ; </bibl>
      <bibl><idno type="ArchivalIdentifier"> Archives de la Préfecture de Police ou APP : les registres d’écrou à la Conciergerie
       (AB 128 : 12 mars 1785-12 décembre 1786, AB 129 : 14 décembre 1786-20 janvier 1789, AB 130 :
       21 janvier 1789-26 janvier 1792)</idno> ; </bibl>
      <bibl type="sources"> Encyclopédie méthodique. Jurisprudence. Tome quatrième, Paris/Liège,
       Panckoucke/Plombieux, 1784, « écrou », p. 200 ; </bibl>
      <bibl type="sources"> Pierre-François Muyart de Vouglans, Les loix criminelles de France dans leur ordre
       naturel. Dédiées au roi, 1780 ; </bibl>
      <bibl type="sources"> Encyclopédie méthodique. Jurisprudence. Tome quatrième, « guichet », p. 831 ; </bibl>
      <bibl>
       <idno type="ArchivalIdentifier"> AN, Z1G 211, Information contre Pierre Desgrais, accusé de
        fraude et rébellion (P-V. des commis du bureau du Temple, interrogatoire de Desgrais,
        audition des commis de la Ferme, sentence de l’Élection, arrêt de la cour des Aides) ;
       </idno>
      </bibl>
      <bibl type="sources"> Encyclopédie méthodique. Jurisprudence. Tome sixième, « prise de corps », p. 789 ; </bibl>
      <bibl type="sources"> Encyclopédie méthodique. Jurisprudence. Tome
       premier, « ajournement », p. 254 ; </bibl>
      <bibl type="sources"> Encyclopédie méthodique. Jurisprudence. Tome cinquième, « main de
       justice » et « mainlevée », p. 677 ; </bibl>
      <bibl type="sources"> Jean-Joseph Expilly, Dictionnaire géographique, historique et politique des Gaules et
       de la France, tome 5, Amsterdam, 1764, « Paris » (pour le pardon des fautes), p. 538 ; </bibl>
      <bibl> Pascal Bastien, L’exécution publique à Paris au XVIIIe siècle. Une histoire des rituels
       judiciaires, Seyssel, Champ Vallon, 2006 ; </bibl>
      <bibl> Stéphanie Blot-Maccagnan, Procédure criminelle et défense de l’accusé à la fin de
       l’Ancien Régime. Étude de la pratique angevine, Rennes, PUR, 2010 ; </bibl>
      <bibl> Jean-Marie Carbasse, Histoire du droit pénal et de la justice criminelle, Paris, PUF,
       2014 ; </bibl>
      <bibl> Frédéric Chauvaud, Pierre Prétou (dir.), L’arrestation. Interpellations, prises de
       corps et captures depuis le Moyen Âge, Rennes, PUR, 2015 ; </bibl>
      <bibl> Camille Dégez, Un univers carcéral (XVIe-XVIIe siècles) : la prison de la Conciergerie
       et sa société, thèse soutenue à l’université Paris IV, 2005 ; </bibl>
      <bibl> Camille Dégez, « La mémoire de la prison : les greffiers de la Conciergerie (Paris, fin
       du XVIe siècle-milieu du XVIIe siècle) », dans Olivier Poncet, Isabelle Storez-Brancourt
       (dir.), Une histoire de la mémoire judiciaire. De l’Antiquité à nos jours,
       Paris, Publications de l’Ecole nationale des chartes, 2009, p. 233-243 ; </bibl>
      <bibl> Julie Doyon, « Ecrouer et punir. Les registres de la Conciergerie au siècle des
       Lumières », dans Michel Porret (dir.), Histoire matérielle du droit de punir. Bois, fers et
       papiers de justice, Genève, L’Equinoxe, Georg, 2012, p. 48-76 ; </bibl>
      <bibl> Benoît Garnot, Histoire de la justice. France, XVIe– XXIe siècles, Folio histoire,
       2009 ; </bibl>
      <bibl> Momcilo Markovic, Paris brûle ! L’incendie des barrières de l’octroi en juillet 1789,
       L’Harmattan, 2019, p. 97-117] </bibl>
     </bibl>
    </sense>
   </entry>
  </body>
 </text>
</TEI>
