<?xml version="1.0" encoding="utf-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title type="notice"> Nantes </title>
    <author>Marie-Laure Legay</author>
   </titleStmt>
   <publicationStmt>
    <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer le privilège la Ferme
     générale dans l'espace français et européen (1664-1794)Axe 1 : Dictionnaire de la Ferme
     générale, objet d'histoire totale </publisher>
    <pubPlace>
     <address>
      <addrLine> Maison Européenne des Sciences de l'Homme et de la Société </addrLine>
      <addrLine> 2 rue des cannoniers </addrLine>
      <addrLine> 59002 Lille Cedex </addrLine>
     </address>
     <date> 2021-2025 </date>
    </pubPlace>
    <availability>
     <licence> [A déterminer. Ex:Creative Commons Attribution 3.0 non transposé (CC BY 3.0)]
     </licence>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title> Dictionnaire numérique de la Ferme générale </title>
    <respStmt>
     <resp> Coordinateurs de l'axe : Dictionnaire numérique de la Ferme générale, objet d'histoire
      totale. </resp>
     <persName> Marie-Laure Legay </persName>
     <persName> Thomas Boullu </persName>
    </respStmt>
   </seriesStmt>
   <sourceDesc>
    <p> Dictionnaire numérique de la Ferme générale </p>
   </sourceDesc>
  </fileDesc>
  <encodingDesc>
   <projectDesc
    source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
    <p> Le projet FermGé vise à étudier l’impact d’une organisation fiscale (1664-1794),
     discriminante mais rationnelle, sur les territoires et les sociétés de la France moderne/p&gt;
    </p>
   </projectDesc>
  </encodingDesc><profileDesc>
    <textClass>
     <keywords scheme="#fr_RAMEAU">
      <list>
       <item> Histoire -- Histoire moderne -- Histoire fiscale </item>
       <item> Histoire -- Histoire moderne -- Histoire financière </item>
      </list>
     </keywords>
    </textClass>
   </profileDesc>
   <revisionDesc>
    <change type="AutomaticallyEncoded"> 2022-12-19T17:13:37.749504+2:00 </change>
   </revisionDesc>
 </teiHeader>
 <text>
  <body>
   <entry type="N" xml:id="Nantes">
    <form type="lemma">
     <orth> Nantes </orth>
    </form>
    <sense>
     <def> Ce port changea de dimension au cours du XVIIIe siècle pour devenir l’un des centres
      majeurs du commerce triangulaire, assurant 65 % du trafic entre <date when="1713"> 1713
      </date> et <date when="1722"> 1722 </date> . Sa part relative déclina (34 % entre <date
       when="1783"> 1783 </date> et <date when="1790"> 1790 </date> ), mais pas le nombre
      d’armements qui atteignait la centaine annuelle à la fin de l’Ancien régime. Il recevait le
       <ref target="#sucre"> sucre </ref> , le <ref target="#café"> café </ref> et l’indigo (pour
      l’essentiel) des Antilles, trafic retour d’une traite des <ref target="#noirs"> noirs </ref>
      qui commençait sur les côtes de Guinée et du Sénégal. En conséquence de cette intense
      activité, la Ferme générale, doublée de la Ferme du Domaine d’ <ref target="#Occident">
       Occident </ref> , entretint dans le port une armada d’employés en charge de vérifier les
      cargaisons, de viser les certificats de ventes des esclaves, sur lesquels des réductions
      fiscales de moitié étaient accordées à l’entrée, et de lever les droits selon divers tarifs,
      celui de l’Exclusif, celui du Domaine d’ <ref target="#Occident"> Occident </ref> et celui de
      la Prévôté de Nantes. En <date when="1775"> 1775 </date> , la Ferme générale fit recette de
      2 100 577 livres pour les droits levés sur les seules marchandises revenant des îles
      d’Amérique. Les charges étaient lourdes néanmoins. Pour surveiller l’activité du port, sept
      brigades composaient la capitainerie générale de Nantes en <date when="1777"> 1777 </date> :
      la brigade à cheval de la ville, celle de Trentemoult, la brigade du quai d’Aiguillon, de
      Richebourg, de La Fosse, de Chézine et de Pirmil, soit en tout 63 hommes. D’après un mémoire
      de <date when="1765"> 1765 </date> , quatre magasins servaient d’entrepôts et y recevaient les
      marchandises : l’un pour les <ref target="#cafés"> cafés </ref> tant des Indes que des îles,
      l’autre pour les marchandises <ref target="#prohibées"> prohibées </ref> de la Compagnie des
       <ref target="#Indes"> Indes </ref> , un troisième pour les marchandises du royaume qui
      étaient déclarées tant pour les îles que pour la Guinée et l’étranger, le quatrième destiné à
      recevoir les marchandises venant du Nord et de Hollande avec déclaration pour la Guinée. Le
      bénéfice de l’entrepôt n’était accordé qu’aux négociants qui faisaient le commerce directement
      avec les colonies et avec l’étranger. Lorsque Pinel, négociant à Carcassonne demanda que ses
      draps, destinés à un correspondant qui faisait le commerce avec la Guinée, fussent entreposés
      non dans le magasin des marchandises de France, mal tenu, mais dans celui de la Guinée (le
      quatrième), il ne fut pas entendu. Le 17 octobre <date when="1782"> 1782 </date> , un nouvel
      ordre de régie des déclarations d’entrée et de sortie fut établi. Les deux receveurs des
      déclarations, l’un pour les entrées, l’autre pour les sorties, étaient astreints à tenir une
      dizaine de <ref target="#registres"> registres </ref> chacun, tant pour le gros que pour le
      détail. Port privilégié de <orgName subtype="province" type="pays_et_provinces"> Bretagne
      </orgName> situé hors des Cinq grosses <ref target="#fermes"> fermes </ref> , Nantes taxait
      les marchandises au titre des traites par les bureaux de la Prévôté. Naturellement, les
      recettes augmentaient à proportion des arrivées. Entre <date when="1762"> 1762 </date> et
       <date when="1784"> 1784 </date> , elles furent plus que doublées.Produits bruts du bureau de
      la Prévôté pendant les baux de Prévost, Alaterre et David et les trois premières années du
      bail Salzard (en livres tournois ; AN G1 83, dossier 10):Nantes était également un haut lieu
      de dépôt du sel issu des régions productrices de l’Ouest atlantique et destiné aux pays de
      Grandes <ref target="#gabelles"> gabelles </ref> . En <date when="1788"> 1788 </date> , 2 515
      muids y furent déposés pour le fournissement des <ref target="#greniers"> greniers </ref> par
      la rivière de la Loire, et 1 285 muids pour les greniers des bassins du Maine, de la Sarthe et
      du Loir. Le sel de masse séchait dans les entrepôts de la ville puis était embarqué pour être
      distribué. Il était taxé d’un quarantième à la sortie selon la pancarte de la Prévôté. Les
      entrepreneurs assuraient cette distribution en employant des voituriers étroitement
      surveillés. En amont de Nantes, le <placeName type="lieux_controle"> bureau d’Ingrandes
      </placeName> , à l’entrée des pays de Grandes <ref target="#gabelles"> gabelles </ref> et des
      Cinq grosses <ref target="#fermes"> fermes </ref> , assurait un premier contrôle systématique
      des bateaux. </def>
     <bibl type="references">
      <bibl> [ <idno type="ArchivalIdentifier"> AN, G1 73 : personnel. Brigades et recettes de la
        Prévôté de Nantes ; </idno>
      </bibl>
      <bibl>
       <idno type="ArchivalIdentifier"> AN G1 80, dossier 16, mémoire de mars 1765 ; </idno>
      </bibl>
      <bibl>
       <idno type="ArchivalIdentifier"> AN G1 83, dossier 10 : « Ordre de régie pour Nantes » ;
       </idno>
      </bibl>
      <bibl type="sources">
       <idno type="ArchivalIdentifier"> AN G1 97 Dossier 5 : Traité de l’entreprise de la voiture
        des sels pendant le bail Mager (1787-1793), Fournissement pour la 2e année (1788) ; </idno>
      </bibl>
      <bibl type="sources">
       <idno type="ArchivalIdentifier"> AN, H1 1686, 1775 : « Etat des denrées portées en 1775 des
        colonies françaises de l’Amérique dans les ports de la Métropole, leur valeur déterminée sur
        le prix commun, produit des droits qu’elles ont payés à leur sortie des isles et à leur
        entrée en France, quantités de celles qui ont passé à l’étranger et de celles qui ont été
        consommées dans le Royaume, avec les droits de consommation qui ont été perçus, argent venu
        des isles, valeur arbitrée des production peu importantes qui ne sont pas détaillées dans ce
        tableau ». Arrêt du Conseil d'Etat qui ordonne que les Sucres de Cayenne provenant de la
        traite des noirs que la Compagnie des Indes fera entrer dans le royaume par les ports de
        Bretagne ne paieront que la moitié des droits de la prévôté de Nantes et des autres droits
        locaux, et 40 sols du cent pesant lorsqu'elle les fera entrer dans l'intérieur des cinq
        grosses fermes, pour y être consommés, 5 juin 1725 ; </idno>
      </bibl>
      <bibl type="sources"> Arrêt du Conseil d'Etat qui déboute les communautés de Guérande et du
       Croisic, ainsi que les états et procureur général-syndic de Bretagne, de leurs demandes, et
       ordonne l'exécution de celui du 9 avril 1718, par lequel les sels venant du territoire de
       Guérande, après avoir passé le Trépas de Saint-Nazaire, ont été déclarés sujets au droit de
       quarantième dû dans l'étendue de la prévôté de Nantes, quelle qu'en soit la destination, 30
       septembre 1738 ; </bibl>
      <bibl> Jean Meyer, L’armement nantais dans la deuxième moitié du XVIIIe siècle, Paris, SEVPEN,
       1969 ; </bibl>
      <bibl> Guy Saupin, Nantes au XVIIe siècle. Vie politique et société urbaine, Rennes, PUR,
       1996 ; </bibl>
      <bibl> Gérard Le Bouëdec, Activités maritimes et sociétés littorales de l’Europe atlantique,
       1690-1790, Paris, A. Colin, 1997 ; </bibl>
      <bibl> Bernard Michon, Olivier Pétré-Grenouilleau, « Les activités commerciales de Louis
       Acland, marchand d’origine anglaise, installé à Nantes au milieu du xviie siècle », Revue
       d’histoire maritime, n° 5, mai 2006, p. 239-261 ; </bibl>
      <bibl> Bernard Michon, Le port de Nantes au XVIIIe siècle. Construction d’une aire portuaire,
       Rennes, PUR, 2011] </bibl>
     </bibl>
    </sense>
   </entry>
  </body>
 </text>
</TEI>
