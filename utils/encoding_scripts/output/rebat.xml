<?xml version="1.0" encoding="utf-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title type="notice">
                    Rebat (feuille de)
                </title>
                <author>Arnaud Le Gonidec</author>
            </titleStmt>
            <publicationStmt>
                <publisher>
                    MESHS de Lille dans le cadre de l'ANR FermeGé Administrer le privilège la Ferme générale dans l'espace français et européen (1664-1794)Axe 1 : Dictionnaire de la Ferme générale, objet d'histoire totale
                </publisher>
                <pubPlace>
                    <address>
                        <addrLine>
                            Maison Européenne des Sciences de l'Homme et de la Société
                        </addrLine>
                        <addrLine>
                            2 rue des cannoniers
                        </addrLine>
                        <addrLine>
                            59002 Lille Cedex
                        </addrLine>
                    </address>
                    <date>
                        2021-2025
                    </date>
                </pubPlace>
                <availability>
                    <licence>
                        [A déterminer. Ex:Creative Commons Attribution 3.0 non transposé (CC BY 3.0)]
                    </licence>
                </availability>
            </publicationStmt>
            <seriesStmt>
                <title>
                    Dictionnaire numérique de la Ferme générale
                </title>
                <respStmt>
                    <resp>
                        Coordinateurs de l'axe : Dictionnaire numérique de la Ferme générale, objet d'histoire totale.
                    </resp>
                    <persName>
                        Marie-Laure Legay
                    </persName>
                    <persName>
                        Thomas Boullu
                    </persName>
                </respStmt>
            </seriesStmt>
            <sourceDesc>
                <p>
                    Dictionnaire numérique de la Ferme générale
                </p>
            </sourceDesc>
        </fileDesc>
        <encodingDesc>
            <projectDesc source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
                <p>
                    Le projet FermGé vise à étudier l’impact d’une organisation fiscale (1664-1794), discriminante mais rationnelle, sur les territoires et les sociétés de la France moderne/p&gt;
                </p>
            </projectDesc>
            <profileDesc>
                <textClass>
                    <keywords scheme="#fr_RAMEAU">
                        <list>
                            <item>
                                Histoire -- Histoire moderne -- Histoire administrative
                            </item>
                        </list>
                    </keywords>
                </textClass>
            </profileDesc>
            <revisionDesc>
                <change type="AutomaticallyEncoded">
                    2023-02-20T11:02:58.825409+2:00
                </change>
            </revisionDesc>
        </encodingDesc>
    </teiHeader>
    <text>
        <body>
            <entry type="R" xml:id="Rebat_feuille_de">
                <form type="lemma">
                    <orth>
                        Rebat (feuille de)
                    </orth>
                </form>
                <sense>
                    <def>
                        Les feuilles de rebat, comme beaucoup de papiers utilisés par la Ferme générale, ont une double finalité
                        <span subtype="normes" type="normes_contestations">
                            <span subtype="contestation" type="normes_contestations">
                                : la lutte contre la contrebande et le contrôle de l’administration sur ses agents
                            </span>
                        </span>
                        .Le rebat est une tactique utilisée par les
                        <ref target="#brigades">
                            brigades
                        </ref>
                        dans l’étendue des quatre lieues
                        <ref target="#limitrophes">
                            limitrophes
                        </ref>
                        . Ce terme renverrait selon Surgy à la technique de chasse de la battue ou du « rabat ». Bien que cette étymologie soit douteuse, le rapprochement n’est pas moins à propos puisque, en effet, le rebat consiste, pour les brigades, à remonter ou à descendre la ligne, de poste en poste, pour relever les indices laissés par les contrebandiers et, le cas échant, les interpeller.
                        <span subtype="normes" type="normes_contestations">
                            Suivant l’ordre de travail du 5 décembre
                            <date when="1758">
                                1758
                            </date>
                            , il existe un rebat de jour et un rebat de nuit, qualifié de «
                        </span>
                        patrouille », mais il est peu probable que les brigades aient pu soutenir ce rythme puisque l’exploitation des indices récoltés demandait du temps, notamment pour les embuscades.Les rebats sont dressés jour par jour par les capitaines généraux qui établissent les circuits et les horaires que les gardes devront suivre ainsi que le « mot du guet » à utiliser. Ces informations sont transcrites sur des « feuilles », numérotées et cachetées, au dos desquelles est indiquée la date à laquelle elles devront être ouvertes. Les feuilles de rebat sont envoyées tous les dix jours aux brigadiers des postes des extrémités des lignes qui les ouvriront à la date indiquée en présence des employés du poste. Les gardes qui assurent le rebat du jour, le « détachement », se voient remettre leur feuille de route qui est cachetée et adressée au brigadier du poste suivant qui devra la faire viser par tous ses employés pour s’assurer de leur présence. Cette opération est répétée jusqu’au dernier poste de l’extrémité. La feuille renseigne en outre sur l’identité des gardes qui assureront le rebat du lendemain ainsi que sur leurs horaires, aussi chaque brigadier doit-il veiller à cacher ces informations, en pliant cette colonne, pour « prévenir toute intelligence » avec les fraudeurs et les contrebandiers. Les brigadiers doivent rapporter sur la feuille l’heure à laquelle les gardes se sont présentés à leur poste et celle à laquelle ils en sont repartis ainsi que tous les évènements qu’ils auraient pu rencontrer sur leur chemin.
                        <span subtype="normes" type="normes_contestations">
                            Si une saisie ou une poursuite de contrebandier contrarie la bonne marche du détachement, la feuille de rebat est remise au poste par le détachement du rebat du lendemain
                        </span>
                        . Les horaires n’ayant pu être annoncés pour le jour suivant, les gardes suivront ceux de la veille.
                        <span subtype="normes" type="normes_contestations">
                            Les feuilles de rebat usagées restent entre les mains du brigadier du dernier poste de la ligne pour être récupérées, tous les dix jours, lors de la visite du capitaine général
                        </span>
                        .
                        <span subtype="normes" type="normes_contestations">
                            Selon les termes de l’ordre de travail du 5 décembre
                            <date when="1758">
                                1758
                            </date>
                            , les rôles d’appointements ne peuvent être arrêtés par les capitaines généraux qu’après examen et vérification des feuilles de rebat avec les registres
                            <ref target="#portatifs">
                                portatifs
                            </ref>
                            des brigadiers
                        </span>
                        . Cette même confrontation est assurée par le
                        <ref target="#contrôleur">
                            contrôleur
                        </ref>
                        général qui doit viser feuilles et registres pour en rendre compte au bureau de la direction dans laquelle sont expédiés les documents à l’issue de chaque mois pour que, de nouveau, ils puissent être vérifiés par le directeur. Si une absence injustifiée est avérée, le garde ou le brigadier fautif se voit suspendre son salaire pendant huit jours et il est révoqué en cas de récidive. Les feuilles de rebat et les registres portatifs servent aussi « à donner des preuves du zèle  de la fidélité » des employés qui peuvent être récompensés par un avancement ou par une
                        <ref target="#retraite">
                            retraite
                        </ref>
                        . Les feuilles de rebat sont un bel exemple de continuité dans les pratiques administratives en-deçà et au-delà de la Révolution puisqu’elles sont utilisées par les douanes jusque dans les premières décennies du XXe siècle.
                    </def>
                    <bibl type="references">
                        <bibl>
                            [
                            <idno type="ArchivalIdentifier">
                                AN G1 63, dossier 7 : Ordre de travail pour les gardes et employés dans les Brigades de la Ferme Générale, 5 décembre 1758 ;
                            </idno>
                        </bibl>
                        <bibl type="sources">
                            Jacques-Philibert Rousselot de Surgy, Encyclopédie méthodique. Finances. Paris, Chez Panckouke, 1787, t. 3, v° « rebat », p. 440 ;
                        </bibl>
                        <bibl>
                            Jean Clinquart, Les services extérieurs de la Ferme générale à la fin de l’Ancien Régime. L’exemple de la direction des fermes du Hainaut, Vincennes, Comité pour l’Histoire Économique et Financière de la France, 1995, p. 175-183 ;
                        </bibl>
                        <bibl>
                            id., L’administration des douanes en France de 1914 à 1940, Vincennes, Comité pour l’Histoire Économique et Financière de la France, 2000, p. 245]
                        </bibl>
                    </bibl>
                </sense>
            </entry>
        </body>
    </text>
</TEI>