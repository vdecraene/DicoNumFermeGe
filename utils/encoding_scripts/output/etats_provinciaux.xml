<?xml version="1.0" encoding="utf-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title type="notice"> Etats provinciaux </title>
    <author>Marie-Laure Legay</author>
   </titleStmt>
   <publicationStmt>
    <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer le privilège la Ferme
     générale dans l'espace français et européen (1664-1794)Axe 1 : Dictionnaire de la Ferme
     générale, objet d'histoire totale </publisher>
    <pubPlace>
     <address>
      <addrLine> Maison Européenne des Sciences de l'Homme et de la Société </addrLine>
      <addrLine> 2 rue des cannoniers </addrLine>
      <addrLine> 59002 Lille Cedex </addrLine>
     </address>
     <date> 2021-2025 </date>
    </pubPlace>
    <availability>
     <licence> [A déterminer. Ex:Creative Commons Attribution 3.0 non transposé (CC BY 3.0)]
     </licence>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title> Dictionnaire numérique de la Ferme générale </title>
    <respStmt>
     <resp> Coordinateurs de l'axe : Dictionnaire numérique de la Ferme générale, objet d'histoire
      totale. </resp>
     <persName> Marie-Laure Legay </persName>
     <persName> Thomas Boullu </persName>
    </respStmt>
   </seriesStmt>
   <sourceDesc>
    <p> Dictionnaire numérique de la Ferme générale </p>
   </sourceDesc>
  </fileDesc>
  <encodingDesc>
   <projectDesc
    source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
    <p> Le projet FermGé vise à étudier l’impact d’une organisation fiscale (1664-1794),
     discriminante mais rationnelle, sur les territoires et les sociétés de la France moderne/p&gt;
    </p>
   </projectDesc>
  </encodingDesc>
  <profileDesc>
   <textClass>
    <keywords scheme="#fr_RAMEAU">
     <list>
      <item> Histoire -- Histoire moderne -- Histoire politique </item>
     </list>
    </keywords>
   </textClass>
  </profileDesc>
  <revisionDesc>
   <change type="AutomaticallyEncoded"> 2022-12-15T09:25:09.887689+2:00 </change>
  </revisionDesc>
 </teiHeader>
 <text>
  <body>
   <entry type="E" xml:id="Etats_provinciaux">
    <form type="lemma">
     <orth> Etats provinciaux </orth>
    </form>
    <sense>
     <def> Les relations des Etats provinciaux
       avec les Fermiers généraux furent souvent des plus mauvaises. Les deux
      administrations étaient antinomiques jusque dans leur principe. D’un côté, l’expression de
      toute une province jalouse de ses libertés : « <span subtype="privileges"
       type="privleges_encadrement"> L’ <orgName subtype="province" type="pays_et_provinces"> Artois
       </orgName> est un pays gouverné par les Etats où les lois des fermes générales ont toujours
       été inconnues, où tout commerce est libre, où nulle imposition n’a lieu moyennant une somme
       fixe à laquelle le pays a toujours été abonné avec son souverain </span> ». Les provinces
      d’Etats, tant <orgName subtype="province" type="pays_et_provinces">
       <ref target="#Bretagne"> Bretagne </ref>
      </orgName> que <orgName subtype="province" type="pays_et_provinces">
       <ref target="#Languedoc"> Languedoc </ref>
      </orgName> , nourrissaient une même aversion « pour les compagnies nouvelles et étrangères »
      et considéraient la Ferme comme un « adversaire redoutable ». De l’autre côté, une compagnie
      financière chargée d’alimenter les caisses du Trésor royal. La Ferme générale n’attaquait pas
      les Etats. <span subtype="normes" type="normes_contestations"> Structure apolitique, elle ne
       dénonçait pas les franchises des provinces privilégiées, telles qu’elles avaient été
       préservées par les souverains, mais plutôt la mauvaise mise en œuvre des contrôles aux
       frontières avec le ressort des Cinq grosses <ref target="#fermes"> fermes </ref>
      </span>. Les Etats s’escrimaient à démontrer tout l’avantage de leur juste administration: « sous eux, la liberté
      excite l’industrie des peuples »,
       indiquait encore l’assemblée d’ <orgName subtype="province" type="pays_et_provinces">
        <ref target="#Artois"> Artois </ref>
       </orgName>. Leur régime, en théorie, s’opposait en tout point au régime de <ref
       target="#vexations"> vexations </ref> qu’introduisaient les Fermiers du roi. Ces derniers se
      défendaient en faisant valoir la supériorité de l’intérêt général de la monarchie sur les
      intérêts particuliers des provinces. Nulle autre affaire que celle du <ref target="#tabac">
       tabac </ref> ou du <ref target="#sel"> sel </ref> ne porta le débat à ce point d’affrontement
      dialectique d’où ressortait l’impossible conciliation des arguments et avec elle, l’impossible
      intégration des provinces frontières au reste du royaume. En <orgName subtype="province"
       type="pays_et_provinces">
       <ref target="#Bretagne"> Bretagne </ref>
      </orgName> notamment, la lutte judiciaire entre les instances provinciales et le Conseil
      d’Etat fut permanente. S’il était d’usage partout dans le royaume que la vente de sel dans les
      dépôts fût enregistrée et fit l’objet de billets de délivrance, en <orgName subtype="province"
       type="pays_et_provinces">
       <ref target="#Bretagne"> Bretagne </ref>
      </orgName> en revanche, les employés des dépôts avaient interdiction formelle de tenir
      registre ! Ou encore : les Etats jugeaient que les Bretons domiciliés dans les deux lieues <ref target="#limitrophes">
        limitrophes </ref> avaient le droit de s’approvisionner où bon leur semblait  … Dans
      les instances pendantes devant les tribunaux de gabelles, leur procureur-syndic se tenait aux
      côtés des contrebandiers pour faire valoir les privilèges de la province, comme dans l’affaire
      Simon Bouteiller et François Oger ( <date when="1726"> 1726 </date> ) pris à un quart de lieue
      de la province d’ <orgName subtype="province" type="pays_et_provinces">
       <ref target="#Anjou"> Anjou </ref>
      </orgName> avec une quantité massive de sel. <span subtype="privileges"
       type="privleges_encadrement"> Les privilèges de franche gabelle reconnus par Louis XIV ne
       pouvaient donc être remis en cause sans atteindre la constitution même de ces provinces
      </span>. A contrario du cas breton,
       la <orgName subtype="province" type="pays_et_provinces">
        <ref target="#Bourgogne"> Bourgogne </ref>
       </orgName> perdit l’essence même de son statut de province d’Etats lorsqu’elle fut intégrée
       dans les Cinq grosses <ref target="#fermes"> fermes </ref> à partir du <ref target="#tarif">
        tarif </ref> commun de <date when="1664"> 1664 </date>. Néanmoins, s’il ne fut pas
       question de revenir sur les franchises et libertés des autres Etats, il demeure que certains
       d’entre eux, soucieux de se muer en véritables administrateurs, ont su s’adapter aux
       exigences de <ref target="#contrôle"> contrôle </ref> de la Ferme et entamer avec elle un
       dialogue constructif  . <span subtype="privileges" type="privleges_encadrement">
       <span subtype="encadrement" type="privleges_encadrement"> Philibert <ref target="#Orry"> Orry
        </ref> , contrôleur général de <date when="1730"> 1730 </date> à <date when="1745"> 1745
        </date> , entra pour une bonne part dans cette entente en engageant les Etats provinciaux à
        seconder le gouvernement dans la suppression de la <ref target="#contrebande"> contrebande
        </ref> , sous menace de limiter leurs <ref target="#privilèges"> privilèges </ref>
       </span>
      </span> : « Loin que leur intérêt soit de s’opposer à la proposition des Fermiers généraux,
      ils en ont au contraire un sensible d’y concourir, s’ils veullent s’assurer de plusieurs
      avantages et mettre les privilèges de la province a couvert de toute contraction, qu’ils ne
      doivent pas se flatter que le Roÿ soit toujours disposé à souffrir les préjudices réels et
      considérables… Vu l’augmentation excessive de l’abus et de la contrebande, Sa Majesté prendra
      des partis dans lesquels leurs privilèges et leurs interetz ne seront certainement pas aussi
      ménagés qu’ils le seront quand on verra qu’ils se prêteront de bonne grâce aux arrangements
      raisonnables qu’ils auront faits de concert avec eux ». Le ministre suivant, Machault
      d’Arnouville poursuivit cette politique néo-colbertiste. <span subtype="privileges"
       type="privleges_encadrement"> Ainsi, certaines provinces en vinrent à se prêter elles-mêmes à
       la police de leurs frontières, à l’instar des Etats du <orgName subtype="province"
        type="pays_et_provinces">
        <ref target="#Cambrésis"> Cambrésis </ref>
       </orgName> qui obtinrent en <date when="1746"> 1746 </date> la juridiction de la police dans
       les trois lieues <ref target="#limitrophes"> limitrophes </ref> qui les séparaient de la
        <orgName subtype="province" type="pays_et_provinces">
        <ref target="#Picardie"> Picardie </ref>
       </orgName>
      </span> . De même, il faut considérer les liens parfois étroits entre les financiers
      provinciaux et la finance parisienne. <span subtype="privileges" type="privleges_encadrement">
       Dans le cas du <orgName subtype="province" type="pays_et_provinces">
        <ref target="#Languedoc"> Languedoc </ref>
       </orgName> , si l’assemblée lutta pour éviter une trop grande emprise des fermiers parisiens
       sur les baux de l’équivalent (taxe sur le commerce de la viande, du poisson et du <ref
        target="#vin"> vin </ref> ) ou de l’étape, elle dut toutefois acquiescer en <date
        when="1754"> 1754 </date> , sous la pression de Machault d’Arnouville, l’offre faite par les
       Parisiens </span>. Cette entente financière entre financiers languedociens et parisiens ne résista pas au temps toutefois
      les aides des provinces d’élections, scandalisèrent les habitants. L’archevêque de Narbonne
      pria donc de faire cesser le bail incriminé : « les Etats n’admettront jamais aussy les principes étrangers et
       inouis que les fermiers veulent établir  ». Les assemblées demeuraient susceptibles sur leur autonomie de
       gestion des taxes  . Pour défendre leurs libertés, les assemblées entraient comme
      partie prenante dans les procès qui opposaient à l’occasion une paroisse de leur ressort à la
      Ferme générale. Ce fut le cas en <date when="1732"> 1732 </date> des Etats d’<orgName subtype="province"
        type="pays_et_provinces">
        <ref target="#Artois"> Artois </ref>
       </orgName> , aux côtés des paroisses de Comtes, Cauron et St Vast qui contestaient leur
       intégration aux trois lieues <ref target="#limitrophes"> limitrophes </ref> ou du Bilçar du
       pays de <ref target="#Labourd"> Labourd </ref> , qui s’engagea au côté de la paroisse de Sare
       en <date when="1772"> 1772 </date>
       . </def>
     <bibl type="references">
      <bibl> [ <idno type="ArchivalIdentifier"> AD Nord, C 873, Mémoire pour les Etats d’Artois
        contre les Fermiers généraux, Paris, 1740 ; </idno>
      </bibl>
      <bibl>
       <idno type="ArchivalIdentifier"> AD Nord, C 868, copie de la lettre écrite à M. de La
        Granville par le Contrôleur général, 9 mai 1740 ; </idno>
      </bibl>
      <bibl>
       <idno type="ArchivalIdentifier"> AN, H1 839, pièce 11, mémoire sur la ferme de l’équivalent,
        octobre 1747 ; </idno>
      </bibl>
      <bibl>
       <idno type="ArchivalIdentifier"> AN, H1 861, pièce 85, lettre de l’archevêque de Narbonne au
        Contrôleur général, 24 février 1756 ; </idno>
      </bibl>
      <bibl type="sources"> Encyclopédie méthodique, Finances, vol.1, article « dépôt », 1787, p.
       505-508 ; </bibl>
      <bibl> Marie-Laure Legay, Les Etats provinciaux dans la construction de l’Etat moderne aux
       XVIIe et XVIIIe siècles, Genève-Paris, Droz, 2001, « Les Etats provinciaux, garde-sel du
       roi ? », p. 208-216 ; </bibl>
      <bibl> Arlette Jouanna, « Le combat des Etats pour écarter les financiers « étrangers » des
       baux de l’équivalent et de l’étape, 1748-1757 », dans Les Etats dans l’Etat, Les Etats de
       Languedoc de la Fronde à la Révolution, S. Durand, A. Jouanna, E. Pélaquier (dir), Genève,
       2014, p. 561-579] </bibl>
     </bibl>
    </sense>
   </entry>
  </body>
 </text>
</TEI>
