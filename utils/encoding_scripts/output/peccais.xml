<?xml version="1.0" encoding="utf-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title type="notice"> Peccais </title>
    <author>Marie-Laure Legay</author>
   </titleStmt>
   <publicationStmt>
    <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer le privilège la Ferme
     générale dans l'espace français et européen (1664-1794)Axe 1 : Dictionnaire de la Ferme
     générale, objet d'histoire totale </publisher>
    <pubPlace>
     <address>
      <addrLine> Maison Européenne des Sciences de l'Homme et de la Société </addrLine>
      <addrLine> 2 rue des cannoniers </addrLine>
      <addrLine> 59002 Lille Cedex </addrLine>
     </address>
     <date> 2021-2025 </date>
    </pubPlace>
    <availability>
     <licence> [A déterminer. Ex:Creative Commons Attribution 3.0 non transposé (CC BY 3.0)]
     </licence>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title> Dictionnaire numérique de la Ferme générale </title>
    <respStmt>
     <resp> Coordinateurs de l'axe : Dictionnaire numérique de la Ferme générale, objet d'histoire
      totale. </resp>
     <persName> Marie-Laure Legay </persName>
     <persName> Thomas Boullu </persName>
    </respStmt>
   </seriesStmt>
   <sourceDesc>
    <p> Dictionnaire numérique de la Ferme générale </p>
   </sourceDesc>
  </fileDesc>
  <encodingDesc>
   <projectDesc
    source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
    <p> Le projet FermGé vise à étudier l’impact d’une organisation fiscale (1664-1794),
     discriminante mais rationnelle, sur les territoires et les sociétés de la France moderne/p&gt;
    </p>
   </projectDesc>
  </encodingDesc><profileDesc>
    <textClass>
     <keywords scheme="#fr_RAMEAU">
      <list>
       <item> Histoire -- Histoire moderne -- Histoire commerciale </item>
       <item> Histoire -- Histoire moderne -- Histoire fiscale </item>
      </list>
     </keywords>
    </textClass>
   </profileDesc>
   <revisionDesc>
    <change type="AutomaticallyEncoded"> 2022-12-20T09:58:00.762163+2:00 </change>
   </revisionDesc>
 </teiHeader>
 <text>
  <body>
   <entry type="P" xml:id="Peccais">
    <form type="lemma">
     <orth> Peccais </orth>
    </form>
    <sense>
     <def> Le centre de production de sel de Peccais était l’un des plus anciens et des plus
      importants de la partie méridionale du royaume. Situé à l’embouchure du petit <ref
       target="#Rhône"> Rhône </ref> , près d’ <ref target="#Aigues-mortes"> Aigues-mortes </ref> ,
      le parc comprenait dix-sept salines à la fin de l’Ancien régime. La plus grande, dite « saline
      de l’Abbé », appartenait à l’évêque d’Alès. Celle dite « de Saint-Jean »  appartenait au prieur de Saint-Gilles, ordre de
       Malte  et fut toujours exploitée à part; les quinze autres étaient exploitées
      collectivement par bail emphytéotique, puis par <ref target="#régie"> régie </ref> à partir de
       <date when="1716"> 1716 </date> . En <date when="1723"> 1723 </date> , l’évêque d’Alès
      intégra cette société, moyennant le huitième des profits. Les propriétaires associés vendaient
      la production à la Ferme des gabelles du <orgName subtype="province" type="pays_et_provinces">
       <ref target="#Languedoc"> Languedoc </ref>
      </orgName> pour un prix modeste de trois sous dix deniers le <ref target="#minot"> minot
      </ref> à la fin du XVIe siècle, puis par augmentations successives : six sous trois deniers
      (arrêt du 24 mai <date when="1768"> 1768 </date> ), puis douze sous le <ref target="#minot">
       minot </ref> (arrêt du 29 septembre <date when="1785"> 1785 </date> ). Ce prix était un peu
      plus élevé que celui payé aux salines de Peyriac situées lus à l’Ouest, Badon ou Maries. Les
      propriétaires jouissaient d’un droit d’attribution de deux livres cinq sols par muid, pour
      tenir lieu des intérêts d’une somme de 100 000 livres qu’ils avaient versée au Trésor royal en
       <date when="1641"> 1641 </date> . Ils disposaient en outre pour l’entretien d’un droit dit
      « droit de blanque », mais ce dernier fut toujours jugé insuffisant. Les inondations du Rhône
      exigeaient des frais importants. A la suite des dégâts provoqués par les hivers <date
       when="1706"> 1706 </date> et <date when="1707"> 1707 </date> , l’Etat leva une crue
      exceptionnelle de cinq sols sur tous les <ref target="#minots"> minots </ref> de sel vendus
      dans les <ref target="#greniers"> greniers </ref> du <ref target="#Lyonnais"> Lyonnais </ref>
      , Bas- <orgName subtype="province" type="pays_et_provinces">
       <ref target="#Languedoc"> Languedoc </ref>
      </orgName> , <orgName subtype="province" type="pays_et_provinces"> Rouergue </orgName> et
       <orgName subtype="province" type="pays_et_provinces">
       <ref target="#Auvergne"> Auvergne </ref>
      </orgName> pour aider la régie. Cette crue, confiée à Jacques Rocher, rapporta plus de 600 000
      livres. Malgré ce soutien, les propriétaires se plaignaient régulièrement des charges. D’après
      eux, l’introduction de la nouvelle <ref target="#trémie"> trémie </ref> dans le <orgName
       subtype="province" type="pays_et_provinces"> Languedoc </orgName> ( <date when="1711"> 1711
      </date> ) augmenta le nombre de <ref target="#minots"> minots </ref> par muid (de 144 à 171)
      et provoqua un manque à gagner. Pour défendre encore la production de Peccais, on convint
      d’étendre les lieux de vente au <orgName subtype="province" type="pays_et_provinces">
       Roussillon </orgName> et à la Cerdagne jusque-là alimentés par le sel de <ref
       target="#Peyriac"> Peyriac </ref> et de Saint-Jean ( <date when="1717"> 1717 </date>
      ).Moyennant quoi, les propriétaires étaient tenus de fournir à la Ferme 300 000 minots de sel
      par an, avec une réserve pour quatre ans, et d’entretenir les salins, les digues et chaussées
      alentours. Les salins de Peccais étaient sous la police des officiers du bureau des finances
      de Montpellier qui se transportaient chaque année sur les lieux pour évaluer la récolte et la
      qualité du sel. Les salines fournissaient le <orgName subtype="province"
       type="pays_et_provinces">
       <ref target="#Languedoc"> Languedoc </ref>
      </orgName> , l’ <orgName subtype="province" type="pays_et_provinces">
       <ref target="#Auvergne"> Auvergne </ref>
      </orgName> et le <orgName subtype="province" type="pays_et_provinces"> Rouergue </orgName> ,
      le <orgName subtype="province" type="pays_et_provinces">
       <ref target="#Roussillon"> Roussillon </ref>
      </orgName> , mais aussi la <ref target="#Savoie"> Savoie </ref> , la <ref target="#Suisse">
       Suisse </ref> et les <ref target="#Dombes"> Dombes </ref> . Le <orgName subtype="province"
       type="pays_et_provinces">
       <ref target="#Dauphiné"> Dauphiné </ref>
      </orgName> en fut également approvisionné, mais jusqu’au milieu du XVIIe siècle seulement. Les
       <ref target="#voituriers"> voituriers </ref> faisaient enregistrer leurs lettres à la
      juridiction des gabelles d’ <ref target="#Aigues-Mortes"> Aigues-Mortes </ref> et en donnaient
      copie au procureur de Peccais. Chargements et déchargements des sels étaient strictement
      contrôlés par les gardes et contre-gardes de la Ferme, en liaison avec les officiers des
      gabelles de Beaucaire, Tarascon, Saint-Esprit, selon les routes empruntées par les voitures.Le
      sel de cette région fut toujours apprécié des consommateurs. Voltaire en défendit
      l’approvisionnement pour le pays de <ref target="#Gex"> Gex </ref> : « Au sel de Peccais, dont
      le pays de Gex a toujours fait usage, a été substitué, le 1er octobre <date when="1774"> 1774
      </date> , du sel de <orgName subtype="province" type="pays_et_provinces"> Provence </orgName>
      , sale, dégoûtant, mélangé d’une terre rouge, nuisible aux hommes, aux bestiaux et à la
      fabrication des fromages du pays ». Bibliothèque nationale de France, <orgName
       subtype="administrations_juridictions_fermes" type="administrations_et_juridictions">
       département Cartes </orgName> et plans, GE D- <date when="1784"> 1784 </date> 2. Carte des
      salines de Peccais dans le Bas <orgName subtype="province" type="pays_et_provinces"> Languedoc
      </orgName> , <date when="1763"> 1763 </date>
     </def>
     <bibl type="references">
      <bibl> [ <idno type="ArchivalIdentifier"> AD de l’Hérault, 3F 1 à 77 : Fonds des Salins de
        Peccais (1538-1892) ; </idno>
      </bibl>
      <bibl type="sources"> Lettres patentes du Roy qui ordonnent, conformément à l’arrêt du
       vingt-sept novembre 1717, que les greniers &amp; chambres à sel du païs de Roussillon,
       Conflans &amp; Cerdaigne seront fournis du Sel de Peccais à commencer du premier octobre
       1718, Paris, 20 janvier 1718 ; </bibl>
      <bibl type="sources"> Arrêt du Conseil d’Etat autorisant Armand Pillavoine, fermier général
       des gabelles, à fournir de sel de Peccais les greniers de Dauphiné, 22 novembre 1720 ; </bibl>
      <bibl type="sources"> Arrêt du Conseil d’Etat qui révoque l’adjudication faite à J. Rocher des
       cinq sols par minot de sel ordonnés être levés dans les greniers et chambres à sel des
       gabelles de Languedoc, Lyonnais, Rouerge et Auvergne, pour les réparations à faire aux
       chaussées du Rhône et salins de Peccais ; et ordonne que lesdits cinq sols seront levés au profit du roi, 12 avril 1723 ; </bibl>
      <bibl type="sources"> Jacques-Nicolas Bellin, Environs d'Aigue Mortes, de Peccais etc. et la petite Camargue,
       1764 (carte) ; </bibl>
      <bibl type="sources"> Jean-Louis Moreau de Beaumont, Mémoires concernant les droits &amp; impositions en
       Europe, tome 3, Paris, Imprimerie royale, 1769, p. 168-1773; </bibl>
      <bibl type="sources"> Voltaire, Mémoire sur le pays de Gex, 31 mars 1775, dans Œuvres complètes, tome 29,
       éditions Garnier, 1879, p.351 (Voltaire s’appuie sur les données des trois syndics du pays)]
      </bibl>
     </bibl>
    </sense>
   </entry>
  </body>
 </text>
</TEI>
