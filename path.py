# Fichiers contenant les variables relative à la gestion des chemins de fichiers dans l'application.
import os

chemin_actuel = os.path.dirname(os.path.abspath(__file__))
print(chemin_actuel)

templates = os.path.join(chemin_actuel, "templates")
print(templates)