# ReadMe : Dictionnaire Numérique de la Ferme générale

## Présentation du projet et fonctionnalités
 Ce dépôt a été créé dans le cadre de l’ANR FermeGé et de son axe 1 « Dictionnaire numérique de la Ferme générale » (https://dicofg.hypotheses.org/category/presentation-du-dictionnaire). Le projet entend proposer une histoire totale de la Ferme générale et de son emprise spatiale entre 1640 et 1794. Au niveau technique, l’axe 1 entend développer un dictionnaire numérique accueillant les notices rédigées par l’ensemble des historiens contributeurs du projet. Pour ce faire, trois livrables ont été réalisés :
- un ensemble de **schémas d’encodage** et leur documentation, respectant le modèle conceptuel préalablement établi.
- une **chaîne de traitement de la donnée** permettant l’encodage structurel automatique des notices, composée d’un ensemble de scripts Python.
- une **application web** permettant de visualiser les notices, adossée à un ensemble de moteur de recherche et d'index, à un graphe de connaissance des citations entre les notices.

NB: l'ensemble des notices et données produites sont disponible sur le dépôt Nakala du projet disponible à l'adresse suivante : [insérer adresser dépôt Nakala]

## Organisation de l’arborescence du Gitlab v2

[Arborescence du repository Gitlab](PythonProjects/DicoNumFermeGe/arbo_gitlab_2.jpg)
## Installation et utilisation
*Nota bene* : commandes à exécuter dans le terminal (Linux ou macOS).

- Cloner le dossier : `git clone https://gitlab.huma-num.fr/vdecraene/DicoNumFermeGe.git`
- Télécharger un package d’environnement virtuel : `sudo apt-get install python3 libfreetype6-dev python3-pip python3-virtualenv`
- Vérifier que la version de Python est supérieure à 3.7 : `python --version`;
- Installer l'environnement virtuel : `virtualenv ~/.DicoNumFermeGe -p python3` ;
- Activer l'environnement : `source ~/.DicoNumFermeGe/bin/activate` ;
- Lancer la commande (télécharge les modules et dépendances nécessaires) : `pip install -r requirements.txt`
- Lancer l'application : `python3 run.py` ;
- Aller sur http://127.0.0.1:5000/ ou cliquez sur ce même lien dans votre IDE.
