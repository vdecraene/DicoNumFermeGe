# Fichier exécutable permettant de l'application avec la commande python3 run.py

from app.app import app


if __name__ == "__main__":
    app.run(debug=True)