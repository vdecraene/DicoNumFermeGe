<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE stylesheet [
  <!ENTITY emsp   "&#x2003;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:date="http://exslt.org/dates-and-times" extension-element-prefixes="date" version="1.0">
    <xsl:output method="html" indent="yes" encoding="UTF-8"/>
    <xsl:strip-space elements="*"/>
    <xsl:param name="numero"/>
    <xsl:variable name="notice_arbo" select="//entry[@n = $numero]"/>
<xsl:key name="authorKey" match="li" use="substring-before(substring-after(., ','), ',')" />
    <!-- STRUCTURATION GLOBALE -->

    <xsl:template match="entry">
        <head>
            <xsl:for-each select=".//orth">
                <div class="text-center h1">
                    <h1 class="font-weight-bold" style="font-family: 'Bitter', serif; !important; font-size: 1.2em; ">
                        <xsl:value-of select="." disable-output-escaping="yes"/>
                    </h1>
                </div>
                <div class="text-center h5">
                    <p id="author">
                        <xsl:choose>
                            <xsl:when test="ancestor::TEI//author[3]">
            <xsl:value-of select="ancestor::TEI//author/text()"/><br/><xsl:value-of select="ancestor::TEI//author[3]/text()"/>
   </xsl:when>
   <xsl:when test="ancestor::TEI//author[2]">
            <xsl:value-of select="ancestor::TEI//author/text()"/> et <xsl:value-of select="ancestor::TEI//author[2]/text()" disable-output-escaping="yes"/>
   </xsl:when>

   <xsl:otherwise>
      <xsl:value-of select="ancestor::TEI//author/text()" disable-output-escaping="yes"/>
   </xsl:otherwise>
</xsl:choose>
</p>
                </div>
                <br/>
            </xsl:for-each>
        </head>

        <xsl:choose>
            <xsl:when test=".//def/text()">
                <br/>
                <br/>
                <br/>
<span id="notice">
                <div>
                    <xsl:for-each select=".//def">
                        <p>
                            <xsl:copy>
                                <xsl:apply-templates/>
                            </xsl:copy>
                        </p>
                    </xsl:for-each>

                </div>
                <br/>



                <xsl:element name="div">
                    <xsl:element name="head">
                        <br/>
<hr style="border: 1px solid; color: #932514"/>
                        <br/>
                        <p class="h3">
                            <b>
                                <xsl:text>Sources et références bibliographiques:</xsl:text>
                            </b>
                        </p>
                    </xsl:element>
                    <xsl:choose>
                        <xsl:when test=".//bibl/bibl/idno">
                            <ul>
                                <head class="h3">
                                    <b>Sources archivistiques:</b>
                                </head>
                                <xsl:for-each select=".//bibl/bibl/idno">
                                    <xsl:element name="li">
                                        <xsl:apply-templates />
                                        <xsl:text>.</xsl:text>
                                    </xsl:element>
                                </xsl:for-each>
                            </ul>
                        </xsl:when>
                        <xsl:otherwise/>
                    </xsl:choose>
                    <br/>
            <xsl:choose>
                        <xsl:when test=".//bibl/bibl[@type = 'sources']">
                            <ul>
                                <head class="h3">
                                    <b>Sources imprimées:</b>
                                </head>
                                <xsl:for-each select=".//bibl/bibl[@type = 'sources']">
                                    <xsl:element name="li">
                                        <xsl:apply-templates />
                                        <xsl:text>.</xsl:text>
                                    </xsl:element>
                                </xsl:for-each>
                            </ul>
                            <br />
                        </xsl:when>
                        <xsl:otherwise />
            </xsl:choose>
                    <br />
<xsl:choose>
    <xsl:when test=".//bibl/bibl[not(.//idno)][not(@type)]">
        <ul type="bibliographie">
            <head class="h3">
                <b>Bibliographie scientifique:</b>
            </head>
            <xsl:for-each select=".//bibl/bibl[not(.//idno)][not(@type)]">
                <xsl:element name="li">
                    <xsl:attribute name="type">bibliographie</xsl:attribute>
                    <xsl:apply-templates/>
                    <xsl:text>.</xsl:text>
                </xsl:element>
            </xsl:for-each>
        </ul>
    </xsl:when>
    <xsl:otherwise />
</xsl:choose>

<br />

                </xsl:element>
</span>
<br/>
<hr style="border: 1px solid; color: #932514"/>
<br/>
                <div class="text-center card border-dark m-2">
                    <div class="card-header h-3" id="myInput"><b>Citer cette notice:</b>
                    </div>
                    <br/>
                    <div class="card-body">
                        <p class="text-justify">
                            <xsl:choose>
                                <xsl:when test="ancestor::TEI//author[3]">
            <xsl:value-of select="ancestor::TEI//author[1]/text()"/><xsl:text> </xsl:text><xsl:value-of select="ancestor::TEI//author[3]/text()"/>, « <xsl:value-of select=".//orth/text()"/> » (2023) in Marie-Laure Legay, Thomas Boullu (dir.), <i>Dictionnaire Numérique de la Ferme générale</i>, <a href="https://fermege.meshs.fr/" id="links_notices">https://fermege.meshs.fr</a>.<br/> Date de consultation : <xsl:value-of select="concat(substring(date:date(), 9, 2), '/', substring(date:date(), 6, 2), '/', substring(date:date(), 1, 4))"/><br/>
                            DOI :
   </xsl:when>
   <xsl:when test="ancestor::TEI//author[2]">
            <xsl:value-of select="ancestor::TEI//author[1]/text()"/> et <xsl:value-of select="ancestor::TEI//author[2]/text()"/>, « <xsl:value-of select=".//orth/text()"/> » (2023) in Marie-Laure Legay, Thomas Boullu (dir.), <i>Dictionnaire Numérique de la Ferme générale</i>, <a href="https://fermege.meshs.fr/" id="links_notices">https://fermege.meshs.fr</a>.<br/> Date de consultation : <xsl:value-of select="concat(substring(date:date(), 9, 2), '/', substring(date:date(), 6, 2), '/', substring(date:date(), 1, 4))"/><br/>
                            DOI :
   </xsl:when>

   <xsl:otherwise>
       <span class="author"><xsl:value-of select="ancestor::TEI//author/text()"/></span>, « <span class="title"><xsl:value-of select=".//orth/text()"/></span> » (<span class="year">2023</span>) in <span class="booktitle">Marie-Laure Legay, Thomas Boullu (dir.), <i>Dictionnaire Numérique de la Ferme générale</i></span>,<span class="url"> <a href="https://fermege.meshs.fr/" id="links_notices"> https://fermege.meshs.fr</a>.</span><br/> Date de consultation : <xsl:value-of select="concat(substring(date:date(), 9, 2), '/', substring(date:date(), 6, 2), '/', substring(date:date(), 1, 4))"/><br/>
                            DOI :
   </xsl:otherwise>
</xsl:choose>
                        </p>
                    </div>
                    <button class="btn btn-light"  onclick="copyText()"  style="cursor:pointer; color: #d7270c !important;">Copier la ciation au format texte brut</button>
                    <button id="copy-citation-btn" class="btn btn-light" style="cursor:pointer; color: #d7270c !important;">Copier la citation au format BibTeX</button>
                    <button id="copy-citation-btn-biblatex" class="btn btn-light" style="cursor:pointer; color: #d7270c !important;">Copier la citation au format BibLaTeX</button>


                </div>
                <script>
                    function copyText() {
                    var text = document.querySelector(".card-body p").innerText;
                    var input = document.createElement("input");
                    input.value = text;
                    document.body.appendChild(input);
                    input.select();
                    document.execCommand("copy");
                    document.body.removeChild(input);
                    alert("Citation copié dans le presse-papier au format texte brut: " + text);
                    }
                </script>
                <br/>
                <br/>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="/">
        <xsl:apply-templates select="$notice_arbo"/>
    </xsl:template>

        <xsl:template match="ref | orgName//ref | placeName//ref">
        <a>
            <xsl:attribute name="href">
                <xsl:choose>
                    <xsl:when test="@target='#Acquit_à_caution' or @target='#acquit_à_caution' or @target='#acquits'">
                        <xsl:value-of select="concat('/notice/', '224')"/>
                    </xsl:when>
                    <xsl:when test="@target='#adjudicataire' or @target='#adjudication' or @target='#adjudications' or @target='#adjudicataires'">
                        <xsl:value-of select="concat('/notice/', '56')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Agenais' or @target='#agenais' or @target='#Agen' or @target='#Agenais'">
                        <xsl:value-of select="concat('/notice/', '26')"/>
                    </xsl:when>
                    <xsl:when test="@target='#agent' or @target='#Agent'">
                        <xsl:value-of select="concat('/notice/', '71')"/>
                    </xsl:when>
                    <xsl:when test="@target='#rosière' or @target='#rosières'">
                        <xsl:value-of select="concat('/notice/', '75')"/>
                    </xsl:when>
                    <xsl:when test="@target='#aides' or @target='#Aides'">
                        <xsl:value-of select="concat('/notice/', '181')"/>
                    </xsl:when>
                    <xsl:when test="@target='#aigues-Mortes' or @target='#Aigues-Mortes'">
                        <xsl:value-of select="concat('/notice/', '83')"/>
                    </xsl:when>
                    <xsl:when test="@target='#allège' or @target='#Allège' or @target='#allèges'">
                        <xsl:value-of select="concat('/notice/', '240')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Allemagne' or @target='#allemagne'">
                        <xsl:value-of select="concat('/notice/', '27')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Alsace'">
                        <xsl:value-of select="concat('/notice/', '41')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Alun' or @target='#alun'">
                        <xsl:value-of select="concat('/notice/', '146')"/>
                    </xsl:when>
                    <xsl:when test="@target='#amidon' or @target='#Amidon'">
                        <xsl:value-of select="concat('/notice/', '171')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Anjou' or @target='#anjou'">
                        <xsl:value-of select="concat('/notice/', '195')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Angleterre' or @target='#angleterre'">
                        <xsl:value-of select="concat('/notice/', '148')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Annuel' or @target='#annuel' or @target='#annuels'">
                        <xsl:value-of select="concat('/notice/', '47')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Arc-et-Senans'">
                        <xsl:value-of select="concat('/notice/', '233')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Arles'">
                        <xsl:value-of select="concat('/notice/', '124')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Artois'">
                        <xsl:value-of select="concat('/notice/', '230')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Authie'">
                        <xsl:value-of select="concat('/notice/', '178')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Auvergne'">
                        <xsl:value-of select="concat('/notice/', '235')"/>
                    </xsl:when>
                    <xsl:when test="@target='#paiement' or @target='#acquit_de_payement'">
                        <xsl:value-of select="concat('/notice/', '229')"/>
                    </xsl:when>
                    <xsl:when test="@target='#bande' or @target='#banditisme' or @target='#bandes' or @target='#bandits' or @target='bandit'">
                        <xsl:value-of select="concat('/notice/', '176')"/>
                    </xsl:when>
                    <xsl:when test="@target='#bandoulière' or @target='#bandoulières' or @target='#bandouliere' or @target='#bandouliere'">
                        <xsl:value-of select="concat('/notice/', '185')"/>
                    </xsl:when>
                    <xsl:when test="@target='#banvin' or @target='#Banvin'">
                        <xsl:value-of select="concat('/notice/', '111')"/>
                    </xsl:when>
                    <xsl:when test="@target='#octroi' or @target='#barrière' or @target='#barrières'">
                        <xsl:value-of select="concat('/notice/', '161')"/>
                    </xsl:when>
                    <xsl:when test="@target='#bateau-maire' or @target='#Bateau-maire' or @target='#bateaux-maires'">
                        <xsl:value-of select="concat('/notice/', '4')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Bayonne'">
                        <xsl:value-of select="concat('/notice/', '155')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Berre'">
                        <xsl:value-of select="concat('/notice/', '144')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Berry' or @target='#berry'">
                        <xsl:value-of select="concat('/notice/', '137')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Billet_de_finances' or @target='#finances' or @target='#billet'">
                        <xsl:value-of select="concat('/notice/', '74')"/>
                    </xsl:when>
                    <xsl:when test="@target='#gabellement'">
                        <xsl:value-of select="concat('/notice/', '86')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Billet_sommaire' or @target='#sommaire'">
                        <xsl:value-of select="concat('/notice/', '63')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Bière' or @target='#bière' or @target='#bières'">
                        <xsl:value-of select="concat('/notice/', '214')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Boissons' or @target='#boissons' or @target='#boisson'">
                        <xsl:value-of select="concat('/notice/', '110')"/>
                    </xsl:when>
                    <xsl:when test="@target='#bon_de_masse'">
                        <xsl:value-of select="concat('/notice/', '215')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Bordeaux'">
                        <xsl:value-of select="concat('/notice/', '55')"/>
                    </xsl:when>
                    <xsl:when test="@target='#boucher' or @target='#boucherie' or @target='#boucheries' or @target='#bouchers'">
                        <xsl:value-of select="concat('/notice/', '46')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Bourgneuf'">
                        <xsl:value-of select="concat('/notice/', '90')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Bourgogne'">
                        <xsl:value-of select="concat('/notice/', '213')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Boutavant' or @target='#boutavant' or @target='#boutavants'">
                        <xsl:value-of select="concat('/notice/', '156')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Bretagne' or @target='#Bretagnes'">
                        <xsl:value-of select="concat('/notice/', '190')"/>
                    </xsl:when>
                    <xsl:when test="@target='#brigades' or @target='#brigade' or @target='#brigadier' or @target='#brigadiers'">
                        <xsl:value-of select="concat('/notice/', '239')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Brouage' or @target='#brouage' or @target='#Saintonge'">
                        <xsl:value-of select="concat('/notice/', '31')"/>
                    </xsl:when>
                    <xsl:when test="@target='#conserve' or @target='#conserves' or @target='#Conserve' or @target='#Conserves'">
                        <xsl:value-of select="concat('/notice/', '80')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Bureau_du_contentieux' or @target='#bureau_du_contentieux'">
                        <xsl:value-of select="concat('/notice/', '104')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Béarn' or @target='#béarnais' or @target='#Béarnais'">
                        <xsl:value-of select="concat('/notice/', '53')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Bénéfices' or @target='#bénéfices' or @target='#bénéfice' or @target='#Bénéfice'">
                        <xsl:value-of select="concat('/notice/', '140')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Cabaret' or @target='#cabaret' or @target='#Cabarets' or @target='#cabarets' or @target='#cabaretier' or @target='#cabaretiers'">
                        <xsl:value-of select="concat('/notice/', '44')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Re' or @target='#Ré' or @target='#île_de_Ré'">
                        <xsl:value-of select="concat('/notice/', '107')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Café' or @target='#café' or @target='#cafés'">
                        <xsl:value-of select="concat('/notice/', '232')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Camargue' or @target='#Camargues'">
                        <xsl:value-of select="concat('/notice/', '169')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Cambrai' or @target='#Cambrésis'">
                        <xsl:value-of select="concat('/notice/', '244')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Canada'">
                        <xsl:value-of select="concat('/notice/', '91')"/>
                    </xsl:when>
                    <xsl:when test="@target='#instar'">
                        <xsl:value-of select="concat('/notice/', '92')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Carte' or @target='#carte' or @target='#cartes'">
                        <xsl:value-of select="concat('/notice/', '36')"/>
                    </xsl:when>
                    <xsl:when test="@target='#caution' or @target='#cautions' or @target='#cautionnement' or @target='#cautionnements'">
                        <xsl:value-of select="concat('/notice/', '194')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Champagne' or @target='#Champagnes'">
                        <xsl:value-of select="concat('/notice/', '183')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Charente'">
                        <xsl:value-of select="concat('/notice/', '112')"/>
                    </xsl:when>
                    <xsl:when test="@target='#sous'">
                        <xsl:value-of select="concat('/notice/', '248')"/>
                    </xsl:when>
                    <xsl:when test="@target='#clergé' or @target='#Clergé'">
                        <xsl:value-of select="concat('/notice/', '242')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Clermontois' or @target='#Clermont' or @target='#Clermont-Ferrand'">
                        <xsl:value-of select="concat('/notice/', '33')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Colonies' or @target='#Colonie' or @target='#colonie' or @target='#colonies'">
                        <xsl:value-of select="concat('/notice/', '198')"/>
                    </xsl:when>
                    <xsl:when test="@target='#cave' or @target='#caves'">
                        <xsl:value-of select="concat('/notice/', '76')"/>
                    </xsl:when>
                    <xsl:when test="@target='#orientales' or @target='#Compagnie' or @target='#Indes' or @target='#Inde' or @target='#indes' or @target='#inde' or @target='#indiennes'">
                        <xsl:value-of select="concat('/notice/', '243')"/>
                    </xsl:when>
                    <xsl:when test="@target='#compte' or @target='#comptes' or @target='#comptabilité' or @target='#comptabilités'">
                        <xsl:value-of select="concat('/notice/', '234')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Comtat-Venaissin'">
                        <xsl:value-of select="concat('/notice/', '219')"/>
                    </xsl:when>
                    <xsl:when test="@target='#conflit_de_juridiction' or @target='#juridiction'">
                        <xsl:value-of select="concat('/notice/', '8')"/>
                    </xsl:when>
                    <xsl:when test="@target='#congé_de_remuage' or @target='#remuage'">
                        <xsl:value-of select="concat('/notice/', '11')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Conseil' or @target='#conseil'">
                        <xsl:value-of select="concat('/notice/', '20')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Contrebande' or @target='#contrebande' or @target='#contredandier' or @target='#contrebandiers'">
                        <xsl:value-of select="concat('/notice/', '22')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Contrôle_des_actes' or @target='#contrôle_des_actes' or @target='#actes'">
                        <xsl:value-of select="concat('/notice/', '138')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Cour_des_aides' or @target='#Cours_des_aides' or @target='#cour_des_aides' or @target='#cours_des_aides' or @target='#Cour' or @target='#cour'">
                        <xsl:value-of select="concat('/notice/', '182')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Courtiers-jaugeurs' or @target='#Courtiers-jaugeur' or @target='#courtiers-jaugeur' or @target='#courtiers-jaugeurs' or @target='#jaugeurs'">
                        <xsl:value-of select="concat('/notice/', '173')"/>
                    </xsl:when>
                    <xsl:when test="@target='#croupe' or @target='#Croupes' or @target='#croupier' or @target='#croupiers'">
                        <xsl:value-of select="concat('/notice/', '228')"/>
                    </xsl:when>
                    <xsl:when test="@target='#cuirs' or @target='#Cuirs' or @target='#cuir' or @target='#peaux' or @target='#Cuirs_et_peaux'">
                        <xsl:value-of select="concat('/notice/', '38')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Darigrand_Edme-François(1735-après_1796)' or @target='#Darigrand' or @target='#Edme-François_Darigrand'">
                        <xsl:value-of select="concat('/notice/', '25')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Denier_Saint-André' or @target='#dernier_Saint-André'">
                        <xsl:value-of select="concat('/notice/', '66')"/>
                    </xsl:when>
                    <xsl:when test="@target='#descente' or @target='#descentes'">
                        <xsl:value-of select="concat('/notice/', '29')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Dieppe'">
                        <xsl:value-of select="concat('/notice/', '128')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Dieppedalle'">
                        <xsl:value-of select="concat('/notice/', '246')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Dieuze'">
                        <xsl:value-of select="concat('/notice/', '5')"/>
                    </xsl:when>
                    <xsl:when test="@target='#directeur' or @target='#directeurs' or @target='#direction' or @target='#Direction_des_fermes' or @target='Directeur' or @target='#Direction'">
                        <xsl:value-of select="concat('/notice/', '216')"/>
                    </xsl:when>
                    <xsl:when test="@target='#domaine' or @target='#Domaine' or @target='#domaines'">
                        <xsl:value-of select="concat('/notice/', '136')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Domaine_d_Occident' or @target='#Occident'">
                        <xsl:value-of select="concat('/notice/', '120')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Dombes'">
                        <xsl:value-of select="concat('/notice/', '191')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Douane_de_Lyon' or @target='#douane_de_Lyon'">
                        <xsl:value-of select="concat('/notice/', '18')"/>
                    </xsl:when>

                    <xsl:when test="@target='#Valence' or @target='#douane_de_Valence'">
                        <xsl:value-of select="concat('/notice/', '99')"/>
                    </xsl:when>
                    <xsl:when test="@target='#drogueries_et_épiceries' or @target='#drogueries' or @target='#droguerie' or @target='#épicerie' or @target='#épiceries'">
                        <xsl:value-of select="concat('/notice/', '205')"/>
                    </xsl:when>
                    <xsl:when test="@target='#droits_réservés' or @target='#réservés'">
                        <xsl:value-of select="concat('/notice/', '42')"/>
                    </xsl:when>
                    <xsl:when test="@target='#droits_réunis' or @target='#réunis'">
                        <xsl:value-of select="concat('/notice/', '223')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Dunkerque' or @target='#Dunkerquois'">
                        <xsl:value-of select="concat('/notice/', '208')"/>
                    </xsl:when>
                    <xsl:when test="@target='#déchet' or @target='#déchets'">
                        <xsl:value-of select="concat('/notice/', '82')"/>
                    </xsl:when>
                    <xsl:when test="@target='#déclaration' or @target='#Déclaration' or @target='#déclarations'">
                        <xsl:value-of select="concat('/notice/', '187')"/>
                    </xsl:when>
                    <xsl:when test="@target='#délivrance' or @target='#délivrances' or @target='#Délivrance'">
                        <xsl:value-of select="concat('/notice/', '10')"/>
                    </xsl:when>
                    <xsl:when test="@target='#dépôt' or @target='#dépôts'">
                        <xsl:value-of select="concat('/notice/', '98')"/>
                    </xsl:when>
                    <xsl:when test="@target='#régie_des_dépôt'">
                        <xsl:value-of select="concat('/notice/', '206')"/>
                    </xsl:when>
                    <xsl:when test="@target='#eaux-de-vie'">
                        <xsl:value-of select="concat('/notice/', '210')"/>
                    </xsl:when>
                    <xsl:when test="@target='#écrou' or @target='#écrous'">
                        <xsl:value-of select="concat('/notice/', '203')"/>
                    </xsl:when>
                    <xsl:when test="@target='#délibération' or @target='#délibérations'">
                        <xsl:value-of select="concat('/notice/', '147')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Election' or @target='#élection' or @target='#élections' or @target='#Elections' or @target='#élu' or @target='#Elu' or @target='#Elus' or @target='#élus'">
                        <xsl:value-of select="concat('/notice/', '236')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Espagne'">
                        <xsl:value-of select="concat('/notice/', '89')"/>
                    </xsl:when>
                    <xsl:when test="@target='#étain' or @target='#Etain' or @target='#étains' or @target='#Etains'">
                        <xsl:value-of select="concat('/notice/', '209')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Etats_provinciaux' or @target='#provinciaux' or @target='#Etats' or @target='#Etat'">
                        <xsl:value-of select="concat('/notice/', '3')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Faux-saunage' or @target='#faux-saunage' or @target='#saunage'">
                        <xsl:value-of select="concat('/notice/', '60')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Ferme_générale' or @target='#générale' or @target='#ferme_générale'">
                        <xsl:value-of select="concat('/notice/', '93')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Flandres' or @target='#flandres' or @target='#Flandre' or @target='#flandre'">
                        <xsl:value-of select="concat('/notice/', '85')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Fonds_de_ferme' or @target='#fonds'">
                        <xsl:value-of select="concat('/notice/', '172')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Foraine' or @target='#foraine'">
                        <xsl:value-of select="concat('/notice/', '186')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Franc-Lyonnais'">
                        <xsl:value-of select="concat('/notice/', '6')"/>
                    </xsl:when>
                    <xsl:when test="@target='#penthières' or @target='#penthière'">
                        <xsl:value-of select="concat('/notice/', '81')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Franc-salé' or @target='#franc-salé' or @target='#franc-salés'">
                        <xsl:value-of select="concat('/notice/', '62')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Franche-Comté'">
                        <xsl:value-of select="concat('/notice/', '51')"/>
                    </xsl:when>
                    <xsl:when test="@target='#fraude' or @target='#fraudes' or @target='#fraudeurs'">
                        <xsl:value-of select="concat('/notice/', '122')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Gascogne'">
                        <xsl:value-of select="concat('/notice/', '193')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Gex'">
                        <xsl:value-of select="concat('/notice/', '165')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Grandes_gabelles' or @target='#grandes_gabelles'">
                        <xsl:value-of select="concat('/notice/', '196')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Dauphiné'">
                        <xsl:value-of select="concat('/notice/', '252')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Gros' or @target='#gros'">
                        <xsl:value-of select="concat('/notice/', '77')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Guérande'">
                        <xsl:value-of select="concat('/notice/', '16')"/>
                    </xsl:when>
                    <xsl:when test="@target='#grenier' or @target='#greniers'">
                        <xsl:value-of select="concat('/notice/', '68')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Hainaut'">
                        <xsl:value-of select="concat('/notice/', '188')"/>
                    </xsl:when>
                    <xsl:when test="@target='#huiles' or @target='#savons' or @target='#savon' or target='#huile'">
                        <xsl:value-of select="concat('/notice/', '115')"/>
                    </xsl:when>
                    <xsl:when test="@target='#huitième' or @target='#Huitième'">
                        <xsl:value-of select="concat('/notice/', '202')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Hôtel_des_Fermes' or @target='#hôtel' or @target='#hotel'">
                        <xsl:value-of select="concat('/notice/', '170')"/>
                    </xsl:when>
                    <xsl:when test="@target='#indult' or @target='#indults'">
                        <xsl:value-of select="concat('/notice/', '78')"/>
                    </xsl:when>
                    <xsl:when test="@target='#epsom' or @target='#Epsom'">
                        <xsl:value-of select="concat('/notice/', '253')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Ingrandes'">
                        <xsl:value-of select="concat('/notice/', '189')"/>
                    </xsl:when>
                    <xsl:when test="@target='#inscription_de_faux' or @target='#faux'">
                        <xsl:value-of select="concat('/notice/', '108')"/>
                    </xsl:when>
                    <xsl:when test="@target='#intendant' or @target='#Intendant' or @target='#intendants'">
                        <xsl:value-of select="concat('/notice/', '180')"/>
                    </xsl:when>
                    <xsl:when test="@target='#inventaire' or @target='#Inventaire' or @target='#inventaires'">
                        <xsl:value-of select="concat('/notice/', '160')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Joigny'">
                        <xsl:value-of select="concat('/notice/', '162')"/>
                    </xsl:when>
                    <xsl:when test="@target='#juridiction_des_traites'">
                        <xsl:value-of select="concat('/notice/', '225')"/>
                    </xsl:when>
                    <xsl:when test="@target='#La_Rochelle' or @target='#Rochelle'">
                        <xsl:value-of select="concat('/notice/', '14')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Labourd'">
                        <xsl:value-of select="concat('/notice/', '130')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Languedoc'">
                        <xsl:value-of select="concat('/notice/', '237')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Le_Havre' or @target='#Havre'">
                        <xsl:value-of select="concat('/notice/', '30')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Levant' or @target='#levant'">
                        <xsl:value-of select="concat('/notice/', '114')"/>
                    </xsl:when>
                    <xsl:when test="@target='#lieux_limitrophes' or @target='#limitrophes' or @target='#limitrophe'">
                        <xsl:value-of select="concat('/notice/', '7')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Limousin'">
                        <xsl:value-of select="concat('/notice/', '116')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Lorraine'">
                        <xsl:value-of select="concat('/notice/', '145')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Lyon' or @target='#Lyonnais' or @target='#lyonnais'">
                        <xsl:value-of select="concat('/notice/', '64')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Mandrin' or @target='#Louis_Mandrin'">
                        <xsl:value-of select="concat('/notice/', '204')"/>
                    </xsl:when>
                    <xsl:when test="@target='#or' or @target='#argent' or @target='#marques'">
                        <xsl:value-of select="concat('/notice/', '54')"/>
                    </xsl:when>
                    <xsl:when test="@target='#fer' or @target='#fers'">
                        <xsl:value-of select="concat('/notice/', '134')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Marseille' or @target='#marseille'">
                        <xsl:value-of select="concat('/notice/', '149')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Maspfenning' or @target='#maspfenning'">
                        <xsl:value-of select="concat('/notice/', '129')"/>
                    </xsl:when>
                    <xsl:when test="@target='#masse' or @target='#masses'">
                        <xsl:value-of select="concat('/notice/', '163')"/>
                    </xsl:when>
                    <xsl:when test="@target='#maître_des_ports' or @target='#ports'">
                        <xsl:value-of select="concat('/notice/', '67')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Minot' or @target='#minot' or @target='#minots'">
                        <xsl:value-of select="concat('/notice/', '221')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Montcornet'">
                        <xsl:value-of select="concat('/notice/', '152')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Montmorot'">
                        <xsl:value-of select="concat('/notice/', '179')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Mur' or @target='#mur'">
                        <xsl:value-of select="concat('/notice/', '103')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Mâconnais' or @target='#Mâcon' or @target='#Macon'">
                        <xsl:value-of select="concat('/notice/', '164')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Nantes' or @target='#Nantais'">
                        <xsl:value-of select="concat('/notice/', '212')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Nassau'">
                        <xsl:value-of select="concat('/notice/', '151')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Normandie' or @target='#normands' or @target='#normand'">
                        <xsl:value-of select="concat('/notice/', '97')"/>
                    </xsl:when>
                    <xsl:when test="@target='#ordonnance' or @target='#ordonnances' or @target='#Ordonnance'">
                        <xsl:value-of select="concat('/notice/', '100')"/>
                    </xsl:when>
                    <xsl:when test="@target='#pancarte' or @target='#pancartes'">
                        <xsl:value-of select="concat('/notice/', '58')"/>
                    </xsl:when>
                    <xsl:when test="@target='#papier_timbré' or @target='#timbrés' or @target='#timbres' or @target='#timbré'">
                        <xsl:value-of select="concat('/notice/', '1')"/>
                    </xsl:when>
                    <xsl:when test="@target='#papiers' or @target='#cartons' or @target='#papier' or @target='#carton'">
                        <xsl:value-of select="concat('/notice/', '158')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Passavant'">
                        <xsl:value-of select="concat('/notice/', '79')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Passe-debout' or @target='#debout'">
                        <xsl:value-of select="concat('/notice/', '133')"/>
                    </xsl:when>
                    <xsl:when test="@target='#passeports' or @target='#passeport' or @target='#Passeport'">
                        <xsl:value-of select="concat('/notice/', '154')"/>
                    </xsl:when>
                    <xsl:when test="@target='#patache' or @target='#pataches'">
                        <xsl:value-of select="concat('/notice/', '142')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Quart-Bouillon' or @target='#Bouillon' or @target='#Quart-bouillon'">
                        <xsl:value-of select="concat('/notice/', '197')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Salines'">
                        <xsl:value-of select="concat('/notice/', '24')"/>
                    </xsl:when>
                    <xsl:when test="@target='#rédimés' or @target='#rédimé' or @target='#rédimée' or @target='#rédimées'">
                        <xsl:value-of select="concat('/notice/', '211')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Pays-Bas'">
                        <xsl:value-of select="concat('/notice/', '123')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Peccais'">
                        <xsl:value-of select="concat('/notice/', '88')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Penthière'">
                        <xsl:value-of select="concat('/notice/', '81')"/>
                    </xsl:when>
                    <xsl:when test="@target='#petites_gabelles' or @target='#petite_gabelle'">
                        <xsl:value-of select="concat('/notice/', '2')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Peyriac' or @target='#Sigean'">
                        <xsl:value-of select="concat('/notice/', '12')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Picardie' or @target='#picard' or @target='#picards'">
                        <xsl:value-of select="concat('/notice/', '102')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Pont-de-Beauvoisin' or @target='#Beauvoisin'">
                        <xsl:value-of select="concat('/notice/', '127')"/>
                    </xsl:when>
                    <xsl:when test="@target='#porte-à-col' or @target='#Porte-à-col' or @target='#col'">
                        <xsl:value-of select="concat('/notice/', '96')"/>
                    </xsl:when>
                    <xsl:when test="@target='#porteur' or @target='#porteurs'">
                        <xsl:value-of select="concat('/notice/', '166')"/>
                    </xsl:when>
                    <xsl:when test="@target='#privilèges' or @target='#privileges' or @target='#privilège' or @target='#privilèges'">
                        <xsl:value-of select="concat('/notice/', '39')"/>
                    </xsl:when>
                    <xsl:when test="@target='#procès-verbal' or @target='#procès-verbaux'">
                        <xsl:value-of select="concat('/notice/', '32')"/>
                    </xsl:when>
                    <xsl:when test="@target='#prohibition' or @target='#Prohibition' or @target='#prohibées'">
                        <xsl:value-of select="concat('/notice/', '40')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Provence'">
                        <xsl:value-of select="concat('/notice/', '135')"/>
                    </xsl:when>
                    <xsl:when test="@target='#fermes'">
                        <xsl:value-of select="concat('/notice/', '168')"/>
                    </xsl:when>
                    <xsl:when test="@target='#réputés' or @target='#réputé' or @target='#étrangères'">
                        <xsl:value-of select="concat('/notice/', '119')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Pâris'">
                        <xsl:value-of select="concat('/notice/', '245')"/>
                    </xsl:when>
                    <xsl:when test="@target='#péages' or @target='#péage'">
                        <xsl:value-of select="concat('/notice/', '143')"/>
                    </xsl:when>
                    <xsl:when test="@target='#peche' or @target='#pêche' or @target='#pêches'">
                        <xsl:value-of select="concat('/notice/', '125')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Quatrième' or @target='#Quatrièmes' or @target='#quatrièmes' or @target='#quatrième'">
                        <xsl:value-of select="concat('/notice/', '117')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Quêtes' or @target='#quêtes' or @target='#quête' or @target='#Quête'">
                        <xsl:value-of select="concat('/notice/', '72')"/>
                    </xsl:when>
                    <xsl:when test="@target='#rebat' or @target='#rebats'">
                        <xsl:value-of select="concat('/notice/', '70')"/>
                    </xsl:when>
                    <xsl:when test="@target='#receveur' or @target='#receveurs' or @target='#général' or @target='#généraux'">
                        <xsl:value-of select="concat('/notice/', '226')"/>
                    </xsl:when>
                    <xsl:when test="@target='#particulier' or @target='#particuliers'">
                        <xsl:value-of select="concat('/notice/', '201')"/>
                    </xsl:when>
                    <xsl:when test="@target='#registre' or @target='#Registre' or @target='#registres' or @target='#Registres'">
                        <xsl:value-of select="concat('/notice/', '153')"/>
                    </xsl:when>
                    <xsl:when test="@target='#sexté' or @target='#sextés'">
                        <xsl:value-of select="concat('/notice/', '69')"/>
                    </xsl:when>
                    <xsl:when test="@target='#portatifs' or @target='#portatif'">
                        <xsl:value-of select="concat('/notice/', '249')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Registre-journal' or @target='#registre-journal'">
                        <xsl:value-of select="concat('/notice/', '109')"/>
                    </xsl:when>
                    <xsl:when test="@target='#regrat' or @target='#regrattier' or @target='#regrats' or @target='#regrattiers'">
                        <xsl:value-of select="concat('/notice/', '61')"/>
                    </xsl:when>
                    <xsl:when test="@target='#remontrances' or @target='#remontrance'">
                        <xsl:value-of select="concat('/notice/', '45')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Rethel' or @target='#Rethélois'">
                        <xsl:value-of select="concat('/notice/', '131')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Richelieu'">
                        <xsl:value-of select="concat('/notice/', '17')"/>
                    </xsl:when>
                    <xsl:when test="@target='#rivière' or @target='#rivières'">
                        <xsl:value-of select="concat('/notice/', '121')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Rochechouart'">
                        <xsl:value-of select="concat('/notice/', '175')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Rochefort'">
                        <xsl:value-of select="concat('/notice/', '87')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Rosière' or @target='#Rosières'">
                        <xsl:value-of select="concat('/notice/', '75')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Rouen'">
                        <xsl:value-of select="concat('/notice/', '217')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Roussillon'">
                        <xsl:value-of select="concat('/notice/', '94')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Ré'">
                        <xsl:value-of select="concat('/notice/', '107')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Prusse'">
                        <xsl:value-of select="concat('/notice/', '15')"/>
                    </xsl:when>
                    <xsl:when test="@target='#régie' or @target='#Régie'">
                        <xsl:value-of select="concat('/notice/', '139')"/>
                    </xsl:when>
                    <xsl:when test="@target='#sac' or @target='#sacs' or @target='#sacqueries' or @target='#sacquerie'">
                        <xsl:value-of select="concat('/notice/', '141')"/>
                    </xsl:when>
                    <xsl:when test="@target='#salines'">
                        <xsl:value-of select="concat('/notice/', '207')"/>
                    </xsl:when>
                    <xsl:when test="@target='#pays_de_salines'">
                        <xsl:value-of select="concat('/notice/', '24')"/>
                    </xsl:when>

                    <xsl:when test="@target='#Salins' or @target='#salins'">
                        <xsl:value-of select="concat('/notice/', '241')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Sault'">
                        <xsl:value-of select="concat('/notice/', '247')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Sedan'">
                        <xsl:value-of select="concat('/notice/', '59')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Seine'">
                        <xsl:value-of select="concat('/notice/', '35')"/>
                    </xsl:when>
                    <xsl:when test="@target='#aumône' or @target='#aumônes'">
                        <xsl:value-of select="concat('/notice/', '118')"/>
                    </xsl:when>
                    <xsl:when test="@target='#franchise' or @target='#franchises'">
                        <xsl:value-of select="concat('/notice/', '84')"/>
                    </xsl:when>
                    <xsl:when test="@target='#morue' or @target='#morues'">
                        <xsl:value-of select="concat('/notice/', '200')"/>
                    </xsl:when>
                    <xsl:when test="@target='#salaison' or @target='#salaisons' or @target='#sel'">
                        <xsl:value-of select="concat('/notice/', '13')"/>
                    </xsl:when>
                    <xsl:when test="@target='#salpêtre' or @target='#salpêtres'">
                        <xsl:value-of select="concat('/notice/', '177')"/>
                    </xsl:when>
                    <xsl:when test="@target='#gris' or @target='#sel_gris'">
                        <xsl:value-of select="concat('/notice/', '28')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Septèmes' or @target='#Les_Pennes' or @target='#Pennes'">
                        <xsl:value-of select="concat('/notice/', '231')"/>
                    </xsl:when>
                    <xsl:when test="@target='#serment' or @target='#serments' or @target='#catholicité'">
                        <xsl:value-of select="concat('/notice/', '95')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Sisteron'">
                        <xsl:value-of select="concat('/notice/', '218')"/>
                    </xsl:when>
                    <xsl:when test="@target='#soldat' or @target='#soldats'">
                        <xsl:value-of select="concat('/notice/', '150')"/>
                    </xsl:when>
                    <xsl:when test="@target='#sous-ferme' or @target='#sous-fermes'">
                        <xsl:value-of select="concat('/notice/', '227')"/>
                    </xsl:when>
                    <xsl:when test="@target='#subvention' or @target='#subventions'">
                        <xsl:value-of select="concat('/notice/', '184')"/>
                    </xsl:when>
                    <xsl:when test="@target='#sucre' or @target='#sucres'">
                        <xsl:value-of select="concat('/notice/', '174')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Suisse' or @target='#Suisses' or @target='#suisse' or @target='#suisses' ">
                        <xsl:value-of select="concat('/notice/', '126')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Sète'">
                        <xsl:value-of select="concat('/notice/', '113')"/>
                    </xsl:when>
                    <xsl:when test="@target='#tabac' or @target='#tabacs'">
                        <xsl:value-of select="concat('/notice/', '220')"/>
                    </xsl:when>
                    <xsl:when test="@target='#tabac_étranger' or @target='#étranger' or @target='#étrangers'">
                        <xsl:value-of select="concat('/notice/', '48')"/>
                    </xsl:when>
                    <xsl:when test="@target='#table_de_mer' or @target='#mer'">
                        <xsl:value-of select="concat('/notice/', '159')"/>
                    </xsl:when>
                    <xsl:when test="@target='#thé' or @target='#thés'">
                        <xsl:value-of select="concat('/notice/', '167')"/>
                    </xsl:when>
                    <xsl:when test="@target='#toiles' or @target='#coton' or @target='#cotons' or @target='#toile' or @target='#peintes'">
                        <xsl:value-of select="concat('/notice/', '238')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Tonneins'">
                        <xsl:value-of select="concat('/notice/', '49')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Touques'">
                        <xsl:value-of select="concat('/notice/', '43')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Touraine'">
                        <xsl:value-of select="concat('/notice/', '222')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Arzac'">
                        <xsl:value-of select="concat('/notice/', '57')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Tadoussac'">
                        <xsl:value-of select="concat('/notice/', '34')"/>
                    </xsl:when>
                    <xsl:when test="@target='#traite' or @target='#noirs'">
                        <xsl:value-of select="concat('/notice/', '106')"/>
                    </xsl:when>
                    <xsl:when test="@target='#transit' or @target='#transits'">
                        <xsl:value-of select="concat('/notice/', '157')"/>
                    </xsl:when>
                    <xsl:when test="@target='#trémie' or @target='#trémies'">
                        <xsl:value-of select="concat('/notice/', '65')"/>
                    </xsl:when>
                    <xsl:when test="@target='#trépas' or @target='#Trépas'">
                        <xsl:value-of select="concat('/notice/', '101')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Turenne'">
                        <xsl:value-of select="concat('/notice/', '23')"/>
                    </xsl:when>
                    <xsl:when test="@target='#vexations' or @target='#vexation' or @target='#véxations' or @target='#véxation'">
                        <xsl:value-of select="concat('/notice/', '37')"/>
                    </xsl:when>
                    <xsl:when test="@target='#vin' or @target='#vins'">
                        <xsl:value-of select="concat('/notice/', '19')"/>
                    </xsl:when>
                    <xsl:when test="@target='#voiture' or @target='#voitures' or @target='#voituriers' or @target='#voiturier'">
                        <xsl:value-of select="concat('/notice/', '199')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Welferding' or @target='#Sarreguemines'">
                        <xsl:value-of select="concat('/notice/', '50')"/>
                    </xsl:when>
                    <xsl:when test="@target='#effectif' or @target='#effectifs'">
                        <xsl:value-of select="concat('/notice/', '92')"/>
                    </xsl:when>
                    <xsl:when test="@target='#traite' or @target='#traites'">
                        <xsl:value-of select="concat('/notice/', '21')"/>
                    </xsl:when>
                    <xsl:when test="@target='#gabelles' or @target='#gabelle'">
                        <xsl:value-of select="concat('/notice/', '250')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Rhône' or @target='#rhône' or @target='#Rhone'">
                        <xsl:value-of select="concat('/notice/', '251')"/>
                    </xsl:when>
                    <xsl:when test="@target='#commis' or @target='#Commis'">
                        <xsl:value-of select="concat('/notice/', '76')"/>
                    </xsl:when>
                    <xsl:when test="@target='#bandoulières' or @target='#bandoulière'">
                        <xsl:value-of select="concat('/notice/', '185')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Savoie' or @target='#savoie'">
                        <xsl:value-of select="concat('/notice/', '257')"/>
                    </xsl:when>
                    <xsl:when test="@target='#sols_pour_livre' or @target='#sols' or @target='#sol' or @target='#sol_pour_livre'">
                        <xsl:value-of select="concat('/notice/', '256')"/>
                    </xsl:when>
                    <xsl:when test="@target='#visite' or @target='#Visite' or @target='#visites'">
                        <xsl:value-of select="concat('/notice/', '255')"/>
                    </xsl:when>
                    <xsl:when test="@target='#accommodement' or @target='#accommodements'">
                        <xsl:value-of select="concat('/notice/', '254')"/>
                    </xsl:when>
                                        <xsl:when test="@target='#acte_de_societe' or @target='#actes_de_societe' or  @target='#acte_de_société' or @target='#actes_de_société'">
                        <xsl:value-of select="concat('/notice/', '258')"/>
                    </xsl:when>
                    <xsl:when test="@target='#amende' or @target='#amendes'">
                        <xsl:value-of select="concat('/notice/', '259')"/>
                    </xsl:when>
                    <xsl:when test="@target='#bail' or @target='#bails' or @target='#baux'">
                        <xsl:value-of select="concat('/notice/', '260')"/>
                    </xsl:when>
                    <xsl:when test="@target='#bureau_des_finances' or @target='#bureaux_des_finances'">
                        <xsl:value-of select="concat('/notice/', '261')"/>
                    </xsl:when>
                    <xsl:when test="@target='#bureau_de_commerce' or @target='#bureaux_de_commerce'">
                        <xsl:value-of select="concat('/notice/', '262')"/>
                    </xsl:when>
                    <xsl:when test="@target='#commission' or @target='#commissions'">
                        <xsl:value-of select="concat('/notice/', '263')"/>
                    </xsl:when>
                    <xsl:when test="@target='#contrainte' or @target='#contraintes' or @target='#Contrainte'">
                        <xsl:value-of select="concat('/notice/', '264')"/>
                    </xsl:when>
                    <xsl:when test="@target='#controleur' or @target='#controleurs' or @target='#contrôleurs' or @target='#contrôleurs'">
                        <xsl:value-of select="concat('/notice/', '265')"/>
                    </xsl:when>
                    <xsl:when test="@target='#inspecteur' or @target='#inspecteurs'">
                        <xsl:value-of select="concat('/notice/', '266')"/>
                    </xsl:when>
                    <xsl:when test="@target='#Montauban' or @target='#montauban'">
                        <xsl:value-of select="concat('/notice/', '267')"/>
                    </xsl:when>
                    <xsl:when test="@target='#poitou' or @target='#Poitou'">
                        <xsl:value-of select="concat('/notice/', '268')"/>
                    </xsl:when>
                    <xsl:when test="@target='#procès_des_Fermiers_généraux' or @target='#procès'">
                        <xsl:value-of select="concat('/notice/', '269')"/>
                    </xsl:when>
                    <xsl:when test="@target='#femmes' or @target='#filles' or @target='#femmes,filles'">
                        <xsl:value-of select="concat('/notice/', '270')"/>
                    </xsl:when>
                    <xsl:when test="@target='#rébellion' or @target='#rébellions'">
                        <xsl:value-of select="concat('/notice/', '271')"/>
                    </xsl:when>
                    <xsl:when test="@target='#retraite' or @target='#retraites'">
                        <xsl:value-of select="concat('/notice/', '272')"/>
                    </xsl:when>
                    <xsl:when test="@target='#saisie' or @target='#saisies'">
                        <xsl:value-of select="concat('/notice/', '273')"/>
                    </xsl:when>
                    <xsl:when test="@target='#tounée' or @target='#tournées'">
                        <xsl:value-of select="concat('/notice/', '274')"/>
                    </xsl:when>
                    <xsl:when test="@target='#fermier_général_de_correspondance' or @target='#fermiers_généraux_de_correspondance'">
                        <xsl:value-of select="concat('/notice/', '275')"/>
                    </xsl:when>
                    <xsl:when test="@target='#passavant' or @target='#passavants'">
                        <xsl:value-of select="concat('/notice/', '79')"/>
                    </xsl:when>
                    <xsl:when test="@target='#sel_national' or @target='#national'">
                        <xsl:value-of select="concat('/notice/', '276')"/>
                    </xsl:when>
<!--                    <xsl:when test="@target='#' or @target='#'">-->
<!--                        <xsl:value-of select="concat('/notice/', '')"/>-->
<!--                    </xsl:when>-->




                    <xsl:otherwise/>
                </xsl:choose>


            </xsl:attribute>
            <xsl:attribute name="id">
                <xsl:text>links_notices</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="normalize-space(.)"/>
        </a>

    </xsl:template>

    <xsl:template match="graphic">
        <div class="text-center">
        <img class="img-thumbnail tableau">
            <xsl:attribute name="src">
                <xsl:value-of select="@url"/>
            </xsl:attribute>
        </img>
            <xsl:apply-templates/>
        </div>

    </xsl:template>
    <xsl:template match="head">
        <p class="text-center" id="img_title">
            <i>
        <xsl:apply-templates/>
            </i>
        </p>

    </xsl:template>

  <xsl:template match="br">
    <xsl:text>&emsp;&emsp;</xsl:text>
  </xsl:template>

    <xsl:template match="lb">
        <br/>
        <br/>
    </xsl:template>

    <xsl:template match="emph">
        <em class="font-weight-bold">
            <xsl:value-of select="."/>
        </em>
    </xsl:template>

<xsl:template match="hi[@rend='italic']">
  <em>
<xsl:apply-templates/>
  </em>
</xsl:template>

<xsl:template match="hi[@rend='sup'] | head//hi[@rend='sup']">
    <sup>
      <xsl:apply-templates/>
    </sup>
</xsl:template>


    <xsl:template match="br[@class='test']">
        <br/>
    </xsl:template>

</xsl:stylesheet>
