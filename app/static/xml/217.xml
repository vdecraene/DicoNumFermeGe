<?xml version='1.0' encoding='UTF-8'?>
<TEI n="217" xmlns="http://www.tei-c.org/ns/1.0" version="3.3.0">
            <teiHeader>
                  <fileDesc>
                        <titleStmt>
                              <title type="notice"> Rouen </title>
                              <author>Marie-Laure Legay</author>
                        </titleStmt>
                        <publicationStmt>
                              <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer
                                    le privilège la Ferme générale dans l'espace français et
                                    européen (1664-1794)Axe 1 : Dictionnaire de la Ferme générale,
                                    objet d'histoire totale </publisher>
                              <pubPlace>
                                    <address>
                                          <addrLine> Maison Européenne des Sciences de l'Homme et de
                                                la Société </addrLine>
                                          <addrLine> 2 rue des cannoniers </addrLine>
                                          <addrLine> 59002 Lille Cedex </addrLine>
                                    </address>
                                    2021-2025
                              </pubPlace>
                              <availability>
                                    <licence>Creative Commons Attribution 3.0 non
                                          transposé (CC BY 3.0)</licence>
                              </availability>
                        </publicationStmt>
                        <seriesStmt>
                              <title> Dictionnaire numérique de la Ferme générale </title>
                              <respStmt>
                                    <resp> Coordinateurs de l'axe : Dictionnaire numérique de la
                                          Ferme générale, objet d'histoire totale. </resp>
                                    <persName> Marie-Laure Legay </persName>
                                    <persName> Thomas Boullu </persName>
                              </respStmt>
                        </seriesStmt>
                        <sourceDesc><bibl><idno type="ArchivalIdentifier">AN, G1 73, pièce
                                                  17 ter : Direction des traites de Rouen : tableau
                                                  des receveurs et employés existants dans les
                                                  bureaux des fermes du département de Rouen au 1er
                                                  octobre 1785</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN, G1 79, dossier
                                                  4</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN G1 91, dossier
                                                  26 : « Salines du département de Rouen »</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN, G1 114 dossier
                                                  4, Rapport sur la réforme des débitants de tabac à
                                                  Rouen, 31 octobre 1788</idno>
                                          </bibl>
                                          <bibl><idno type="ArchivalIdentifier">Bibliothèque de
                                                  Rouen, Ms Montbret Y 15 : « Aides de Normandie,
                                                  Manuscrit appartenant à M. Monteil », XVIIIe
                                                  siècle</idno>
                                          </bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui ordonne
                                                que toutes les manufactures de toiles et étoffes de
                                                fil et de coton de toutes couleurs mêlées de soie et
                                                autres matières qui sont établies dans la Normandie,
                                                à l'exception de celles de Rouen et bourg de
                                                Dernetal, cesseront tout travail à commencer au 1er
                                              juillet de chaque année jusqu'au 15 septembre</hi>, 26
                                                juin 1723</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui prescrit
                                                les formalités à observer par les raffineurs de
                                                Bordeaux, La Rochelle, Rouen et Dieppe pour jouir de
                                                la restitution des droits d'entrée sur les sucres
                                                par eux raffinés provenant des sucres bruts des îles
                                                et colonies françaises de l'Amérique et qu'ils
                                              enverront à l'étranger</hi>, 17 novembre 1733</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat du roi qui
                                                déclare n'avoir entendu exempter du droit de
                                                consommation le poisson de mer pêché dans la Seine
                                                et porté à Rouen ordonne que les aloses et autres poissons de mer,
                                                quoique pêchés dans ladite rivière, acquitteront le
                                                droit à raison de 13 sols 5 denier par panier de
                                              quatre aloses et ainsi à proportion</hi>, 12 avril 1740</bibl>
                                          <bibl type="sources"><hi rend="italic">Lettres patentes du roi qui
                                                ordonnent que les généralités de Rouen, Caen et
                                                Alençon seront ajoutées, pour deux ans seulement, au
                                                ressort de la commission établie à Reims pour juger
                                              les contrebandiers</hi>, données à Versailles le 8
                                                janvier 1767</bibl>
                                          <bibl>Pierre Dardel, <hi rend="italic">Navires et marchandises dans les
                                              ports de Rouen et du Havre au XVIIIe siècle</hi>, Paris,
                                                Sevpen, 1963</bibl>
                                          <bibl>Jean-Pierre Bardet, <hi rend="italic">Rouen au XVIIe et XVIIIe
                                              siècles : les mutations d’un espace social</hi>, Paris,
                                                SEDES, 1983</bibl>
                                          <bibl>Vida Azimi, <hi rend="italic">Un modèle administratif de l’ancien
                                                régime. Les commis de la Ferme générale et de la
                                              régie générale des aides</hi>, Paris, éditions du CNRS,
                                                1987 p. 28-29</bibl>
                                          <bibl>Reynald Abad, <hi rend="italic">Le grand marché. L’approvisionnement
                                              alimentaire de Paris sous l’Ancien régime</hi>, Paris,
                                                Fayard, 2002</bibl>
                                          <bibl>Jacques Bottin, « De la toile au change :
                                                l’entrepôt rouennais et le commerce de Séville au
                                              début de l’époque moderne », <hi rend="italic">Annales du Midi</hi>, t.
                                                117, n°251, 2005, p. 323-345</bibl>
                                          <bibl>Jérôme Pigeon, <hi rend="italic">L’intendant de Rouen, juge du
                                                contentieux fiscal au XVIIIe siècle</hi>, Rouen, Presses
                                                universitaires de Rouen et du Havre, 2011</bibl>
                                    </sourceDesc></fileDesc>
                  <encodingDesc>
                        <projectDesc source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
                              <p> Le projet FermGé vise à étudier l’impact d’une organisation
                                    fiscale (1664-1794), discriminante mais rationnelle, sur les
                                    territoires et les sociétés de la France moderne/p </p>
                        </projectDesc>
                  </encodingDesc>
                  <profileDesc>
                        <textClass>
                              <keywords scheme="#fr_RAMEAU">
                                    <list>
                                          <item> Histoire -- Histoire moderne -- Histoire
                                                administrative </item>
                                          <item> Histoire -- Histoire moderne -- Histoire fiscale </item>
                                          <item> Histoire -- Histoire moderne -- Histoire politique
                                          </item>
                                    </list>
                              </keywords>
                        </textClass>
                  </profileDesc>
                  <revisionDesc>
                        <change type="AutomaticallyEncoded"> 2022-12-20T14:44:25.662076+2:00
                        </change>
                  </revisionDesc>
            </teiHeader>
            <text>
                  <body>
                        <entry n="217" type="R" xml:id="Rouen">
                              <form type="lemma">
                                    <orth> Rouen </orth>
                              </form>
                              <sense>
                                    <def> La situation de cette ville sur la <ref target="#Seine">
                                                Seine</ref>, entre le port du <ref target="#Havre"> Havre</ref> auquel elle était connectée par son
                                          propre port fluvial, et Paris, en faisait tout à la fois
                                          un centre de redistribution du sel et du <ref target="#tabac"> tabac</ref>, un port d’entrée
                                          pour de très nombreuses denrées et marchandises, y compris
                                          coloniales, et un centre fiscal majeur à l’entrée des <ref target="#fermes">Cinq grosses fermes</ref>. On y
                                          trouvait une <ref target="#direction"> direction</ref> de
                                          la Ferme générale avec un <ref target="#directeur">
                                                directeur</ref> et un <ref target="#receveur">
                                                receveur</ref> de première classe. Capitale de la
                                                <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Normandie"> Normandie</ref></orgName>, elle abritait en outre une <ref target="#Cour"> Cour</ref> des <ref target="#aides"> aides</ref> qui veillait aux <ref target="#privilèges"> privilèges</ref> de la
                                          province, notamment vis-à-vis des droits sur les boissons,
                                          particulièrement nombreux en ces contrées.<ref target="#épices"> épices</ref> dès 1549, le <ref target="#sucre">
                                                sucre</ref> (12 février 1665), le <ref target="#étranger">tabac étranger
                                         </ref> (1681), les castors (1685), les toiles
                                          étrangères (1692), les <ref target="#cuirs"> cuirs</ref> étrangers (1718), le <ref target="#café">
                                                café</ref> (à partir de 1736). Vis-à-vis des drogueries et <ref target="#épiceries"> épiceries</ref>, la Chambre
                                          de commerce de la ville participait à l’évaluation des
                                          tarifs car elle était considérée comme « la plus à portée
                                        d’en connaître la juste valeur » (AN, G1 79, dossier 4).
                                          En revanche, si <ref target="#Rouen">Rouen</ref> fut admise comme port d’entrée pour
                                          les marchandises du <ref target="#Levant"> Levant</ref>
                                          (arrêt du 15 août 1685 pour le
                                        paiement des droits de vingt pour cent <hi rend="italic">ad valorem</hi>), ses
                                          négociants durent se plier aux évaluations tarifaires de
                                          la Chambre de commerce de <ref target="#Marseille">
                                                Marseille</ref>. Au XVIIIe siècle toutefois, la
                                          vocation coloniale de son port s’affirma. <ref target="#Rouen">Rouen</ref> obtint le
                                          privilège d’armement pour les îles américaines (patentes
                                          d’avril 1717) et celui
                                          d’armement pour l’Afrique (patentes de 1719) ; il devint entrepôt pour le cacao. A
                                          la fin de l’Ancien régime, le complexe portuaire Le
                                          Havre-Rouen captait 20 % du trafic colonial à l’entrée du
                                          royaume.<ref target="#greniers"> greniers</ref>
                                          à partir du <ref target="#dépôt"> dépôt</ref> de <ref target="#Dieppedalle"> Dieppedalle</ref> qui
                                          recevait les sels de <ref target="#Marennes"> Marennes
                                         </ref>, de la Tremblade et du Seudres. Huit à neuf mille
                                          muids de sel y étaient redistribués vers l’Ile-de-France,
                                          la <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Champagne"> Champagne</ref></orgName>, la <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Bourgogne"> Bourgogne</ref></orgName>, le Soissonnais, par la <ref target="#Seine">
                                                Seine</ref>, l’Oise et l’Aisne, la Marne et
                                          l’Yonne. Pour surveiller la manutention et lutter contre
                                          les versements frauduleux, vingt employés, dont onze
                                          gardes, deux <ref target="#pataches"> patachiers</ref>
                                          et deux matelots, surveillaient le dépôt. La <ref target="#direction"> direction</ref> des <ref target="#gabelles"> gabelles</ref> faisait état en
                                                1787, en dehors du <ref target="#directeur"> directeur</ref> général de la
                                          Ferme et de son receveur <ref target="#général"> général
                                         </ref>, de deux avocats et un procureur de la Ferme près
                                          de la <ref target="#Cour"> Cour</ref> des <ref target="#aides"> aides</ref>, d’un clerc
                                          procureur, d’un procureur près de l’<ref target="#Election"> Election</ref> de Rouen, et
                                          d’un <ref target="#agent"> agent</ref> général pour
                                          toutes les parties. Le <ref target="#grenier"> grenier
                                         </ref> de la ville, dressé sur de nouveaux plans en 1725, comptait quant à lui un
                                          receveur, quatre gardes, un porteur de sel, un briseur, un
                                          contrôleur des salines et un tonnelier.
                                        <figure>
                                            <graphic url="/static/images/rouen_img.jpg"/>
                                            <head>Grenier à sel de Rouen : projet d’élévation de nouveaux pavillons, 1725
(BNF, département Estampes et photographie, RESERVE HA-18 (45)-FOL ; Gallica)</head>
                                        </figure><orgName subtype="administrations_juridictions_fermes" type="administrations_et_juridictions"> direction de
                                                Rouen</orgName> comprenait également les bureaux
                                          d’enregistrement et de paiement pour le <ref target="#tabac"> tabac</ref>, les huiles et <ref target="#savons"> savons</ref>, les droits d’abord
                                          et de consommation sur les poissons et aloses, les <ref target="#fers">marques de  fers</ref>, les <ref target="#aides"> aides</ref> (notamment le <ref target="#Gros"> Gros</ref> et pied fourché, les
                                          droits de Jauge et <ref target="#courtage"> courtage
                                         </ref>, octrois et <ref target="#eaux-de-vie">
                                                eaux-de-vie</ref>, droits dits « Pont de Rouen »,
                                                <ref target="#Quatrième"> Quatrième</ref>, <ref target="#Subvention"> Subvention</ref>, Formule de
                                          la ville), le <ref target="#Occident">Domaine d’Occident
                                         </ref>, les <ref target="#traites"> traites</ref> en
                                          général. Le bureau d’entrée du port comprenait 19 employés
                                          (un receveur, un contrôleur, un inspecteur, deux
                                          visiteurs, un receveur aux déclarations, un commis aux
                                          <ref target="#caution">acquits à caution</ref>, un
                                          peseur au poids principal, un contrôleur au poids
                                          principal, un peseur au grand poids, un contrôleur au
                                          grand poids, un commis aux enregistrements des visites et
                                          pesées, un commis aux expéditions, un peseur au poids de
                                          la chèvre, un contrôleur au poids de la chèvre, un commis
                                          aux déclarations, un concierge, un tonnelier emballeur et
                                          un deuxième tonnelier). Le bureau de sortie rassemblait
                                          douze employés (un receveur, un inspecteur au transit, un
                                          contrôleur, deux visiteurs, un peseur, un commis aux
                                          <ref target="#caution">acquits à caution</ref>, un
                                          receveur aux déclarations, un commis aux expéditions, un
                                          concierge, un tonnelier, un emballeur). Ce dernier bureau
                                          traitait notamment la sortie de la production du cru de la
                                          ville comme les toiles de coton, le <ref target="#sucre">
                                                sucre</ref> raffiné. Les raffineurs de Rouen
                                          étaient fiscalement encouragés : la Ferme générale
                                          restituait les droits d’entrée sur les sucres bruts dès
                                          lors que le raffiné était destiné à l’exportation. En
                                          revanche, le droit particulier que les bureaux levaient à
                                          Rouen (50 sous sur le quintal de sucre) demeurait au
                                          profit de la ferme du <ref target="#Occident">Domaine d’Occident</ref>.<ref target="#fraudes"> fraudes</ref>. La plus commune
                                          concernait les alcools, le sel et le tabac. Comme le reste
                                          de la <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Normandie"> Normandie</ref></orgName>, Rouen cumulait toutes les taxes sur les <ref target="#boissons"> boissons</ref> : droits
                                          d’entrée (<ref target="#sous">cinq sous</ref> anciens et
                                          nouveaux), de <ref target="#quatrième"> quatrième</ref>,
                                          de <ref target="#gros"> gros</ref> (à Rouen seulement),
                                          de <ref target="#subvention"> subvention</ref>, <ref target="#courtiers-jaugeurs"> courtiers-jaugeurs
                                         </ref> et inspecteurs aux <ref target="#boissons">
                                                boissons</ref>. Cette accumulation explique
                                          l’étendue de la <ref target="#fraude"> fraude</ref>
                                          rapportée dans un mémoire du fonds Monbret de la
                                          bibliothèque de Rouen : « Les espèces de fraudes qui sont
                                          les plus communes sont, les ventes au détail sans
                                          déclaration, appellées vulgairement vente à muchepot, les
                                          entrepôts que les cabaretiers font chez leurs voisins et
                                          chez ceux qui ont communication avec leurs cabarets,
                                          l’entrée en fraude des boissons dans les lieux qui sont
                                          sujets aux droits de subvention qui ne se trouvent point
                                          fermés de portes ny de barrières, le transport des
                                          eaux-de-vie, cidre et poiré de la fabrication de la
                                          province sans congé ny soumission pris au lieu
                                          d’enlèvement d’où elles sont ainsy conduittes chez des
                                          vendans en détail qui les cachent et les débitent en
                                          fraude desdits droits de détail ou sont voiturées aux
                                          environs de Roüen ou Caen, dans lesquelles villes on les
                                          fait entrer nuitamment en fraude des droits de gros et de
                                          détail ». Vis-à-vis du sel, le faux-saunage provenait
                                          surtout des marchands de poissons : le débit de la pêche
                                          devenait si important que le contrôleur des <ref target="#salines"> salines</ref> ne pouvait visiter
                                          tous les barils sans compromettre le commerce. Le 13 mai
                                                1718, trente
                                          faux-sauniers entrèrent en lutte avec les archers (arrêt
                                          de la Cour des Aides, 2 juin 1718). <ref target="#gabelles"> gabelles</ref> de Rouen réunit
                                          en 1727 les principaux
                                          négociants de la ville, avec l’avocat, le procureur de la
                                          ferme et le contrôleur du <placeName type="lieux_controle"> bureau de Pecq</placeName>, près de Paris. Cette
                                          concertation visait à engager les négociants à prendre
                                          leurs <ref target="#caution">acquits à caution</ref>
                                          pour toutes les <ref target="#salines"> salines</ref>
                                          destinées au port du Pecq. La <ref target="#Direction">
                                                Direction</ref> des <ref target="#gabelles">
                                                gabelles</ref> entretenait à la fin de l’Ancien
                                          régime cinq <ref target="#brigades"> brigades</ref> à
                                          Rouen, une sur la rive droite de la <ref target="#Seine">
                                                Seine</ref> (six employés), une brigade fluviale
                                          appelée « biscayenne », du nom d’une petite embarcation à
                                          rames (sept employés dont le pilote), et trois autres <ref target="#brigades"> brigades</ref> de six
                                          personnes. Une autre fraude massive fut repérée
                                          tardivement : les détaillants du tabac dans la <placeName type="lieux_habitations"> ville de Rouen</placeName>, au nombre de onze, coupaient le tabac du
                                          monopole en le mélangeant. Un plan de réforme des
                                          détaillants fut donc arrêté par la délibération du 25
                                          janvier 1788, mais le <ref target="#directeur"> directeur</ref> de la Ferme
                                          s’y opposa, « oppositions fondées, d’après les Fermiers,
                                          plutôt sur les préjugés d’une routine ancienne et
                                          vicieuse ». En réalité, le directeur relayait l’opposition
                                          politique des Magistrats de la Cour des aides et du
                                          Parlement vis-à-vis de la Ferme générale.<ref target="#contrebande">
                                                contrebande</ref>. Le 30 septembre 1728 par exemple, treize
                                          contrebandiers de tabac et d’indiennes entrèrent dans la
                                          ville. La <ref target="#direction"> direction</ref> de la
                                          Ferme fit évoluer ses <ref target="#brigades"> brigades
                                         </ref>, mais ce ne fut guère suffisant. En 1767, la ville entra, comme le
                                          reste de la <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Normandie"> Normandie</ref></orgName>, dans le ressort de la <ref target="#commission"> commission</ref> de Reims
                                          chargée de juger les <ref target="#contrebandiers">
                                                contrebandiers</ref>. En 1787, la brigade des <ref target="#traites"> traites
                                         </ref> de Rouen comptait 12 employés, 61 gardes et trois
                                          matelots. </def>
                                    <listBibl><bibl><idno type="ArchivalIdentifier">AN, G1 73, pièce
                                                  17 ter : Direction des traites de Rouen : tableau
                                                  des receveurs et employés existants dans les
                                                  bureaux des fermes du département de Rouen au 1er
                                                  octobre 1785</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN, G1 79, dossier
                                                  4</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN G1 91, dossier
                                                  26 : « Salines du département de Rouen »</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN, G1 114 dossier
                                                  4, Rapport sur la réforme des débitants de tabac à
                                                  Rouen, 31 octobre 1788</idno>
                                          </bibl>
                                          <bibl><idno type="ArchivalIdentifier">Bibliothèque de
                                                  Rouen, Ms Montbret Y 15 : « Aides de Normandie,
                                                  Manuscrit appartenant à M. Monteil », XVIIIe
                                                  siècle</idno>
                                          </bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui ordonne
                                                que toutes les manufactures de toiles et étoffes de
                                                fil et de coton de toutes couleurs mêlées de soie et
                                                autres matières qui sont établies dans la Normandie,
                                                à l'exception de celles de Rouen et bourg de
                                                Dernetal, cesseront tout travail à commencer au 1er
                                              juillet de chaque année jusqu'au 15 septembre</hi>, 26
                                                juin 1723</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui prescrit
                                                les formalités à observer par les raffineurs de
                                                Bordeaux, La Rochelle, Rouen et Dieppe pour jouir de
                                                la restitution des droits d'entrée sur les sucres
                                                par eux raffinés provenant des sucres bruts des îles
                                                et colonies françaises de l'Amérique et qu'ils
                                              enverront à l'étranger</hi>, 17 novembre 1733</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat du roi qui
                                                déclare n'avoir entendu exempter du droit de
                                                consommation le poisson de mer pêché dans la Seine
                                                et porté à Rouen ordonne que les aloses et autres poissons de mer,
                                                quoique pêchés dans ladite rivière, acquitteront le
                                                droit à raison de 13 sols 5 denier par panier de
                                              quatre aloses et ainsi à proportion</hi>, 12 avril 1740</bibl>
                                          <bibl type="sources"><hi rend="italic">Lettres patentes du roi qui
                                                ordonnent que les généralités de Rouen, Caen et
                                                Alençon seront ajoutées, pour deux ans seulement, au
                                                ressort de la commission établie à Reims pour juger
                                              les contrebandiers</hi>, données à Versailles le 8
                                                janvier 1767</bibl>
                                          <bibl>Pierre Dardel, <hi rend="italic">Navires et marchandises dans les
                                              ports de Rouen et du Havre au XVIIIe siècle</hi>, Paris,
                                                Sevpen, 1963</bibl>
                                          <bibl>Jean-Pierre Bardet, <hi rend="italic">Rouen au XVIIe et XVIIIe
                                              siècles : les mutations d’un espace social</hi>, Paris,
                                                SEDES, 1983</bibl>
                                          <bibl>Vida Azimi, <hi rend="italic">Un modèle administratif de l’ancien
                                                régime. Les commis de la Ferme générale et de la
                                              régie générale des aides</hi>, Paris, éditions du CNRS,
                                                1987 p. 28-29</bibl>
                                          <bibl>Reynald Abad, <hi rend="italic">Le grand marché. L’approvisionnement
                                              alimentaire de Paris sous l’Ancien régime</hi>, Paris,
                                                Fayard, 2002</bibl>
                                          <bibl>Jacques Bottin, « De la toile au change :
                                                l’entrepôt rouennais et le commerce de Séville au
                                              début de l’époque moderne », <hi rend="italic">Annales du Midi</hi>, t.
                                                117, n°251, 2005, p. 323-345</bibl>
                                          <bibl>Jérôme Pigeon, <hi rend="italic">L’intendant de Rouen, juge du
                                                contentieux fiscal au XVIIIe siècle</hi>, Rouen, Presses
                                                universitaires de Rouen et du Havre, 2011</bibl>
                                    </listBibl></sense>
                        </entry>
                  </body>
            </text>
      </TEI>
      