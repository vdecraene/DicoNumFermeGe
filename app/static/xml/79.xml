<?xml version='1.0' encoding='UTF-8'?>
<TEI n="79" xmlns="http://www.tei-c.org/ns/1.0" version="3.3.0">
            <teiHeader>
                  <fileDesc>
                        <titleStmt>
                              <title type="notice"> Passavant </title>
                              <author>Marie-Laure Legay</author>
                        </titleStmt>
                        <publicationStmt>
                              <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer
                                    le privilège la Ferme générale dans l'espace français et
                                    européen (1664-1794)Axe 1 : Dictionnaire de la Ferme générale,
                                    objet d'histoire totale </publisher>
                              <pubPlace>
                                    <address>
                                          <addrLine> Maison Européenne des Sciences de l'Homme et de
                                                la Société </addrLine>
                                          <addrLine> 2 rue des cannoniers </addrLine>
                                          <addrLine> 59002 Lille Cedex </addrLine>
                                    </address>
                                    2021-2025
                              </pubPlace>
                              <availability>
                                    <licence>Creative Commons Attribution 3.0 non
                                          transposé (CC BY 3.0)</licence>
                              </availability>
                        </publicationStmt>
                        <seriesStmt>
                              <title> Dictionnaire numérique de la Ferme générale </title>
                              <respStmt>
                                    <resp> Coordinateurs de l'axe : Dictionnaire numérique de la
                                          Ferme générale, objet d'histoire totale. </resp>
                                    <persName> Marie-Laure Legay </persName>
                                    <persName> Thomas Boullu </persName>
                              </respStmt>
                        </seriesStmt>
                        <sourceDesc><bibl><idno type="ArchivalIdentifier">AN, G1 79,
                                                  dossier 18 : <hi rend="italic">Instruction concernant la régie qui
                                                  doit être suivie pour les marchandises et effets
                                                  qui sont expédiés par passeports du Roi, ordres
                                                  des ministres, ou de la compagnie en conséquence</hi>,
                                                  septembre 1763</idno>
                                          </bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat, et lettres
                                                patentes sur icelui, qui ordonnent l'exécution des
                                                édits d'août 1703 et mai 1705, et que les fermiers
                                                de la vente du sel Rozière seront tenus de faire
                                                prendre par leurs voituriers des congés ou passavant
                                                pour les sels qu’ils lèveront aux sauneries de
                                                Salins, et de les rapporter dans un mois de leur
                                                date, endossés des certificats de la remise desdits
                                                sels dans les entrepôts spécifiés auxdits congés, le
                                              tout à peine de 1000 livres d'amende</hi>, 26 avril
                                                1723</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat portant
                                                règlement pour les déclarations à faire et les
                                                passavants à prendre par les marchands, regratiers
                                                et autres vendant sel, dans les pays exempts ou
                                                rédimés des droits de gabelles, dans l'étendue des
                                              cinq lieues des frontières des pays de gabelle</hi>, 31
                                                mai 1723</bibl>
                                    </sourceDesc></fileDesc>
                  <encodingDesc>
                        <projectDesc source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
                              <p> Le projet FermGé vise à étudier l’impact d’une organisation
                                    fiscale (1664-1794), discriminante mais rationnelle, sur les
                                    territoires et les sociétés de la France moderne/p </p>
                        </projectDesc>
                  </encodingDesc>
                  <profileDesc>
                        <textClass>
                              <keywords scheme="#fr_RAMEAU">
                                    <list>
                                          <item> Histoire -- Histoire moderne -- Histoire
                                                administrative </item>
                                    </list>
                              </keywords>
                        </textClass>
                  </profileDesc>
                  <revisionDesc>
                        <change type="AutomaticallyEncoded"> 2022-12-20T09:35:52.424556+2:00
                        </change>
                  </revisionDesc>
            </teiHeader>
            <text>
                  <body>
                        <entry n="79" type="P" xml:id="Passavant">
                              <form type="lemma">
                                    <orth> Passavant </orth>
                              </form>
                              <sense>
                                    <def> Passe-droit. Ce terme désignait le billet que les
                     voituriers prenaient au bureau pour avoir permission de
                     conduire des marchandises d’une ville à l’autre, soit
                     après avoir payé les droits, soit pour marquer qu’il
                     fallait les payer à un autre bureau, soit pour indiquer
                     qu’elles ne devaient rien. Le plus souvent, cette
                     expédition était délivrée pour des marchandises exemptes :
                      « si d’un côté, le passavant constate l’observation de la
                     règle prescrite pour les déclarations, visites, etc, d’un
                     autre côté, il assure à la marchandise qu’elle accompagne
                     l’exemption dont elle jouit, elle prévient tout
                     inconvénient dans la route que tient la marchandise » (AN
                     G1 <ref target="#311"> 311</ref>, f° 50 v°, mémoire du
                                          15 avril 1775). Le prix d’un
                                          passavant montait à un sou deux deniers. A <ref target="#Bordeaux"> Bordeaux</ref>, la <ref target="#direction"> direction</ref> des fermes
                                          jugea nécessaire d’élever le prix à 3 sols sous prétexte
                                          que les commis pour l’expédition travaillaient avec zèle.<ref target="#rédimés"> rédimés</ref>, ou pays de
                                                  <ref target="#salines"> salines</ref>, ainsi que
                                                dans les cinq <ref target="#limitrophes">lieues limitrophes</ref> des pays de <ref target="#gabelle"> gabelle</ref> (déclaration du
                                                21 avril 1705).
                                          Pour contourner cette exigence, les habitants déclaraient
                                          lors des visites avoir pris les passavants et les avoir
                                          laissés au contrôleur. Les commis de la Ferme assignaient
                                          les suspects devant le juge mais le faux-saunier demandait
                                          à procéder par témoin et, « contre la foi publique des
                                          registres des contrôleurs » obtenaient le plus souvent
                                          gain de cause non seulement des premiers juges, mais aussi
                                          des <ref target="#cour_des_aides">cours des aides</ref>. Dans les pays de <ref target="#Quart-bouillon"> Quart-bouillon</ref>,
                                          les passavants, délivrés par les bureaux de revente,
                                          permettaient d’aller chercher le sel aux salines et de le
                                          conduire aux domiciles des habitants. La <ref target="#fraude"> fraude</ref> consistait en leur
                                          falsification, pratique courante encore en 1776, lorsque le Conseil du
                                          roi en constata l’importance. De même en <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Franche-Comté"> Franche-Comté</ref></orgName>, <ref target="#pays_de_salines">pays de salines
                                         </ref>, les <ref target="#sous-fermiers"> sous-fermiers
                                         </ref>, employés, voituriers… vendaient en chemin le sel
                                                <ref target="#rosière"> rosière</ref> chargé à <ref target="#Salins"> Salins</ref> et destiné aux
                                          communautés d’habitants. Louis XV les obligea donc à
                                          prendre des passavants indiquant la date de l’enlèvement,
                                          le nombre de pains de sels, l’entrepôt auxquels ils sont
                                          destinés… et à présenter ces passavants un mois après pour
                                          décharge aux contrôleurs.<figure>
                                        <graphic url="/static/images/passavant_img.jpg"/>
                                        <head>Archives départementales du Doubs, 1C 1336, Passavants,
Direction de Lyon (1764) et Direction de Besançon (1767)</head>
                                    </figure><figure>
                                        <graphic url="/static/images/passavant_img_2.jpg"/>

                                    </figure></def>
                                    <listBibl><bibl><idno type="ArchivalIdentifier">AN, G1 79,
                                                  dossier 18 : <hi rend="italic">Instruction concernant la régie qui
                                                  doit être suivie pour les marchandises et effets
                                                  qui sont expédiés par passeports du Roi, ordres
                                                  des ministres, ou de la compagnie en conséquence</hi>,
                                                  septembre 1763</idno>
                                          </bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat, et lettres
                                                patentes sur icelui, qui ordonnent l'exécution des
                                                édits d'août 1703 et mai 1705, et que les fermiers
                                                de la vente du sel Rozière seront tenus de faire
                                                prendre par leurs voituriers des congés ou passavant
                                                pour les sels qu’ils lèveront aux sauneries de
                                                Salins, et de les rapporter dans un mois de leur
                                                date, endossés des certificats de la remise desdits
                                                sels dans les entrepôts spécifiés auxdits congés, le
                                              tout à peine de 1000 livres d'amende</hi>, 26 avril
                                                1723</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat portant
                                                règlement pour les déclarations à faire et les
                                                passavants à prendre par les marchands, regratiers
                                                et autres vendant sel, dans les pays exempts ou
                                                rédimés des droits de gabelles, dans l'étendue des
                                              cinq lieues des frontières des pays de gabelle</hi>, 31
                                                mai 1723</bibl>
                                    </listBibl></sense>
                        </entry>
                  </body>
            </text>
      </TEI>
      