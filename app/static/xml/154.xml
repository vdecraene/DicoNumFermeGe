<?xml version='1.0' encoding='UTF-8'?>
<TEI n="154" xmlns="http://www.tei-c.org/ns/1.0" version="3.3.0">
            <teiHeader>
                  <fileDesc>
                        <titleStmt>
                              <title type="notice"> Passeport </title>
                              <author>Arnaud Le Gonidec</author>
                        </titleStmt>
                        <publicationStmt>
                              <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer
                                    le privilège la Ferme générale dans l'espace français et
                                    européen (1664-1794)Axe 1 : Dictionnaire de la Ferme générale,
                                    objet d'histoire totale </publisher>
                              <pubPlace>
                                    <address>
                                          <addrLine> Maison Européenne des Sciences de l'Homme et de
                                                la Société </addrLine>
                                          <addrLine> 2 rue des cannoniers </addrLine>
                                          <addrLine> 59002 Lille Cedex </addrLine>
                                    </address>
                                    2021-2025
                              </pubPlace>
                              <availability>
                                    <licence>Creative Commons Attribution 3.0 non
                                          transposé (CC BY 3.0)</licence>
                              </availability>
                        </publicationStmt>
                        <seriesStmt>
                              <title> Dictionnaire numérique de la Ferme générale </title>
                              <respStmt>
                                    <resp> Coordinateurs de l'axe : Dictionnaire numérique de la
                                          Ferme générale, objet d'histoire totale. </resp>
                                    <persName> Marie-Laure Legay </persName>
                                    <persName> Thomas Boullu </persName>
                              </respStmt>
                        </seriesStmt>
                        <sourceDesc><bibl><idno type="ArchivalIdentifier">AN G1 79,
                                                  dossier 18 : « Instruction de ce qui doit être
                                                  observé dans les Bureaux par rapport aux
                                                  Marchandises qui y passent en exemption des
                                                  Droits, en vertu de Passeports pour le Service du
                                                  Roy », mars 1747  id. :« Instruction concernant
                                                  la Régie qui doit être suivie pour les
                                                  marchandises effets, qui sont expédiés par
                                                  Passeports du Roi, Ordres du Ministre, ou de la
                                                  Compagnie en conséquence », septembre 1763</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN G7 1171 :
                                                  Passeports</idno>
                                          </bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil du 24 avril 1742,
                                                in Table des edicts, declarations, arrests et
                                                reglemens, rendus pendant la quatrième année du Bail
                                              de Me Jacques Forceville</hi>, t. 10, Chez Pierre Prault,
                                                1768, p. 41-42</bibl>
                                          <bibl type="sources"><hi rend="italic">Bail des gabelles de France, entrées
                                                sorties du Royaume, Doüane de Lyon Valence, Patente
                                                de Languedoc, Convoy Comptablie de Bourdeaux,
                                                Entrées de Paris Roüen, Aydes de France, Fret,
                                              autres Fermes Royales unies</hi></bibl>
                                          <bibl type="sources"><hi rend="italic">Fait à Maistre François le Gendre,
                                                Bourgeois de Paris, pour six années, commencées au
                                              premier Octobre 1668</hi>. Chez Frederic Leonard, 1680</bibl>
                                          <bibl type="sources"><hi rend="italic">Édit de décembre 1781 fixant les
                                                privilèges des sujets des états du corps Helvétique
                                              dans le royaume</hi>, in Magnien (V.), <hi rend="italic">Recueil
                                                alphabétique des droits de traites uniformes, de
                                                ceux d’entrées et de sortie des cinq grosses fermes,
                                              de douane de Lyon et de Valence</hi>, c., t. 3, s.l.,
                                                1786, p. 340</bibl>
                                          <bibl type="sources">Guyoy (J.-N.), <hi rend="italic">Répertoire universel
                                                et raisonné de jurisprudence civile, criminelle,
                                              canonique et bénéficiale</hi>, t. 44, Chez Panckoucke et
                                                Dupuis, 1781, v° « Passe-port », p. 481-485</bibl>
                                          <bibl type="sources">Rousselot de Surgy (J.-P.),
                                              <hi rend="italic">Encyclopédie méthodique. Finances</hi>. T. 3, Chez
                                                Panckouke, 1787, v° « passeport », p. 300-304</bibl>
                                        <bibl>A. Sée, <hi rend="italic">Le passeport en France</hi>, Chartres, Edmond
                                                Garnier, 1907, p. 23-33</bibl>
                                          <bibl>Lucien Bély, <hi rend="italic">Espions et ambassadeurs au temps de
                                              Louis XIV</hi>, Fayard, 1990, p. 610-653</bibl>
                                          <bibl>Jean Clinquart, <hi rend="italic">Les services extérieurs de la Ferme
                                                générale à la fin de l’Ancien Régime. L’exemple de
                                              la direction des fermes du Hainaut</hi>, CHEFF, 1995, p.
                                                80 et 155</bibl>
                                          <bibl>Daniel Nordman, « Sauf-conduits et
                                                Passeports », <hi rend="italic">Dictionnaire de l’Ancien Régime :
                                                  Royaume de France XVIe – XVIIIe siècle</hi>, dir. L.
                                                Bély, Paris, PUF, 1996, p. 1122-1124</bibl>
                                          <bibl>Jean Clinquart, « Le dédouanement des marchandises
                                                sous l’Ancien Régime », <hi rend="italic">La circulation des
                                                  marchandises dans la France de l’Ancien Régime</hi>, dir.
                                                Woronoff (D.), CHEFF, 1998, p. 103-144</bibl>
                                          <bibl>Gérard Noiriel, « Surveiller les déplacements ou
                                                identifier les personnes ? Contribution à l’histoire
                                                du passeport en France de la Ière à la IIIème
                                                République », in <hi rend="italic">Genèses</hi>, 1998/30, Émigrés,
                                                vagabonds, passeports, dir. Leroy (J.), p. 77-100</bibl>
                                          <bibl>Corinne Thépaut-Cabasset, « Garde-robe de souverain
                                                et réseau international : l’exemple de la Bavière
                                                dans les années 1680 », <hi rend="italic">Se vêtir à la cour en Europe
                                                1400-1815 : Cultures matérielles, cultures visuelles
                                                  du costume dans les cours européennes</hi>, dir. I.
                                                Paresys et N. Coquery, Villeneuve d’Ascq,
                                                Publications de l’Irhis, 2011, p. 177-193</bibl>
                                          <bibl>Cédric Glineur, « Le régime juridique des
                                                passeports sous l’Ancien Régime, L’exemple des
                                                provinces du Nord », dans <hi rend="italic">Guerre, frontière,
                                                barrière et paix en Flandre</hi>, dir. Rickebusch (O) et
                                                R. Opsommer (R.), Ypres, 2014, p. 59-73</bibl>
                                    </sourceDesc></fileDesc>
                  <encodingDesc>
                        <projectDesc source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
                              <p> Le projet FermGé vise à étudier l’impact d’une organisation
                                    fiscale (1664-1794), discriminante mais rationnelle, sur les
                                    territoires et les sociétés de la France moderne/p </p>
                        </projectDesc>
                  </encodingDesc>
                  <profileDesc>
                        <textClass>
                              <keywords scheme="#fr_RAMEAU">
                                    <list>
                                          <item> Histoire -- Histoire moderne -- Histoire
                                                administrative </item>
                                          <item> Histoire -- Histoire moderne -- Histoire
                                                commerciale </item>
                                    </list>
                              </keywords>
                        </textClass>
                  </profileDesc>
                  <revisionDesc>
                        <change type="AutomaticallyEncoded"> 2022-12-20T09:39:21.036710+2:00
                        </change>
                  </revisionDesc>
            </teiHeader>
            <text>
                  <body>
                        <entry n="154" type="P" xml:id="Passeport">
                              <form type="lemma">
                                    <orth> Passeport </orth>
                              </form>
                              <sense>
                                    <def> Les passeports sont délivrés pour autoriser la circulation
                     de certaines personnes ou de certains biens. Une
                     instruction rédigée en 1747 à
                                          l’Hôtel des <ref target="#Fermes"> Fermes</ref> assure
                                          que ces derniers « font le même effet pour le Fermier que
                                          le payement des droits ». Au sens strict, un passeport est
                                          un « passeport du roi » signé par un secrétaire d’État
                                          puis visé par le contrôleur général (bail Legendre, art.
                                          57 ; ord. fév. 1687, tit. 8,
                                          art. 5 et 8 ; bail Forceville, art. 393). Cet acte de
                                          souveraineté est décliné par la Ferme en divers
                                          documents : « ordres de la Compagnie » s’ils émanent des
                                          services centraux ou « passavants » s’ils sont expédiés
                                          par les bureaux particuliers. Véritables privilèges
                                          libérant les marchandises des barrières douanières, les
                                          passeports pouvaient être porteurs de deux ordres
                                          distincts dont le cumul, le cas échéant, devait toujours
                                          être précisé.<orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Bretagne"> Bretagne</ref></orgName>, adresse une lettre au Contrôleur général pour
                                          obtenir l’autorisation d’exporter 60 tonneaux de blé vers
                                          l’<ref target="#Espagne"> Espagne</ref> (AN G7 1171). Le blé étant une
                                          marchandise défendue à l’exportation, le défaut de
                                          passeport expose le marchand à une amende de 500 livres et
                                          à la confiscation de sa cargaison (bail Forceville, art.
                                          390). De même, certaines marchandises sont réputées de
                                                <ref target="#contrebande"> contrebande</ref> si
                                          elles ne sont pas accompagnées de passeport, exposant
                                          ainsi le criminel à des peines exemplaires. Les passeports
                                          peuvent également porter exemption des droits. De tels
                                          types de passeport sont souvent accordés pour les
                                          marchandises destinées « au service du roi » (bail
                                          Forceville, art. 394), pour le bon fonctionnement de la
                                          régie des fermes (bail. Fauconnet, art. 34), ou encore
                                          pour l’usage domestique des ambassadeurs et des princes
                                          étrangers (bail Forceville, art. 400). L’exemption portait
                                          sur la totalité des droits locaux prélevés par les villes,
                                          communautés et seigneuries tels que les octrois, péages et
                                          pontonnages. En matière de <ref target="#traites"> traites
                                         </ref>, la délivrance de ces passeports par la royauté
                                          suppose une indemnisation des pertes supportées par la
                                          Ferme générale. Le Trésor royal compense ainsi le montant
                                          des droits non perçus lors du passage des marchandises
                                          (bail Legendre, art. 136 ; bail Forceville, art. 589). Ces
                                          exemptions sont nombreuses et constituent un volume
                                          financier considérable. À titre d’exemple, l’arrêt du
                                          Conseil du 24 avril 1742
                                          accorde une indemnité de 502 020 livres  « pour
                                                le montant des Droits sur les Marchandises autres
                                                effets mentionnés aux Passeports expédiés par ordre
                                                du Roi pendant la seconde année du Bail dudit
                                                Forceville ». L’indemnisation est calculée
                                          grâce aux passeports qui sont collectés au sein du
                                          « bureau des passeports » de l’Hôtel des <ref target="#Fermes"> Fermes</ref> avant d’être soumis
                                          au Conseil du roi.<ref target="#contrôle"> contrôle
                                         </ref>, ou <ref target="#visiteur"> visiteur</ref>,
                                          vérifie les marchandises et en cas de conformité,
                                          conjointement avec le <ref target="#receveur"> receveur
                                         </ref>, il « liquide » les droits au dos du document qui
                                          prend dès lors l’effet d’un titre de créance. La
                                          liquidation, réalisée à chaque bureau traversé, doit
                                          mentionner la quotité des droits de chaque espèce de
                                          marchandise ainsi que le tarif qui les frappe. Après avoir
                                          calculé le montant total des droits, les commis datent et
                                          signent le document. À la suite de ce premier certificat,
                                          toujours sur le même passeport, les commis reçoivent le
                                          certificat de non-paiement du conducteur qu’il date et
                                          signe à son tour. Si ce dernier ne sait pas signer, les
                                          commis ajoutent au premier certificat : « Et sous le
                                          serment que nous avons en Justice, certifions en outre que
                                          ledit conducteur n’a payé aucuns des droits cy-dessus, n’a
                                          pû en donner sa reconnoissance ne sçachant signer »
                                          (instruction, 1747). Le
                                          passeport est ensuite retenu par les commis qui délivrent
                                          au conducteur un  « passavant en exemption des droits ». Ce
                                                <ref target="#passavant"> passavant</ref>, qui
                                          « tient lieu de passeport », est présenté au second bureau
                                          de la route et, après avoir répété les procédures de
                                          vérification, de liquidation et de certification, les
                                          commis le retiennent pour en délivrer un nouveau. Ce
                                          schéma se répète dans tous les bureaux où les droits sont
                                          dus. Si la cargaison traverse un bureau dans lequel elle
                                          n’est redevable d’aucun droit, il suffit au commis,
                                          « après avoir vérifié en gros », de viser le document.
                                          Arrivée à destination, la cargaison est une dernière fois
                                          vérifiée, les droits liquidés et l’exonération certifiée
                                          par le voiturier. Tous ces « passeports », depuis le
                                          passeport du roi original jusqu’au dernier passavant
                                          d’exemption, sont affiliés les uns aux autres par un même
                                          numéro, facilitant ainsi leur regroupement mensuel au
                                          bureau des passeports de l’Hôtel des <ref target="#Fermes"> Fermes</ref>.<ref target="#étrangères">réputées étrangères</ref>,
                                          l’expédition est faite par <ref target="#acquit-à-caution"> acquit-à-caution</ref> pour la douane de Paris.
                                          Ce n’est qu’au terme du transport des marchandises que les
                                          droits sont liquidés et l’exonération certifiée. À
                                          l’inverse, si les biens sont exportés, la douane de Paris
                                          délivre un passavant sur lequel les droits sont liquidés
                                          au premier bureau de la route qui retient alors le
                                          document pour en délivrer un nouveau, et ainsi de suite
                                          jusqu’à la sortie du territoire.Pour le Contrôleur
                                          général, les termes du passeport « font loi »
                                          (instruction, septembre 1763).
                                          En principe, les commis sont de simples exécutants qui ne
                                          peuvent en aucun cas interpréter la législation royale.
                                          Par exemple, si l’échéance du passeport est périmée ne
                                          serait-ce « que d’un jour », le document perd son effet et
                                          les droits, jusqu’ici exemptés, doivent être payés. Toute
                                          infraction doit être justifiée. Si les marchandises
                                                sont transportées par mer et qu’il arrive un
                                                accident, le conducteur doit rapporter les causes du
                                                retard de livraison sur un procès-verbal qui, après
                                                avoir été enregistré par l’amirauté du port
                                                d’arrivée, est déposé au bureau des fermes pour
                                                prolonger la validité du passeport (règlement du 21
                                                février 1770, art.1). La quantité et la qualité des marchandises
                                          doivent être scrupuleusement respectées. Si un passeport
                                          est délivré pour une certaine quantité, tout surplus
                                          serait redevable des droits et mentionné sur le nouveau
                                          passavant. Si le commis découvre une qualité dont la
                                          valeur est supérieure à celle énoncée, il peut dresser un
                                          procès-verbal de saisie. Les conséquences sont identiques
                                          si la marchandise ne suit pas l’itinéraire prescrit.

                                        <figure>
                                            <graphic url="/static/images/passeport_img_1.jpg"/>
                                            <head>Modèle de passavant d’exemption délivré en conséquence
                                          d’un passeport. AN G179, dossier 18 </head>
                                        </figure><figure>
                                            <graphic url="/static/images/passeport_img_2.jpg"/>
                                            <head/>
                                        </figure></def>

                                    <listBibl><bibl><idno type="ArchivalIdentifier">AN G1 79,
                                                  dossier 18 : « Instruction de ce qui doit être
                                                  observé dans les Bureaux par rapport aux
                                                  Marchandises qui y passent en exemption des
                                                  Droits, en vertu de Passeports pour le Service du
                                                  Roy », mars 1747  id. :« Instruction concernant
                                                  la Régie qui doit être suivie pour les
                                                  marchandises effets, qui sont expédiés par
                                                  Passeports du Roi, Ordres du Ministre, ou de la
                                                  Compagnie en conséquence », septembre 1763</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN G7 1171 :
                                                  Passeports</idno>
                                          </bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil du 24 avril 1742,
                                                in Table des edicts, declarations, arrests et
                                                reglemens, rendus pendant la quatrième année du Bail
                                              de Me Jacques Forceville</hi>, t. 10, Chez Pierre Prault,
                                                1768, p. 41-42</bibl>
                                          <bibl type="sources"><hi rend="italic">Bail des gabelles de France, entrées
                                                sorties du Royaume, Doüane de Lyon Valence, Patente
                                                de Languedoc, Convoy Comptablie de Bourdeaux,
                                                Entrées de Paris Roüen, Aydes de France, Fret,
                                              autres Fermes Royales unies</hi></bibl>
                                          <bibl type="sources"><hi rend="italic">Fait à Maistre François le Gendre,
                                                Bourgeois de Paris, pour six années, commencées au
                                              premier Octobre 1668</hi>. Chez Frederic Leonard, 1680</bibl>
                                          <bibl type="sources"><hi rend="italic">Édit de décembre 1781 fixant les
                                                privilèges des sujets des états du corps Helvétique
                                              dans le royaume</hi>, in Magnien (V.), <hi rend="italic">Recueil
                                                alphabétique des droits de traites uniformes, de
                                                ceux d’entrées et de sortie des cinq grosses fermes,
                                              de douane de Lyon et de Valence</hi>, c., t. 3, s.l.,
                                                1786, p. 340</bibl>
                                          <bibl type="sources">Guyoy (J.-N.), <hi rend="italic">Répertoire universel
                                                et raisonné de jurisprudence civile, criminelle,
                                              canonique et bénéficiale</hi>, t. 44, Chez Panckoucke et
                                                Dupuis, 1781, v° « Passe-port », p. 481-485</bibl>
                                          <bibl type="sources">Rousselot de Surgy (J.-P.),
                                              <hi rend="italic">Encyclopédie méthodique. Finances</hi>. T. 3, Chez
                                                Panckouke, 1787, v° « passeport », p. 300-304</bibl>
                                        <bibl>A. Sée, <hi rend="italic">Le passeport en France</hi>, Chartres, Edmond
                                                Garnier, 1907, p. 23-33</bibl>
                                          <bibl>Lucien Bély, <hi rend="italic">Espions et ambassadeurs au temps de
                                              Louis XIV</hi>, Fayard, 1990, p. 610-653</bibl>
                                          <bibl>Jean Clinquart, <hi rend="italic">Les services extérieurs de la Ferme
                                                générale à la fin de l’Ancien Régime. L’exemple de
                                              la direction des fermes du Hainaut</hi>, CHEFF, 1995, p.
                                                80 et 155</bibl>
                                          <bibl>Daniel Nordman, « Sauf-conduits et
                                                Passeports », <hi rend="italic">Dictionnaire de l’Ancien Régime :
                                                  Royaume de France XVIe – XVIIIe siècle</hi>, dir. L.
                                                Bély, Paris, PUF, 1996, p. 1122-1124</bibl>
                                          <bibl>Jean Clinquart, « Le dédouanement des marchandises
                                                sous l’Ancien Régime », <hi rend="italic">La circulation des
                                                  marchandises dans la France de l’Ancien Régime</hi>, dir.
                                                Woronoff (D.), CHEFF, 1998, p. 103-144</bibl>
                                          <bibl>Gérard Noiriel, « Surveiller les déplacements ou
                                                identifier les personnes ? Contribution à l’histoire
                                                du passeport en France de la Ière à la IIIème
                                                République », in <hi rend="italic">Genèses</hi>, 1998/30, Émigrés,
                                                vagabonds, passeports, dir. Leroy (J.), p. 77-100</bibl>
                                          <bibl>Corinne Thépaut-Cabasset, « Garde-robe de souverain
                                                et réseau international : l’exemple de la Bavière
                                                dans les années 1680 », <hi rend="italic">Se vêtir à la cour en Europe
                                                1400-1815 : Cultures matérielles, cultures visuelles
                                                  du costume dans les cours européennes</hi>, dir. I.
                                                Paresys et N. Coquery, Villeneuve d’Ascq,
                                                Publications de l’Irhis, 2011, p. 177-193</bibl>
                                          <bibl>Cédric Glineur, « Le régime juridique des
                                                passeports sous l’Ancien Régime, L’exemple des
                                                provinces du Nord », dans <hi rend="italic">Guerre, frontière,
                                                barrière et paix en Flandre</hi>, dir. Rickebusch (O) et
                                                R. Opsommer (R.), Ypres, 2014, p. 59-73</bibl>
                                    </listBibl></sense>
                        </entry>
                  </body>
            </text>
      </TEI>
      