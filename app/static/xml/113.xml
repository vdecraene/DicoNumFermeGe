<?xml version='1.0' encoding='UTF-8'?>
<TEI n="113" xmlns="http://www.tei-c.org/ns/1.0" version="3.3.0">
            <teiHeader>
                  <fileDesc>
                        <titleStmt>
                              <title type="notice"> Sète </title>
                              <author>Marie-Laure Legay</author>
                        </titleStmt>
                        <publicationStmt>
                              <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer
                                    le privilège la Ferme générale dans l'espace français et
                                    européen (1664-1794)Axe 1 : Dictionnaire de la Ferme générale,
                                    objet d'histoire totale </publisher>
                              <pubPlace>
                                    <address>
                                          <addrLine> Maison Européenne des Sciences de l'Homme et de
                                                la Société </addrLine>
                                          <addrLine> 2 rue des cannoniers </addrLine>
                                          <addrLine> 59002 Lille Cedex </addrLine>
                                    </address>
                                    2021-2025
                              </pubPlace>
                              <availability>
                                    <licence>Creative Commons Attribution 3.0 non
                                          transposé (CC BY 3.0)</licence>
                              </availability>
                        </publicationStmt>
                        <seriesStmt>
                              <title> Dictionnaire numérique de la Ferme générale </title>
                              <respStmt>
                                    <resp> Coordinateurs de l'axe : Dictionnaire numérique de la
                                          Ferme générale, objet d'histoire totale. </resp>
                                    <persName> Marie-Laure Legay </persName>
                                    <persName> Thomas Boullu </persName>
                              </respStmt>
                        </seriesStmt>
                        <sourceDesc><bibl><idno type="ArchivalIdentifier">AN G1 15,
                                                  dossier 15</idno>
                                          </bibl>
                                          <bibl type="sources"><hi rend="italic">Délibération du 28 août 1760 sur les
                                              brigades de Sète</hi></bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du 15 juin 1779 pour la
                                              concession des étangs salins</hi></bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui réduit à
                                                six livres le minot de sel à Cette, pour être
                                              employé au salage du poisson</hi>, 15 mai 1714</bibl>
                                          <bibl type="sources"><hi rend="italic">Lettres patentes du Roy pour
                                                permettre aux négocians de Languedoc de faire le
                                              commerce de Guinée</hi>, données à Paris au mois de
                                                janvier 1719</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui permet,
                                                à commencer du 1er janvier 1729, l'entrée par le
                                                port de Cette des drogueries et épiceries qui
                                              viennent du Nord</hi>, 25 octobre 1728</bibl>
                                          <bibl type="sources"><hi rend="italic">Carte Topographique des Salins
                                              Privilégiés de Sette</hi>, 1780</bibl>
                                          <bibl> Louis Dermigny, <hi rend="italic">Naissance et croissance d’un port,
                                              Sète de 1666 à 1880</hi>, Montpellier, 1955 </bibl>
                                    </sourceDesc></fileDesc>
                  <encodingDesc>
                        <projectDesc source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
                              <p> Le projet FermGé vise à étudier l’impact d’une organisation
                                    fiscale (1664-1794), discriminante mais rationnelle, sur les
                                    territoires et les sociétés de la France moderne/p </p>
                        </projectDesc>
                  </encodingDesc>
                  <profileDesc>
                        <textClass>
                              <keywords scheme="#fr_RAMEAU">
                                    <list>
                                          <item> Histoire -- Histoire moderne -- Histoire
                                                commerciale </item>
                                          <item> Histoire -- Histoire moderne -- Histoire fiscale </item>
                                          <item> Histoire -- Histoire moderne -- Histoire financière
                                          </item>
                                    </list>
                              </keywords>
                        </textClass>
                  </profileDesc>
                  <revisionDesc>
                        <change type="AutomaticallyEncoded"> 2022-12-20T15:16:32.529433+2:00
                        </change>
                  </revisionDesc>
            </teiHeader>
            <text>
                  <body>
                        <entry n="113" type="S" xml:id="Sete">
                              <form type="lemma">
                                    <orth> Sète </orth>
                              </form>
                              <sense>
                                    <def> Port du <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Languedoc"> Languedoc</ref>
                                         </orgName> créé par Colbert en 1666, dans le cadre des grands aménagements de la côte
                                          et de la construction du canal du Midi. Les débuts
                                          commerciaux de ce site furent difficiles. Ensablé, le port
                                          fit l’objet d’une convention entre les commissaires du roi
                                          et les <ref target="#provinciaux">Etats provinciaux
                                         </ref> du <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Languedoc"> Languedoc</ref>
                                         </orgName> (13 décembre 1690)
                                          pour être mis à flot. Il fut doté d’une amirauté en 1691 et à partir de cette date,
                                          les autorités tant locales que provinciales défendirent
                                          ses prérogatives. Il obtint le <ref target="#privilège">
                                                privilège</ref> d’armement pour les îles
                                          américaines (patentes d’avril 1717) et celui d’armement pour l’Afrique (patentes de
                                                1719), comme <ref target="#Rouen"> Rouen</ref>, <ref target="#Rochelle">La Rochelle</ref>, <ref target="#Bordeaux"> Bordeaux</ref> et <ref target="#Nantes"> Nantes</ref>. Les capitaines qui
                                          se livraient au commerce triangulaire depuis Sète
                                          faisaient donc leur déclaration à l’Amirauté d’une part,
                                          et au bureau des Fermes de cette même ville. Les
                                          négociants du <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Languedoc"> Languedoc</ref>
                                         </orgName> qui faisaient le commerce en droiture des côtes
                                          de Guinée vers Sète étaient exempts de la moitié de tout
                                          droit d’entrée des Fermes royales et locales. De même, le
                                                <ref target="#sucre"> sucre</ref> apporté des îles
                                          d’Amérique vers Sète, dès lors qu’il provenait de la <ref target="#noirs">vente des noirs</ref> de Guinée, était
                                          déchargé de la moitié des droits d’entrée. Les négociants
                                          devaient se munir de certificats des commis du Domaine d’<ref target="#Occident">Occident</ref>. Les
                                          droits de sortie pour toute marchandise à destination de
                                          la Guinée étaient entièrement remis par la Ferme, sur
                                          <ref target="#caution">acquits à caution</ref> dûment
                                          enregistrés et contrôlés. Sète devint donc un port
                                          d’entrepôts que les commis des Fermes durent surveiller.
                                          Les sinuosités de la côte offraient des abris nombreux
                                          pour la <ref target="#contrebande"> contrebande</ref>.
                                          Elles nécessitèrent l’emploi de nombreux <ref target="#brigadiers"> brigadiers</ref>. En 1760, la Ferme générale
                                          supprima la <ref target="#brigade"> brigade</ref>
                                          sédentaire de Sète et créa trois <ref target="#brigades">
                                                brigades</ref> ambulantes de douze hommes chacune.
                                          En outre, Sète obtint d’être port d’entrée pour les
                                          épiceries et <ref target="#drogueries"> drogueries</ref>
                                          venant du Nord, ce qui lui permit d’échanger <ref target="#vins"> vins</ref> et <ref target="#eaux-de-vie"> eaux-de-vie</ref> (1728) plus facilement.
                                          Vis-à-vis des draps en revanche, le port souffrit de la
                                          concurrence de <ref target="#Marseille"> Marseille</ref>.<ref target="#minot"> minot</ref> de sel destiné au
                                          salage du poisson. Ce <ref target="#privilège"> privilège
                                         </ref> fut accordé pour lutter contre la concurrence des
                                          Catalans qui pratiquaient comme les Sétois la pêche à la
                                          sardine avec un coût de revient du salage beaucoup plus
                                          bas. Cette concurrence avec l’<ref target="#Espagne">
                                                Espagne</ref> fut également à l’origine de
                                          l’exploitation des étangs salins concédés à une compagnie
                                          de négociants par l’arrêt du 15 juin 1779 : Jean-Louis François David Serène,
                                          François Castillon et François-Martin Rey prirent
                                          possession de la plage située à l’ouest de la montagne de
                                          Sète et jusqu’à la pointe d’Agde. L’objectif de
                                          l’entreprise était de produire du sel non pour la Ferme
                                          générale mais pour l’exportation, vers l’Italie notamment.
                                          Le sel produit devait néanmoins être vérifié par les
                                          commis de la Ferme, être ensaqué et plombé, placé dans des
                                          gabarres pour rejoindre le port. De même, la Ferme
                                          générale faisait la garde de ces étangs. En 1786, elle fut rémunérée plus
                                          de 20 000 livres pour cette surveillance annuelle. </def>
                                    <listBibl><bibl><idno type="ArchivalIdentifier">AN G1 15,
                                                  dossier 15</idno>
                                          </bibl>
                                          <bibl type="sources"><hi rend="italic">Délibération du 28 août 1760 sur les
                                              brigades de Sète</hi></bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du 15 juin 1779 pour la
                                              concession des étangs salins</hi></bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui réduit à
                                                six livres le minot de sel à Cette, pour être
                                              employé au salage du poisson</hi>, 15 mai 1714</bibl>
                                          <bibl type="sources"><hi rend="italic">Lettres patentes du Roy pour
                                                permettre aux négocians de Languedoc de faire le
                                              commerce de Guinée</hi>, données à Paris au mois de
                                                janvier 1719</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui permet,
                                                à commencer du 1er janvier 1729, l'entrée par le
                                                port de Cette des drogueries et épiceries qui
                                              viennent du Nord</hi>, 25 octobre 1728</bibl>
                                          <bibl type="sources"><hi rend="italic">Carte Topographique des Salins
                                              Privilégiés de Sette</hi>, 1780</bibl>
                                          <bibl> Louis Dermigny, <hi rend="italic">Naissance et croissance d’un port,
                                              Sète de 1666 à 1880</hi>, Montpellier, 1955 </bibl>
                                    </listBibl></sense>
                        </entry>
                  </body>
            </text>
      </TEI>
      