<?xml version='1.0' encoding='UTF-8'?>
<TEI n="39" xmlns="http://www.tei-c.org/ns/1.0" version="3.3.0">
            <teiHeader>
                  <fileDesc>
                        <titleStmt>
                              <title type="notice"> Privilèges </title>
                              <author>Marie-Laure Legay</author>
                        </titleStmt>
                        <publicationStmt>
                              <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer
                                    le privilège la Ferme générale dans l'espace français et
                                    européen (1664-1794)Axe 1 : Dictionnaire de la Ferme générale,
                                    objet d'histoire totale </publisher>
                              <pubPlace>
                                    <address>
                                          <addrLine> Maison Européenne des Sciences de l'Homme et de
                                                la Société </addrLine>
                                          <addrLine> 2 rue des cannoniers </addrLine>
                                          <addrLine> 59002 Lille Cedex </addrLine>
                                    </address>
                                    2021-2025
                              </pubPlace>
                              <availability>
                                    <licence>Creative Commons Attribution 3.0 non
                                          transposé (CC BY 3.0)</licence>
                              </availability>
                        </publicationStmt>
                        <seriesStmt>
                              <title> Dictionnaire numérique de la Ferme générale </title>
                              <respStmt>
                                    <resp> Coordinateurs de l'axe : Dictionnaire numérique de la
                                          Ferme générale, objet d'histoire totale. </resp>
                                    <persName> Marie-Laure Legay </persName>
                                    <persName> Thomas Boullu </persName>
                              </respStmt>
                        </seriesStmt>
                        <sourceDesc><bibl>Denis Alland, Stéphane Rials (dir.), <hi rend="italic">Dictionnaire
                                              de la culture juridique</hi>, Paris, PUF, 2003,
                                                « privilège », p. 1209-1212</bibl>
                                        <bibl>Jochen Hoock, « Libertés et privilèges dans le discours économique et commercial du XVIIIe siècle » dans G. Garner (dir.), <hi rend="italic">Die Ökonomie des Privilegs, Westeuropa 16.-19. Jahrhundert</hi>, Francfort-sur-le-Main, Klostermann, 2016, p. 349-364</bibl>
                                          <bibl>Marie-Laure Legay, « L’arbitraire fiscal en France
                                                au XVIIIe siècle : acteurs, discours et réalités de
                                                gestion », dans Benjamin Deruelle, Michel Hébert
                                                (dir.), <hi rend="italic">Arbitraire et arbitrage. Les zones grises du
                                                  pouvoir XIIe-XVIIIe siècles</hi>, Villeneuve d’Ascq,
                                                Presses universitaires du Septentrion, 2024</bibl>
                                    </sourceDesc></fileDesc>
                  <encodingDesc>
                        <projectDesc source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
                              <p> Le projet FermGé vise à étudier l’impact d’une organisation
                                    fiscale (1664-1794), discriminante mais rationnelle, sur les
                                    territoires et les sociétés de la France moderne/p </p>
                        </projectDesc>
                  </encodingDesc>
                  <profileDesc>
                        <textClass>
                              <keywords scheme="#fr_RAMEAU">
                                    <list>
                                          <item> Histoire -- Histoire moderne -- Histoire politique
                                          </item>
                                    </list>
                              </keywords>
                        </textClass>
                  </profileDesc>
                  <revisionDesc>
                        <change type="AutomaticallyEncoded"> 2022-12-20T10:20:41.785193+2:00
                        </change>
                  </revisionDesc>
            </teiHeader>
            <text>
                  <body>
                        <entry n="39" type="P" xml:id="Privilèges">
                              <form type="lemma">
                                    <orth> Privilèges </orth>
                              </form>
                              <sense>
                                    <def> La société d’Ancien régime avait une structure
                     juridiquement inégalitaire. Chacun était privilégié dans
                     le sens où chacun appartenait à une communauté
                     particulière à laquelle étaient attachés des droits
                     singuliers. Les privilèges furent d’abord reconnus comme
                     lois privées. Toutefois, la place du privilège dans les
                     ordonnancements juridiques évolua et le privilège fut
                     placé au rang de mesure d’administration ou de
                     gouvernement. Identifié comme gardien de la chose publique
                     à partir du XIIIe siècle, le souverain utilisa le
                     privilège comme moyen de conduire ses sujets : il
                     concédait des droits particuliers, ce qui définit le
                     privilège non plus comme loi privée mais comme exception
                     au droit commun. Apparut dès lors dans les ordonnances
                     générales une clause dérogatoire : « nonobstant tous
                     privilèges à ce contraire ». Le titre commun pour les
                     Fermes de l’<ref target="#ordonnance"> ordonnance</ref>
                                          de juillet 1681 établissait par
                                          exemple, article 2, « que Sa Majesté n’entend point
                                          préjudicier aux privilèges exemptions de droits dont les
                                          villes, bourgs paroisses auroient joui en vertu de lettres
                                          de concessions des Rois ses prédécesseurs ». En doctrine,
                                          l’autorité royale pouvait modifier, voire révoquer le
                                          privilège en fonction des circonstances de temps et de
                                          lieu si elle le jugeait nécessaire, c’est-à-dire utile à
                                          la chose publique. En pratique, elle respectait les
                                          privilèges de nature « contractuelle » qui, à l’instar des
                                          conventions, avait vocation à la perpétuité selon les
                                          civilistes médiévaux. La Ferme générale ne levait donc pas
                                          de droits partout où les corps, seigneuries, villes, <ref target="#états"> états</ref>, disposaient de
                                          titres anciennement accordés qui contenaient le privilège
                                          fiscal, titres le plus souvent renouvelés par lettres de
                                          confirmation. Particulièrement en pays d’<ref target="#Etats"> Etats</ref>, où la règle du
                                          consentement à l’impôt était régulièrement affirmée par
                                          les assemblées, la compagnie financière avait une activité
                                          limitée. « Aucune somme de deniers ne leur puisse estre
                                          imposée si préalablement elle n’a esté demandée aux
                                          Etats », promit François Ier à la <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Bretagne"> Bretagne</ref>
                                         </orgName> en septembre 1532,
                                          par exemple.<ref target="#aides">
                                                aides</ref> pour subvenir aux dépenses de guerre.
                                          Seuls les ecclésiastiques, nobles, officiers des cours
                                          souveraines et officiers commensaux actifs furent
                                          maintenus dans l’exemption de <ref target="#gros">droits de gros</ref> sur les <ref target="#vins"> vins</ref> et denrées. Le
                                          gouvernement entendait par officiers commensaux ceux de
                                          première classe, attachés directement à la personne du
                                          roi, excluant les valets-de-pied, gardes-chasses,
                                          officiers de la vénerie, louveterie, fauconnerie, mais
                                          aussi les Trésoriers de France, officiers des <ref target="#bureau_des_finances">bureaux de
                                            finances</ref> (déclarés sujets aux droits de gros en octobre 1722), commissaires de
                                          guerre, officiers des Invalides, officiers des
                                          Monnaies…qui, bien que confirmés dans les privilèges de
                                          commensaux et à ce titre exemptés de la taille, ne furent
                                          pas reconnus comme exempts des droits d’<ref target="#aides"> aides</ref> « parce qu’ils n’ont
                                          pas livrée, ni bouche à la Cour ». De même, l’édit de
                                          révocation générale d’août 1715
                                          revenait sur les privilèges concédés depuis 1689 à l’occasion des créations
                                          d’offices et ventes de lettres de noblesse pour
                                          financement des guerres. <ref target="#aides">aides</ref>, droits de <ref target="#traites"> traites</ref> ne relevaient pas
                                          d’un même héritage juridique. L’évolution doctrinale
                                          consista à unifier les principes d’action pour tous les
                                          droits, à en affirmer à la fois le caractère public,
                                          général, et arbitraire.<emph>Public:</emph> Dans la contestation qui
                                          opposa les négociants de <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Bretagne"> Bretagne</ref>
                                         </orgName> à la Ferme générale à propos de la réactivation
                                          des droits de « ports et havres » (1725), l’adjudicataire fit valoir l’édit
                                          d’avril 1667 portant que ce qui
                                          avait été administré par les receveurs et fermiers du roi
                                          pendant dix ans était réputé du <ref target="#domaine">
                                                domaine</ref> de la Couronne et ne pouvait être
                                          contesté. Cette maxime reprenait les principes de
                                          l’ordonnance de Moulins de 1566. Elle intégrait les droits au domaine fixe de la
                                          Couronne, c’est-à-dire à l’Etat.<emph>Général:</emph> Remarquable par
                                          sa généralité, l’édit du mois d’août 1717 révoqua l’exemption des droits de <ref target="#traite"> traite</ref> dont bénéficiaient
                                          certains corps ou particuliers. Par cette loi, les
                                          privilèges des gentilshommes et maîtres verriers de <ref target="#Bordeaux"> Bordeaux</ref> furent ainsi
                                          anéantis par exemple. Lors de la création de nouveaux
                                          impôts (sur les <ref target="#cartes"> cartes</ref>, sur
                                          les <ref target="#cuirs"> cuirs</ref>…), le gouvernement
                                          affirmait que le privilège n’était pas opposable à
                                          l’activité de la Ferme dès lors qu’il n’était pas inscrit
                                          dans la loi générale. La <placeName type="lieux_habitations"> ville de Rocroi
                                    </placeName> eut beau faire valoir l’exemption accordée en
                                          août 1717 de toute imposition
                                          pour raison de marchandises, denrées et boissons
                                          consommées à Rocroi pour éviter l’impôt sur les <ref target="#cuirs"> cuirs</ref>, le Conseil du roi
                                          considéra que de tels privilèges ne valaient que pour les
                                          droits imposés lors de leurs concessions. A cet égard, il
                                          fut rappelé qu’il était « de principe en fait
                                          d’administration de commerce qu’on ne peut opposer aucuns
                                          privilèges particuliers à une Loi générale, à moins que la
                                          Loi même n’en fasse l’exception » (1760).<emph>Arbitraire:</emph> les Conseillers d’Etat
                                          s’appuyèrent sur la nature particulière des droits sur les
                                          marchandises et denrées en circulation pour affirmer le
                                          caractère purement exceptionnel, dispensatoire du
                                          privilège. En matière de commerce en effet, le privilège
                                          ne revêtait pas la même force juridique que le <hi rend="italic">privilegium</hi>
                                          concédé à des corps. Simple mesure d’administration, le
                                          privilège commercial était une préférence révocable donnée
                                          à une compagnie, un particulier, un ressort territorial,
                                          comme lorsque, par exemple, Louis XV exemptait les
                                          marchandises de droits d’entrée ou de droits de sortie
                                          pour faciliter le commerce en telle ou telle occasion.
                                          L’arbitrage fréquemment employé consista à affirmer que
                                          les franchises obtenues anciennement en matière
                                          d’« aides » (au sens générique du terme, c’est-à-dire
                                          « don », soutien) pour les corps et communautés ne
                                          pouvaient s’entendre pour les droits des fermes qui
                                          étaient « droit de commerce ». S’engagea donc une lutte
                                          entre les corps privilégiés, certaines villes, certaines
                                          communautés, et le Conseil d’Etat pour imposer cette
                                          doctrine. Le <ref target="#annuel">droit annuel</ref>
                                          sur les boissons par exemple, fut présenté comme un droit
                                          « de commerce » qui devait assujettir tout « vendant
                                          vin », même privilégié. La <placeName type="lieux_habitations"> ville de Blois
                                    </placeName> perdit donc, comme la <placeName type="lieux_habitations"> ville de Grandville
                                    </placeName> (1675),
                                          Cherbourg (1676), Montargis (1681), Vervins (1682)…, contre la Ferme
                                          générale dans la reconnaissance du privilège de ses
                                          marchands à ne pas payer l’<ref target="#annuel"> annuel
                                         </ref>, notamment pour la vente en gros (1718). L’aboutissement de
                                          cette approche fut la royalisation, par l’abbé Terray, des
                                          taxes perçues sur les marchandises : la déclaration du
                                          premier juin 1771 mit le roi en
                                          possession de tous les droits de <ref target="#traites">
                                                traites</ref> et <ref target="#foraines"> foraines
                                         </ref>. Ainsi, le souverain confondait définitivement
                                          dans un même ensemble les droits relevant des privilèges
                                          locaux et taxes royales.<orgName subtype="province" type="pays_et_provinces">
                                                Bretagne</orgName> entrant par la <orgName subtype="province" type="pays_et_provinces">
                                                Normandie</orgName> etoient plus que compensez par
                                          les avantages dont la <orgName subtype="province" type="pays_et_provinces"> Bretagne</orgName>
                                          jouit » (1739). L’équilibre
                                          entre privilèges devait garantir l’ordre général. Le
                                          Conseil fit même du privilège une arme politique pour
                                          amener les corps constitués à collaborer à l’action
                                          gouvernementale. Philibert Orry, contrôleur général de
                                                1730 à
1745, engagea par exemple les <ref target="#provinciaux">Etats provinciaux</ref> à lutter
                                          contre la contrebande, sous menace de limiter leurs
                                          privilèges : « Loin que leur intérêt soit de s’opposer à
                                          la proposition des Fermiers généraux, ils en ont au
                                          contraire un sensible d’y concourir, s’ils veullent
                                          s’assurer de plusieurs avantages et mettre les privilèges
                                          de la province a couvert de toute contraction, qu’ils ne
                                          doivent pas se flatter que le Roÿ soit toujours disposé à
                                          souffrir les préjudices réels et considérables… Vu
                                          l’augmentation excessive de l’abus et de la contrebande,
                                          Sa Majesté prendra des partis dans lesquels leurs
                                          privilèges et leurs interetz ne seront certainement pas
                                          aussi ménagés qu’ils le seront quand on verra qu’ils se
                                          prêteront de bonne grâce aux arrangements raisonnables
                                          qu’ils auront faits de concert avec eux » (1740). Turgot, en 1776, fit accorder au pays de
                                                <ref target="#Gex"> Gex</ref> d’importants
                                          privilèges (suppression des bureaux de traites, exemptions
                                          du monopole de la vente du sel et du monopole de la vente
                                          du tabac), moyennant le règlement d’un abonnement annuel
                                          de 30 000 livres à lever sur les biens-fonds. </def>
                                    <listBibl><bibl>Denis Alland, Stéphane Rials (dir.), <hi rend="italic">Dictionnaire
                                              de la culture juridique</hi>, Paris, PUF, 2003,
                                                « privilège », p. 1209-1212</bibl>
                                        <bibl>Jochen Hoock, « Libertés et privilèges dans le discours économique et commercial du XVIIIe siècle » dans G. Garner (dir.), <hi rend="italic">Die Ökonomie des Privilegs, Westeuropa 16.-19. Jahrhundert</hi>, Francfort-sur-le-Main, Klostermann, 2016, p. 349-364</bibl>
                                          <bibl>Marie-Laure Legay, « L’arbitraire fiscal en France
                                                au XVIIIe siècle : acteurs, discours et réalités de
                                                gestion », dans Benjamin Deruelle, Michel Hébert
                                                (dir.), <hi rend="italic">Arbitraire et arbitrage. Les zones grises du
                                                  pouvoir XIIe-XVIIIe siècles</hi>, Villeneuve d’Ascq,
                                                Presses universitaires du Septentrion, 2024</bibl>
                                    </listBibl></sense>
                        </entry>
                  </body>
            </text>
      </TEI>
      