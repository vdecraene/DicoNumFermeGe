<?xml version='1.0' encoding='UTF-8'?>
<TEI n="177" xmlns="http://www.tei-c.org/ns/1.0" version="3.3.0">
            <teiHeader>
                  <fileDesc>
                        <titleStmt>
                              <title type="notice"> Sel de salpêtre </title>
                              <author>Marie-Laure Legay</author>
                        </titleStmt>
                        <publicationStmt>
                              <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer
                                    le privilège la Ferme générale dans l'espace français et
                                    européen (1664-1794)Axe 1 : Dictionnaire de la Ferme générale,
                                    objet d'histoire totale </publisher>
                              <pubPlace>
                                    <address>
                                          <addrLine> Maison Européenne des Sciences de l'Homme et de
                                                la Société </addrLine>
                                          <addrLine> 2 rue des cannoniers </addrLine>
                                          <addrLine> 59002 Lille Cedex </addrLine>
                                    </address>
                                    2021-2025
                              </pubPlace>
                              <availability>
                                    <licence>Creative Commons Attribution 3.0 non
                                          transposé (CC BY 3.0)</licence>
                              </availability>
                        </publicationStmt>
                        <seriesStmt>
                              <title> Dictionnaire numérique de la Ferme générale </title>
                              <respStmt>
                                    <resp> Coordinateurs de l'axe : Dictionnaire numérique de la
                                          Ferme générale, objet d'histoire totale. </resp>
                                    <persName> Marie-Laure Legay </persName>
                                    <persName> Thomas Boullu </persName>
                              </respStmt>
                        </seriesStmt>
                        <sourceDesc><bibl><idno type="ArchivalIdentifier">AN, G1 91,
                                                  dossier 33  dossier 34  dossier 35</idno> ; </bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du conseil d’Etat du roi du 16
                                                février 1786 qui ordonne la remise à l’Adjudicataire
                                                général des Fermes de tout sel provenant du travail
                                              du Salpêtre</hi></bibl>
                                          <bibl type="sources">Lavoisier, « Expériences sur la
                                                cendre qu’emploient les salpêtriers de Paris et sur
                                                son usage dans la fabrication du salpêtre », <hi rend="italic">Mémoire
                                                  de l’Académie des Sciences</hi>, année 1777, p. 123</bibl>
                                    </sourceDesc></fileDesc>
                  <encodingDesc>
                        <projectDesc source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
                              <p> Le projet FermGé vise à étudier l’impact d’une organisation
                                    fiscale (1664-1794), discriminante mais rationnelle, sur les
                                    territoires et les sociétés de la France moderne/p </p>
                        </projectDesc>
                  </encodingDesc>
                  <profileDesc>
                        <textClass>
                              <keywords scheme="#fr_RAMEAU">
                                    <list>
                                          <item> Histoire -- Histoire moderne -- Histoire fiscale
                                          </item>
                                    </list>
                              </keywords>
                        </textClass>
                  </profileDesc>
                  <revisionDesc>
                        <change type="AutomaticallyEncoded"> 2022-12-20T15:08:26.878556+2:00
                        </change>
                  </revisionDesc>
            </teiHeader>
            <text>
                  <body>
                        <entry n="177" type="S" xml:id="Sel_de_salpêtre">
                              <form type="lemma">
                                    <orth> Sel de salpêtre </orth>
                              </form>
                              <sense>
                                    <def> Il s’agit du sel qui provenait de la fabrication des
                     salpêtres. Chaque quintal de salpêtre devait produire
                     normalement au moins dix-huit livres-poids de sel.
                     L’article 28 du titre 17 de l’<ref target="#ordonnance">
                                                ordonnance</ref> des <ref target="#Gabelles">
                                                Gabelles</ref> de 1680
                                          défendit tout usage et tout commerce de ce sel. Les
                                          salpêtriers avaient l’obligation de le déclarer et de le
                                          livrer au <ref target="#grenier"> grenier</ref> le plus
                                          proche qui en faisait l’achat. On établit le tarif comme
                                          suit : sept sous à Paris, quatre sous pour les salpêtriers
                                          qui travaillent dans les pays de Grandes <ref target="#Gabelles"> Gabelles</ref>, deux sous pour
                                          ceux qui travaillent dans les pays de petites <ref target="#Gabelles"> Gabelles</ref> et un sou six
                                          deniers dans les provinces des <orgName subtype="province" type="pays_et_provinces"> Trois-Evêchés</orgName>,
                                                <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Lorraine"> Lorraine</ref>
                                         </orgName> et <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Franche-Comté"> Franche-Comté</ref>
                                         </orgName> par livre de sel. Les fabricants étaient soumis
                                          aux <ref target="#visites"> visites</ref> des employés de
                                          la Ferme générale. S’ils étaient convaincus d’avoir vendu
                                          ou donné du sel de salpêtre, ils étaient poursuivis comme
                                                <ref target="#faux-sauniers"> faux-sauniers</ref>.
                                          Le sel de salpêtre était demandé par des manufacturiers à
                                          la recherche de sel bon marché. Le sieur de la Belouse en
                                          sollicita pour sa quincaillerie le 9 septembre 1768 ; de même le baron
                                          d’Espulles pour des essais la même année. Les Fermiers
                                          généraux traitaient ces demandes particulières.<ref target="#ordonnance"> ordonnance</ref> de 1680. </def>
                                    <listBibl><bibl><idno type="ArchivalIdentifier">AN, G1 91,
                                                  dossier 33  dossier 34  dossier 35</idno> ; </bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du conseil d’Etat du roi du 16
                                                février 1786 qui ordonne la remise à l’Adjudicataire
                                                général des Fermes de tout sel provenant du travail
                                              du Salpêtre</hi></bibl>
                                          <bibl type="sources">Lavoisier, « Expériences sur la
                                                cendre qu’emploient les salpêtriers de Paris et sur
                                                son usage dans la fabrication du salpêtre », <hi rend="italic">Mémoire
                                                  de l’Académie des Sciences</hi>, année 1777, p. 123</bibl>
                                    </listBibl></sense>
                        </entry>
                  </body>
            </text>
      </TEI>
      