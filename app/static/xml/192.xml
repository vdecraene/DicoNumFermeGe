<?xml version='1.0' encoding='UTF-8'?>
<TEI n="192" xmlns="http://www.tei-c.org/ns/1.0" version="3.3.0">
            <teiHeader>
                  <fileDesc>
                        <titleStmt>
                              <title type="notice"> Passavant (seigneurie de) </title>
                              <author>Marie-Laure Legay</author>
                        </titleStmt>
                        <publicationStmt>
                              <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer
                                    le privilège la Ferme générale dans l'espace français et
                                    européen (1664-1794)Axe 1 : Dictionnaire de la Ferme générale,
                                    objet d'histoire totale </publisher>
                              <pubPlace>
                                    <address>
                                          <addrLine> Maison Européenne des Sciences de l'Homme et de
                                                la Société </addrLine>
                                          <addrLine> 2 rue des cannoniers </addrLine>
                                          <addrLine> 59002 Lille Cedex </addrLine>
                                    </address>
                                    2021-2025
                              </pubPlace>
                              <availability>
                                    <licence>Creative Commons Attribution 3.0 non
                                          transposé (CC BY 3.0)</licence>
                              </availability>
                        </publicationStmt>
                        <seriesStmt>
                              <title> Dictionnaire numérique de la Ferme générale </title>
                              <respStmt>
                                    <resp> Coordinateurs de l'axe : Dictionnaire numérique de la
                                          Ferme générale, objet d'histoire totale. </resp>
                                    <persName> Marie-Laure Legay </persName>
                                    <persName> Thomas Boullu </persName>
                              </respStmt>
                        </seriesStmt>
                        <sourceDesc><bibl type="sources">Arrêt du conseil d’Etat qui
                                                confirme les privilèges et exemptions des habitants
                                                des paroisses de Passavant, Coste de Voge et
                                                Vogecourt, frontières de Lorraine, 5 mai 1733</bibl>
                                          <bibl type="sources">Arrêt du Conseil d’Etat qui fixe la
                                                consommation du vin pour les habitants de Passavant,
                                                Côte-de-Voge et Vogecourt, et autres paroisses
                                                enclavées dans la Lorraine et le comté de Bourgogne,
                                                à raison de six muids par an pour chaque laboureur
                                                et marchand de bois, et trois muids pour chacun
                                                manouvrier, 15 avril 1738</bibl>
                                    </sourceDesc></fileDesc>
                  <encodingDesc>
                        <projectDesc source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
                              <p> Le projet FermGé vise à étudier l’impact d’une organisation
                                    fiscale (1664-1794), discriminante mais rationnelle, sur les
                                    territoires et les sociétés de la France moderne/p </p>
                        </projectDesc>
                  </encodingDesc>
                  <profileDesc>
                        <textClass>
                              <keywords scheme="#fr_RAMEAU">
                                    <list>
                                          <item> Histoire -- Histoire moderne -- Histoire fiscale </item>
                                          <item> Histoire -- Histoire moderne -- Histoire politique
                                          </item>
                                    </list>
                              </keywords>
                        </textClass>
                  </profileDesc>
                  <revisionDesc>
                        <change type="AutomaticallyEncoded"> 2022-12-20T09:34:23.490286+2:00
                        </change>
                  </revisionDesc>
            </teiHeader>
            <text>
                  <body>
                        <entry n="192" type="P" xml:id="Passavant_seigneurie_de">
                              <form type="lemma">
                                    <orth> Passavant (seigneurie de) </orth>
                              </form>
                              <sense>
                                    <def> Lorsqu’en prenant la régie des Fermes, Pierre Carlier
                     établit un bureau de <ref target="#traites"> traites
                                         </ref> à Passavant, territoire français enclavé dans la
                                                <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Lorraine"> Lorraine</ref>
                                         </orgName> de toutes parts, il fit immédiatement réagir
                                          les habitants dont les titres anciens établissaient
                                          clairement les <ref target="#privilèges"> privilèges
                                         </ref>, tant vis-à-vis de la <ref target="#gabelle">
                                                gabelle</ref> que des droits indirects. Ces
                                          franchises, concédées en 1562,
                                          furent confirmées à maintes reprises et encore un siècle
                                          plus tard par Louis XIV (1661) pour encourager l’installation des habitants dans cette
                                          région ravagée par la guerre de Trente-Ans. Pour toute
                                          contribution, les habitants ne payaient qu’une modique
                                          redevance au <ref target="#Domaine"> Domaine</ref> de
                                          Chaumont. L’insistance de la Ferme générale à établir ses
                                          bureaux, déjà perceptible sous le bail Charles Cordier,
                                          échoua: Louis XV fit supprimer le bureau de <ref target="#traites"> traites</ref>. En revanche, il
                                          exigea des habitants de Passavant d’aller chercher leur
                                          sel, à prix marchand, au <ref target="#grenier"> grenier
                                         </ref> de Langres, et limita la vente des bouteilles des
                                          maîtres verriers du bourg de Passavant, pour éviter la
                                                <ref target="#fraude"> fraude</ref> venue de
                                          l’étranger. Dans le sens de la sortie, la Ferme générale
                                          revint à la charge. Elle jugea que, sous prétexte de leur
                                          consommation, les habitants tiraient des fortes quantités
                                          de <ref target="#vin"> vin</ref> qu’ils faisaient passer
                                          à l’étranger sans payer les droits de sortie,  « ce qui
                                          porte à la Ferme un préjudice d’autant plus grand que la
                                          situation des lieux qui sont moitié France moitié <orgName subtype="province" type="pays_et_provinces">
                                                Lorraine</orgName>, les met à l’abri des plus
                                          exacts recherches ». De fait, la consommation fut limitée
                                          à partir de 1738. </def>
                                    <listBibl><bibl type="sources">Arrêt du conseil d’Etat qui
                                                confirme les privilèges et exemptions des habitants
                                                des paroisses de Passavant, Coste de Voge et
                                                Vogecourt, frontières de Lorraine, 5 mai 1733</bibl>
                                          <bibl type="sources">Arrêt du Conseil d’Etat qui fixe la
                                                consommation du vin pour les habitants de Passavant,
                                                Côte-de-Voge et Vogecourt, et autres paroisses
                                                enclavées dans la Lorraine et le comté de Bourgogne,
                                                à raison de six muids par an pour chaque laboureur
                                                et marchand de bois, et trois muids pour chacun
                                                manouvrier, 15 avril 1738</bibl>
                                    </listBibl></sense>
                        </entry>
                  </body>
            </text>
      </TEI>
      