<?xml version='1.0' encoding='UTF-8'?>
<TEI n="233" xmlns="http://www.tei-c.org/ns/1.0" version="3.3.0">
            <teiHeader>
                  <fileDesc>
                        <titleStmt>
                              <title type="notice">Arc-et-Senans</title>
                            <author>Paul Delsalle</author>
                        </titleStmt>
                        <publicationStmt>
                              <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer
                                    le privilège la Ferme générale dans l'espace français et
                                    européen (1664-1794)Axe 1 : Dictionnaire de la Ferme générale,
                                    objet d'histoire totale </publisher>
                              <pubPlace>
                                    <address>
                                          <addrLine> Maison Européenne des Sciences de l'Homme et de
                                                la Société </addrLine>
                                          <addrLine> 2 rue des cannoniers </addrLine>
                                          <addrLine> 59002 Lille Cedex </addrLine>
                                    </address>
                                    2021-2025
                              </pubPlace>
                              <availability>
                                    <licence>Creative Commons Attribution 3.0 non
                                          transposé (CC BY 3.0)</licence>
                              </availability>
                        </publicationStmt>
                        <seriesStmt>
                              <title> Dictionnaire numérique de la Ferme générale </title>
                              <respStmt>
                                    <resp> Coordinateurs de l'axe : Dictionnaire numérique de la
                                          Ferme générale, objet d'histoire totale. </resp>
                                    <persName> Marie-Laure Legay </persName>
                                    <persName> Thomas Boullu </persName>
                              </respStmt>
                        </seriesStmt>
                        <sourceDesc><bibl><idno type="ArchivalIdentifier">AN, F14 4267, «
                                                  Détails historiques sur les salines de Lorraine
                                                  des trois Evêchés et de Franche-Comté dont le
                                                  principal objet est d’exposer les variations que
                                                  ces mines ont successivement éprouvées dans leur
                                                  administration », après 1782</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN, G1 93 (Salines
                                                  de Salins dont traité du 12 mars 1774), 95
                                                  (dossiers. 13 et 15)</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">G7 280, 281</idno>
                                          </bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui annulle
                                                tous traités faits à des particuliers, pour
                                                fourniture de Salin et potasse, par Jean Roux
                                                Montclar, chargé précédemment de l'exploitation des
                                                salines de Lorraine et Franche-Comté, sans qu'il
                                                puisse être exigé aucune indemnité de
                                              l'adjudicataire des fermes générales</hi>, 19 juillet
                                                1782</bibl>
                                          <bibl>Thierry Claeys, <hi rend="italic">Dictionnaire biographique des
                                              financiers en France au XVIIIe siècle</hi>, Paris,
                                                éditions SPM, 2011, t. 1, notice Haudry, p.
                                                1087-1089  t. 2, notices Parseval, p. 1863-1867</bibl>
                                          <bibl>Roger Humbert, <hi rend="italic">Institutions et gens de finances en
                                              Franche-Comté, 1674-1790</hi>, Besançon, Annales
                                                littéraires de l’Université de Franche-Comté, n°
                                                605, Diffusion Les Belles-Lettres, 1996</bibl>
                                          <bibl>Philippe Mairot, <hi rend="italic">Salins-les-Bains, Arc-et-Senans,
                                              fortunes du sel comtois</hi>, Lyon, Le Progrès, 2010</bibl>
                                          <bibl>Emmeline Scachetti, « La saline d’Arc-et-Senans,
                                                Manufacture, utopie et patrimoine (1773-2011) »,
                                                thèse d’histoire contemporaine sous la direction de
                                                Jean-Claude Daumas, Université de Franche-Comté, 23
                                                novembre 2013</bibl>
                                    </sourceDesc></fileDesc>
                  <encodingDesc>
                        <projectDesc source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
                              <p> Le projet FermGé vise à étudier l’impact d’une organisation
                                    fiscale (1664-1794), discriminante mais rationnelle, sur les
                                    territoires et les sociétés de la France moderne/p </p>
                        </projectDesc>
                  </encodingDesc>
                  <profileDesc>
                        <textClass>
                              <keywords scheme="#fr_RAMEAU">
                                    <list>
                                          <item>Histoire -- Histoire moderne -- Histoire
                                                économique</item>
                                          <item>Histoire -- Histoire moderne -- Histoire
                                                financière</item>
                                    </list>
                              </keywords>
                        </textClass>
                  </profileDesc>
                  <revisionDesc>
                        <change type="AutomaticallyEncoded"> 2022-12-12T13:45:58.565289+2:00
                        </change>
                  </revisionDesc>
            </teiHeader>
            <text>
                  <body>
                        <entry n="233" type="A" xml:id="Arc_et_Senans">
                              <form type="lemma">
                                    <orth> Arc-et-Senans </orth>
                              </form>
                              <sense>
                                    <def> Arc-et-Senans est le nom actuel du village où
                     se trouvait l’usine de Chaux, une dépendance de <ref target="#Salins"> Salins</ref>. Les salines de
                                          Salins utilisaient beaucoup de bois pour chauffer l’eau
                                          salée et confectionner des pains de sel. Au milieu du
                                          XVIIIe siècle, le prix du transport du bois était devenu
                                          considérable. Afin de réduire le prix de revient du sel,
                                          il fut décidé par arrêt du 29 avril
1773, de construire une nouvelle usine à
                                          proximité immédiate de l’immense forêt de Chaux (20 000
                                          hectares), située entre le <placeName type="lieux_habitations"> village nommé Arc
                                    </placeName> et le <placeName type="lieux_habitations">
                                                village de Senans</placeName>. Un saumoduc long de
                                          plus de vingt et un kilomètres, constitué par 15 000
                                          troncs de sapin percés à la tarière, fut réalisé pour
                                          conduire la muire (eau salée) en pente douce jusqu’à la
                                          nouvelle usine. On espérait notamment tirer davantage
                                          profit des « petites eaux » de Salins, c’est-à-dire de la
                                          muire faiblement chargée en salure et « rejetées du
                                          service faute de bois pour les travailler et réduire en
                                          sel ». Cette nouvelle usine devait non seulement dynamiser
                                          la production de Salins, mais aussi permettre d’honorer
                                          plus facilement les traités de livraison de sel passés
                                          avec les <ref target="#suisses">cantons suisses</ref>.
                                          Chaux, simple dépendance de Salins, était donc une
                                          entreprise d’Etat.A l’initiative du projet, la Ferme
                                          générale ne s’engagea pas directement dans cette aventure
                                          industrielle, mais opta pour l’établissement d’une régie
                                          dans laquelle elle prit des intérêts. Par un traité
                                          particulier en date du 12 mars 1774, elle en confia l’exécution à une compagnie
                                          distincte qui devait la mener à son terme. Sous le prête-nom de Jean Roux-Monclar, neuf
                                                associés appartenant à la haute finance parisienne,
                                                parmi lesquels Alexandre Parseval Deschênes et son
                                                fils Marc Antoine, Philibert Parseval de Fontaine,
                                                financiers très proches de la Ferme générale, deux
                                                directeurs des Fermes à Paris, Pierre-Martin
                                                Dewismes et Pierre Royer, un receveur général des
                                                finances…, financèrent l’investissement de l’usine
                                                en échange d’un privilège d’exploitation de
                                                vingt-quatre ans des salines de <orgName subtype="province" type="pays_et_provinces">
                                                  Franche-Comté</orgName> et <orgName subtype="province" type="pays_et_provinces">
                                                  Lorraine</orgName>. Parseval Deschênes par
                                                exemple connaissait les salines de <orgName subtype="province" type="pays_et_provinces">
                                                  Franche-Comté</orgName> et avait déjà obtenu
                                                l’exploitation du sel à <ref target="#Montmorot">
                                                  Montmorot</ref>. Le montage financier
                                          était complexe. Les régisseurs participaient aux fonds de
                                          dépense (tant pour la construction de la nouvelle saline à
                                          concurrence de 600 000 livres que pour l’exploitation des
                                          cinq anciennes) et donc aux produits nets de la régie,
                                          mais les cautions de Laurent David, c’est-à-dire les
                                          Fermiers généraux, étaient autorisés à prendre des
                                          intérêts dans l’affaire, à concurrence de la moitié des
                                          fonds et des produits nets.L’architecte Claude-Nicolas
                                          Ledoux supervisa la construction de
1775 à 1779 avec
                                          l’aide d’André Haudry de Soucy, sans doute le fils du
                                          Fermier général allié aux Parseval. Il est clair que le
                                          montage financier visait tout à la fois à redynamiser les
                                          salines tout en préservant les intérêts des Fermiers
                                          généraux.L’entreprise ne parvint pas à son terme : en
                                                1782, le traité fut
                                          résilié, la nouvelle saline remise à la Ferme générale et
                                          comprise dans le nouveau bail <ref target="#Salzard">
                                                Salzard</ref>. Plusieurs raisons expliquent cet
                                          échec. La question financière d’abord : estimée
                                          initialement à 600 000 livres, la réalisation nécessita le
                                          triple des fonds. Outre le caractère prestigieux du projet
                                          de Ledoux, la défectuosité de la canalisation reliant
                                          Chaux à <ref target="#Salins"> Salins</ref>, avec une
                                          perte assez importante de sel, fut mise en cause. Les
                                          associés de la régie intéressée ne tirèrent donc pas grand
                                          profit de la compagnie, d’autant que l’exploitation des
                                          salines anciennes rendait peu. Il faut également évoquer
                                          les plaintes liées à la surexploitation de la forêt. Ces
                                          doléances émanaient de plusieurs corps dont la maîtrise
                                          des Eaux et Forêts qui perdait sa juridiction sur des bois
                                          destinés à la cuite du sel au profit de la commission de
                                          réformation. A Paris, Jean-Mathieu Augeard, chef du
                                          département des salines de la Ferme générale, supervisa la
                                          suite des opérations, mais la production ne dépassa jamais
                                          35 000 quintaux de sel, soit 3 500 tonnes c’est-à-dire
                                          quatre fois moins qu’à Salins au début du XVIIe siècle.
                                          Autant <ref target="#Salins"> Salins</ref> fut une
                                          réussite éblouissante, puisqu’elle contribua à la
                                          prospérité de la <orgName subtype="province" type="pays_et_provinces"> Franche-Comté</orgName>
                                          pendant des siècles, autant Arc-et-Senans fut un échec
                                          industriel et un gouffre financier. </def>
                                    <listBibl><bibl><idno type="ArchivalIdentifier">AN, F14 4267, «
                                                  Détails historiques sur les salines de Lorraine
                                                  des trois Evêchés et de Franche-Comté dont le
                                                  principal objet est d’exposer les variations que
                                                  ces mines ont successivement éprouvées dans leur
                                                  administration », après 1782</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN, G1 93 (Salines
                                                  de Salins dont traité du 12 mars 1774), 95
                                                  (dossiers. 13 et 15)</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">G7 280, 281</idno>
                                          </bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui annulle
                                                tous traités faits à des particuliers, pour
                                                fourniture de Salin et potasse, par Jean Roux
                                                Montclar, chargé précédemment de l'exploitation des
                                                salines de Lorraine et Franche-Comté, sans qu'il
                                                puisse être exigé aucune indemnité de
                                              l'adjudicataire des fermes générales</hi>, 19 juillet
                                                1782</bibl>
                                          <bibl>Thierry Claeys, <hi rend="italic">Dictionnaire biographique des
                                              financiers en France au XVIIIe siècle</hi>, Paris,
                                                éditions SPM, 2011, t. 1, notice Haudry, p.
                                                1087-1089  t. 2, notices Parseval, p. 1863-1867</bibl>
                                          <bibl>Roger Humbert, <hi rend="italic">Institutions et gens de finances en
                                              Franche-Comté, 1674-1790</hi>, Besançon, Annales
                                                littéraires de l’Université de Franche-Comté, n°
                                                605, Diffusion Les Belles-Lettres, 1996</bibl>
                                          <bibl>Philippe Mairot, <hi rend="italic">Salins-les-Bains, Arc-et-Senans,
                                              fortunes du sel comtois</hi>, Lyon, Le Progrès, 2010</bibl>
                                          <bibl>Emmeline Scachetti, « La saline d’Arc-et-Senans,
                                                Manufacture, utopie et patrimoine (1773-2011) »,
                                                thèse d’histoire contemporaine sous la direction de
                                                Jean-Claude Daumas, Université de Franche-Comté, 23
                                                novembre 2013</bibl>
                                    </listBibl></sense>
                        </entry>
                  </body>
            </text>
      </TEI>
      