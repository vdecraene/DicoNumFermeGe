<?xml version='1.0' encoding='UTF-8'?>
<TEI n="69" xmlns="http://www.tei-c.org/ns/1.0" version="3.3.0">
            <teiHeader>
                  <fileDesc>
                        <titleStmt>
                              <title type="notice"> Registre sexté </title>
                              <author>Marie-Laure Legay</author>
                        </titleStmt>
                        <publicationStmt>
                              <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer
                                    le privilège la Ferme générale dans l'espace français et
                                    européen (1664-1794)Axe 1 : Dictionnaire de la Ferme générale,
                                    objet d'histoire totale </publisher>
                              <pubPlace>
                                    <address>
                                          <addrLine> Maison Européenne des Sciences de l'Homme et de
                                                la Société </addrLine>
                                          <addrLine> 2 rue des cannoniers </addrLine>
                                          <addrLine> 59002 Lille Cedex </addrLine>
                                    </address>
                                    2021-2025
                              </pubPlace>
                              <availability>
                                    <licence>Creative Commons Attribution 3.0 non
                                          transposé (CC BY 3.0)</licence>
                              </availability>
                        </publicationStmt>
                        <seriesStmt>
                              <title> Dictionnaire numérique de la Ferme générale </title>
                              <respStmt>
                                    <resp> Coordinateurs de l'axe : Dictionnaire numérique de la
                                          Ferme générale, objet d'histoire totale. </resp>
                                    <persName> Marie-Laure Legay </persName>
                                    <persName> Thomas Boullu </persName>
                              </respStmt>
                        </seriesStmt>
                        <sourceDesc><bibl><idno type="ArchivalIdentifier">AN, G7 1170,
                                                  procès-verbal de la tournée faite par l’inspecteur
                                                  Languérat dans la direction des fermes de
                                                  Saint-Quentin, 1708 et Mémoire de Faury,
                                                  inspecteur des fermes, sur la visite des gabelles
                                                  et des traites d'Abbeville, 12 janvier 1709</idno>
                                          </bibl>
                                          <bibl type="sources"><hi rend="italic">Édit du Roy portant création de
                                                commissaires-vérificateurs des rolles pour la
                                                distribution du sel, tant dans les greniers de vente
                                                volontaire et d'impost que dans l'étenduë des
                                                gabelles de Lyonnois, parroisses de Normandie et du
                                              Rhételois</hi>, Versailles, mai 1702</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat portant que
                                                tous les sujets demeurant en pays de gabelles,
                                                seront tenus de lever le sel pour leur provision,
                                                grosses et menues salaisons aux greniers dans le
                                              ressort desquels ils sont domiciliés</hi>, 10 mars 1722</bibl>
                                          <bibl type="sources"><hi rend="italic">Déclaration du Roy portant règlement
                                                pour les regrats, les restitutions des droits de
                                              gabelles et le devoir des gabellans</hi>, Fontainebleau,
                                                29 août 1724</bibl>
                                          <bibl>Gérard Gayot, « La ferme générale dans les Ardennes
                                                en 1738. Le témoignage d'Helvétius », dans
                                              <hi rend="italic">Dix-huitième Siècle</hi>, n°3, 1971, p. 73-94</bibl>
                                    </sourceDesc></fileDesc>
                  <encodingDesc>
                        <projectDesc source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
                              <p> Le projet FermGé vise à étudier l’impact d’une organisation
                                    fiscale (1664-1794), discriminante mais rationnelle, sur les
                                    territoires et les sociétés de la France moderne/p </p>
                        </projectDesc>
                  </encodingDesc>
                  <profileDesc>
                        <textClass>
                              <keywords scheme="#fr_RAMEAU">
                                    <list>
                                          <item> Histoire -- Histoire moderne -- Histoire
                                                administrative </item>
                                    </list>
                              </keywords>
                        </textClass>
                  </profileDesc>
                  <revisionDesc>
                        <change type="AutomaticallyEncoded"> 2022-12-20T14:05:37.919428+2:00
                        </change>
                  </revisionDesc>
            </teiHeader>
            <text>
                  <body>
                        <entry n="69" type="R" xml:id="Registre_sexté">
                              <form type="lemma">
                                    <orth> Registre sexté </orth>
                              </form>
                              <sense>
                                    <def> Registre dressé dans les greniers à <ref target="#sel">
                                                sel</ref> pour dénombrer le nombre de feux et de
                                          personnes dont chaque famille est composée et calculer la
                                          distribution des <ref target="#minots"> minots</ref> de
                                          sel. « Les registres sextez [sont] regardez comme le
                                          principe le principal soutien de la Régie des grandes
                                          gabelles de France », lit-on dans une déclaration de
                                                1724. <ref target="#Helvétius"> Helvétius</ref>, dans son
                                          rapport de 1738 pour
                                          l’inspection des Ardennes, fit une description précise du
                                          registre sexté établi par le receveur : « Le sexté est un
                                          registre dans lequel est écrit les noms de tous les
                                          ressortissants exempts et non exempts, même les pauvres et
                                          mendiants, et il y a une case particulière destinée pour
                                          chaque feu, contenant les noms, surnoms et qualités, la
                                          cotte de la taille ou la capitation, et le nombre des
                                          personnes dont il est composé à l’exception des enfants
                                          au-dessous de huit ans, comme aussi le nombre et l’espèce
                                          des bestiaux et à mesure que le chef de famille lève le
                                          sel, on le décharge du registre des ventes sur celuy-ci ».<ref target="#ordonnance">
                                                  ordonnance</ref> de mai 1680 (titre VI, article VII) et les lois
                                                postérieures (9 mai 1702, 21 octobre 1710, 18
                                                août 1711…) définirent
                                                des règles strictes qui astreignirent les autorités
                                                municipales, asséeurs, collecteurs et syndics des
                                                paroisses pour la taille, maires, échevins et
                                                syndics des villes franches et abonnées pour la
                                                capitation…, à donner copie des rôles fiscaux aux
                                                receveurs des <ref target="#greniers"> greniers
                                               </ref> ou à des « commissaires-vérificateurs des
                                                rôles pour la distribution du sel » créés à cet
                                                effet (1702).
                                          Il arrivait qu'aucun registre sexté ne pût être établi par
                                          le receveur, faute de document, comme ce fut le cas entre
                                                1703 et 1709 à Abbeville. De même, si les rôles de
                                          taille ou états de dénombrements que les officiers
                                          municipaux et autres collecteurs fournissaient à la Ferme
                                          générale étaient mal faits, les registres sextés
                                          dupliquaient les erreurs. Les contribuables avaient
                                          obligation d’aller chercher leur sel de provision au
                                          grenier où ils étaient inscrits. Certains, « dans
                                          l’espérance de rendre inutile la preuve du registre sexté
                                          qui [doit] être tenu à l’effet de connoître ceux des
                                          domiciliez de chaque grenier qui auroient manqué de
                                          satisfaire au devoir de gabelle », affectaient d’aller en
                                          prendre ailleurs, ce qui fut strictement interdit. Le
                                          registre sexté devait distinguer les sels de salaisons et
                                          ceux pris pour pot et salière, afin de savoir si une
                                          famille avait fait son « devoir de gabelle ».
                                                Toutefois, le receveur du grenier ne s’astreignait
                                                pas toujours à cette obligation, comme le remarqua
                                                l’inspecteur Languérat en 1708 lors de sa visite du <orgName subtype="administrations_juridictions_royales" type="administrations_juridictions"> grenier de
                                                  Saint-Quentin</orgName>. De même, cet inspecteur constata l’absence de
                                          registre sexté pour le sel des ecclésiastiques et nobles,
                                          malgré les instructions des Fermiers généraux. A partir de
                                                1724, les copies des
                                          registres sextés devaient faire mention dans un chapitre à
                                          part des contribuables pauvres qui payaient moins de 30
                                          sous de taille ou de capitation et n’étaient donc pas
                                          obligés d’aller chercher le sel du devoir au grenier. </def>
                                    <listBibl><bibl><idno type="ArchivalIdentifier">AN, G7 1170,
                                                  procès-verbal de la tournée faite par l’inspecteur
                                                  Languérat dans la direction des fermes de
                                                  Saint-Quentin, 1708 et Mémoire de Faury,
                                                  inspecteur des fermes, sur la visite des gabelles
                                                  et des traites d'Abbeville, 12 janvier 1709</idno>
                                          </bibl>
                                          <bibl type="sources"><hi rend="italic">Édit du Roy portant création de
                                                commissaires-vérificateurs des rolles pour la
                                                distribution du sel, tant dans les greniers de vente
                                                volontaire et d'impost que dans l'étenduë des
                                                gabelles de Lyonnois, parroisses de Normandie et du
                                              Rhételois</hi>, Versailles, mai 1702</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat portant que
                                                tous les sujets demeurant en pays de gabelles,
                                                seront tenus de lever le sel pour leur provision,
                                                grosses et menues salaisons aux greniers dans le
                                              ressort desquels ils sont domiciliés</hi>, 10 mars 1722</bibl>
                                          <bibl type="sources"><hi rend="italic">Déclaration du Roy portant règlement
                                                pour les regrats, les restitutions des droits de
                                              gabelles et le devoir des gabellans</hi>, Fontainebleau,
                                                29 août 1724</bibl>
                                          <bibl>Gérard Gayot, « La ferme générale dans les Ardennes
                                                en 1738. Le témoignage d'Helvétius », dans
                                              <hi rend="italic">Dix-huitième Siècle</hi>, n°3, 1971, p. 73-94</bibl>
                                    </listBibl></sense>
                        </entry>
                  </body>
            </text>
      </TEI>
