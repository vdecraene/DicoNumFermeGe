<?xml version='1.0' encoding='UTF-8'?>
<TEI n="44" xmlns="http://www.tei-c.org/ns/1.0" version="3.3.0">
            <teiHeader>
                  <fileDesc>
                        <titleStmt>
                              <title type="notice"> Cabaret, cabaretier </title>
                              <author>Marie-Laure Legay</author>
                        </titleStmt>
                        <publicationStmt>
                              <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer
                                    le privilège la Ferme générale dans l'espace français et
                                    européen (1664-1794)Axe 1 : Dictionnaire de la Ferme générale,
                                    objet d'histoire totale </publisher>
                              <pubPlace>
                                    <address>
                                          <addrLine> Maison Européenne des Sciences de l'Homme et de
                                                la Société </addrLine>
                                          <addrLine> 2 rue des cannoniers </addrLine>
                                          <addrLine> 59002 Lille Cedex </addrLine>
                                    </address>
                                    2021-2025
                              </pubPlace>
                              <availability>
                                    <licence>Creative Commons Attribution 3.0 non
                                          transposé (CC BY 3.0)</licence>
                              </availability>
                        </publicationStmt>
                        <seriesStmt>
                              <title> Dictionnaire numérique de la Ferme générale </title>
                              <respStmt>
                                    <resp> Coordinateurs de l'axe : Dictionnaire numérique de la
                                          Ferme générale, objet d'histoire totale. </resp>
                                    <persName> Marie-Laure Legay </persName>
                                    <persName> Thomas Boullu </persName>
                              </respStmt>
                        </seriesStmt>
                        <sourceDesc><bibl type="sources"><hi rend="italic">Instruction pour bien exercer la
                                                charge de commis aux caves dans la province de
                                                Normandie, sans contrevenir à la nouvelle ordonnance
                                              du Roy, portant règlement sur les Fermes</hi>, 16
                                                septembre 1680</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui
                                                confisque dix-huit demi-queues de vin, sur le nommé
                                                Chenet Cabaretier, rue de Bièvre et le condamne en
                                              600 livres d'amende pour fausse déclaration</hi>, 24
                                                avril 1722</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui casse un
                                                arrêt de la cour des aides de Rouen et ordonne
                                                l'exécution de deux sentences des élus de
                                                Pont-de-l'Arche, par lesquelles le nommé Postel,
                                                cabaretier à Valdreuil, a été condamné en 225 livres
                                              d'amende pour rébellion aux commis</hi>, 25 octobre
                                                1723</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat du Roy et
                                                lettres patentes qui ordonnent que les particuliers
                                                qui demeurent dans les maisons où il est tenu
                                                cabaret, souffriront les visites et exercices des
                                                commis, et payeront les droits de détail des vins et
                                                autres boissons, qu'ils consommeront de même que les
                                              cabaretiers</hi>, 3 janvier 1726</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêts du Conseil d’Etat du roy, le
                                                premier confisque sur Etienne Sellier, cabaretier,
                                                une demi-queue de vin… Et le second déboute ledit
                                              Sellier de son opposition,</hi> 17 juillet et 11
                                                septembre 1731</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui casse
                                                une ordonnance de police, rendue en la sénéchaussée
                                                de la Rochelle le 24 janvier 1769 et ordonne qu'il
                                                soit libre à toutes personnes de tenir hotellerie ou
                                                cabaret et de vendre vin à la Rochelle et dans
                                                l'étendue de la généralité, sans la permission des
                                              officiers de police</hi>, 5 septembre 1769</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat concernant
                                                les détailleurs d'eau-de-vie et les vendant vins et
                                                autres boissons, dans les villes où il a été établi
                                                des Communautés de cabaretiers-aubergistes,
                                              cafetiers-limonadiers, par l'édit d'avril 1777</hi>, 23
                                                mai 1778</bibl>
                                          <bibl type="sources">Jean-Louis Lefebvre de Bellande,
                                                <hi rend="italic">Traité général des droits d’aides</hi>, 2 vol., Paris,
                                                chez Pierre Prault, 1760</bibl>
                                          <bibl>Marcel Lachiver, <hi rend="italic">Vin, vignes et vignerons, histoire
                                                du vignoble français</hi>, Paris, Fayard, 1988</bibl>
                                          <bibl>Matthieu Lecoutre, <hi rend="italic">Ivresse et ivrognerie dans la
                                              France moderne</hi>, Rennes, PUR, 2011, p. 314-323</bibl>
                                    </sourceDesc></fileDesc>
                  <encodingDesc>

                        <projectDesc source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
                              <p> Le projet FermGé vise à étudier l’impact d’une organisation
                                    fiscale (1664-1794), discriminante mais rationnelle, sur les
                                    territoires et les sociétés de la France moderne </p>
                        </projectDesc>
                  </encodingDesc>
                  <profileDesc>
                        <textClass>
                              <keywords scheme="#fr_RAMEAU">
                                    <list>
                                          <item> Histoire -- Histoire moderne -- Histoire commerciale </item>
                                          <item> Histoire -- Histoire moderne -- Histoire fiscale </item>
                                          <item> Histoire -- Histoire moderne -- Histoire judiciaire
                                          </item>
                                    </list>
                              </keywords>
                        </textClass>
                  </profileDesc>
                  <revisionDesc>
                        <change type="AutomaticallyEncoded"> 2022-12-13T11:52:05.919349+2:00
                        </change>
                  </revisionDesc>
            </teiHeader>
            <text>
                  <body>
                        <entry n="44" type="C" xml:id="Cabaret_cabaretier">
                              <form type="lemma">
                                    <orth> Cabaret, cabaretier </orth>
                              </form>
                              <sense>
                                    <def> Marchands de vin, taverniers, cabaretiers et hôteliers
                     pratiquaient tous le « commerce de vin au détail ». Le
                     marchand faisait de la vente de vin « à pot » : le
                     consommateur ne s’attablait pas. Le tavernier vendait du
                     vin marchand, tandis que le cabaretier vendait le vin « à
                     l’assiette ». Dans tous les cas, les droits sur le <ref target="#vin"> vin</ref> et les boissons qui se
                                          levaient au détail étaient dus. Les débitants de boissons,
                                          de plus en plus nombreux dans les grandes villes (1 500 à
                                          2 000 à Paris dans les années 1780, environ 1 500 à Lyon ou à Bordeaux) écoulaient
                                          une production importante. Selon l’<ref target="#ordonnance"> ordonnance</ref> des <ref target="#aides"> aides</ref> de 1680, hôteliers et cabaretiers étaient
                                                libres de s’établir et de vendre du <ref target="#vin"> vin</ref> et boissons au détail
                                                sans autre formalité qu’une déclaration au bureau
                                                des <ref target="#aides"> aides</ref>. Cette disposition fut encore rappelée en 1778 lorsque les nouvelles
                                          communautés de cabaretiers-aubergistes créées dans le
                                          ressort du parlement de Paris réclamèrent le monopole de
                                          la vente au détail. Le Conseil du roi jugea que la liberté
                                          de s’établir « vendant vin » moyennant le paiement du
                                          <ref target="#annuel">droit annuel</ref> et déclaration
                                          au bureau des <ref target="#aides"> aides</ref> devait
                                          être maintenue. De même, les tentatives
                                                des autorités de police ou des corps municipaux pour
                                                soumettre les cabaretiers à d’autres contraintes
                                                furent toujours combattues par la Ferme générale,
                                                par exemple à <placeName type="lieux_habitations">Moulins</placeName> en 1760
 ou à <ref target="#Rochelle">La Rochelle
                                               </ref> en 1769. Les forces de police cherchaient à combattre la
                                          criminalité, l’ivrognerie, le tapage nocturne. Dans
                                          l'Ouest, la <ref target="#commission"> commission de
                                                Saumur</ref> cita régulièrement les cabaretiers
                                          comme acteurs des réseaux de vente du sel. Dans le cas de
                                          criminalité organisée en <ref target="#bandes"> bandes
                                         </ref>, le cabaret constituait également un lieu de repli.
                                          Ce fut le cas du cabaret de Germain Savard, condamné à
                                          mort pour avoir caché Louis Dominique Cartouche, comme des
                                          cabarets d’<orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Auvergne"> Auvergne</ref>
                                         </orgName> ou du <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Dauphiné"> Dauphiné</ref>
                                         </orgName> dont les propriétaires étaient complices des
                                          contrebandiers du <ref target="#tabac"> tabac</ref> ou
                                          des <ref target="#toiles"> toiles</ref> en fraude de ses
                                          droits. Dans ces derniers cas, la Ferme générale
                                          acceptait l’aide de l’armée. Dans les villes en revanche,
                                          elle se défendait contre les prétentions des autorités de
                                          police à vouloir contrôler les débitants de boissons. Le
                                          cabaret était soumis à la seule rigueur des visites des
                                          commis aux <ref target="#aides"> aides</ref>, et plus
                                          particulièrement des <ref target="#caves">commis aux caves</ref> dont les instructions étaient très
                                          précises. Celles-ci insistaient notamment sur la nécessité
                                          de vérifier l’ordonnancement interne des cabarets car les
                                          débitants de vins avaient tendance « à se loger dans de
                                          grandes maisons dont ils n’occup[ai]ent que partie, le
                                          surplus [était] occupé par différents particuliers sous le
                                          nom desquels ils recel[ai]ent des vins et autres boissons
                                          dans les caves et celliers ». Outre les cachettes,
                                                les commis traquaient la fraude (le nombre de barils
                                                percés devait être limité), vérifiaient les prix
                                          … Les droits qui pesaient sur les boissons au
                                          détail donnaient lieu en effet à toutes sortes de
                                          resquilles.La fraude de  « était la plus courante. Selon
                                          les lieux, le cabaretier devait régler les droits de <ref target="#quatrième"> quatrième</ref>, ceux de <ref target="#huitième"> huitième</ref>, les droits de
                                                <ref target="#subvention"> subvention</ref> qui
                                          formaient également une taxe au détail. Dans
                                                le cas de règlement des droits à l’entrée des
                                                villes, les commis des bureaux des aides situés aux
                                                portes vérifiaient le registre des déclarations au
                                                passage des voitures chargées de boissons pour
                                                connaître la destination et si les alcools étaient
                                                destinés à des cabaretiers ou marchands ou bien des
                                                particuliers. Les cabaretiers étaient
                                          également soumis aux obligations du <ref target="#remuage">congé de remuage</ref>. </def>
                                    <listBibl><bibl type="sources"><hi rend="italic">Instruction pour bien exercer la
                                                charge de commis aux caves dans la province de
                                                Normandie, sans contrevenir à la nouvelle ordonnance
                                              du Roy, portant règlement sur les Fermes</hi>, 16
                                                septembre 1680</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui
                                                confisque dix-huit demi-queues de vin, sur le nommé
                                                Chenet Cabaretier, rue de Bièvre et le condamne en
                                              600 livres d'amende pour fausse déclaration</hi>, 24
                                                avril 1722</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui casse un
                                                arrêt de la cour des aides de Rouen et ordonne
                                                l'exécution de deux sentences des élus de
                                                Pont-de-l'Arche, par lesquelles le nommé Postel,
                                                cabaretier à Valdreuil, a été condamné en 225 livres
                                              d'amende pour rébellion aux commis</hi>, 25 octobre
                                                1723</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat du Roy et
                                                lettres patentes qui ordonnent que les particuliers
                                                qui demeurent dans les maisons où il est tenu
                                                cabaret, souffriront les visites et exercices des
                                                commis, et payeront les droits de détail des vins et
                                                autres boissons, qu'ils consommeront de même que les
                                              cabaretiers</hi>, 3 janvier 1726</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêts du Conseil d’Etat du roy, le
                                                premier confisque sur Etienne Sellier, cabaretier,
                                                une demi-queue de vin… Et le second déboute ledit
                                              Sellier de son opposition,</hi> 17 juillet et 11
                                                septembre 1731</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui casse
                                                une ordonnance de police, rendue en la sénéchaussée
                                                de la Rochelle le 24 janvier 1769 et ordonne qu'il
                                                soit libre à toutes personnes de tenir hotellerie ou
                                                cabaret et de vendre vin à la Rochelle et dans
                                                l'étendue de la généralité, sans la permission des
                                              officiers de police</hi>, 5 septembre 1769</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat concernant
                                                les détailleurs d'eau-de-vie et les vendant vins et
                                                autres boissons, dans les villes où il a été établi
                                                des Communautés de cabaretiers-aubergistes,
                                              cafetiers-limonadiers, par l'édit d'avril 1777</hi>, 23
                                                mai 1778</bibl>
                                          <bibl type="sources">Jean-Louis Lefebvre de Bellande,
                                                <hi rend="italic">Traité général des droits d’aides</hi>, 2 vol., Paris,
                                                chez Pierre Prault, 1760</bibl>
                                          <bibl>Marcel Lachiver, <hi rend="italic">Vin, vignes et vignerons, histoire
                                                du vignoble français</hi>, Paris, Fayard, 1988</bibl>
                                          <bibl>Matthieu Lecoutre, <hi rend="italic">Ivresse et ivrognerie dans la
                                              France moderne</hi>, Rennes, PUR, 2011, p. 314-323</bibl>
                                    </listBibl></sense>
                        </entry>
                  </body>
            </text>
      </TEI>
      