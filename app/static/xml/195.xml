<?xml version='1.0' encoding='UTF-8'?>
<TEI n="195" xmlns="http://www.tei-c.org/ns/1.0" version="3.3.0">
            <teiHeader>
                  <fileDesc>
                        <titleStmt>
                              <title type="notice">Anjou</title>
                              <author>Marie-Laure Legay</author>
                        </titleStmt>
                        <publicationStmt>
                              <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer
                                    le privilège la Ferme générale dans l'espace français et
                                    européen (1664-1794)Axe 1 : Dictionnaire de la Ferme générale,
                                    objet d'histoire totale </publisher>
                              <pubPlace>
                                    <address>
                                          <addrLine> Maison Européenne des Sciences de l'Homme et de
                                                la Société </addrLine>
                                          <addrLine> 2 rue des cannoniers </addrLine>
                                          <addrLine> 59002 Lille Cedex </addrLine>
                                    </address>
                                    2021-2025
                              </pubPlace>
                              <availability>
                                    <licence>Creative Commons Attribution 3.0 non
                                          transposé (CC BY 3.0)</licence>
                              </availability>
                        </publicationStmt>
                        <seriesStmt>
                              <title> Dictionnaire numérique de la Ferme générale </title>
                              <respStmt>
                                    <resp> Coordinateurs de l'axe : Dictionnaire numérique de la
                                          Ferme générale, objet d'histoire totale. </resp>
                                    <persName> Marie-Laure Legay </persName>
                                    <persName> Thomas Boullu </persName>
                              </respStmt>
                        </seriesStmt>
                        <sourceDesc><bibl type="sources"><hi rend="italic">Déclaration du Roy, laquelle
                                                maintient les habitans de la province de Bretagne
                                                dans l’exemption des droits de gabelles et règle
                                                l'ordre de procéder contre les faux-sauniers et gens
                                                attroupez venans des autres provinces, et l’usage du
                                                sel dans les paroisses voisines des provinces de
                                              Normandie, le Maine et l’Anjou</hi>, décembre 1680</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui ordonne
                                                que les propriétaires par engagement du droit de la
                                                traite par terre, appelée traite d'Anjou,
                                                continueront de jouir dudit droit sur les vins,
                                                toiles et autres marchandises qui sortiront pour
                                              Bretagne par les bureaux des duchés d'Anjou</hi>, Thouars
                                                et Beaumont, du 13 octobre 1703, Paris, Veuve
                                                Saugrain, 1715</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat par lequel
                                                S. M. déclare que dans la remise du tiers des droits
                                                de sortie pour les vins d’Anjou destinés pour la
                                                Bretagne ou pour sortir par mer, ni dans la remise
                                                entière des droits sur les vins qui seront
                                                transportés dans les provinces du Maine, Normandie
                                                et autres du dedans des cinq grosses fermes, elle n'a point entendu comprendre les anciens et nouveaux
                                                cinq sols dûs à la sortie pour la Bretagne où pour
                                                sortir par mer, ni les anciens cinq sols dûs à
                                                l'arrivée dans les lieux du Maine, Normandie et
                                                autres où ils ont cours, et autres droits d'octrois
                                              réunis à la ferme des aides</hi>, 29 mai 1725</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui continue
                                                pendant deux années la modération des droits
                                                d'entrée, d'abord et consommation sur les sardines
                                                venant de la province de Bretagne dans celle
                                              d'Anjou</hi>, 24 février 1728</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui casse
                                                celui de la cour des aides de Paris, du 12 août
                                                1735, en ce qu'il modère une saisie domiciliaire de
                                                faux-sel, et condamne Fr. Biotteau, métayer au
                                                Housset, paroisse de St-Michel-de-Montélimard en
                                              Anjou, en 200 livres d’amende</hi>, 8 mai 1736</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui déclare
                                                commun pour la paroisse de Bouzillé en Anjou, celui
                                                du 14 septembre 1745, portant modération des droits
                                                des 5 grosses fermes, sur les vins du crû de la
                                              châtellenie de Chantoceaux, qui passent en Bretagne</hi>,
                                                14 janvier 1749</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui modère
                                                les droits d’entrée du tarif de 1664, ceux de
                                                subvention par doublement et de jauge et courtage à
                                                5 livres 3 sols dix deniers par tonneau de vin,
                                                sortant du comté Nantois, pour entrer par terre dans
                                              plusieurs paroisses d'Anjou et du Poitou</hi>, 2 mai
                                                1752</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt de la Cour des aides qui juge
                                                qu’un habitant de la paroisse de Saint-Christophe,
                                                pays rédimé quant aux gabelles, est assujetti au
                                                devoir de gabelles, parce que la maison où il est
                                              domicilié, est situé en Anjou</hi>, 12 juin 1761</bibl>
                                          <bibl type="sources"><hi rend="italic">Procès-verbal de l'assemblée
                                                générale de la Province d'Anjou, tenue le 6 octobre
                                              1789 concernant le remplacement de l'impôt du sel</hi>,
                                                1789</bibl>
                                          <bibl>Yves Durand, « La contrebande du sel au XVIIIe
                                                siècle aux frontières de Bretagne, du Maine et de
                                              l’Anjou », <hi rend="italic">Histoire sociale 7</hi>, 1974, p. 227-269</bibl>
                                          <bibl>Henri Bellugou, « La réforme des traites d’Anjou
                                                sous Louis XIV », <hi rend="italic">Droit privé et Institutions
                                                régionales : Études offertes à Jean
                                                  Yver</hi>, Mont-Saint-Aignan, Presses universitaires de
                                                Rouen et du Havre, 1976, p. 57-62</bibl>
                                          <bibl>Micheline Huvet-Martinet, « La répression du
                                                faux-saunage dans la France de l'Ouest et du Centre
                                                à la fin de l'Ancien Régime (1764-1789) », <hi rend="italic">Annales
                                                  de Bretagne et des pays de l'Ouest</hi>, t.84, 2, 1977,
                                                p. 423-443</bibl>
                                          <bibl>Micheline Huvet-Martinet, « Faux-saunage et faux-sauniers dans la France de
                                                l'Ouest et du Centre à la fin de l'Ancien Régime
                                                (1764-1789) », <hi rend="italic">Annales de Bretagne et des pays de
                                                  l'Ouest</hi>, t. 85, 4, 1978, p. 573-594</bibl>
                                          <bibl>Philippe Béchu, « Les officiers du grenier à sel
                                                d'Angers sous l’Ancien Régime », <hi rend="italic">Annales de Bretagne
                                                  et des pays de l'Ouest</hi>, t. 84, 1977, p. 61-74</bibl>
                                    </sourceDesc></fileDesc>
                  <encodingDesc>
                        <projectDesc source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
                              <p> Le projet FermGé vise à étudier l’impact d’une organisation
                                    fiscale (1664-1794), discriminante mais rationnelle, sur les
                                    territoires et les sociétés de la France moderne/p </p>
                        </projectDesc>
                  </encodingDesc>
                  <profileDesc>
                        <textClass>
                              <keywords scheme="#fr_RAMEAU">
                                    <list>
                                          <item>Histoire -- Histoire moderne -- Histoire
                                                administrative</item>
                                          <item>Histoire -- Histoire moderne -- Histoire
                                                économique</item>
                                          <item>Histoire -- Histoire moderne -- Histoire
                                                fiscale</item>
                                          <item>Histoire -- Histoire moderne -- Histoire
                                                politique</item>
                                    </list>
                              </keywords>
                        </textClass>
                  </profileDesc>
                  <revisionDesc>
                        <change type="AutomaticallyEncoded"> 2022-12-12T13:39:42.440400+2:00
                        </change>
                  </revisionDesc>
            </teiHeader>
            <text>
                  <body>
                        <entry n="195" type="A" xml:id="Anjou">
                              <form type="lemma">
                                    <orth>Anjou </orth>
                              </form>
                              <sense>
                                    <def> Les régimes fiscaux de l’<orgName subtype="province" type="pays_et_provinces">Anjou</orgName> plaçaient
                                          cette province dans une situation complexe. Encerclés de
                                          pays privilégiés, ses habitants, notamment ceux domiciliés
                                          aux limites, saisissaient fréquemment le Conseil du roi
                                          pour faire entendre leurs doléances.
                                                La Ferme générale fit preuve de compréhension et
                                                donna régulièrement son accord pour des réductions
                                                de taxes, notamment sur le transport des <ref target="#vins"> vins</ref>, ou pour régler
                                                administrativement les litiges quand ces derniers
                                                relevaient de situations sinon absurdes, du moins
                                                délicates, tant vis-à-vis de l’impôt du sel que
                                                vis-à-vis des taxes douanières et des aides. Pays de <ref target="#grandes_gabelles">grandes gabelles</ref>, l’<orgName subtype="province" type="pays_et_provinces">Anjou</orgName> disposait de greniers de « vente
                                          volontaire », mais aussi de greniers à sel « de plein
                                          impôt » et, dans les <ref target="#limitrophes">lieues limitrophes</ref> où la consommation était
                                          strictement mesurée, de  « <ref target="#dépôts"> dépôts
                                         </ref> » appelés aussi « contrôles » par le géographe
                                          Sanson en 1665. Angers et ses
                                          faubourgs relevaient de la vente volontaire, mais les 75
                                          paroisses du ressort du grenier étaient de « plein
                                          impôt » ; la même répartition s’observait entre Saumur
                                          (« vente volontaire ») et les 95 paroisses du ressort de
                                          son grenier. Baugé, Beaufort, la Flèche et leurs 58
                                          paroisses environnantes étaient toutes de « vente
                                          volontaire », tandis que Saint-Florent le Vieil et ses 36
                                          paroisses, Cholet (et ses 14 paroisses) et Vihiers (19
                                          paroisses) étaient de « plein impôt ». Dans les Mauges,
                                          pays limitrophe de la province <ref target="#rédimée">
                                                rédimée</ref> du <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Poitou"> Poitou</ref></orgName>, on trouvait les <orgName subtype="administrations_juridictions_fermes" type="administrations_et_juridictions"> dépôts de
                                                Mortagne</orgName> (dont dépendaient les paroisses
                                          de Mortagne, Saint-Hilaire de Mortagne, le
                                          Puy-Saint-Bonnet, la Tessoualle, Saint-Christophe du Bois,
                                          la Séguinière, le May, Saint-Léger, Saint- André, Etusson,
                                          la Verrie, Saint-Laurent sur Sèvre), Tiffauges (pour
                                          Tiffauges, la Romagne, Roussay, Montigné, l’enclave du
                                          Longeron, les Landes-Genusson, Saint-Aubin des Ormeaux,
                                          Saint- Martin L’Ars) et Mauléon. A la frontière entre
                                                <orgName subtype="province" type="pays_et_provinces">Anjou</orgName>
                                                                  et <ref target="#rédimé">pays rédimé</ref>, des
                                                contestations concernant le régime fiscal des lieux
                                                étaient régulièrement soulevées. Le
                                          seigneur Emery de Messemé, assujetti à la gabelle par l’<ref target="#intendant">intendant</ref> de Tours
                                          comme dépendant du <ref target="#grenier"> grenier</ref>
                                          de <ref target="#Richelieu"> Richelieu</ref>, fit valoir
                                          qu’il se trouvait en pays rédimé, mais fut débouté par
                                          arrêt du conseil en 1761.<ref target="#contrebande"> contrebande</ref> circulait massivement à la
                                                frontière avec la <orgName subtype="province" type="pays_et_provinces">
                                                  <ref target="#Bretagne"> Bretagne</ref></orgName>, pays exempt de gabelle et avec le
                                                  <orgName subtype="province" type="pays_et_provinces"> Poitou</orgName>, pays
                                                rédimé. Micheline Huvet-Martinet, en étudiant
                                          la <orgName subtype="administrations_juridictions_royales" type="administrations_juridictions"> commission de
                                                Saumur</orgName> instituée entre
1742 et 1789, a
                                          analysé cette contrebande massive à laquelle tous les
                                          groupes sociaux participaient, y compris la noblesse et la
                                          bourgeoisie marchande des <placeName type="lieux_habitations"> villes d’Angers</placeName>, <placeName type="lieux_habitations">Cholet</placeName> et <placeName type="lieux_habitations">Saumur</placeName>. Au Sud
                                          de la Loire, le dispositif de défense mobilisait plus de
                                          1 000 brigadiers et employés, tantôt répartis davantage le
                                          long des rivières, comme en 1769
 et 1775, tantôt
                                          établis sur les chemins et voies terrestres comme lors du
                                          remaniement des brigades de 1773. Pour juger tous les cas de fraudes et
                                          contrebandes, la commission fut divisée en subdélégations,
                                        établies à <placeName>Ancenis</placeName> et <ref target="#Ingrandes"> Ingrandes
                                         </ref> pour le <orgName subtype="administrations_juridictions_fermes" type="administrations_et_juridictions"> département d’Angers</orgName>.<orgName subtype="province" type="pays_et_provinces">Anjou</orgName> était
                                          située dans le ressort des <ref target="#fermes">Cinq grosses fermes</ref>. Aux <placeName type="lieux_controle"> bureaux d’Angers</placeName>, de La Pointe, Rochefort, Lyon d’Angers, Pont de Cée,
                                                <ref target="#Ingrandes"> Ingrandes</ref>, Condé,
                                          Champtoceaux, Landemont, Les Leards, Saint-Florent et
                                          Saumur, on levait les droits du tarif de 1664, mais aussi les droits d’abord et
                                          consommation sur les poissons de mer, la <ref target="#subvention"> subvention</ref> sur les
                                          vins, les droits de Prévôté de Nantes et les droits locaux
                                          de la Rivière de <ref target="#Loire"> Loire</ref>. Les
                                          prix des vins d’<orgName subtype="province" type="pays_et_provinces">Anjou</orgName> taxés à
                                          la sortie devaient rester attractifs à l’exportation ;
                                          c’est pourquoi Louis XV accepta en 1725
 (arrêt du 25 mars) de réduire les droits d’un
                                          tiers quand les tonneaux étaient destinés aux ports
                                          bretons. L’administration royale essayait de compenser et
                                          d’équilibrer fiscalement les échanges entre les deux
                                          provinces. Vis-à-vis des sardines de <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Bretagne"> Bretagne</ref></orgName>, la Ferme générale accepta de réduire les
                                          droits d’abord et de consommation à l’entrée en 1726 au regard de « la mauvaise
                                          récolte des vins » qui affaiblit l’économie angevine
                                                ; cette réduction fut prolongée jusqu’à la fin de
                                                l’Ancien régime. Le baril de 300 livres-poids de sardines était donc taxé à 4 livres 15
                                          sous et 6 deniers. La Ferme générale assouplit également
                                          sa position dans certaines affaires limitrophes. La
                                          paroisse de Bouzillé par exemple, moitié dépendante de
                                          l’évêché de <ref target="#Nantes"> Nantes</ref>, moitié
                                          dépendante de l’évêché d’Angers obtint des aménagements en
                                                1749 : la Ferme concéda une réduction des droits de sortie
                                                et droits d’<ref target="#aides"> aides</ref> aux
                                                habitants de la moitié angevine, producteurs de <ref target="#vins"> vins</ref> tout comme ceux de la
                                                moitié bretonne, pour éviter  « la facilité qu’ils
                                                avoient de les frauder ». De même accepta
                                          t-elle en 1752 de réduire les
                                          droits d’entrée sur les <ref target="#vins"> vins</ref>
                                          bretons pour les paroisses limitrophes du <orgName subtype="province" type="pays_et_provinces">Bas-Anjou</orgName> et du Bas-<orgName subtype="province" type="pays_et_provinces">Poitou</orgName>, non productrices de vins et obligées de
                                          s’approvisionner à prix fort soit en faisant venir les
                                          barriques de l’intérieur de la province, soit en payant
                                          les droits sur les vins du comté nantais tout proche. Les
                                          droits (tant droits d’entrée du tarif de 1664, subvention par doublement et droits
                                          de jauge et <ref target="#courtage"> courtage</ref>)
                                          s’élevaient à 18 livres, 13 sous et 10 deniers pour un
                                          tonneau comprenant 3 muids et demis (mesure de Paris), ce
                                          qui doublait le prix du vin ; ils furent réduits à 5
                                          livres, 3 sous et 10 deniers pour les tonneaux qui
                                          entraient par voie terrestre « à la gauche de la Loire »
                                          en passant par les <placeName type="lieux_controle">
                                                bureaux de Landemont</placeName>, Montrevault,
                                          Gesté, Tillers, Montigné, Torfou ; et à 7 livres, 15 sous
                                          et 9 deniers pour les tonneaux qui entraient par la
                                          rivière de la <ref target="#Loire"> Loire</ref>.
                                          Globalement, l’administration royale entendait favoriser
                                          le commerce grâce aux exemptions ou réductions, sans
                                          néanmoins diminuer trop sensiblement les recettes.
                                                Lorsque Louis XV accorda la réduction du tiers des
                                                droits de sortie sur les <ref target="#vins"> vins
                                               </ref> d’<orgName subtype="province" type="pays_et_provinces">Anjou</orgName>
                                                destinés à être embarqués dans les ports de <orgName subtype="province" type="pays_et_provinces">
                                                  <ref target="#Bretagne"> Bretagne</ref>
                                               </orgName> en 1725, il
                                                n’entendait pas comprendre dans cette réduction les
                                                droits de la Ferme des <ref target="#aides"> aides
                                               </ref>.<orgName subtype="province" type="pays_et_provinces">Anjou
                                         </orgName> » constituait une particularité locale : ce
                                          droit, appelé aussi « imposition foraine d’<orgName subtype="province" type="pays_et_provinces">Anjou
                                         </orgName> » appartenait au roi et consistait à lever des
                                          taxes sur les <ref target="#vins"> vins</ref>, les <ref target="#toiles"> toiles</ref> et autres
                                          marchandises qui sortaient vers la <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Bretagne"> Bretagne</ref></orgName> par les bureaux du duché d’<orgName subtype="province" type="pays_et_provinces">Anjou</orgName>, Thouars et Beaumont et des <placeName type="lieux_habitations"> villes de Mayenne</placeName>, Laval et Ernée (voir Traite d’<orgName subtype="province" type="pays_et_provinces">Anjou</orgName>). </def>
                                    <listBibl><bibl type="sources"><hi rend="italic">Déclaration du Roy, laquelle
                                                maintient les habitans de la province de Bretagne
                                                dans l’exemption des droits de gabelles et règle
                                                l'ordre de procéder contre les faux-sauniers et gens
                                                attroupez venans des autres provinces, et l’usage du
                                                sel dans les paroisses voisines des provinces de
                                              Normandie, le Maine et l’Anjou</hi>, décembre 1680</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui ordonne
                                                que les propriétaires par engagement du droit de la
                                                traite par terre, appelée traite d'Anjou,
                                                continueront de jouir dudit droit sur les vins,
                                                toiles et autres marchandises qui sortiront pour
                                              Bretagne par les bureaux des duchés d'Anjou</hi>, Thouars
                                                et Beaumont, du 13 octobre 1703, Paris, Veuve
                                                Saugrain, 1715</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat par lequel
                                                S. M. déclare que dans la remise du tiers des droits
                                                de sortie pour les vins d’Anjou destinés pour la
                                                Bretagne ou pour sortir par mer, ni dans la remise
                                                entière des droits sur les vins qui seront
                                                transportés dans les provinces du Maine, Normandie
                                                et autres du dedans des cinq grosses fermes, elle n'a point entendu comprendre les anciens et nouveaux
                                                cinq sols dûs à la sortie pour la Bretagne où pour
                                                sortir par mer, ni les anciens cinq sols dûs à
                                                l'arrivée dans les lieux du Maine, Normandie et
                                                autres où ils ont cours, et autres droits d'octrois
                                              réunis à la ferme des aides</hi>, 29 mai 1725</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui continue
                                                pendant deux années la modération des droits
                                                d'entrée, d'abord et consommation sur les sardines
                                                venant de la province de Bretagne dans celle
                                              d'Anjou</hi>, 24 février 1728</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui casse
                                                celui de la cour des aides de Paris, du 12 août
                                                1735, en ce qu'il modère une saisie domiciliaire de
                                                faux-sel, et condamne Fr. Biotteau, métayer au
                                                Housset, paroisse de St-Michel-de-Montélimard en
                                              Anjou, en 200 livres d’amende</hi>, 8 mai 1736</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui déclare
                                                commun pour la paroisse de Bouzillé en Anjou, celui
                                                du 14 septembre 1745, portant modération des droits
                                                des 5 grosses fermes, sur les vins du crû de la
                                              châtellenie de Chantoceaux, qui passent en Bretagne</hi>,
                                                14 janvier 1749</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui modère
                                                les droits d’entrée du tarif de 1664, ceux de
                                                subvention par doublement et de jauge et courtage à
                                                5 livres 3 sols dix deniers par tonneau de vin,
                                                sortant du comté Nantois, pour entrer par terre dans
                                              plusieurs paroisses d'Anjou et du Poitou</hi>, 2 mai
                                                1752</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt de la Cour des aides qui juge
                                                qu’un habitant de la paroisse de Saint-Christophe,
                                                pays rédimé quant aux gabelles, est assujetti au
                                                devoir de gabelles, parce que la maison où il est
                                              domicilié, est situé en Anjou</hi>, 12 juin 1761</bibl>
                                          <bibl type="sources"><hi rend="italic">Procès-verbal de l'assemblée
                                                générale de la Province d'Anjou, tenue le 6 octobre
                                              1789 concernant le remplacement de l'impôt du sel</hi>,
                                                1789</bibl>
                                          <bibl>Yves Durand, « La contrebande du sel au XVIIIe
                                                siècle aux frontières de Bretagne, du Maine et de
                                              l’Anjou », <hi rend="italic">Histoire sociale 7</hi>, 1974, p. 227-269</bibl>
                                          <bibl>Henri Bellugou, « La réforme des traites d’Anjou
                                                sous Louis XIV », <hi rend="italic">Droit privé et Institutions
                                                régionales : Études offertes à Jean
                                                  Yver</hi>, Mont-Saint-Aignan, Presses universitaires de
                                                Rouen et du Havre, 1976, p. 57-62</bibl>
                                          <bibl>Micheline Huvet-Martinet, « La répression du
                                                faux-saunage dans la France de l'Ouest et du Centre
                                                à la fin de l'Ancien Régime (1764-1789) », <hi rend="italic">Annales
                                                  de Bretagne et des pays de l'Ouest</hi>, t.84, 2, 1977,
                                                p. 423-443</bibl>
                                          <bibl>Micheline Huvet-Martinet, « Faux-saunage et faux-sauniers dans la France de
                                                l'Ouest et du Centre à la fin de l'Ancien Régime
                                                (1764-1789) », <hi rend="italic">Annales de Bretagne et des pays de
                                                  l'Ouest</hi>, t. 85, 4, 1978, p. 573-594</bibl>
                                          <bibl>Philippe Béchu, « Les officiers du grenier à sel
                                                d'Angers sous l’Ancien Régime », <hi rend="italic">Annales de Bretagne
                                                  et des pays de l'Ouest</hi>, t. 84, 1977, p. 61-74</bibl>
                                    </listBibl></sense>
                        </entry>
                  </body>
            </text>
      </TEI>
      