<?xml version='1.0' encoding='UTF-8'?>
<TEI n="145" xmlns="http://www.tei-c.org/ns/1.0" version="3.3.0">
            <teiHeader>
                  <fileDesc>
                        <titleStmt>
                              <title type="notice"> Lorraine </title>
                              <author>Marie-Laure Legay</author>
                        </titleStmt>
                        <publicationStmt>
                              <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer
                                    le privilège la Ferme générale dans l'espace français et
                                    européen (1664-1794)Axe 1 : Dictionnaire de la Ferme générale,
                                    objet d'histoire totale </publisher>
                              <pubPlace>
                                    <address>
                                          <addrLine> Maison Européenne des Sciences de l'Homme et de
                                                la Société </addrLine>
                                          <addrLine> 2 rue des cannoniers </addrLine>
                                          <addrLine> 59002 Lille Cedex </addrLine>
                                    </address>
                                    2021-2025
                              </pubPlace>
                              <availability>
                                    <licence>Creative Commons Attribution 3.0 non
                                          transposé (CC BY 3.0)</licence>
                              </availability>
                        </publicationStmt>
                        <seriesStmt>
                              <title> Dictionnaire numérique de la Ferme générale </title>
                              <respStmt>
                                    <resp> Coordinateurs de l'axe : Dictionnaire numérique de la
                                          Ferme générale, objet d'histoire totale. </resp>
                                    <persName> Marie-Laure Legay </persName>
                                    <persName> Thomas Boullu </persName>
                              </respStmt>
                        </seriesStmt>
                        <sourceDesc><bibl><idno type="ArchivalIdentifier">AN, G1 93B,
                                              <hi rend="italic">Atlas des salines de Lorraine et de Franche-Comté</hi>,
                                                  planche 5 « Routes des entrepôts des sels de
                                                  Lorraine», vers 1755</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN G1 131, dont
                                                  <hi rend="italic">Déclaration du roi (Stanislas, roi de Pologne
                                                  grand-duc de Lithuanie… duc de Lorraine et Bar
                                                  faisant bail des fermes générales des domaines,
                                                  gabelles, salines, tabacs, autres droits de
                                                  Lorraine Barrois à Jean-Louis Bonnard pour six
                                                  années qui commenceront le 1er octobre 1756,
                                                      donnée à Lunéville le 6 novembre 1755</hi>, 64 pages 
                                                  dont « Mémoire sur les droits de Lorraine », vers
                                                  1773  dont dossier 3 sur la régie
                                                  Fouache</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN G1 132, dossier
                                                  1 sur le projet de régie pour la vente des sels à
                                                  l’étranger  dossier 4 sur le droit de
                                                  Haut-Conduit</idno>
                                          </bibl>
                                          <bibl type="sources"><hi rend="italic">Lettres patentes pour l'exécution du
                                                traité conclu à Paris le 21 janvier 1718 entre le
                                                Roi et le duc de Lorraine, en conséquence du traité
                                              de Ryswick, du 30 octobre 1697</hi>, données à Paris le
                                                11 février 1718</bibl>
                                          <bibl type="sources"><hi rend="italic">Lettres patentes du roi, permettant
                                                aux commis de Lorraine et de Bar de poursuivre les
                                              contrebandiers sur les terres du royaume</hi>, 4 février
                                                1738</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt de la Cour des aides qui
                                                confirme avec amende et dépens, deux sentences de la
                                                juridiction des traites de Langres par lesquelles le
                                                nommé Cerf d'Alsace entrepreneur de la fourniture
                                                des étapes aux troupes du roi et Antoine Lallemant
                                                son voiturier, ont été condamnés en 300 livres
                                              d'amende et en confiscation de 4 muids de vin</hi>, 15
                                                décembre 1752</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat du Roi, et
                                                lettres patentes sur icelui, qui autorisent les
                                                employés des fermes de Lorraine Barrois à arrêter
                                                les contrebandiers sur les terres de France,
                                              Versailles</hi>, 9 avril 1754</bibl>
                                          <bibl type="sources"><hi rend="italic">Recueil des édits, ordonnances,
                                                déclarations, tarifs, traités, règlements et arrêts
                                                sur le fait des droits de haut-conduit, entrée et
                                                issue foraine, traverse, impôt sur les toiles et
                                                acquits à caution de Lorraine et Barrois, avec une
                                                instruction pour les receveurs des droits de la
                                              marque des fers, Nancy, Leseure et Drouin</hi>, 1757</bibl>
                                          <bibl>Pierre Boyé, « Le budget de la province de Lorraine
                                                et Barrois sous le règne nominal de Stanislas
                                                (1737-1766), d'après des documents inédits », thèse
                                                pour le doctorat en droit, Impr. Crépin-Leblond,
                                                1896, quatrième partie, chapitre 1 : « La
                                                comptabilité domaniale et la Ferme générale »</bibl>
                                          <bibl>Pierre Boyé, <hi rend="italic">Les salines et le sel en Lorraine au
                                              XVIIIe siècle</hi>, Nancy, 1904</bibl>
                                          <bibl>Charles Hiegel, « L’industrie du sel en Lorraine du
                                                IXe au XVIIe siècle, Thèse de l'Ecole des Chartes »,
                                                1961</bibl>
                                          <bibl>Michel Dorban, « Un aspect du commerce
                                                Luxembourgeois au XVIIIe siècle : le sel lorrain »,
                                              <hi rend="italic">Annales de l’Est</hi>, n° 2, 1975, p. 143-155</bibl>
                                          <bibl>Carole Perceval, « La législation du tabac sous
                                                l'Ancien Régime, Ferme Générale et contrebandiers en
                                                France et en Lorraine » DEA, Université de Nancy Il,
                                                1997</bibl>
                                          <bibl>Pierre Vicq, « Une prise de pouvoir de la Ferme
                                                générale de Lorraine : bois des salines et
                                                faux-saunage de 1698 à la Révolution », thèse de
                                                doctorat en droit, Nancy, Université de Nancy 2,
                                                1998</bibl>
                                          <bibl>François Lormant, « La question fiscale du sel au
                                                temps de Louis XIV et de Vauban : la gabelle et le
                                                faux-saunage en Lorraine », HAL, id : hal-02170889</bibl>
                                        <bibl> Julien Villain, « Les marchands ruraux et la commercialisation des campagnes dans l’Europe du XVIIIe siècle. Le cas de la Lorraine centrale et méridionale », <hi rend="italic">Histoire et Sociétés Rurales</hi>, 2021, 1, p. 43-84</bibl>
                                    </sourceDesc></fileDesc>
                  <encodingDesc>
                        <projectDesc source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
                              <p> Le projet FermGé vise à étudier l’impact d’une organisation
                                    fiscale (1664-1794), discriminante mais rationnelle, sur les
                                    territoires et les sociétés de la France moderne/p </p>
                        </projectDesc>
                  </encodingDesc>
                  <profileDesc>
                        <textClass>
                              <keywords scheme="#fr_RAMEAU">
                                    <list>
                                          <item> Histoire -- Histoire moderne -- Histoire
                                                administrative </item>
                                          <item> Histoire -- Histoire moderne -- Histoire fiscale </item>
                                          <item> Histoire -- Histoire moderne -- Histoire financière </item>
                                          <item> Histoire -- Histoire moderne -- Histoire politique </item>
                                          <item> Histoire -- Histoire moderne -- Histoire des
                                                relations internationales </item>
                                    </list>
                              </keywords>
                        </textClass>
                  </profileDesc>
                  <revisionDesc>
                        <change type="AutomaticallyEncoded"> 2022-12-19T15:53:29.156463+2:00
                        </change>
                  </revisionDesc>
            </teiHeader>
            <text>
                  <body>
                        <entry n="145" type="L" xml:id="Lorraine">
                              <form type="lemma">
                                    <orth> Lorraine </orth>
                              </form>
                              <sense>
                                    <def> Bien qu’intégrée tardivement à la Ferme générale (1766), la sous-ferme de
                                                <orgName subtype="province" type="pays_et_provinces"> Lorraine</orgName>, mise en œuvre par des agents
                                          français et maintenue par Léopold Ier, forma une
                                          « filiale » du consortium financier français dès la fin du
                                          règne de Louis XIV. Cette ferme comprenait la <ref target="#gabelle"> gabelle</ref>, des droits
                                          domaniaux (comme le <ref target="#actes">contrôle des actes</ref> et insinuations),
                                          et divers autres droits. Le monopole du <ref target="#tabac"> tabac</ref> y fut adjoint en 1720. Lors des préliminaires
                                          de Vienne signés en novembre 1735, la propriété de la <orgName subtype="province" type="pays_et_provinces"> Lorraine</orgName> et du
                                          Barrois fut assurée à la France en en laissant l’usufruit
                                          au roi de Pologne, Stanislas Leszczynski. Confirmé en
                                                1736, puis par le traité
                                          de 1738, ce rattachement
                                          prévoyait que les revenus de ces provinces devaient être
                                          versés au Trésor royal français qui fournissait une
                                          pension au duc de <orgName subtype="province" type="pays_et_provinces"> Lorraine</orgName>.
                                          Toutefois, ce prince exerçant la souveraineté, les baux de
                                          la Ferme de <orgName subtype="province" type="pays_et_provinces"> Lorraine</orgName> furent
                                          maintenus à son nom jusqu’à sa mort (1766), « quoique ce fut à la même compagnie
                                          des fermiers généraux qui avaient les fermes générales de
                                          France » (AN, G1 131, Mémoire sur les fermes de <orgName subtype="province" type="pays_et_provinces">
                                                Lorraine</orgName>, vers 1773). Philippe Le Mire, bourgeois de Lunéville, prit
                                          en sous-bail les « fermes générales des Domaines,
                                          gabelles, salines et tabac autres droits de <orgName subtype="province" type="pays_et_provinces">
                                                Lorraine</orgName> et Barrois » jusqu’en 1744 pour 3 300 000 livres par
                                          an, relayé à cette date par l’adjudicataire Jean Duménil
                                          pour la même somme, puis en 1750 par Stanislas Louis Diétrich, pour 3 334 500
                                          livres, et en 1756 par
                                          Jean-Louis Bonnard, bourgeois de Paris, toujours pour 3, 3
                                          millions. A cette époque, l’hôtel de la Ferme de <orgName subtype="province" type="pays_et_provinces">
                                                Lorraine</orgName> fut bâti sur la place royale de
                                          Nancy. Achevé pour un montant de plus de 132 000 livres,
                                          cet hôtel (hôtel dit « Alliot », du nom de François
                                          Antoine Alliot, fermier général de France à partir de
                                                1756) symbolisa l’union
                                          des deux fermes. Jusqu'en 1750, le prix intégral du bail fut versé chaque année entre
                                          les mains du receveur général des finances, en quatre
                                          payements égaux ; puis la Ferme acquitta directement la
                                          pension du roi de Pologne chaque année en douze versements
                                          de 2 583 333 livres de mois en mois, le solde étant versé
                                          dans les caisses royales de France. Après 1766, les différents droits de
                                                <orgName subtype="province" type="pays_et_provinces"> Lorraine</orgName> et du Bar furent intégrés au
                                          bail général Jean-Jacques Prévôt, adjudicataire de la
                                          Ferme de France, pour 3 389 075 livres, mais formèrent une
                                                <ref target="#sous-ferme"> sous-ferme</ref> malgré
                                          leur analogie avec ceux de France. Le prix de cette <ref target="#sous-ferme"> sous-ferme</ref> fut calculé
                                          à partir d’évaluations tant du Domaine, droits domaniaux
                                          et seigneuriaux à 808 406 livres de <orgName subtype="province" type="pays_et_provinces">
                                                Lorraine</orgName>, des droits domaniaux régaliens
                                          et <ref target="#fers">marque des fers</ref> à 263 000,
                                          du produit des salines de <orgName subtype="province" type="pays_et_provinces"> Lorraine</orgName>
                                          (dépenses déduites) à 1 848 390 livres, de la vente des
                                                <ref target="#tabacs"> tabacs</ref> à 270 000
                                          livres, de la Foraine à 120 000 livres et finalement de
                                          prix du bail des postes et messageries à 25 833 livres. La
                                          compagnie française fit augmenter le produit des droits
                                          dans les années 1770. En 1773, sous le ministériat de
                                          l’abbé Terray, les produits bruts s’élevaient comme
                                          suit au moment des négociations pour la bail Laurent
                                          David:
                                        <figure>
                                            <graphic url="/static/images/lorraine.jpg"/>
                                            <head>Produits bruts (en livres, argent de France) des droits levés en Lorraine
                                                (AN, G1 131, « Mémoire sur les droits de Lorraine, 1773)</head>
                                        </figure><ref target="#gabelle"> gabelle</ref> de
                                                <orgName subtype="province" type="pays_et_provinces"> Lorraine</orgName>, <ref target="#pays_de_salines">pays de salines</ref>, ressemblait
                                          beaucoup à celle des <orgName subtype="province" type="pays_et_provinces"> Trois-Evêchés</orgName> :
                                          le sel s’y formait dans les salins de <ref target="#Dieuze"> Dieuze</ref>, Château-Salins et
                                          Moyenvic, et était vendu au public par 150 magasiniers
                                          (équivalent des greniers à sel, tant magasin et tribunal),
                                          répartis en sept départements. Pour cette distribution du
                                          sel dans les magasins, les Fermiers généraux de France
                                          traitaient avec les sept sous-fermiers des <ref target="#Domaines"> Domaines</ref> de <orgName subtype="province" type="pays_et_provinces">
                                                Lorraine</orgName> qui étaient responsables du prix
                                          envers la Ferme générale, moyennant une remise sur le sel
                                          livré aux magasiniers. Le pot de sel (le muid de 704
                                          livres formait 16 vaxels et un vaxel contenait 22 pots) se
                                          payait 5 sols six deniers la livre dans 147 magasins et
                                          trois sols seulement dans les trois magasins qui
                                          confinaient à l’étranger. En 1773, le produit brut de vente du sel s’éleva à 10 924
                                          muids, soit 76 905 quintaux pour une somme de 2 260 000
                                          livres. L’exploitation du sel s’intensifia tellement au
                                          XVIIIe siècle, qu’elle provoqua de multiples plaintes
                                          contre les Fermiers généraux. A <ref target="#Dieuze">
                                                Dieuze</ref> par exemple, le nombre de poêles en
                                          service utilisés pour l’évaporation de la saumure passa de
                                          6 en 1720 à 37 en 1779, et 48 en en 1789. Cette intense production
                                          entraîna une surexploitation des forêts alentours, tandis
                                          que le transport tant du bois que du sel dans la région
                                          provoquait une dégradation des routes. Régulièrement, les
                                                <ref target="#intendants"> intendants</ref> de la
                                          province alertaient le Contrôle général des finances, en
                                          vain. Cette production était vendue à l’intérieur du
                                          royaume de France, mais elle trouvait également des
                                          débouchés en <ref target="#Allemagne"> Allemagne</ref>,
                                          au <ref target="#Luxembourg"> Luxembourg</ref> et en <ref target="#Suisse"> Suisse</ref>, états auxquels on
                                          réservait le meilleur sel, c’est à dire le « gros » sel de
                                          qualité. Il se pratiquait en <orgName subtype="province" type="pays_et_provinces"> Lorraine</orgName> une
                                                <ref target="#contrebande"> contrebande</ref> très
                                          active à cause du différentiel élevé des prix su sel :
                                          environ 31 livres le quintal à l’intérieur du duché,
                                          contre 62 livres dans la <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Champagne"> Champagne</ref>
                                         </orgName> voisine. La Chambre des comptes de <orgName subtype="province" type="pays_et_provinces">
                                                Lorraine</orgName> avait compétence exclusive pour
                                          connaître des faits de <ref target="#gabelles"> gabelles
                                         </ref>. Bien que cette compétence lui fût contestée par
                                          les juges des bailliages, elle exerça continument sa
                                          justice répressive contre les faux-sauniers. Entre 1738 et 1790, elle traita 436 affaires de <ref target="#faux-saunage"> faux-saunage</ref>, soit
                                          un peu plus de 8 affaires par an, donnant lieu à 215
                                          condamnations, dont 185 peines de galères.<ref target="#tabac"> tabac</ref> furent
                                          calquées tardivement sur celle de France à partir de
                                          l’édit de nombre 1771. Les
                                          recettes brutes pour cette vente s’élevèrent alors à
                                          1 100 000 livres. Quant aux droits <ref target="#domaniaux"> domaniaux</ref>, il fut
                                          rappelé en 1773 que le roi de
                                          France possédait une grande quantité des fiefs en <orgName subtype="province" type="pays_et_provinces">
                                                Lorraine</orgName>, de sorte que les droits des
                                          domaines fixes et corporels s’y trouvaient affermés à
                                          hauteur de 780 000 livres en 1773, somme à rapprocher de ce que ces mêmes droits
                                          rapportaient dans le reste du royaume (1 091 873
                                          livres).<ref target="#traites"> traites
                                         </ref>, la <orgName subtype="province" type="pays_et_provinces"> Lorraine</orgName> était
                                          considérée « pays à l’instar de l’étranger effectif». Les
                                          marchandises ne réglaient donc pas le tarif de France en
                                          entrant et en sortant vers l’<ref target="#Allemagne">
                                                Allemagne</ref>. Toutefois, d’anciens droits de
                                          « péage » se levaient sur les marchandises qui passaient
                                          d’une enclave à une autre, de sorte que le commerce était
                                          fortement entravé. Cinq droits différents subsistaient :
                                          le droit de haut-conduit d’entrée et sortie sur les
                                          voitures et bêtes de somme, levé depuis 1597 et selon des modalités révisées en août
                                                1704 ; un droit d’entrée
                                          et issue <ref target="#Foraine"> Foraine</ref> sur les
                                          marchandises, selon un tarif défini en décembre 1704 ; un droit de traverse
                                          établi en 1615 ; un droit
                                          particulier sur les <ref target="#toiles"> toiles</ref>
                                          établi en 1629 ; un droit
                                          d’<ref target="#caution">acquit à caution</ref>. Le
                                          contrôle des voitures de commerce nécessitait plus de 700
                                          postes, presque un dans chaque village. En plus, le traité
                                          de Paris signé en 1718 avec le
                                          duc de <orgName subtype="province" type="pays_et_provinces"> Lorraine</orgName>
                                          établit une ligne de <ref target="#bureaux"> bureaux
                                         </ref> qui, sans être régis par la Ferme générale, mais
                                          placés sous l’inspection de l’<ref target="#intendant">
                                                intendant</ref> de Metz, formait une barrière pour
                                          les marchands qui devaient notamment faire leurs
                                          déclarations et plomber leurs ballots aux bureaux dressés
                                          à cet effet sur la route de Verdun (Mouzon, Consenvoye, et
                                          Verdun), sur la route d’Arlon (à Longwy), de Luxembourg (à
                                          Thionville), le long de la Moselle à Sierck, sur la voie
                                          d’eau de la Sarre à Vaudrevange, à Metz pour la route de
                                          Francfort à Metz, à Teting sur la route de Sarrebruck par
                                          Saint-Avold, et un à Vic (Vic-sur-Seille) pour la route de
                                          <orgName subtype="province" type="pays_et_provinces">Haute-Lorraine</orgName>
                                          (article 62 des Lettres patentes de 1718). Le coût de ce contrôle réduisait la
                                          recette brute de plus du quart : en 1773, les droits rapportaient brut 201 000
                                          livres, mais 150 000 livres net. Il fut régulièrement
                                          question de supprimer ces anciens droits. Dans un mémoire des années 1770 conservé en
                                          G1 132, on peut lire : « les droits de haut conduit,
                                          traverse, issue foraine, acquit à caution et impôts sur
                                          les toiles, quoique justes dans leur principe et d’un
                                          modique objet, n’en sont pas moins fatiguant pour le
                                          commerce ; ils sont fixés sans proportion ; le nombre de
                                          bureaux est infini, il y en a dans presque tous les
                                          villages frontières à cause du mélange des deux
                                          dominations et des terres d’empire. L’obscurité des
                                          ordonnances en rend la régie très difficile et par la
                                          modicité des droits et par la multiplicité des
                                          préposés ».<ref target="#amidon"> amidon</ref>, les poudres,
                                          papiers et <ref target="#cartons"> cartons</ref> et
                                          <ref target="#fer"> marques de fer</ref> : divisée en
                                          deux <ref target="#directions"> directions</ref> (Metz et
                                          Nancy), cette régie leva 671 660 livres par an tant par
                                          exercice que par abonnement vers 1773. </def>
                                    <listBibl><bibl><idno type="ArchivalIdentifier">AN, G1 93B,
                                              <hi rend="italic">Atlas des salines de Lorraine et de Franche-Comté</hi>,
                                                  planche 5 « Routes des entrepôts des sels de
                                                  Lorraine», vers 1755</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN G1 131, dont
                                                  <hi rend="italic">Déclaration du roi (Stanislas, roi de Pologne
                                                  grand-duc de Lithuanie… duc de Lorraine et Bar
                                                  faisant bail des fermes générales des domaines,
                                                  gabelles, salines, tabacs, autres droits de
                                                  Lorraine Barrois à Jean-Louis Bonnard pour six
                                                  années qui commenceront le 1er octobre 1756,
                                                      donnée à Lunéville le 6 novembre 1755</hi>, 64 pages 
                                                  dont « Mémoire sur les droits de Lorraine », vers
                                                  1773  dont dossier 3 sur la régie
                                                  Fouache</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN G1 132, dossier
                                                  1 sur le projet de régie pour la vente des sels à
                                                  l’étranger  dossier 4 sur le droit de
                                                  Haut-Conduit</idno>
                                          </bibl>
                                          <bibl type="sources"><hi rend="italic">Lettres patentes pour l'exécution du
                                                traité conclu à Paris le 21 janvier 1718 entre le
                                                Roi et le duc de Lorraine, en conséquence du traité
                                              de Ryswick, du 30 octobre 1697</hi>, données à Paris le
                                                11 février 1718</bibl>
                                          <bibl type="sources"><hi rend="italic">Lettres patentes du roi, permettant
                                                aux commis de Lorraine et de Bar de poursuivre les
                                              contrebandiers sur les terres du royaume</hi>, 4 février
                                                1738</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt de la Cour des aides qui
                                                confirme avec amende et dépens, deux sentences de la
                                                juridiction des traites de Langres par lesquelles le
                                                nommé Cerf d'Alsace entrepreneur de la fourniture
                                                des étapes aux troupes du roi et Antoine Lallemant
                                                son voiturier, ont été condamnés en 300 livres
                                              d'amende et en confiscation de 4 muids de vin</hi>, 15
                                                décembre 1752</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat du Roi, et
                                                lettres patentes sur icelui, qui autorisent les
                                                employés des fermes de Lorraine Barrois à arrêter
                                                les contrebandiers sur les terres de France,
                                              Versailles</hi>, 9 avril 1754</bibl>
                                          <bibl type="sources"><hi rend="italic">Recueil des édits, ordonnances,
                                                déclarations, tarifs, traités, règlements et arrêts
                                                sur le fait des droits de haut-conduit, entrée et
                                                issue foraine, traverse, impôt sur les toiles et
                                                acquits à caution de Lorraine et Barrois, avec une
                                                instruction pour les receveurs des droits de la
                                              marque des fers, Nancy, Leseure et Drouin</hi>, 1757</bibl>
                                          <bibl>Pierre Boyé, « Le budget de la province de Lorraine
                                                et Barrois sous le règne nominal de Stanislas
                                                (1737-1766), d'après des documents inédits », thèse
                                                pour le doctorat en droit, Impr. Crépin-Leblond,
                                                1896, quatrième partie, chapitre 1 : « La
                                                comptabilité domaniale et la Ferme générale »</bibl>
                                          <bibl>Pierre Boyé, <hi rend="italic">Les salines et le sel en Lorraine au
                                              XVIIIe siècle</hi>, Nancy, 1904</bibl>
                                          <bibl>Charles Hiegel, « L’industrie du sel en Lorraine du
                                                IXe au XVIIe siècle, Thèse de l'Ecole des Chartes »,
                                                1961</bibl>
                                          <bibl>Michel Dorban, « Un aspect du commerce
                                                Luxembourgeois au XVIIIe siècle : le sel lorrain »,
                                              <hi rend="italic">Annales de l’Est</hi>, n° 2, 1975, p. 143-155</bibl>
                                          <bibl>Carole Perceval, « La législation du tabac sous
                                                l'Ancien Régime, Ferme Générale et contrebandiers en
                                                France et en Lorraine » DEA, Université de Nancy Il,
                                                1997</bibl>
                                          <bibl>Pierre Vicq, « Une prise de pouvoir de la Ferme
                                                générale de Lorraine : bois des salines et
                                                faux-saunage de 1698 à la Révolution », thèse de
                                                doctorat en droit, Nancy, Université de Nancy 2,
                                                1998</bibl>
                                          <bibl>François Lormant, « La question fiscale du sel au
                                                temps de Louis XIV et de Vauban : la gabelle et le
                                                faux-saunage en Lorraine », HAL, id : hal-02170889</bibl>
                                        <bibl> Julien Villain, « Les marchands ruraux et la commercialisation des campagnes dans l’Europe du XVIIIe siècle. Le cas de la Lorraine centrale et méridionale », <hi rend="italic">Histoire et Sociétés Rurales</hi>, 2021, 1, p. 43-84</bibl>
                                    </listBibl></sense>
                        </entry>
                  </body>
            </text>
      </TEI>
      