<?xml version='1.0' encoding='UTF-8'?>
<TEI n="174" xmlns="http://www.tei-c.org/ns/1.0" version="3.3.0">
            <teiHeader>
                  <fileDesc>
                        <titleStmt>
                              <title type="notice"> Sucre </title>
                              <author>Marie-Laure Legay</author>
                        </titleStmt>
                        <publicationStmt>
                              <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer
                                    le privilège la Ferme générale dans l'espace français et
                                    européen (1664-1794)Axe 1 : Dictionnaire de la Ferme générale,
                                    objet d'histoire totale </publisher>
                              <pubPlace>
                                    <address>
                                          <addrLine> Maison Européenne des Sciences de l'Homme et de
                                                la Société </addrLine>
                                          <addrLine> 2 rue des cannoniers </addrLine>
                                          <addrLine> 59002 Lille Cedex </addrLine>
                                    </address>
                                    2021-2025
                              </pubPlace>
                              <availability>
                                    <licence>Creative Commons Attribution 3.0 non
                                          transposé (CC BY 3.0)</licence>
                              </availability>
                        </publicationStmt>
                        <seriesStmt>
                              <title> Dictionnaire numérique de la Ferme générale </title>
                              <respStmt>
                                    <resp> Coordinateurs de l'axe : Dictionnaire numérique de la
                                          Ferme générale, objet d'histoire totale. </resp>
                                    <persName> Marie-Laure Legay </persName>
                                    <persName> Thomas Boullu </persName>
                              </respStmt>
                        </seriesStmt>
                        <sourceDesc><bibl>
                                                <idno type="ArchivalIdentifier">AD Rhône, 1C 284,
                                                  procédure contre les raffineurs de Bordeaux pour
                                                  fraude sur les droits de sucre, 1732-1733</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AD Somme, 1C 2927 :
                                                  tarif des denrées coloniales pour 1764</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN G1 31, Traites,
                                                  direction de Bordeaux, Mémoires (1773-1778), f°
                                                  67, mémoire 1428, 22 septembre 1775</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN, G7 1316 :
                                                  dossier 2 sur les sucres de prises et dossier 3</idno>
                                          </bibl>
                                        <bibl>
                                                <idno type="ArchivalIdentifier">AN, H1 1686 :
                                                  « Etat des marchandises des îles et droits acquittés  pour 1775 »</idno>
                                          </bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat du roi qui
                                                ordonne que le droit de trois pour cent de la valeur
                                                des marchandises appartenant à la Ferme du Domaine
                                                d’Occident sera payé pour les sucres et autres
                                                marchandises venant des îles françaises de
                                                l’Amérique sur des vaisseaux français, lesquels
                                                ayant été pris par les armateurs ennemis et repris
                                                sur eux, seront amenés dans les ports du royaume
                                                quoique lesdits vaisseaux soient déclarés de bonne
                                              prise</hi>, 18 octobre 1704</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil qui ordonne que du
                                                droit de trois livres qui sera perçu sur les sucres
                                                de prises provenant tant des îles françaises que des
                                                colonies étrangères, il en sera payé 40 sols au
                                                Fermier du Domaine d’Occident et 20 sols au Fermier
                                              des Cinq grosses fermes</hi>, 4 février 1710 (G7 1316,
                                                dossier 4)</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui ordonne
                                                que les sucres raffinés à Bordeaux et à La Rochelle,
                                                qui seront déclarés pour la Franche-Comté, Alsace,
                                                Genève, Savoie, Piémont, Italie et Espagne,
                                                sortiront par le bureau d'Auxonne quand ils seront
                                                destinés pour la Franche-Comté et l'Alsace par les
                                                bureaux de Louans, Colonges ou Seissel pour Genève
                                                par les bureaux de Pont-Beauvoisin ou de Chaparillan
                                                pour la Savoie et le Piémont et par le bureau
                                                d'Agde pour les sucres qui vont en Italie et en
                                              Espagne par la voie de la Méditerranée</hi>, 27 octobre
                                                1711</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d'Etat qui ordonne
                                                que les Sucres de Cayenne provenant de la traite des
                                                noirs que la Compagnie des Indes fera entrer dans le
                                                royaume par les ports de Bretagne ne paieront que la
                                                moitié des droits de la prévôté de Nantes et des
                                                autres droits locaux, et 40 sols du cent pesant
                                                lorsqu'elle les fera entrer dans l'intérieur des
                                              cinq grosses fermes, pour y être consommés</hi>, 5 juin
                                                1725</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui prescrit
                                                les formalités à observer par les raffineurs de
                                                Bordeaux, La Rochelle, Rouen et Dieppe pour jouir de
                                                la restitution des droits d'entrée sur les sucres
                                                par eux raffinés provenant des sucres bruts des îles
                                                et colonies françaises de l'Amérique et qu'ils
                                              enverront à l'étranger</hi>, 17 novembre 1733</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui ordonne
                                                que les sucres bruts venant des colonies françaises,
                                                seront exempts pendant la durée de la guerre, des
                                                droits portés par les lettres patentes de 1717 et
                                              des droits locaux dus en Bretagne</hi>, 4 juillet 1762</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’État du Roi, qui
                                                ordonne qu'à l'avenir les sucres raffinés, en pains
                                                en poudre ou candi, provenant du commerce des iles
                                                de France de Bourbon, payeront comme ceux provenant
                                                des isles colonies françaises de l'Amérique,
                                                vingt-deux livres dix sous par quintal à toutes les
                                                entrées du Royaume, tant de la Bretagne des
                                                provinces réputées étrangères, que des cinq grosses
                                              fermes</hi>, 5 avril 1775</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat concernant
                                                les raffineries de sucres établies dans les
                                              différents ports du royaume</hi>, 25 mai 1786</bibl>
                                          <bibl type="sources">Vivent Magnien, <hi rend="italic">Recueil alphabétique
                                                des droits de traites uniformes, de ceux d'entrée et
                                                de sortie, des cinq grosses fermes, de douane de
                                              Lyon et de Valence</hi>, tome 3, 1786, p. 98-110</bibl>
                                          <bibl>Richard Sheridan, <hi rend="italic">Sugar and slavery: an economic
                                              history of the British West Indies</hi>, Eagle Hall,
                                                Caribbean University Press, 1974</bibl>
                                          <bibl>Robert Louis Stein, <hi rend="italic">The French sugar business in
                                              the eighteenth century</hi>, Bâton-Rouge, Louisiana
                                                University State, 1988</bibl>
                                        <bibl>Jean Meyer, <hi rend="italic">Histoire du sucre</hi>, Paris,
                                                Desjonquières, 1989</bibl>
                                          <bibl>Paul Butel, <hi rend="italic">Histoire des Antilles françaises,
                                              XVIe-XXe siècles</hi>, Paris, Perrin, 2002</bibl>
                                          <bibl>François Crouzet, <hi rend="italic">La guerre économique
                                              franco-anglaise au XVIIIe siècle</hi>, Paris, Fayard,
                                                2008</bibl>
                                          <bibl>Maud Villeret, <hi rend="italic">Le goût de l’or blanc. Le sucre en
                                              France au XVIIIe siècle</hi>, Rennes-Tours, PUR-PUFR,
                                                2017</bibl>
                                    </sourceDesc></fileDesc>
                  <encodingDesc>
                        <projectDesc source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
                              <p> Le projet FermGé vise à étudier l’impact d’une organisation
                                    fiscale (1664-1794), discriminante mais rationnelle, sur les
                                    territoires et les sociétés de la France moderne/p </p>
                        </projectDesc>
                  </encodingDesc>
                  <profileDesc>
                        <textClass>
                              <keywords scheme="#fr_RAMEAU">
                                    <list>
                                          <item> Histoire -- Histoire moderne -- Histoire fiscale
                                          </item>
                                    </list>
                              </keywords>
                        </textClass>
                  </profileDesc>
                  <revisionDesc>
                        <change type="AutomaticallyEncoded"> 2022-12-20T15:38:56.183541+2:00
                        </change>
                  </revisionDesc>
            </teiHeader>
            <text>
                  <body>
                        <entry n="174" type="S" xml:id="Sucre">
                              <form type="lemma">
                                    <orth> Sucre </orth>
                              </form>
                              <sense>
                                    <def> La fiscalité du sucre était complexe : l’objectif était
                     d’encourager la production coloniale, le débarquement en
                     métropole des sucres bruts, le raffinage métropolitain et
                     l’exportation à l’étranger. Grâce à Saint-Domingue qui
                     fournit différentes qualités de sucre (terré et brut), et
                     dans une moindre mesure aux îles du Vent (Guadeloupe,
                     Martinique et Sainte-Lucie), la France devint la première
                     puissance importatrice de sucre en Europe. Pour éviter de
                     dépendre des raffineries d’Amsterdam, Louis XIV mit en
                     place des mesures favorables au raffinage français (tarifs
                     de 1664, 1665 et 1667) et encouragea l’installation d’ouvriers qualifiés originaires
                                          d’Europe du Nord, détenteurs du savoir technique
                                          du raffinage. A partir de 1684, aucune nouvelle raffinerie ne put être établie dans les
                                          îles. Les <placeName type="lieux_habitations"> villes de
                                                Bordeaux</placeName>, <ref target="#Rochelle">La
                                                Rochelle</ref>, <ref target="#Nantes"> Nantes
                                         </ref>, directement connectées aux colonies sucrières,
                                          mais aussi les <placeName type="lieux_habitations"> villes
                                                d’Angers</placeName>, Saumur et Orléans devinrent
                                          d’importants centres de production. <ref target="#Occident">Domaine d’Occident
                                         </ref>) et les droits de <ref target="#traites"> traites
                                         </ref> à visée protectionniste (définis par les tarifs
                                          d’entrée dans les <ref target="#fermes">Cinq grosses
                                                fermes</ref>). Les sucres furent d’emblée taxés
                                          sur place dans les <ref target="#colonies"> colonies
                                         </ref> au titre du <ref target="#Occident">Domaine d’Occident</ref>. A partir de 1671, 3% de leur valeur furent en outre
                                          prélevés par la Ferme du Domaine à l’arrivée en métropole
                                          (3, 5 % à partir de 1727), tant
                                          dans les provinces des <ref target="#fermes">Cinq grosses
                                                fermes</ref> que dans celles <ref target="#étrangères">réputées étrangères</ref>,
                                          c’est-à-dire dans les ports privilégiés <ref target="#bretons"> bretons</ref> (droits de Prévôté
                                          de <ref target="#Nantes"> Nantes</ref>). La Ferme du
                                          <ref target="#Occident">Domaine d'Occident</ref>
                                          enregistra en 1774 une recette
                                        de 300 000 livres sur les sucres. D’après l’état des marchandises des îles et droits acquittés pour 1775, les sucres (tant sucre brut, sucre terré) rapportèrent au total au fisc 4 542 720 livres (AN, H1 1686).
                                        <figure>
                                            <graphic url="/static/images/sucres.jpg"/>
                                            <head/>
                                        </figure>

                                        <ref target="#traites"> traites</ref>, le tarif de
                                                1667, repris de celui de
                                                1664, taxait à l’entrée
                                          dans l’<ref target="#Etendue"> Etendue</ref> les sucres
                                          des îles d’Amérique à quatre livres le cent pesant, quelle
                                          que fût leur qualité (brut ou terré), et tous les sucres
                                          raffinés étrangers à 22 livres dix sols (40 livres suivant
                                          l’arrêt du 17 mars 1782 qui
                                          durcit la protection douanière). Pour encourager le
                                          raffinage en métropole, la taxation des sucres raffinés
                                          dans les <ref target="#colonies"> colonies</ref>
                                          françaises demeura élevée à l’instar de celle qui pesaient
                                          sur les sucres étrangers. Tant le sucre raffiné des îles
                                          d’Amérique que celui des iles de France et de Bourbon
                                          (bien moins important en volume), restèrent pénalisés. En
                                          métropole, le gouvernement dut régler le sort des sucres
                                          raffinés dans les provinces « réputées étrangères » du
                                          royaume notamment à <ref target="#Bordeaux"> Bordeaux
                                         </ref> et <ref target="#Rochelle">La Rochelle</ref> :
                                          d’abord fixés à 15 livres le cent pesant (tarif de 1664), les droits furent
                                          réduits à 3 livres deux sols et trois deniers en 1725. Les sucres arrivés à
                                                <ref target="#Marseille"> Marseille</ref> et
                                          raffinés dans la ville devaient, selon les instructions
                                          pour le <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Languedoc"> Languedoc</ref>
                                         </orgName> du premier septembre 1691, quatre livres par quintal au titre du Domaine d’<ref target="#Occident">Occident</ref> et quatre
                                          livres pour la Ferme générale quand ils entraient dans le
                                          royaume. <ref target="#Bordeaux"> Bordeaux</ref>, <ref target="#Rochelle">La Rochelle</ref>, <ref target="#Rouen"> Rouen</ref> et <ref target="#Dieppe"> Dieppe</ref> en prévoyant la
                                          restitution des droits d’entrée sur les sucres bruts des
                                          îles, dès lors que les produits raffinés étaient destinés
                                          à l’exportation. Pour obtenir cette restitution, les
                                          raffineurs devaient prendre des <ref target="#caution">acquits à caution</ref> dans les bureaux
                                          de sortie (Auxonne, Louans, Collonges ou Seyssel pour
                                          Genève, <ref target="#Pont-de-Beauvoisin">
                                                Pont-de-Beauvoisin</ref> ou Chaparillan pour la
                                                <ref target="#Savoie"> Savoie</ref> et le Piémont ;
                                          Agde pour l’Italie et l’<ref target="#Espagne"> Espagne
                                         </ref> par la voie de la Méditerranée). Une partie des
                                          droits fut remboursée d’abord (cinq livres, 12 sous et 6
                                          deniers par quintal selon l’arrêt du 17 novembre 1733). Cette faveur engagea
                                          les raffineurs de <ref target="#Bordeaux"> Bordeaux</ref>
                                          à considérer les droits d’entrée comme simples droits de
                                          consommation : « les droits imposés par ce règlement
                                          [celui de 1717 ] sont, comme
                                          tous les autres droits des marchandises venant de
                                          l’étranger, purement droits d’entrée, dus et acquis au
                                          Fermier dès l’instant de l’arrivée, leur perception est
                                          indépendante de la consommation des marchandises ou
                                          denrées, bien que l’usage, à Bordeaux, est de caractériser
                                          ces droits comme droit de consommation. Or, les sucres des
                                          colonies, quoique destinés aux raffineries, objet
                                          privilégié dans le cas d’exportation à l’étranger, après
                                          avoir été raffinés dans le royaume, sont assujettis aux
                                          droits. Ils les acquittent à leur arrivée. Ils sont
                                          conduits de suite dans la raffinerie ou à la sortie de
                                          l’entrepôt s’ils en sont tirés à cet emploi, le
                                          remboursement du droit d’entrée que les sucres raffinés
                                          acquièrent par leur exportation à l’étranger est une
                                          faveur particulière qui ne dénature ni ne peut dénaturer
                                          la constitution du droit auquel le remboursement même
                                          confirme s’il en est besoin son caractère propre de droit
                                          d’entrée ». En 1732, une
                                          procédure extraordinaire fut intentée contre Daniel
                                          Dormand, Jean Lambert, Elie Hagon, Jean Laroque, Baltazar
                                          David et Antoine Royés, raffineurs et négociants à <ref target="#Bordeaux"> Bordeaux</ref>, tous assignés
                                          à <ref target="#Lyon"> Lyon</ref> après l’arrestation de
                                          leurs voitures sur le chemin entre Belley et Seyssel,
                                          prises sans acquits. Finalement la totalité des droits
                                          d’entrée fut restituée à tous les raffineurs des ports
                                          français qui exportaient, afin de lutter contre la
                                          concurrence des sucres raffinés étrangers (1786). Comme il n’existait pas
                                          de droits de sortie sur les sucres raffinés en métropole
                                          et destinés à l’exportation (article final du tarif de
                                                1664), la filière de
                                          production de sucres raffinés issus des îles était
                                          entièrement défiscalisée.<ref target="#Occident">Domaine d’Occident</ref> et la Ferme générale. Outre les 3 %,
                                          le premier prétendait lever sur les sucres de prises un
                                          droit de 40 sols par cent pesant. Il jugeait qu’une grande
                                          partie des prises de guerre était originaire des îles
                                          françaises. Bénéficiaires de ce droit de prise, les
                                          Fermiers généraux formèrent opposition. Lors de la
                                          première année du bail Charles Ferreau et Charles Ysambert
                                          (octobre 1703 -septembre 1704), les droits avaient
                                          rapporté 56 059 livres ; la 2e année : 126 773 livres ; la
                                          3e année : 39 153 livres ; la 4e année 20 404 et la 6e
                                          année : 47 743 livres, soit en tout 290 135 livres, non
                                          compris les <placeName type="lieux_controle"> bureaux de
                                                Dunkerque</placeName> et Ypres pour la première
                                          année, les <placeName type="lieux_controle"> bureaux de
                                                Roscoff</placeName>, Audierne, Auray, <ref target="#Nantes"> Nantes</ref>, <ref target="#Dunkerque"> Dunkerque</ref> et Ypres pour
                                          la 2e année. Comme les intérêts des deux fermes du roi
                                          étaient somme toute communs et consistaient surtout à
                                          faire en sorte que les sucres fussent ramenés en métropole
                                          et ne passent pas à l’étranger, il fut établi que, sur le
                                          droit de 3 livres perçu sur les sucres de prises provenant
                                          des îles françaises de l’Amérique, 40 sols devaient être
                                          réglés au fermier du Domaine, et 20 sols au fermier des
                                          Cinq grosse <ref target="#fermes"> fermes</ref>. Ainsi,
                                          le sucre blanc provenant de l’escadre commandée par
                                          Duguay-Troin pour l’expédition de Rio de Janeiro rapporta,
                                          d’après le bureau général des Fermes à Rennes, 8 856
                                          livres au titre des droits de 3% et 13 236 livres pour les
                                          40 sols (28 avril 1713). A
                                          l’égard des sucres de prises provenant des colonies
                                          étrangères, le fermier du <ref target="#Occident">Domaine d’Occident</ref> devait pouvoir
                                          jouir du même droit de 40 sols pendant le temps de la
                                          guerre seulement : les sucres pris et repris sur l’ennemi
                                          étaient traités comme s’ils venaient en droiture des îles.
                                          Notons enfin que les sucres de la <ref target="#noirs">traite des noirs</ref> étaient déchargés de
                                          la moitié des droits à l’entrée, sur présentation de
                                          justificatifs signés soit de l’intendant des Îles, soit
                                          d’un commis du <ref target="#Occident">Domaine d’Occident</ref> ou d’un commissaire-ordonnateur. </def>
                                    <listBibl><bibl>
                                                <idno type="ArchivalIdentifier">AD Rhône, 1C 284,
                                                  procédure contre les raffineurs de Bordeaux pour
                                                  fraude sur les droits de sucre, 1732-1733</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AD Somme, 1C 2927 :
                                                  tarif des denrées coloniales pour 1764</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN G1 31, Traites,
                                                  direction de Bordeaux, Mémoires (1773-1778), f°
                                                  67, mémoire 1428, 22 septembre 1775</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN, G7 1316 :
                                                  dossier 2 sur les sucres de prises et dossier 3</idno>
                                          </bibl>
                                        <bibl>
                                                <idno type="ArchivalIdentifier">AN, H1 1686 :
                                                  « Etat des marchandises des îles et droits acquittés  pour 1775 »</idno>
                                          </bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat du roi qui
                                                ordonne que le droit de trois pour cent de la valeur
                                                des marchandises appartenant à la Ferme du Domaine
                                                d’Occident sera payé pour les sucres et autres
                                                marchandises venant des îles françaises de
                                                l’Amérique sur des vaisseaux français, lesquels
                                                ayant été pris par les armateurs ennemis et repris
                                                sur eux, seront amenés dans les ports du royaume
                                                quoique lesdits vaisseaux soient déclarés de bonne
                                              prise</hi>, 18 octobre 1704</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil qui ordonne que du
                                                droit de trois livres qui sera perçu sur les sucres
                                                de prises provenant tant des îles françaises que des
                                                colonies étrangères, il en sera payé 40 sols au
                                                Fermier du Domaine d’Occident et 20 sols au Fermier
                                              des Cinq grosses fermes</hi>, 4 février 1710 (G7 1316,
                                                dossier 4)</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui ordonne
                                                que les sucres raffinés à Bordeaux et à La Rochelle,
                                                qui seront déclarés pour la Franche-Comté, Alsace,
                                                Genève, Savoie, Piémont, Italie et Espagne,
                                                sortiront par le bureau d'Auxonne quand ils seront
                                                destinés pour la Franche-Comté et l'Alsace par les
                                                bureaux de Louans, Colonges ou Seissel pour Genève
                                                par les bureaux de Pont-Beauvoisin ou de Chaparillan
                                                pour la Savoie et le Piémont et par le bureau
                                                d'Agde pour les sucres qui vont en Italie et en
                                              Espagne par la voie de la Méditerranée</hi>, 27 octobre
                                                1711</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d'Etat qui ordonne
                                                que les Sucres de Cayenne provenant de la traite des
                                                noirs que la Compagnie des Indes fera entrer dans le
                                                royaume par les ports de Bretagne ne paieront que la
                                                moitié des droits de la prévôté de Nantes et des
                                                autres droits locaux, et 40 sols du cent pesant
                                                lorsqu'elle les fera entrer dans l'intérieur des
                                              cinq grosses fermes, pour y être consommés</hi>, 5 juin
                                                1725</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui prescrit
                                                les formalités à observer par les raffineurs de
                                                Bordeaux, La Rochelle, Rouen et Dieppe pour jouir de
                                                la restitution des droits d'entrée sur les sucres
                                                par eux raffinés provenant des sucres bruts des îles
                                                et colonies françaises de l'Amérique et qu'ils
                                              enverront à l'étranger</hi>, 17 novembre 1733</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui ordonne
                                                que les sucres bruts venant des colonies françaises,
                                                seront exempts pendant la durée de la guerre, des
                                                droits portés par les lettres patentes de 1717 et
                                              des droits locaux dus en Bretagne</hi>, 4 juillet 1762</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’État du Roi, qui
                                                ordonne qu'à l'avenir les sucres raffinés, en pains
                                                en poudre ou candi, provenant du commerce des iles
                                                de France de Bourbon, payeront comme ceux provenant
                                                des isles colonies françaises de l'Amérique,
                                                vingt-deux livres dix sous par quintal à toutes les
                                                entrées du Royaume, tant de la Bretagne des
                                                provinces réputées étrangères, que des cinq grosses
                                              fermes</hi>, 5 avril 1775</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat concernant
                                                les raffineries de sucres établies dans les
                                              différents ports du royaume</hi>, 25 mai 1786</bibl>
                                          <bibl type="sources">Vivent Magnien, <hi rend="italic">Recueil alphabétique
                                                des droits de traites uniformes, de ceux d'entrée et
                                                de sortie, des cinq grosses fermes, de douane de
                                              Lyon et de Valence</hi>, tome 3, 1786, p. 98-110</bibl>
                                          <bibl>Richard Sheridan, <hi rend="italic">Sugar and slavery: an economic
                                              history of the British West Indies</hi>, Eagle Hall,
                                                Caribbean University Press, 1974</bibl>
                                          <bibl>Robert Louis Stein, <hi rend="italic">The French sugar business in
                                              the eighteenth century</hi>, Bâton-Rouge, Louisiana
                                                University State, 1988</bibl>
                                        <bibl>Jean Meyer, <hi rend="italic">Histoire du sucre</hi>, Paris,
                                                Desjonquières, 1989</bibl>
                                          <bibl>Paul Butel, <hi rend="italic">Histoire des Antilles françaises,
                                              XVIe-XXe siècles</hi>, Paris, Perrin, 2002</bibl>
                                          <bibl>François Crouzet, <hi rend="italic">La guerre économique
                                              franco-anglaise au XVIIIe siècle</hi>, Paris, Fayard,
                                                2008</bibl>
                                          <bibl>Maud Villeret, <hi rend="italic">Le goût de l’or blanc. Le sucre en
                                              France au XVIIIe siècle</hi>, Rennes-Tours, PUR-PUFR,
                                                2017</bibl>
                                    </listBibl></sense>
                        </entry>
                  </body>
            </text>
      </TEI>
      