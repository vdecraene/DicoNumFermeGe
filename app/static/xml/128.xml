<?xml version='1.0' encoding='UTF-8'?>
<TEI n="128" xmlns="http://www.tei-c.org/ns/1.0" version="3.3.0">
            <teiHeader>
                  <fileDesc>
                        <titleStmt>
                              <title type="notice"> Dieppe </title>
                              <author>Marie-Laure Legay</author>
                        </titleStmt>
                        <publicationStmt>
                              <publisher> MESHS de Lille dans le cadre de l'ANR FermeGé Administrer
                                    le privilège la Ferme générale dans l'espace français et
                                    européen (1664-1794)Axe 1 : Dictionnaire de la Ferme générale,
                                    objet d'histoire totale </publisher>
                              <pubPlace>
                                    <address>
                                          <addrLine> Maison Européenne des Sciences de l'Homme et de
                                                la Société </addrLine>
                                          <addrLine> 2 rue des cannoniers </addrLine>
                                          <addrLine> 59002 Lille Cedex </addrLine>
                                    </address>
                                    2021-2025
                              </pubPlace>
                              <availability>
                                    <licence>Creative Commons Attribution 3.0 non
                                          transposé (CC BY 3.0)</licence>
                              </availability>
                        </publicationStmt>
                        <seriesStmt>
                              <title> Dictionnaire numérique de la Ferme générale </title>
                              <respStmt>
                                    <resp> Coordinateurs de l'axe : Dictionnaire numérique de la
                                          Ferme générale, objet d'histoire totale. </resp>
                                    <persName> Marie-Laure Legay </persName>
                                    <persName> Thomas Boullu </persName>
                              </respStmt>
                        </seriesStmt>
                        <sourceDesc><bibl><idno type="ArchivalIdentifier">AN, G1 73, pièce
                                                  17 ter, employés, Direction de Rouen, 1785</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN, G1 91, dossier
                                                  51 : « Franchise de Dieppe »</idno>
                                          </bibl>
                                          <bibl type="sources"><hi rend="italic">Ordonnances sur le fait des gabelles
                                              des aides…</hi>, mai et juin 1680, chapitre XIV, article
                                                1 à 18</bibl>
                                          <bibl type="sources"><hi rend="italic">Déclaration portant règlement pour
                                                la distribution du sel aux habitans des villes de
                                                Dieppe, d’Eu, du Bourg d’Ault, S. Valéry-sur-Somme,
                                                Harfleur, Le Havre de Grace, Fescamp, S. Vallery en
                                                Caux et autres qui jouissent du privilège de
                                                franchise, et pour la réception des bourgeois pour
                                              jouir de ladite franchise</hi>, Versailles, 22 août 1711</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d'Etat portant
                                                règlement pour les privilèges des habitants de la
                                              ville de Dieppe, pour jouir de la franchise du sel</hi>,
                                                2 août 1712</bibl>
                                          <bibl type="sources"><hi rend="italic">Déclaration du Roy pour réprimer le
                                              faux-saunage qui se fait à Dieppe</hi>, Versailles, 15
                                                octobre 1712</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui casse
                                                une sentence des officiers du grenier de Dieppe pour
                                                avoir fait mainlevée d'environ un minot de sel de
                                                franchise, trouvé dans la maison du nommé René
                                                Prevost, matelot, au-delà de la quantité nécessaire
                                                pour sa provision, sous prétexte que l'amas de sel
                                              de franchise n'est point défendu</hi>, 7 mai 1748</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui ordonne
                                                qu’en exécution de l’édit de novembre 1771, les
                                                habitants des villes du Hâvre-de-Grâce, Harfleur,
                                                Fécamp, Saint-Valéry-en-Caux, Dieppe, Honfleur, Eu
                                                et Tréport de Saint-Valéry-Sur-Somme et d’Ault,
                                                seront tenus d'acquitter pour le sel les 8 sous pour
                                              livre</hi>, 2 mai 1773</bibl>
                                          <bibl>Reynald Abad, <hi rend="italic">Le grand marché. L’approvisionnement
                                              alimentaire de Paris sous l’Ancien régime</hi>, Paris,
                                                Fayard, 2002</bibl>
                                          <bibl>Caroline Le Mao, <hi rend="italic">Les villes portuaires maritimes
                                              dans la France moderne</hi>, Paris, A. Colin, 2015</bibl>
                                    </sourceDesc></fileDesc>
                  <encodingDesc>
                        <projectDesc source="https://anr.fr/fr/projets-finances-et-impact/projets-finances/projet/funded/project/anr-21-ce41-0019/?tx_anrprojects_funded%5Bcontroller%5D=Fundedamp;cHash=5c64d94ca826534590e484c6f39658b3">
                              <p> Le projet FermGé vise à étudier l’impact d’une organisation
                                    fiscale (1664-1794), discriminante mais rationnelle, sur les
                                    territoires et les sociétés de la France moderne/p </p>
                        </projectDesc>
                  </encodingDesc>
                  <profileDesc>
                        <textClass>
                              <keywords scheme="#fr_RAMEAU">
                                    <list>
                                          <item> Histoire -- Histoire moderne -- Histoire
                                                commerciale </item>
                                          <item> Histoire -- Histoire moderne -- Histoire fiscale </item>
                                          <item> Histoire -- Histoire moderne -- Histoire politique
                                          </item>
                                    </list>
                              </keywords>
                        </textClass>
                  </profileDesc>
                  <revisionDesc>
                        <change type="AutomaticallyEncoded"> 2022-12-14T10:53:24.776890+2:00
                        </change>
                  </revisionDesc>
            </teiHeader>
            <text>
                  <body>
                        <entry n="128" type="D" xml:id="Dieppe">
                              <form type="lemma">
                                    <orth> Dieppe </orth>
                              </form>
                              <sense>
                                    <def> Dieppe, détruite par la flotte anglo-hollandaise en 1694, constitua au XVIIIe
                                          siècle une ville commerciale d’envergure régionale centrée
                                        sur la <ref target="pêche">pêche</ref>. Le port était alors le premier fournisseur
                                          de Paris en poisson frais, livré dans des bacs d’eau salée
                                          par Rouen et la <ref target="#Seine"> Seine</ref>. Les
                                          habitants de Dieppe étaient affranchis des Gabelles depuis
                                                1420. Ils avaient donc
                                          le droit de faire venir de <ref target="#Brouage"> Brouage
                                         </ref> le sel nécessaire pour leur consommation et leurs
                                          activités halieutiques. En revanche, ils ne pouvaient en
                                          faire un commerce extérieur. L’adjudication du sel de <ref target="#franchise"> franchise</ref> pour les
                                          bourgeois de Dieppe se faisait à l’audience du grenier à
                                                <ref target="#sel"> sel</ref> pour la quantité de
                                          10 muids. Elle était annoncée par un cri public et
                                          affichée. Ce <ref target="#privilège"> privilège</ref>,
                                          confirmé à plusieurs reprises, dut être encadré pour
                                          éviter le faux-saunage.Divers pratiques abusives des
                                          autorités, notamment des officiers du grenier, limitaient
                                          en effet la portée de l’ordonnance des gabelles de 1680. Celle-ci indiquait que
                                          pour bénéficier du privilège du sel, il fallait avoir été
                                          résident pendant trois années au cours desquelles le
                                          prétendant à la bourgeoisie devait avoir pris son sel au
                                          grenier comme tout gabellant. Cependant, les officiers du
                                                <ref target="#grenier"> grenier</ref> de Dieppe
                                          recevaient les bourgeois sans qu’ils eussent forcément
                                          acquis la résidence. En outre, le nombre de <ref target="#minots"> minots</ref> distribués n’était
                                          pas fixé comme ailleurs en <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Poitou"> Poitou</ref></orgName>, <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Bretagne"> Bretagne</ref>
                                         </orgName> ou <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Artois"> Artois</ref></orgName>, limitrophes aux pays de <ref target="#grandes_gabelles">grandes gabelles</ref>, et la
                                          distribution se faisait sans véritable contrôle de manière
                                          plutôt anarchique. De même, le <orgName subtype="administrations_juridictions_royales" type="administrations_juridictions"> grenier de
                                                Dieppe</orgName> n’utilisait pas de <ref target="#trémie"> trémie</ref> homologuée… Il
                                          fallut donc compléter la législation. La déclaration du
                                          mois d’août 1711 renforça les
                                          contraintes : les bourgeois devaient faire leur demande
                                          auprès du greffe de l’Hôtel-de-ville et s’inscrire sur un
                                          registre tenu par le commis de la Ferme générale à cet
                                          effet. La distribution fut proportionnée à l’importance de
                                          la famille dont la composition devait également être
                                          déclarée. De même, les maîtres de navires chargés de sel
                                          devaient doubler leur déclaration auprès du greffe du
                                          grenier d’une inscription auprès du commis de la Ferme
                                          générale, à l’instar des marchands propriétaires. La
                                          déclaration royale d’octobre 1712
 engagea les marchands propriétaires de sel et les
                                          marchands saleurs à utiliser la <ref target="#trémie">
                                                trémie</ref> officielle. Le pouvoir du commis de la
                                          Ferme préposé au sel de <ref target="#franchise">
                                                franchise</ref> fut encore renforcé en lui
                                          déléguant l’inspection des listes que les officiers du
                                          grenier à sel établissaient pour chaque corps de métier
                                          qui jouissait du sel de <ref target="#salaison"> salaison
                                         </ref>, mais aussi en l’autorisant à faire des visites
                                          chez les saleurs pour vérifier les déclarations du volume
                                          de harengs salés… <ref target="#accommodements"> accommodements</ref>
                                          locaux. Par leur sentence du 16 novembre 1747 par exemple, les officiers du <ref target="#grenier"> grenier</ref> déchargèrent la
                                          famille du matelot René Prévost des charges de fraude
                                          avérée et constatée par les commis de la Ferme. Ces
                                          derniers avaient trouvé lors d’une visite un amas
                                          important de sel d’autant plus suspect que René Prévost
                                          n’était pas bourgeois de la ville. Comme partout dans le
                                          royaume, le privilège des Dieppois fut mis en cause par
                                          les mesures de l’abbé <ref target="#Terray"> Terray</ref>
                                          qui imposa en 1771 huit sols
                                          pour <ref target="#livre"> livre</ref> sur tous les
                                          droits de la Ferme générale. Les villes normandes et
                                          picardes qui, comme Dieppe, jouissaient du sel de
                                          franchise, tentèrent d’ignorer cette loi de sorte que le
                                          roi dut leur imposer son application par arrêt en 1773. En
1785, la Ferme générale employait pour le
                                          contrôle de la Franchise de Dieppe un inspecteur, un
                                          contrôleur et six gardes qui procédaient aux visites des
                                          emplacements des sels de pêche et de franchise des
                                          bourgeois et de la salaison du poisson.<ref target="#Havre">Le Havre</ref>, Dieppe bénéficia en
                                          outre de la modération des droits sur la pêche à la morue. Pour autant, les marchands et propriétaires de navires
                                          durent déclarer le nom, le port d’attache et les
                                          intéressés au bâtiment aux commis de la Ferme générale car
                                          plusieurs d’entre eux fraudaient en se faisant passer
                                          comme pêcheurs de Dieppe, tout en étant en réalité
                                          pêcheurs de ports non privilégiés comme le Tréport. Ce fut
                                          le cas de Jean Billard, maître du Marie-Catherine, qui, à
                                          l’aide de Nicolas Dandasne, bourgeois de Dieppe, voulut
                                          payer les droits sur sa pêche dans cette ville. La
                                                Ferme générale procéda contre lui, mais le juge des
                                                  <ref target="#traites"> traites</ref> de Dieppe
                                                dénonça la saisie par sa sentence du 23 février
                                                  1723, approuvé en cela
                                                par la <ref target="#Cour"> Cour</ref> des aides de
                                                Rouen. Pour lutter contre ces <ref target="#fraudes"> fraudes</ref> et enregistrer la
                                          pêche, le bureau aux poissons de la Ferme générale
                                          comprenait en 1785 : un
                                          receveur, un contrôleur, un commis aux <ref target="#caution">acquits à caution</ref>, deux commis aux
                                          expéditions, un contrôleur à la porte de la Barre et un
                                          contrôleur à la porte du Pont. Le bureau des traites
                                          disposait quant à lui d’un receveur, un contrôleur, un
                                          visiteur, un commis aux expéditions et un garde emballeur.
                                          Vis-à-vis des <ref target="#aides"> aides</ref>, la
                                          ville était soumise aux règlements adoptés pour la
                                          province de <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Normandie"> Normandie</ref></orgName>. Elle contesta certains droits levés par les
                                          sous-fermiers, notamment celui de <ref target="#courtiers-jaugeurs"> courtiers-jaugeurs
                                         </ref> et inspecteurs aux <ref target="#boissons">
                                                boissons</ref>, très impopulaires en <orgName subtype="province" type="pays_et_provinces">
                                                <ref target="#Normandie"> Normandie</ref></orgName>. </def>
                                    <listBibl><bibl><idno type="ArchivalIdentifier">AN, G1 73, pièce
                                                  17 ter, employés, Direction de Rouen, 1785</idno>
                                          </bibl>
                                          <bibl>
                                                <idno type="ArchivalIdentifier">AN, G1 91, dossier
                                                  51 : « Franchise de Dieppe »</idno>
                                          </bibl>
                                          <bibl type="sources"><hi rend="italic">Ordonnances sur le fait des gabelles
                                              des aides…</hi>, mai et juin 1680, chapitre XIV, article
                                                1 à 18</bibl>
                                          <bibl type="sources"><hi rend="italic">Déclaration portant règlement pour
                                                la distribution du sel aux habitans des villes de
                                                Dieppe, d’Eu, du Bourg d’Ault, S. Valéry-sur-Somme,
                                                Harfleur, Le Havre de Grace, Fescamp, S. Vallery en
                                                Caux et autres qui jouissent du privilège de
                                                franchise, et pour la réception des bourgeois pour
                                              jouir de ladite franchise</hi>, Versailles, 22 août 1711</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d'Etat portant
                                                règlement pour les privilèges des habitants de la
                                              ville de Dieppe, pour jouir de la franchise du sel</hi>,
                                                2 août 1712</bibl>
                                          <bibl type="sources"><hi rend="italic">Déclaration du Roy pour réprimer le
                                              faux-saunage qui se fait à Dieppe</hi>, Versailles, 15
                                                octobre 1712</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui casse
                                                une sentence des officiers du grenier de Dieppe pour
                                                avoir fait mainlevée d'environ un minot de sel de
                                                franchise, trouvé dans la maison du nommé René
                                                Prevost, matelot, au-delà de la quantité nécessaire
                                                pour sa provision, sous prétexte que l'amas de sel
                                              de franchise n'est point défendu</hi>, 7 mai 1748</bibl>
                                          <bibl type="sources"><hi rend="italic">Arrêt du Conseil d’Etat qui ordonne
                                                qu’en exécution de l’édit de novembre 1771, les
                                                habitants des villes du Hâvre-de-Grâce, Harfleur,
                                                Fécamp, Saint-Valéry-en-Caux, Dieppe, Honfleur, Eu
                                                et Tréport de Saint-Valéry-Sur-Somme et d’Ault,
                                                seront tenus d'acquitter pour le sel les 8 sous pour
                                              livre</hi>, 2 mai 1773</bibl>
                                          <bibl>Reynald Abad, <hi rend="italic">Le grand marché. L’approvisionnement
                                              alimentaire de Paris sous l’Ancien régime</hi>, Paris,
                                                Fayard, 2002</bibl>
                                          <bibl>Caroline Le Mao, <hi rend="italic">Les villes portuaires maritimes
                                              dans la France moderne</hi>, Paris, A. Colin, 2015</bibl>
                                    </listBibl></sense>
                        </entry>
                  </body>
            </text>
      </TEI>
      