# Fichier contenant les variables relatives au fonctionnement global de l'application

from flask import Flask
# Flask est le framework de developpement web
from flask_sqlalchemy import SQLAlchemy
# Flask_sqlalchemy est le module permettant de générer les requetes SQL en Python
import os
# Le module os permet de gérer les chemins de fichiers sur différents systèmes d'exploitation
from .constantes import SECRET_KEY


# Ci-dessous se trouve nos variables globales:
chemin_actuel = os.path.dirname(os.path.abspath(__file__))
templates = os.path.join(chemin_actuel, "templates")
statics = os.path.join(chemin_actuel, "static")

parent_directory = os.path.dirname(chemin_actuel)

# Construire le chemin relatif vers la base de données
db_filename = 'bdd_v2.db'
db_path = os.path.join(parent_directory, db_filename)

# Cette variable app permet de faire le lien entre l'application, les templates assurant la visualisation web et
# les éléments statiques.
app = Flask("Application",
            template_folder=templates,
            static_folder=statics)

# Elements de configuration de l'application et d'association de la base de données à l'application
app.config['SECRET_KEY'] = SECRET_KEY

print(db_path)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + db_path
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False


db = SQLAlchemy(app)

# Initialisation de l'application
db.init_app(app)

from .routes import generic






