# Fichier permettant de transformer l'ensemble des fichiers en une application exécutable
# Ce fichier n'est en théorie plus requis depuis la version Python 2.7. Nous le gardons
# par précaution et soucis des utilisateurs possédant une version ancienne de Python.