# Fichier contenant les "constantes", à savoirles valeurs connues et non modifiables au sein des encoding_scripts

#Import permettant d'afficher des messages d'erreurs.
from warnings import warn
# Import permettant de parser des fichiers XML.
from lxml import etree as ET


#La variable résultats par page est déclarée ici pour afficher 10 résultats par page lors de l'utilisation de la méthode .paginate
RESULTATS_PAR_PAGES = 10
RESULTATS_PAR_PAGES_RECHERCHE_AVANCEE = 1000

# Clé secrète requise pour l'initialisation de l'application.
SECRET_KEY = "SECRET KEY"
if SECRET_KEY == "SECRET KEY":
    warn("Le secret par défaut n'a pas été changé, vous devriez le faire", Warning)

# Constantes constituées des fichiers XSL ou XML parsés grâce au module Element Tree (ET)
CLEAN_CORPUS = ET.parse("app/static/xml/test_clean.xml")
AFFICHAGE_NOTICE = ET.parse("app/static/xsl/affichage_notice.xsl")
