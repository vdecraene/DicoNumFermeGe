# Nous importons la variable db du fichier app permettant d'initialiser la base de données avec le framework Flask
from ..app import db
import re

# Créer ici les classes de l'ORM

# Les classes initialisées sont les suivantes :

class Notices(db.Model):
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    titre = db.Column(db.Text)
    titre2 = db.Column(db.Text)
    auteur = db.Column(db.Text)
    auteur2 = db.Column(db.Text)
    auteur3 = db.Column(db.Text)
    lettre = db.Column(db.Text)
    typologie = db.Column(db.Text)
    typologie_2 = db.Column(db.Text)
    typologie_3 = db.Column(db.Text)
    typologie_4 = db.Column(db.Text)
    typologie_5 = db.Column(db.Text)
    corps = db.Column(db.Text)
    doi = db.Column(db.Text)



class Dates(db.Model):
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    date = db.Column(db.Integer)
    notice = db.Column(db.Text)
    numero_notice = db.Column(db.Integer)


class Ref(db.Model):
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    notice = db.Column(db.Text)
    ref = db.Column(db.Text)
    notice_id = db.Column(db.Integer)
    lien_graph_interne = db.Column(db.Text)
    lien_graph_externe = db.Column(db.Text)


class BiblSourcesArchivalID(db.Model):
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    notice = db.Column(db.Text)
    bibl = db.Column(db.Text)
    notice_id = db.Column(db.Text)


class BiblSources(db.Model):
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    notice = db.Column(db.Text)
    bibl = db.Column(db.Text)
    notice_id = db.Column(db.Text)
    title = db.Column(db.Text)
    date = db.Column(db.Integer)
    auteur = db.Column(db.Text)


class BiblScientific(db.Model):
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    notice = db.Column(db.Text)
    title = db.Column(db.Text)
    notice_id = db.Column(db.Text)
    ark_id = db.Column(db.Text)
    auteur = db.Column(db.Text)


class SpanNorme(db.Model):
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    notice = db.Column(db.Text)
    type = db.Column(db.Text)
    subtype = db.Column(db.Text)
    span = db.Column(db.Text)
    notice_id = db.Column(db.Text)


class SpanPrivilege(db.Model):
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    notice = db.Column(db.Text)
    type = db.Column(db.Text)
    subtype = db.Column(db.Text)
    span = db.Column(db.Text)
    notice_id = db.Column(db.Text)


class ReferentielsAssociesFinalAdmiRoyales(db.Model):
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    org = db.Column(db.Text)
    notice = db.Column(db.Text)
    numero_notice = db.Column(db.Integer)
    longitude = db.Column(db.Integer)
    latitude = db.Column(db.Integer)
    geonames_id = db.Column(db.Integer)
    dicotopo_id = db.Column(db.Text)
    url_dictopo = db.Column(db.Integer)
    id_wikidata = db.Column(db.Integer)
    url_wikidata = db.Column(db.Integer)


class ReferentielsAssociesFinalAdmiFermes(db.Model):
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    org = db.Column(db.Text)
    notice = db.Column(db.Text)
    numero_notice = db.Column(db.Integer)
    longitude = db.Column(db.Integer)
    latitude = db.Column(db.Integer)
    geonames_id = db.Column(db.Integer)
    dicotopo_id = db.Column(db.Text)
    url_dictopo = db.Column(db.Integer)
    id_wikidata = db.Column(db.Integer)
    url_wikidata = db.Column(db.Integer)


class ReferentielsAssociesFinalControle(db.Model):
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    place = db.Column(db.Text)
    notice = db.Column(db.Text)
    numero_notice = db.Column(db.Integer)
    longitude = db.Column(db.Integer)
    latitude = db.Column(db.Integer)
    geonames_id = db.Column(db.Integer)
    dicotopo_id = db.Column(db.Text)
    url_dictopo = db.Column(db.Integer)
    id_wikidata = db.Column(db.Integer)
    url_wikidata = db.Column(db.Integer)


class ReferentielsAssociesFinalHabitations(db.Model):
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    place = db.Column(db.Text)
    notice = db.Column(db.Text)
    numero_notice = db.Column(db.Integer)
    longitude = db.Column(db.Integer)
    latitude = db.Column(db.Integer)
    geonames_id = db.Column(db.Integer)
    dicotopo_id = db.Column(db.Text)
    url_dictopo = db.Column(db.Integer)
    id_wikidata = db.Column(db.Integer)
    url_wikidata = db.Column(db.Integer)


class ReferentielsAssociesFinalPaysFiscauxRoyaux(db.Model):
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    org = db.Column(db.Text)
    notice = db.Column(db.Text)
    numero_notice = db.Column(db.Integer)
    longitude = db.Column(db.Integer)
    latitude = db.Column(db.Integer)
    geonames_id = db.Column(db.Integer)
    dicotopo_id = db.Column(db.Text)
    url_dictopo = db.Column(db.Integer)
    id_wikidata = db.Column(db.Integer)
    url_wikidata = db.Column(db.Integer)


class ReferentielsAssociesFinalProvinces(db.Model):
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    org = db.Column(db.Text)
    notice = db.Column(db.Text)
    numero_notice = db.Column(db.Integer)
    longitude = db.Column(db.Integer)
    latitude = db.Column(db.Integer)
    geonames_id = db.Column(db.Integer)
    dicotopo_id = db.Column(db.Text)
    url_dictopo = db.Column(db.Integer)
    id_wikidata = db.Column(db.Integer)
    url_wikidata = db.Column(db.Integer)


class ReferentielsAssociesFinalTerritoires(db.Model):
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    place = db.Column(db.Text)
    notice = db.Column(db.Text)
    numero_notice = db.Column(db.Integer)
    longitude = db.Column(db.Integer)
    latitude = db.Column(db.Integer)
    geonames_id = db.Column(db.Integer)
    dicotopo_id = db.Column(db.Text)
    url_dictopo = db.Column(db.Integer)
    id_wikidata = db.Column(db.Integer)
    url_wikidata = db.Column(db.Integer)


class Orgnames(db.Model):
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    notice = db.Column(db.Text)
    org = db.Column(db.Text)
    notice_id = db.Column(db.Text)

class Placenames(db.Model):
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    notice = db.Column(db.Text)
    place = db.Column(db.Text)
    notice_id = db.Column(db.Text)


def db_init(tei_corpus):
    """
    Fonction permettant de créer la base de données
    :param tei_corpus: expression Xpath
    :type tei_corpus: str
    """

    # # Mettre le nom des tables + .__table__.drop(db.engine) pour éviter la destruction de la table lors du lancement de l'application
    # # id sert de compter incrémenté pour les clés primaires. A chaque tour dans la boucle for, id est incrémenté de 1.
    id = 0

    Ref.__table__.drop(db.engine)
    Dates.__table__.drop(db.engine)
    BiblSourcesArchivalID.__table__.drop(db.engine)
    Notices.__table__.drop(db.engine)
    BiblScientific.__table__.drop(db.engine)
    BiblSources.__table__.drop(db.engine)
    SpanPrivilege.__table__.drop(db.engine)
    SpanNorme.__table__.drop(db.engine)
    Orgnames.__table__.drop(db.engine)
    Placenames.__table__.drop(db.engine)


    db.drop_all()
    db.create_all()

    lien_graph_interne = "http://127.0.0.1:5000/notice/"
    lien_graph_externe = "https://fermege.meshs.fr/notice/"

    for notice in tei_corpus.xpath("//entry"):
        # Nous ajoutons les données récupérées par les itérations de la boucle à la session qui est ensuite commit à la fin de toutes les boucles
        corps = "".join(notice.xpath(
            ".//*[not(self::form or self::orth or self::bibl or self::bibl//bibl or self::bibl//idno[@type='ArchivalIdentifier'])]/text()"))
        corps = corps.replace("  ", " ")
        corps = re.sub(' +', ' ', corps)
        corps = corps.replace("\n", "").replace("\r", "").replace('\t', '')
        typologie = notice.xpath("ancestor::TEI/teiHeader//textClass//keywords/list/item[text()!=''][1]/text()")[
            0] if notice.xpath("ancestor::TEI/teiHeader//textClass//keywords/list/item[text()!='']") else ''
        typologie = typologie.replace('\n', '').replace('\r', '').replace('\t', '').replace('                        ',
                                                                                            ' ').replace('  ', '')
        typologie = typologie.replace(' Histoire desrelations internationales ',
                                      'Histoire des relations internationales')
        typologie = typologie.replace(' Histoire financière ', 'Histoire financière')
        typologie = typologie.replace(' Histoire fiscale ', 'Histoire fiscale')
        typologie = typologie.replace(' Histoire judiciaire ', 'Histoire judiciaire')
        typologie = typologie.replace(' Histoire fiscale ', 'Histoire fiscale')
        typologie = typologie.replace(' Histoire économique ', 'Histoire fiscale')
        typologie = typologie.replace(' Histoireadministrative ', 'Histoire administrative')
        typologie = typologie.replace('Histoireadministrative', 'Histoire administrative')
        typologie = typologie.replace(' Histoirecommerciale ', 'Histoire commerciale')
        typologie = typologie.replace(' administrative ', 'Histoire administrative')
        typologie = typologie.replace('Histoireadministrative', 'Histoire administrative')
        typologie = typologie.replace('Histoirecommerciale', 'Histoire commerciale')
        typologie = typologie.replace('Histoirefinancière', 'Histoire financière')
        typologie = typologie.replace('Histoireéconomique', 'Histoire économique')
        typologie = typologie.replace('histoireadministrative', 'Histoire administrative')
        typologie = typologie.replace('Histoirefiscale', 'Histoire fiscale')
        typologie = typologie.replace(' Histoire administrative', 'Histoire administrative')
        typologie = typologie.replace(' Histoire politique ', 'Histoire politique')
        typologie = typologie.replace(' Histoirepolitique', 'Histoire politique')
        typologie = typologie.replace(' Histoirejudiciaire', 'Histoire judiciaire')
        typologie = typologie.replace('HistoireHistoire administrative', 'Histoire administrative')
        typologie = re.sub(r'^\s+|\s+$', '', typologie)
        typologie_2 = notice.xpath("ancestor::TEI/teiHeader//textClass//keywords/list/item[text()!=''][2]/text()")
        if len(typologie_2) > 0:
            typologie_2 = typologie_2[0]
            typologie_2 = typologie_2.replace('\n', '').replace('\r', '').replace('\t', '').replace(
                '                        ', ' ').replace('  ', '')
        else:
            typologie_2 = ''
        typologie_2 = typologie_2.replace(' Histoire desrelations internationales ',
                                          'Histoire des relations internationales')
        typologie_2 = typologie_2.replace(' Histoire financière ', 'Histoire financière')
        typologie_2 = typologie_2.replace(' Histoire fiscale ', 'Histoire fiscale')
        typologie_2 = typologie_2.replace(' Histoire judiciaire ', 'Histoire judiciaire')
        typologie_2 = typologie_2.replace(' Histoire fiscale ', 'Histoire fiscale')
        typologie_2 = typologie_2.replace(' Histoire économique ', 'Histoire fiscale')
        typologie_2 = typologie_2.replace(' Histoireadministrative ', 'Histoire administrative')
        typologie_2 = typologie_2.replace('Histoireadministrative', 'Histoire administrative')
        typologie_2 = typologie_2.replace(' Histoirecommerciale ', 'Histoire commerciale')
        typologie_2 = typologie_2.replace(' administrative ', 'Histoire administrative')
        typologie_2 = typologie_2.replace('Histoireadministrative', 'Histoire administrative')
        typologie_2 = typologie_2.replace('Histoirecommerciale', 'Histoire commerciale')
        typologie_2 = typologie_2.replace('Histoirefinancière', 'Histoire financière')
        typologie_2 = typologie_2.replace('Histoireéconomique', 'Histoire économique')
        typologie_2 = typologie_2.replace('histoireadministrative', 'Histoire administrative')
        typologie_2 = typologie_2.replace('Histoirefiscale', 'Histoire fiscale')
        typologie_2 = typologie_2.replace(' Histoire administrative', 'Histoire administrative')
        typologie_2 = typologie_2.replace(' Histoire politique ', 'Histoire politique')
        typologie_2 = typologie_2.replace(' Histoirepolitique', 'Histoire politique')
        typologie_2 = typologie_2.replace(' Histoirejudiciaire', 'Histoire judiciaire')
        typologie_2 = typologie_2.replace('histoirecommerciale', 'Histoire commerciale')
        typologie_2 = typologie_2.replace('histoirefiscale', 'Histoire fiscale')
        typologie_2 = re.sub(r'^\s+|\s+$', '', typologie_2)

        typologie_3 = notice.xpath("ancestor::TEI/teiHeader//textClass//keywords/list/item[text()!=''][3]/text()")
        if len(typologie_3) > 0:
            typologie_3 = typologie_3[0]
            typologie_3 = typologie_3.replace('\n', '').replace('\r', '').replace('\t', '').replace(
                '                        ', ' ').replace('  ', '')
        else:
            typologie_3 = ''
        typologie_3 = typologie_3.replace(' Histoire desrelations internationales ',
                                          'Histoire des relations internationales')
        typologie_3 = typologie_3.replace(' Histoire financière ', 'Histoire financière')
        typologie_3 = typologie_3.replace(' Histoire fiscale ', 'Histoire fiscale')
        typologie_3 = typologie_3.replace(' Histoire judiciaire ', 'Histoire judiciaire')
        typologie_3 = typologie_3.replace(' Histoire fiscale ', 'Histoire fiscale')
        typologie_3 = typologie_3.replace(' Histoire économique ', 'Histoire fiscale')
        typologie_3 = typologie_3.replace(' Histoireadministrative ', 'Histoire administrative')
        typologie_3 = typologie_3.replace('Histoireadministrative', 'Histoire administrative')
        typologie_3 = typologie_3.replace(' Histoirecommerciale ', 'Histoire commerciale')
        typologie_3 = typologie_3.replace(' administrative ', 'Histoire administrative')
        typologie_3 = typologie_3.replace('Histoireadministrative', 'Histoire administrative')
        typologie_3 = typologie_3.replace('Histoirecommerciale', 'Histoire commerciale')
        typologie_3 = typologie_3.replace('Histoirefinancière', 'Histoire financière')
        typologie_3 = typologie_3.replace('Histoireéconomique', 'Histoire économique')
        typologie_3 = typologie_3.replace('histoireadministrative', 'Histoire administrative')
        typologie_3 = typologie_3.replace('Histoirefiscale', 'Histoire fiscale')
        typologie_3 = typologie_3.replace(' Histoire administrative', 'Histoire administrative')
        typologie_3 = typologie_3.replace(' Histoire politique ', 'Histoire politique')
        typologie_3 = typologie_3.replace(' Histoirepolitique', 'Histoire politique')
        typologie_3 = typologie_3.replace(' Histoirejudiciaire', 'Histoire judiciaire')
        typologie_3 = typologie_3.replace('histoirecommerciale', 'Histoire commerciale')
        typologie_3 = typologie_3.replace('histoirefiscale', 'Histoire fiscale')
        typologie_3 = re.sub(r'^\s+|\s+$', '', typologie_3)
        typologie_4 = notice.xpath("ancestor::TEI/teiHeader//textClass//keywords/list/item[text()!=''][4]/text()")
        if len(typologie_4) > 0:
            typologie_4 = typologie_4[0]
            typologie_4 = typologie_4.replace('\n', '').replace('\r', '').replace('\t', '').replace(
                '                        ', ' ').replace('  ', '')
        else:
            typologie_4 = ''
        typologie_4 = typologie_4.replace(' Histoire desrelations internationales ',
                                          'Histoire des relations internationales')
        typologie_4 = typologie_4.replace(' Histoire financière ', 'Histoire financière')
        typologie_4 = typologie_4.replace(' Histoire fiscale ', 'Histoire fiscale')
        typologie_4 = typologie_4.replace(' Histoire judiciaire ', 'Histoire judiciaire')
        typologie_4 = typologie_4.replace(' Histoire fiscale ', 'Histoire fiscale')
        typologie_4 = typologie_4.replace(' Histoire économique ', 'Histoire fiscale')
        typologie_4 = typologie_4.replace(' Histoireadministrative ', 'Histoire administrative')
        typologie_4 = typologie_4.replace('Histoireadministrative', 'Histoire administrative')
        typologie_4 = typologie_4.replace(' Histoirecommerciale ', 'Histoire commerciale')
        typologie_4 = typologie_4.replace(' administrative ', 'Histoire administrative')
        typologie_4 = typologie_4.replace('Histoireadministrative', 'Histoire administrative')
        typologie_4 = typologie_4.replace('Histoirecommerciale', 'Histoire commerciale')
        typologie_4 = typologie_4.replace('Histoirefinancière', 'Histoire financière')
        typologie_4 = typologie_4.replace('Histoireéconomique', 'Histoire économique')
        typologie_4 = typologie_4.replace('histoireadministrative', 'Histoire administrative')
        typologie_4 = typologie_4.replace('Histoirefiscale', 'Histoire fiscale')
        typologie_4 = typologie_4.replace(' Histoire administrative', 'Histoire administrative')
        typologie_4 = typologie_4.replace(' Histoire politique ', 'Histoire politique')
        typologie_4 = typologie_4.replace(' Histoirepolitique', 'Histoire politique')
        typologie_4 = typologie_4.replace(' Histoirejudiciaire', 'Histoire judiciaire')
        typologie_4 = typologie_4.replace(' Histoirejudiciaire', 'Histoire judiciaire')
        typologie_4 = typologie_4.replace('histoirecommerciale', 'Histoire commerciale')
        typologie_4 = typologie_4.replace('histoirefiscale', 'Histoire fiscale')
        typologie_4 = typologie_4.replace('histoirepolitique', 'Histoire politique')
        typologie_4 = re.sub(r'^\s+|\s+$', '', typologie_4)
        typologie_5 = notice.xpath("ancestor::TEI/teiHeader//textClass//keywords/list/item[text()!=''][5]/text()")
        if len(typologie_5) > 0:
            typologie_5 = typologie_5[0]
            typologie_5 = typologie_5.replace('\n', '').replace('\r', '').replace('\t', '').replace(
                '                        ', ' ').replace('  ', '')
        else:
            typologie_5 = ''
        typologie_5 = typologie_5.replace(' Histoire desrelations internationales ',
                                          'Histoire des relations internationales')
        typologie_5 = typologie_5.replace(' Histoire financière ', 'Histoire financière')
        typologie_5 = typologie_5.replace(' Histoire fiscale ', 'Histoire fiscale')
        typologie_5 = typologie_5.replace(' Histoire judiciaire ', 'Histoire judiciaire')
        typologie_5 = typologie_5.replace(' Histoire fiscale ', 'Histoire fiscale')
        typologie_5 = typologie_5.replace(' Histoire économique ', 'Histoire fiscale')
        typologie_5 = typologie_5.replace(' Histoireadministrative ', 'Histoire administrative')
        typologie_5 = typologie_5.replace('Histoireadministrative', 'Histoire administrative')
        typologie_5 = typologie_5.replace(' Histoirecommerciale ', 'Histoire commerciale')
        typologie_5 = typologie_5.replace(' administrative ', 'Histoire administrative')
        typologie_5 = typologie_5.replace('Histoireadministrative', 'Histoire administrative')
        typologie_5 = typologie_5.replace('Histoirecommerciale', 'Histoire commerciale')
        typologie_5 = typologie_5.replace('Histoirefinancière', 'Histoire financière')
        typologie_5 = typologie_5.replace('Histoireéconomique', 'Histoire économique')
        typologie_5 = typologie_5.replace('histoireadministrative', 'Histoire administrative')
        typologie_5 = typologie_5.replace('Histoirefiscale', 'Histoire fiscale')
        typologie_5 = typologie_5.replace(' Histoire administrative', 'Histoire administrative')
        typologie_5 = typologie_5.replace(' Histoire politique ', 'Histoire politique')
        typologie_5 = typologie_5.replace(' Histoirepolitique', 'Histoire politique')
        typologie_5 = typologie_5.replace(' Histoirejudiciaire', 'Histoire judiciaire')
        typologie_5 = typologie_5.replace(' Histoirejudiciaire', 'Histoire judiciaire')
        typologie_5 = typologie_5.replace('histoirecommerciale', 'Histoire commerciale')
        typologie_5 = typologie_5.replace('histoirefiscale', 'Histoire fiscale')
        typologie_5 = re.sub(r'^\s+|\s+$', '', typologie_5)
        auteur = notice.xpath("ancestor::TEI//author[1]/text()")
        titre = notice.xpath("ancestor::TEI//orth/text()")[0]
        titre2= notice.xpath("ancestor::TEI//orth/text()")[0]
        titre = titre.strip().replace("é","e").replace("à","a").replace("â","a").replace("ô","o").replace("ê","e").replace("î","i").replace("è","e")
        if auteur:
            auteur = auteur[0]

        if notice.xpath("count(ancestor::TEI//author) >= 2"):
            auteur2_list = notice.xpath("ancestor::TEI//author[2]/text()")
            if auteur2_list:
                auteur2 = ''.join(auteur2_list)
            else:
                auteur2 = None
        else:
            auteur2 = None

        doi = ""
        db.session.add(Notices(
            id=notice.xpath("./@n")[0],
            # Nous utilisons le prédicat ancestor:: pour remonter dans l'arborescence et obtenir les données recherchées
            titre=titre,
            titre2 = titre2,
            auteur=auteur,
            auteur2=auteur2,
            lettre=notice.xpath("./@type")[0],
            typologie=re.sub(r'Histoire -- Histoire moderne --', '', typologie).strip(),
            typologie_2=re.sub(r'Histoire -- Histoire moderne --', '', typologie_2).strip(),
            typologie_3=re.sub(r'Histoire -- Histoire moderne --', '', typologie_3).strip(),
            typologie_4=re.sub(r'Histoire -- Histoire moderne --', '', typologie_4).strip(),
            typologie_5=re.sub(r'Histoire -- Histoire moderne --', '', typologie_5).strip(),
            corps=corps.strip(),
            doi=doi.strip()
        ))



    for notice in tei_corpus.xpath('//span[@type="normes_contestations"]'):
        id += 1
        db.session.add(SpanNorme(
            id=id,
            notice=notice.xpath("ancestor::TEI//orth/text()")[0],
            type=notice.xpath("normalize-space(./@type)"),
            subtype=notice.xpath("normalize-space(./@subtype)"),
            span=notice.xpath("normalize-space(.)"),
            notice_id=notice.xpath("normalize-space(ancestor::entry/@n)")
        ))


    for notice in tei_corpus.xpath('//span[@type="privleges_encadrement"]'):
        id += 1
        db.session.add(SpanPrivilege(
            id=id,
            notice=notice.xpath("ancestor::TEI//orth/text()")[0],
            type=notice.xpath("normalize-space(./@type)"),
            subtype=notice.xpath("normalize-space(./@subtype)"),
            span=notice.xpath("normalize-space(.)"),
            notice_id=notice.xpath("normalize-space(ancestor::entry/@n)")
        ))



    for notice in tei_corpus.xpath('//bibl/idno'):
        id += 1
        idno_string = notice.xpath("normalize-space(.)")
        db.session.add(BiblSourcesArchivalID(
            id=id,
            notice=notice.xpath("ancestor::TEI//orth/text()")[0],
            bibl=idno_string.strip(),
            notice_id=notice.xpath("normalize-space(ancestor::entry/@n)")
        ))

    for notice in tei_corpus.xpath('//bibl[@type="sources"]'):
        id += 1
        sources_string = notice.xpath("normalize-space(.)")
        db.session.add(BiblSources(
            id=id,
            notice=notice.xpath("ancestor::TEI//orth/text()")[0].strip(),
            bibl=sources_string.strip(),
            notice_id=notice.xpath("normalize-space(ancestor::entry/@n)"),
            auteur = notice.xpath("normalize-space(./author/text())"),
            title = notice.xpath("normalize-space(./title/text())"),
            date = notice.xpath("normalize-space(./date/text())")
        ))

    for notice in tei_corpus.xpath('//bibl[not(@type="sources" or @type="references" or idno)]'):
        id += 1
        bibl_string = notice.xpath("normalize-space(.)")


        db.session.add(BiblScientific(
            id=id,
            notice=notice.xpath("ancestor::TEI//orth/text()")[0].strip(),
            title=bibl_string.strip(),
            notice_id=notice.xpath("normalize-space(ancestor::entry/@n)")
        ))


    for notice in tei_corpus.xpath("//entry//date"):
        id += 1
        db.session.add(Dates(
            id=id,
            date=notice.xpath("normalize-space(.)"),
            notice=notice.xpath("normalize-space(ancestor::entry/@xml:id)"),
            numero_notice=notice.xpath("normalize-space(ancestor::entry/@n)")
        ))

    for notice in tei_corpus.xpath("//entry//ref"):
        id += 1
        db.session.add(Ref(
            id=id,
            ref=notice.xpath("normalize-space(.)"),
            notice=notice.xpath("normalize-space(ancestor::TEI//orth/text())"),
            notice_id=notice.xpath("normalize-space(ancestor::entry/@n)"),
            lien_graph_interne=lien_graph_interne + notice.xpath("normalize-space(ancestor::entry/@n)"),
            lien_graph_externe=lien_graph_externe + notice.xpath("normalize-space(ancestor::entry/@n)")
        ))

    for notice in tei_corpus.xpath('//def//orgName'):
        org_string = notice.xpath("normalize-space(.)")
        org_string = re.sub(r'\bé', 'e', org_string)

        db.session.add(Orgnames(
            notice=notice.xpath("normalize-space(ancestor::TEI//orth/text())"),
            org=org_string,
            notice_id=notice.xpath("normalize-space(ancestor::entry/@n)")
        ))

    for notice in tei_corpus.xpath('//def//placeName'):
        place_string = notice.xpath("normalize-space(.)")
        place_string = re.sub(r'\bé', 'e', place_string)
        place_string = place_string.replace('villes de', '').replace('ville de', '').replace('villes d’', '').replace('ville d’', '').replace('Ville de','')
        place_string = place_string.replace('villages de', '').replace('village de', '').replace('villages d’', '').replace('village nommé', '')


        db.session.add(Placenames(
            notice=notice.xpath("normalize-space(ancestor::TEI//orth/text())"),
            place=place_string.strip(),
            notice_id=notice.xpath("normalize-space(ancestor::entry/@n)")
        ))


    # Nous faisons un "commit" de la session en cours afin d'ajouter les données dans les tables correspondantes.
    db.session.commit()
