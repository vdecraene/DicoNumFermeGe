# Ce fichier contient l'ensemble des fonctions permettant de générer automatiquement les pages de l'application (les 'routes'),
# les fonctions de recherche et requêtes dans la base de données, les fonctions d'indexation des entités nommées, de génération
# de la chronologie et téléchargement des notices.

from warnings import warn
# Nous utilisons le framework Flask pour développer cette application web.
from flask import render_template, flash, redirect, send_file, request

# Le module lxml est utilisé pour parser les données XML au sein de l'application.
from lxml import etree as ET

# Le module flask_login permet la gestion du processus d'enregistrement et connexion aux comptes utilisateurs qui seront implémentés par la suite.

# Le module sqlalchemy et sa fonction or_ permet de formuler des requêtes SQL par le biais de l'ORM.
from sqlalchemy import or_, union, select, union_all, func, distinct

# Ensuite, nous importons les fonctions, variables et constantes requises pour l'exécution de l'application.
from ..app import app, db
from ..constantes import AFFICHAGE_NOTICE, RESULTATS_PAR_PAGES, RESULTATS_PAR_PAGES_RECHERCHE_AVANCEE, CLEAN_CORPUS
from ..modeles.donnees import db_init
# Mettre en import les classes issues de donnees.py
from ..modeles.donnees import Notices, Ref, BiblScientific, BiblSources, BiblSourcesArchivalID, \
    ReferentielsAssociesFinalProvinces, ReferentielsAssociesFinalControle, \
    ReferentielsAssociesFinalHabitations, \
    ReferentielsAssociesFinalTerritoires, ReferentielsAssociesFinalPaysFiscauxRoyaux, \
    ReferentielsAssociesFinalAdmiFermes, ReferentielsAssociesFinalAdmiRoyales, Orgnames, Placenames


# Appel de la fonction assurant l'initialisation de la base de données qui prend en argument la constante contenant le chemin vers le corpus XML/TEI.
db_init(CLEAN_CORPUS)

# Variable utilisée par la suite pour renvoyer et compter l'ensemble des notices dans la base de donnée.
docs = Notices.query.all()

# Le framework Flask utilise des decorators modifiant le comportement des fonctions permettant ici de générer les routes de l'application web (@app.route)



@app.route("/")
def accueil():
    '''Fonction de génération de la page d'accueil à la racine de l'application

    :returns: Fonction de renvoi du template accueil et du paramètre docs (ensemble des notices)
    '''

    return render_template("pages/accueil.html", docs=docs)


@app.route("/graph")
def grap_visualization():
    return render_template("pages/graph.html", docs=docs)


@app.route("/atlas")
def atlas():
    '''Fonction de génération de la page contenant à terme les renvois vers l'Atlas de la Ferme générale

    :returns: Fonction de renvoi du template atlas et du paramètre docs (ensemble des notices)
    '''

    images = [
        "https://fr.wikipedia.org/wiki/Ferme_g%C3%A9n%C3%A9rale#/media/Fichier:Ferle_G%C3%A9n%C3%A9ral_-liquidation_1791.jpg"]

    return render_template("pages/atlas.html", docs=docs, images=images)


@app.route("/science_ouverte")
def science_ouverte():
    '''Fonction de génération de la page contenant les informations sur la documentation et les liens de téléchargement des ODD aux différents formats

    :returns: Fonction de renvoi du template science_ouverte et du paramètre docs (ensemble des notices)
    '''

    return render_template("pages/science_ouverte.html", docs=docs)


@app.route("/equipe")
def equipe():
    '''Fonction de génération de la page contenant la présentation de l'équipe du projet ANR FermeGé

    :returns: Fonction de renvoi du template equipe et du paramètre docs (ensemble des notices)
    '''

    return render_template("pages/equipe.html", docs=docs)


@app.route("/notice/<int:id>")
def notice(id):
    '''Fonction de génération automatique des pages contenant les notices du Dictionnaire.
    :param id : Identifiant numérique
    :type id: int
    :returns: Fonction de renvoi du template notice et des paramètre docs (ensemble des notices), notice_txt (resultat de la transformation XSLT),
    id (paramètre XSLT permettant de générer et parser la notice), nb_notice (nombre de notice additionnées), ref (ensemble des liens entre les notices).
    '''

    doc_xsl = ET.XSLT(AFFICHAGE_NOTICE)
    sortie = doc_xsl(CLEAN_CORPUS, numero=str(id))
    nb_notice = len(docs)
    ref = Ref.query.filter(Ref.ref)
    f = "/static/xml/" + str(id) + ".xml"
    f_pdf = "/static/pdf/" + str(id) + ".pdf"

    return render_template("pages/notice.html",
                           notice_txt=sortie,
                           docs=docs, id=id,
                           nb_notice=nb_notice, ref=ref, f=f, f_pdf=f_pdf)


@app.route("/index_orga")
def index_org():
    '''Fonction de génération automatique e l'index des organisations.

        :returns: Fonction de renvoi du template index_orga et des paramètres index_orga_provinces, index_orga_pays_primitifs,
         index_orga_pays_fiscaux, index_admi_royales, index_admi_fermes (résultat des requêtes Sqlalchemy correspondantes) et
         docs (ensemble des notices).
    '''

    # Elements de génération de la pagination
    page = request.args.get("page", 1)
    if isinstance(page, str) and page.isdigit():
        page = int(page)
    else:
        page = 1

    # Requêtes par le biais de l'ORM avec Sqlalchemy.

    referentiels_province_2 = (db.session.query(ReferentielsAssociesFinalProvinces.org,
                                                func.group_concat(
                                                    distinct(ReferentielsAssociesFinalProvinces.numero_notice)).label(
                                                    "notice_list"),
                                                ReferentielsAssociesFinalProvinces.notice,
                                                func.group_concat(
                                                    distinct(ReferentielsAssociesFinalProvinces.notice)).label(
                                                    "notice_name_list"),
                                                ReferentielsAssociesFinalProvinces.geonames_id,
                                                func.group_concat(
                                                    distinct(ReferentielsAssociesFinalProvinces.geonames_id)).label(
                                                    "notice_geonames_id_list"),
                                                ReferentielsAssociesFinalProvinces.dicotopo_id,
                                                func.group_concat(
                                                    distinct(ReferentielsAssociesFinalProvinces.dicotopo_id)).label(
                                                    "notice_dicotopo_id_list"),
                                                ReferentielsAssociesFinalProvinces.url_dictopo,
                                                func.group_concat(
                                                    distinct(ReferentielsAssociesFinalProvinces.url_dictopo)).label(
                                                    "notice_url_dicotopo_list"),
                                                ReferentielsAssociesFinalProvinces.id_wikidata,
                                                func.group_concat(
                                                    distinct(ReferentielsAssociesFinalProvinces.id_wikidata)).label(
                                                    "notice_id_wikidata_list"),
                                                ReferentielsAssociesFinalProvinces.url_wikidata,
                                                func.group_concat(
                                                    distinct(ReferentielsAssociesFinalProvinces.url_wikidata)).label(
                                                    "notice_url_wikidata_list"),
                                                ReferentielsAssociesFinalProvinces.notice,
                                                ReferentielsAssociesFinalProvinces.longitude,
                                                ReferentielsAssociesFinalProvinces.latitude,
                                                ReferentielsAssociesFinalProvinces.geonames_id,
                                                ReferentielsAssociesFinalProvinces.dicotopo_id,
                                                ReferentielsAssociesFinalProvinces.url_dictopo,
                                                ReferentielsAssociesFinalProvinces.id_wikidata,
                                                ReferentielsAssociesFinalProvinces.url_wikidata).group_by(
        ReferentielsAssociesFinalProvinces.org).distinct())

    referentiels_pays_2 = (db.session.query(ReferentielsAssociesFinalPaysFiscauxRoyaux.org,
                                            func.group_concat(
                                                distinct(
                                                    ReferentielsAssociesFinalPaysFiscauxRoyaux.numero_notice)).label(
                                                "notice_list"),
                                            ReferentielsAssociesFinalPaysFiscauxRoyaux.notice,
                                            func.group_concat(
                                                distinct(ReferentielsAssociesFinalPaysFiscauxRoyaux.notice)).label(
                                                "notice_name_list"),
                                            ReferentielsAssociesFinalPaysFiscauxRoyaux.geonames_id,
                                            func.group_concat(
                                                distinct(ReferentielsAssociesFinalPaysFiscauxRoyaux.geonames_id)).label(
                                                "notice_geonames_id_list"),
                                            ReferentielsAssociesFinalPaysFiscauxRoyaux.dicotopo_id,
                                            func.group_concat(
                                                distinct(ReferentielsAssociesFinalPaysFiscauxRoyaux.dicotopo_id)).label(
                                                "notice_dicotopo_id_list"),
                                            ReferentielsAssociesFinalPaysFiscauxRoyaux.url_dictopo,
                                            func.group_concat(
                                                distinct(ReferentielsAssociesFinalPaysFiscauxRoyaux.url_dictopo)).label(
                                                "notice_url_dicotopo_list"),
                                            ReferentielsAssociesFinalPaysFiscauxRoyaux.id_wikidata,
                                            func.group_concat(
                                                distinct(ReferentielsAssociesFinalPaysFiscauxRoyaux.id_wikidata)).label(
                                                "notice_id_wikidata_list"),
                                            ReferentielsAssociesFinalPaysFiscauxRoyaux.url_wikidata,
                                            func.group_concat(
                                                distinct(
                                                    ReferentielsAssociesFinalPaysFiscauxRoyaux.url_wikidata)).label(
                                                "notice_url_wikidata_list"),
                                            ReferentielsAssociesFinalPaysFiscauxRoyaux.notice,
                                            ReferentielsAssociesFinalPaysFiscauxRoyaux.longitude,
                                            ReferentielsAssociesFinalPaysFiscauxRoyaux.latitude,
                                            ReferentielsAssociesFinalPaysFiscauxRoyaux.geonames_id,
                                            ReferentielsAssociesFinalPaysFiscauxRoyaux.dicotopo_id,
                                            ReferentielsAssociesFinalPaysFiscauxRoyaux.url_dictopo,
                                            ReferentielsAssociesFinalPaysFiscauxRoyaux.id_wikidata,
                                            ReferentielsAssociesFinalPaysFiscauxRoyaux.url_wikidata).group_by(
        ReferentielsAssociesFinalPaysFiscauxRoyaux.org).distinct())

    referentiels_admi_royales_2 = (db.session.query(ReferentielsAssociesFinalAdmiRoyales.org,
                                                    func.group_concat(
                                                        distinct(
                                                            ReferentielsAssociesFinalAdmiRoyales.numero_notice)).label(
                                                        "notice_list"),
                                                    ReferentielsAssociesFinalAdmiRoyales.notice,
                                                    func.group_concat(
                                                        distinct(ReferentielsAssociesFinalAdmiRoyales.notice)).label(
                                                        "notice_name_list"),
                                                    ReferentielsAssociesFinalAdmiRoyales.geonames_id,
                                                    func.group_concat(
                                                        distinct(
                                                            ReferentielsAssociesFinalAdmiRoyales.geonames_id)).label(
                                                        "notice_geonames_id_list"),
                                                    ReferentielsAssociesFinalAdmiRoyales.dicotopo_id,
                                                    func.group_concat(
                                                        distinct(
                                                            ReferentielsAssociesFinalAdmiRoyales.dicotopo_id)).label(
                                                        "notice_dicotopo_id_list"),
                                                    ReferentielsAssociesFinalAdmiRoyales.url_dictopo,
                                                    func.group_concat(
                                                        distinct(
                                                            ReferentielsAssociesFinalAdmiRoyales.url_dictopo)).label(
                                                        "notice_url_dicotopo_list"),
                                                    ReferentielsAssociesFinalAdmiRoyales.id_wikidata,
                                                    func.group_concat(
                                                        distinct(
                                                            ReferentielsAssociesFinalAdmiRoyales.id_wikidata)).label(
                                                        "notice_id_wikidata_list"),
                                                    ReferentielsAssociesFinalAdmiRoyales.url_wikidata,
                                                    func.group_concat(
                                                        distinct(
                                                            ReferentielsAssociesFinalAdmiRoyales.url_wikidata)).label(
                                                        "notice_url_wikidata_list"),
                                                    ReferentielsAssociesFinalAdmiRoyales.notice,
                                                    ReferentielsAssociesFinalAdmiRoyales.longitude,
                                                    ReferentielsAssociesFinalAdmiRoyales.latitude,
                                                    ReferentielsAssociesFinalAdmiRoyales.geonames_id,
                                                    ReferentielsAssociesFinalAdmiRoyales.dicotopo_id,
                                                    ReferentielsAssociesFinalAdmiRoyales.url_dictopo,
                                                    ReferentielsAssociesFinalAdmiRoyales.id_wikidata,
                                                    ReferentielsAssociesFinalAdmiRoyales.url_wikidata).group_by(
        ReferentielsAssociesFinalAdmiRoyales.org).distinct())

    referentiels_admi_fermes_2 = (db.session.query(ReferentielsAssociesFinalAdmiFermes.org,
                                                   func.group_concat(
                                                       distinct(
                                                           ReferentielsAssociesFinalAdmiFermes.numero_notice)).label(
                                                       "notice_list"),
                                                   ReferentielsAssociesFinalAdmiFermes.notice,
                                                   func.group_concat(
                                                       distinct(ReferentielsAssociesFinalAdmiFermes.notice)).label(
                                                       "notice_name_list"),
                                                   ReferentielsAssociesFinalAdmiFermes.geonames_id,
                                                   func.group_concat(
                                                       distinct(
                                                           ReferentielsAssociesFinalAdmiFermes.geonames_id)).label(
                                                       "notice_geonames_id_list"),
                                                   ReferentielsAssociesFinalAdmiFermes.dicotopo_id,
                                                   func.group_concat(
                                                       distinct(
                                                           ReferentielsAssociesFinalAdmiFermes.dicotopo_id)).label(
                                                       "notice_dicotopo_id_list"),
                                                   ReferentielsAssociesFinalAdmiFermes.url_dictopo,
                                                   func.group_concat(
                                                       distinct(
                                                           ReferentielsAssociesFinalAdmiFermes.url_dictopo)).label(
                                                       "notice_url_dicotopo_list"),
                                                   ReferentielsAssociesFinalAdmiFermes.id_wikidata,
                                                   func.group_concat(
                                                       distinct(
                                                           ReferentielsAssociesFinalAdmiFermes.id_wikidata)).label(
                                                       "notice_id_wikidata_list"),
                                                   ReferentielsAssociesFinalAdmiFermes.url_wikidata,
                                                   func.group_concat(
                                                       distinct(
                                                           ReferentielsAssociesFinalAdmiFermes.url_wikidata)).label(
                                                       "notice_url_wikidata_list"),
                                                   ReferentielsAssociesFinalAdmiFermes.notice,
                                                   ReferentielsAssociesFinalAdmiFermes.longitude,
                                                   ReferentielsAssociesFinalAdmiFermes.latitude,
                                                   ReferentielsAssociesFinalAdmiFermes.geonames_id,
                                                   ReferentielsAssociesFinalAdmiFermes.dicotopo_id,
                                                   ReferentielsAssociesFinalAdmiFermes.url_dictopo,
                                                   ReferentielsAssociesFinalAdmiFermes.id_wikidata,
                                                   ReferentielsAssociesFinalAdmiFermes.url_wikidata).group_by(
        ReferentielsAssociesFinalAdmiFermes.org).distinct())

    return render_template("pages/index_orga.html", docs=docs, referentiels_province_2=referentiels_province_2,
                           referentiels_pays_2=referentiels_pays_2,
                           referentiels_admi_royales_2=referentiels_admi_royales_2,
                           referentiels_admi_fermes_2=referentiels_admi_fermes_2)

@app.route("/index_orgname")
def index_orgname():
    orgnames = db.session.query(Orgnames.org, Orgnames.notice, Orgnames.notice_id).distinct()

    org_dict = {}
    for orgname in orgnames:
        org = orgname.org
        notice = orgname.notice
        notice_id = orgname.notice_id
        if org in org_dict:
            org_dict[org].append({'notice': notice, 'notice_id': notice_id})
        else:
            org_dict[org] = [{'notice': notice, 'notice_id': notice_id}]

    orgnames_with_notices = [{'org': org, 'notices': notices} for org, notices in org_dict.items()]

    return render_template("pages/index_orgname.html", orgnames=orgnames_with_notices)

@app.route("/index_placename")
def index_placename():
    placenames = db.session.query(Placenames.place, Placenames.notice, Placenames.notice_id).distinct()

    place_dict = {}
    for placename in placenames:
        place = placename.place
        notice = placename.notice
        notice_id = placename.notice_id
        if place in place_dict:
            place_dict[place].append({'notice': notice, 'notice_id': notice_id})
        else:
            place_dict[place] = [{'notice': notice, 'notice_id': notice_id}]

    placenames_with_notices = [{'place': place, 'notices': notices} for place, notices in place_dict.items()]

    return render_template("pages/index_placename.html", placenames=placenames_with_notices)



@app.route("/index_place")
def index_place():
    '''Fonction de génération automatique de l'index des toponymes.

    :returns: Fonction de renvoi du template index_place et des paramètres index_place_habitations, index_place_controle,
    index_place_territoire (résultat des requêtes Sqlalchemy correspondantes) et docs (ensemble des notices).
    '''

    # Elements de génération de la pagination
    page = request.args.get("page", 1)
    if isinstance(page, str) and page.isdigit():
        page = int(page)
    else:
        page = 1

    # Requêtes par le biais de l'ORM avec Sqlalchemy.

    referentiels_territoire_2 = (db.session.query(ReferentielsAssociesFinalTerritoires.place,
                                                  func.group_concat(
                                                      distinct(
                                                          ReferentielsAssociesFinalTerritoires.numero_notice)).label(
                                                      "notice_list"),
                                                  ReferentielsAssociesFinalTerritoires.notice,
                                                  func.group_concat(
                                                      distinct(ReferentielsAssociesFinalTerritoires.notice)).label(
                                                      "notice_name_list"),
                                                  ReferentielsAssociesFinalTerritoires.geonames_id,
                                                  func.group_concat(
                                                      distinct(ReferentielsAssociesFinalTerritoires.geonames_id)).label(
                                                      "notice_geonames_id_list"),
                                                  ReferentielsAssociesFinalTerritoires.dicotopo_id,
                                                  func.group_concat(
                                                      distinct(ReferentielsAssociesFinalTerritoires.dicotopo_id)).label(
                                                      "notice_dicotopo_id_list"),
                                                  ReferentielsAssociesFinalTerritoires.url_dictopo,
                                                  func.group_concat(
                                                      distinct(ReferentielsAssociesFinalTerritoires.url_dictopo)).label(
                                                      "notice_url_dicotopo_list"),
                                                  ReferentielsAssociesFinalTerritoires.id_wikidata,
                                                  func.group_concat(
                                                      distinct(ReferentielsAssociesFinalTerritoires.id_wikidata)).label(
                                                      "notice_id_wikidata_list"),
                                                  ReferentielsAssociesFinalTerritoires.url_wikidata,
                                                  func.group_concat(
                                                      distinct(
                                                          ReferentielsAssociesFinalTerritoires.url_wikidata)).label(
                                                      "notice_url_wikidata_list"),
                                                  ReferentielsAssociesFinalTerritoires.notice,
                                                  ReferentielsAssociesFinalTerritoires.longitude,
                                                  ReferentielsAssociesFinalTerritoires.latitude,
                                                  ReferentielsAssociesFinalTerritoires.geonames_id,
                                                  ReferentielsAssociesFinalTerritoires.dicotopo_id,
                                                  ReferentielsAssociesFinalTerritoires.url_dictopo,
                                                  ReferentielsAssociesFinalTerritoires.id_wikidata,
                                                  ReferentielsAssociesFinalTerritoires.url_wikidata).group_by(
        ReferentielsAssociesFinalTerritoires.place).distinct())

    referentiels_habitation_2 = (db.session.query(ReferentielsAssociesFinalHabitations.place,
                                                  func.group_concat(
                                                      distinct(
                                                          ReferentielsAssociesFinalHabitations.numero_notice)).label(
                                                      "notice_list"),
                                                  ReferentielsAssociesFinalHabitations.notice,
                                                  func.group_concat(
                                                      distinct(ReferentielsAssociesFinalHabitations.notice)).label(
                                                      "notice_name_list"),
                                                  ReferentielsAssociesFinalHabitations.geonames_id,
                                                  func.group_concat(
                                                      distinct(ReferentielsAssociesFinalHabitations.geonames_id)).label(
                                                      "notice_geonames_id_list"),
                                                  ReferentielsAssociesFinalHabitations.dicotopo_id,
                                                  func.group_concat(
                                                      distinct(ReferentielsAssociesFinalHabitations.dicotopo_id)).label(
                                                      "notice_dicotopo_id_list"),
                                                  ReferentielsAssociesFinalHabitations.url_dictopo,
                                                  func.group_concat(
                                                      distinct(ReferentielsAssociesFinalHabitations.url_dictopo)).label(
                                                      "notice_url_dicotopo_list"),
                                                  ReferentielsAssociesFinalHabitations.id_wikidata,
                                                  func.group_concat(
                                                      distinct(ReferentielsAssociesFinalHabitations.id_wikidata)).label(
                                                      "notice_id_wikidata_list"),
                                                  ReferentielsAssociesFinalHabitations.url_wikidata,
                                                  func.group_concat(
                                                      distinct(
                                                          ReferentielsAssociesFinalHabitations.url_wikidata)).label(
                                                      "notice_url_wikidata_list"),
                                                  ReferentielsAssociesFinalHabitations.notice,
                                                  ReferentielsAssociesFinalHabitations.longitude,
                                                  ReferentielsAssociesFinalHabitations.latitude,
                                                  ReferentielsAssociesFinalHabitations.geonames_id,
                                                  ReferentielsAssociesFinalHabitations.dicotopo_id,
                                                  ReferentielsAssociesFinalHabitations.url_dictopo,
                                                  ReferentielsAssociesFinalHabitations.id_wikidata,
                                                  ReferentielsAssociesFinalHabitations.url_wikidata).group_by(
        ReferentielsAssociesFinalHabitations.place).distinct())

    referentiels_controle_2 = (db.session.query(ReferentielsAssociesFinalControle.place,
                                                func.group_concat(
                                                    distinct(
                                                        ReferentielsAssociesFinalControle.numero_notice)).label(
                                                    "notice_list"),
                                                ReferentielsAssociesFinalControle.notice,
                                                func.group_concat(
                                                    distinct(ReferentielsAssociesFinalControle.notice)).label(
                                                    "notice_name_list"),
                                                ReferentielsAssociesFinalControle.geonames_id,
                                                func.group_concat(
                                                    distinct(ReferentielsAssociesFinalControle.geonames_id)).label(
                                                    "notice_geonames_id_list"),
                                                ReferentielsAssociesFinalControle.dicotopo_id,
                                                func.group_concat(
                                                    distinct(ReferentielsAssociesFinalControle.dicotopo_id)).label(
                                                    "notice_dicotopo_id_list"),
                                                ReferentielsAssociesFinalControle.url_dictopo,
                                                func.group_concat(
                                                    distinct(ReferentielsAssociesFinalControle.url_dictopo)).label(
                                                    "notice_url_dicotopo_list"),
                                                ReferentielsAssociesFinalControle.id_wikidata,
                                                func.group_concat(
                                                    distinct(ReferentielsAssociesFinalControle.id_wikidata)).label(
                                                    "notice_id_wikidata_list"),
                                                ReferentielsAssociesFinalControle.url_wikidata,
                                                func.group_concat(
                                                    distinct(
                                                        ReferentielsAssociesFinalControle.url_wikidata)).label(
                                                    "notice_url_wikidata_list"),
                                                ReferentielsAssociesFinalControle.notice,
                                                ReferentielsAssociesFinalControle.longitude,
                                                ReferentielsAssociesFinalControle.latitude,
                                                ReferentielsAssociesFinalControle.geonames_id,
                                                ReferentielsAssociesFinalControle.dicotopo_id,
                                                ReferentielsAssociesFinalControle.url_dictopo,
                                                ReferentielsAssociesFinalControle.id_wikidata,
                                                ReferentielsAssociesFinalControle.url_wikidata).group_by(
        ReferentielsAssociesFinalControle.place).distinct())

    return render_template("pages/index_place.html", referentiels_territoire_2=referentiels_territoire_2,
                           referentiels_habitation_2=referentiels_habitation_2,
                           referentiels_controle_2=referentiels_controle_2, docs=docs)


@app.route("/index_notices")
def index_notices():
    '''Fonction de génération automatique de l'index des notices.

        :returns: Fonction de renvoi du template index_notices et des paramètres index_notices (résultat de la requête Sqlalchemy correspondante)
         et docs (ensemble des notices).
    '''

    # Elements de génération de la pagination
    page = request.args.get("page", 1)
    if isinstance(page, str) and page.isdigit():
        page = int(page)
    else:
        page = 1

    # Requêtes par le biais de l'ORM avec Sqlalchemy.
    index_notices = db.session.query(Notices.titre, Notices.id, Notices.auteur, Notices.auteur2, Notices.auteur3,
                                     Notices.lettre, Notices.typologie,
                                     Notices.typologie_2, Notices.typologie_3, Notices.typologie_4,
                                     Notices.typologie_5).distinct()

    return render_template("pages/index_notices.html", index_notices=index_notices, docs=docs)


# @app.route("/index_sources_biblio")
# def index_sources_biblio():
#     '''Fonction de génération automatique e l'index des organisations.
#
#         :returns: Fonction de renvoi du template index_orga et des paramètres index_orga_provinces, index_orga_pays_primitifs,
#          index_orga_pays_fiscaux, index_admi_royales, index_admi_fermes (résultat des requêtes Sqlalchemy correspondantes) et
#          docs (ensemble des notices).
#     '''
#
#     # Elements de génération de la pagination
#     page = request.args.get("page", 1)
#     if isinstance(page, str) and page.isdigit():
#         page = int(page)
#     else:
#         page = 1
#
#     # Récupérer les données de la base de données
#     bibl_scientific = db.session.query(BiblScientific.title, func.group_concat(distinct(BiblScientific.notice_id)).label("notice_ids")).group_by(BiblScientific.title).all()
#     bibl_sources = db.session.query(BiblSources.bibl, func.group_concat(distinct(BiblSources.notice_id)).label("notice_ids")).group_by(BiblSources.bibl).all()
#     bibl_sources_primaires = db.session.query(BiblSourcesArchivalID.bibl, func.group_concat(distinct(BiblSourcesArchivalID.notice_id)).label("notice_ids")).group_by(BiblSourcesArchivalID.bibl).all()
#
#     # Regrouper les données selon les org identiques
#     bibl_scientific_dict = {}
#     for item in bibl_scientific:
#         title = item[0]
#         notice_id = item[1]
#         if title not in bibl_scientific_dict:
#             bibl_scientific_dict[title] = []
#         bibl_scientific_dict[title].append(notice_id)
#
#     bibl_sources_dict = {}
#     for item in bibl_sources:
#         bibl = item[0]
#         notice_id = item[1]
#         if bibl not in bibl_sources_dict:
#             bibl_sources_dict[bibl] = []
#         bibl_sources_dict[bibl].append(notice_id)
#
#     bibl_sources_primaires_dict = {}
#     for item in bibl_sources_primaires:
#         bibl = item[0]
#         notice_id = item[1]
#         if bibl not in bibl_sources_primaires_dict:
#             bibl_sources_primaires_dict[bibl] = []
#         bibl_sources_primaires_dict[bibl].append(notice_id)
#
#     bibl_scientific_dict = {title: notice_ids.split(',') for title, notice_ids in bibl_scientific_dict.items()}
#     bibl_sources_dict = {bibl: notice_ids.split(',') for bibl, notice_ids in bibl_sources_dict.items()}
#     bibl_sources_primaires_dict = {bibl: notice_ids.split(',') for bibl, notice_ids in
#                                    bibl_sources_primaires_dict.items()}
#
#     # Rendre le template avec les données regroupées
#     return render_template("pages/index_biblio_sources.html", bibl_scientific=bibl_scientific_dict,
#                            bibl_sources=bibl_sources_dict,
#                            bibl_sources_primaires=bibl_sources_primaires_dict,
#                            docs=docs)

@app.route("/index_sources_biblio")
def index_sources_biblio():
    bibl_scientific = db.session.query(BiblScientific.title, BiblScientific.notice_id, BiblScientific.notice).all()
    bibl_sources = db.session.query(BiblSources.bibl, BiblSources.notice_id, BiblSources.notice).all()
    bibl_sources_primaires = db.session.query(BiblSourcesArchivalID.bibl, BiblSourcesArchivalID.notice_id, BiblSourcesArchivalID.notice).all()

    bibl_dict = {}
    for bibl in bibl_scientific:
        bibl_title = bibl.title
        notice = bibl.notice
        notice_id = bibl.notice_id
        if bibl_title in bibl_dict:
            bibl_dict[bibl_title].append({'notice': notice, 'notice_id': notice_id})
        else:
            bibl_dict[bibl_title] = [{'notice': notice, 'notice_id': notice_id}]

    bibl_scientific_with_notices = [{'bibl': bibl, 'notices': notices} for bibl, notices in bibl_dict.items()]

    sources_dict = {}
    for bibl in bibl_sources:
        bibl_name = bibl.bibl
        notice = bibl.notice
        notice_id = bibl.notice_id
        if bibl_name in sources_dict:
            sources_dict[bibl_name].append({'notice': notice, 'notice_id': notice_id})
        else:
            sources_dict[bibl_name] = [{'notice': notice, 'notice_id': notice_id}]
    bibl_sources_with_notices = [{'bibl': bibl, 'notices': notices} for bibl, notices in sources_dict.items()]

    sources_primaires = {}
    for bibl in bibl_sources_primaires:
        bibl_name = bibl.bibl
        notice = bibl.notice
        notice_id = bibl.notice_id
        if bibl_name in sources_primaires:
            sources_primaires[bibl_name].append({'notice': notice, 'notice_id': notice_id})
        else:
            sources_primaires[bibl_name] = [{'notice': notice, 'notice_id': notice_id}]

    bibl_sources_primaires_with_notices = [{'bibl': bibl, 'notices': notices} for bibl, notices in sources_primaires.items()]

    return render_template("pages/index_biblio_sources.html", bibl_scientific=bibl_scientific_with_notices,
                           bibl_sources=bibl_sources_with_notices,
                           bibl_sources_primaires=bibl_sources_primaires_with_notices,
                           docs=docs)



@app.route("/recherche")
def recherche():
    ''' Fonction permettant une recherche simple dans la base de données (requete sur les titres, identifiants et auteurs des notices).

        :returns: fonction de renvoi du template recherche, du paramètre resultats (résultats de la requete), titre (titre de la page),
        keywords (inputs dans le formulaire de recherche et docs (ensemble des notices).
    '''

    # Variables utilisées par la suite dans le return de la fonction
    motclef = request.args.get("keyword", None)
    page = request.args.get("page", 1)
    resultats = []
    titre = "Recherche"

    # Elements assurant la pagination des résultats de la recherche.
    if isinstance(page, str) and page.isdigit():
        page = int(page)
    else:
        page = 1

    # Si un mot clef a été renseigné dans le formulaire, alors nous effectuons une requete avec Sqalchemy sur le modèle
    # d'une requete like en SQL afin d'englober un maximum de résultats.
    if motclef:
        resultats = \
            Notices.query.filter(or_(
                Notices.titre2.like("%{}%".format(motclef)),
                Notices.id.like("%{}%".format(motclef)),
                Notices.auteur.like("%{}%".format(motclef)),
                Notices.auteur2.like("%{}%".format(motclef)),
                Notices.auteur3.like("%{}%".format(motclef))
            )
            ) \
                .paginate(page=page, per_page=RESULTATS_PAR_PAGES)
        # La méthode .paginate prend en argument un numéro de page et une constantes indiquant le nombre de résultats à afficher par page.
        titre = "Résultat pour la recherche '" + motclef + "'"
    return render_template("pages/recherche.html", resultats=resultats, titre=titre, keyword=motclef, docs=docs)


@app.route('/download/odd_notices')
def download_odd_notices():
    '''Fonction permettant le téléchargement de l'ODD des notices au format XML/TEI (grammaire ODD).

    :returns: fonction intégrée send_file permettant d'envoyer les fichiers à télécharger. L'argument
    attachment_filename est une chaîne de caractères qui sera le nom du fichier téléchargé et as_attachement
    permet de sauvegarder le fichier envoyé
    '''

    # la variable f est une chaine de caractères qui indique le chemin du fichier à télécharger dans l'application.
    f = 'ODD_schemas/ODD_v2.xml'
    return send_file(f, attachment_filename='ODD_notices.xml', as_attachment=True)


@app.route('/download/odd_corpus')
def download_odd_corpus():
    '''Fonction permettant le téléchargement de l'ODD du corpus au format XML/TEI (grammaire ODD).

    :returns: fonction intégrée send_file permettant d'envoyer les fichiers à télécharger. L'argument
    attachment_filename est une chaîne de caractères qui sera le nom du fichier téléchargé et as_attachement
    permet de sauvegarder le fichier envoyé
    '''

    # la variable f est une chaine de caractères qui indique le chemin du fichier à télécharger dans l'application.
    f = 'ODD_schemas/ODD_teiCorpus.xml'
    return send_file(f, attachment_filename='ODD_corpus.xml', as_attachment=True)


@app.route('/download/odd_corpus_html')
def download_odd_corpus_html():
    '''Fonction permettant le téléchargement de l'ODD du corpus au format HTML (visualisable dans le navigateur web).

    :returns: fonction intégrée send_file permettant d'envoyer les fichiers à télécharger. L'argument
    attachment_filename est une chaîne de caractères qui sera le nom du fichier téléchargé et as_attachement
    permet de sauvegarder le fichier envoyé
    '''

    # la variable f est une chaine de caractères qui indique le chemin du fichier à télécharger dans l'application.
    f = 'ODD_schemas/ODD_teiCorpus.html'
    return send_file(f, attachment_filename='ODD_corpus.html', as_attachment=True)


@app.route('/download/odd_notices_html')
def download_odd_notices_html():
    '''Fonction permettant le téléchargement de l'ODD des notices individuelles au format HTML (visualisable dans le navigateur web).

    :returns: fonction intégrée send_file permettant d'envoyer les fichiers à télécharger. L'argument
    attachment_filename est une chaîne de caractères qui sera le nom du fichier téléchargé et as_attachement
    permet de sauvegarder le fichier envoyé
    '''

    # la variable f est une chaine de caractères qui indique le chemin du fichier à télécharger dans l'application.
    f = 'ODD_schemas/ODD_v2.html'
    return send_file(f, attachment_filename='ODD_notices.html', as_attachment=True)


@app.route('/download_bdd')
def download_bdd():
    '''Fonction permettant le téléchargement de la base de données (à implémenter si souhaité).

    :returns: fonction intégrée send_file permettant d'envoyer les fichiers à télécharger. L'argument
    attachment_filename est une chaîne de caractères qui sera le nom du fichier téléchargé et as_attachement
    permet de sauvegarder le fichier envoyé
    '''
    # la variable f est une chaine de caractères qui indique le chemin du fichier à télécharger dans l'application.
    f = './bdd_v2.db'

    return send_file(f, attachment_filename='Base_de_donnees_Ferme_Ge.db', as_attachment=True)


@app.route('/download_sql')
def download_sql():
    '''Fonction permettant le téléchargement du script SQL (à implémenter si souhaité).

        :returns: fonction intégrée send_file permettant d'envoyer les fichiers à télécharger. L'argument
        attachment_filename est une chaîne de caractères qui sera le nom du fichier téléchargé et as_attachement
        permet de sauvegarder le fichier envoyé
        '''
    # la variable f est une chaine de caractères qui indique le chemin du fichier à télécharger dans l'application.
    f = './Base_de_donnees_Ferme_Ge.sql'
    return send_file(f, attachment_filename='Base_de_donnees_Ferme_Ge.sql', as_attachment=True)


@app.route("/about")
def about():
    '''Fonction de génération de la page à propos

    :returns: Fonction de renvoi du template about et du paramètre docs (ensemble des notices)
    '''
    return render_template("pages/about.html", docs=docs)


@app.route('/recherche_avancee', methods=["POST", "GET"])
def rechercheavancee():
    page = request.args.get("page", 1)
    auteurs_notices = Notices.query.with_entities(Notices.id).distinct()
    motclef = request.args.get("keyword", None)

    if isinstance(page, str) and page.isdigit():
        page = int(page)
    else:
        page = 1

    if request.method == "POST":

        motclef = "output_test"
        resultats_notices = []

        questionNotices = Notices.query

        idNotice = request.form.get("idNotice", None)
        titreNotice = request.form.get("titreNotice", None)
        auteurNotice = request.values.get("auteurNotice", None)
        lettreNotice = request.form.get("lettreNotice", None)
        typeNotice = request.form.get("typeNotice", None)

        corpsNotice = request.form.get("corpsNotice", None)

        if idNotice:
            questionNotices = questionNotices.filter(Notices.id.like("%{}%".format(idNotice)))

        if titreNotice:
            questionNotices = questionNotices.filter(Notices.titre2.like("%{}%".format(titreNotice)))

        if auteurNotice:
            questionNotices = questionNotices.filter((Notices.auteur.like("%{}%".format(auteurNotice))) | (
                Notices.auteur2.like("%{}%".format(auteurNotice))))

        if lettreNotice:
            questionNotices = questionNotices.filter(Notices.lettre.like("%{}%".format(lettreNotice)))

        if typeNotice:
            questionNotices = questionNotices.filter((Notices.typologie.like("%{}%".format(typeNotice))) | (
                Notices.typologie_2.like("%{}%".format(typeNotice))) | (
                                                         Notices.typologie_3.like("%{}%".format(typeNotice))) | (
                                                         Notices.typologie_4.like("%{}%".format(typeNotice))) | (
                                                         Notices.typologie_5.like("%{}%".format(typeNotice))))

        if corpsNotice:
            questionNotices = questionNotices.filter(Notices.corps.like("%{}%".format(corpsNotice)))

        # Pour une question de lisibilité des résultats, tout les résultats doivent apparaitrent sur une seule page, d'où le recours à cette constante.
        resultats_notices = questionNotices.paginate(page=page, per_page=RESULTATS_PAR_PAGES_RECHERCHE_AVANCEE)

        if resultats_notices is None:
            warn("Vous devez renseigner au moins un élément dans cette catégorie")

        return render_template(
            "pages/resultats_recherche_avancee.html",
            resultats_notices=resultats_notices, docs=docs)

    return render_template("pages/recherche_avancee.html", motclef=motclef, page=page, docs=docs)


@app.route('/recherche_avancee_bibl_scientific', methods=["POST", "GET"])
def rechercheavancee_bibl_scientific():
    page = request.args.get("page", 1)
    motclef = request.args.get("keyword", None)

    if isinstance(page, str) and page.isdigit():
        page = int(page)
    else:
        page = 1

    if request.method == "POST":

        motclef = "output_test"
        resultats_notices = []

        questionBiblScientific = BiblScientific.query
        # output_test d'ajout sources ici:

        refBibl = request.form.get("refBibl", None)

        if refBibl:
            questionBiblScientific = questionBiblScientific.filter(BiblScientific.title.like("%{}%".format(refBibl)))

        # Pour une question de lisibilité des résultats, tout les résultats doivent apparaitrent sur une seule page, d'où le recours à cette constante.
        resultats_biblScientific = questionBiblScientific.paginate(page=page,
                                                                   per_page=RESULTATS_PAR_PAGES_RECHERCHE_AVANCEE)

        if resultats_notices is None:
            warn("Vous devez renseigner au moins un élément dans cette catégorie")

        return render_template(
            "pages/resultats_recherche_avancee_bibl_scientific.html", docs=docs,
            resultats_biblScientific=resultats_biblScientific)

    return render_template("pages/recherche_avancee_bibl_scientific.html", motclef=motclef, page=page, docs=docs)


@app.route('/recherche_avancee_sources', methods=["POST", "GET"])
def rechercheavancee_sources():
    page = request.args.get("page", 1)
    motclef = request.args.get("keyword", None)

    if isinstance(page, str) and page.isdigit():
        page = int(page)
    else:
        page = 1

    if request.method == "POST":

        motclef = "output_test"
        resultats_notices = []

        questionBiblSources = BiblSources.query
        # output_test d'ajout sources ici:

        refSources = request.form.get("refSources", None)

        if refSources:
            questionBiblSources = questionBiblSources.filter(BiblSources.bibl.like("%{}%".format(refSources)))

        # Pour une question de lisibilité des résultats, tout les résultats doivent apparaitrent sur une seule page, d'où le recours à cette constante.
        resultats_biblSources = questionBiblSources.paginate(page=page, per_page=RESULTATS_PAR_PAGES_RECHERCHE_AVANCEE)

        if resultats_notices is None:
            warn("Vous devez renseigner au moins un élément dans cette catégorie")

        return render_template(
            "pages/resultats_recherche_avancee_sources.html", docs=docs,
            resultats_biblSources=resultats_biblSources)

    return render_template("pages/recherche_avancee_sources.html", motclef=motclef, page=page, docs=docs)